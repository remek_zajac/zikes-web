package com.zikes.api.storage.gcs;

import com.google.appengine.tools.cloudstorage.GcsFileMetadata;
import com.zikes.api.model.AccessControlledResource;

import java.io.IOException;
import java.io.InputStream;

public interface ResourceReaderIfc<T extends AccessControlledResource> {
    T read(GcsFileMetadata fileMetadata, InputStream is) throws IOException;
}
