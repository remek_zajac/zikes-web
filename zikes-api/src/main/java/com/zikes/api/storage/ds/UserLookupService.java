package com.zikes.api.storage.ds;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Work;
import com.zikes.api.model.ZikesUser;
import com.zikes.api.oauth2.OAuthAccessToken;
import org.joda.time.Instant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("userLookupService")
public class UserLookupService {
    @Autowired
    @Qualifier("objectifyService")
    private OfyService objectifyService;

    /**
     * Zikes user lookup done in transaction that includes only single entity group - entity with key 'ZikesUser'/providedId
     * We try at most 3 times before giving up (1 time should be enough since key lookup has strong consistency)
     * ZikesId generation also includes Dastastore RPC call to allocate unique long ID
     * @param accessToken
     * @return ZikesUser - existing or newly created
     */
    public ZikesUser getOrCreate(OAuthAccessToken accessToken) {
        final String providedId = accessToken.getProvider().name().toLowerCase() + "." + accessToken.getUserId();

        return objectifyService.ofy().transactNew(3, new Work<ZikesUser>() {
            @Override
            public ZikesUser run() {
                ZikesUser zikesUser = objectifyService.ofy().load()
                        .key(Key.create(ZikesUser.class, providedId))
                        .now();

                if (null == zikesUser) {
                    zikesUser = new ZikesUser();

                    //allocate unique long id
                    long zikesId = DatastoreServiceFactory.getDatastoreService().allocateIds(ZikesUser.class.getSimpleName(), 1L).getStart().getId();

                    zikesUser.setZikesId(zikesId);
                    zikesUser.setProvidedId(providedId);
                    zikesUser.setCreated(Instant.now().getMillis());
                    zikesUser.setLastModified(zikesUser.getCreated());
                    zikesUser.setOpaque(JsonNodeFactory.instance.objectNode());

                    objectifyService.ofy().save().entity(zikesUser).now();
                    zikesUser.onLoad();
                }

                return zikesUser;
            }
        });
    }
}
