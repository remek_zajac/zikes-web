package com.zikes.api.storage.gcs.impl;

import com.zikes.api.model.Metadata;
import com.zikes.api.storage.ZikesIdManager;
import com.zikes.api.storage.gcs.ResourceLocatorIfc;

public class JourneyLocatorImpl implements ResourceLocatorIfc {

    public static JourneyLocatorImpl plan() {
        return new JourneyLocatorImpl("plan");
    }


    public static JourneyLocatorImpl uploaded() {
        return new JourneyLocatorImpl("uploaded");
    }


    private static final String DIR = "users/%s/%s/journeys/";
    private final String subfolder;
    private JourneyLocatorImpl(String aSubfolder) {
        subfolder = aSubfolder;
    }

    @Override
    public String getPath(Long zikesId) {
        return String.format(DIR, ZikesIdManager.encodeId(zikesId), subfolder);
    }

    @Override
    public String getPath(Long zikesId, String journeyId) {
        return getPath(zikesId) + journeyId;
    }

    @Override
    public String getPath(Metadata metadata) {
        return getPath(metadata.getOwner(), metadata.getId());
    }
}
