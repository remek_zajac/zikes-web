package com.zikes.api.storage.gcs;

import com.zikes.api.model.Metadata;

public interface ResourceLocatorIfc {
    String getPath(Long zikesId);

    String getPath(Long zikesId, String journeyId);

    String getPath(Metadata metadata);
}
