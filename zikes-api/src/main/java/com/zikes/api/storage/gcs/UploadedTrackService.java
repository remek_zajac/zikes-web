package com.zikes.api.storage.gcs;

import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.zikes.api.config.GcsSettings;
import com.zikes.api.storage.ZikesIdManager;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.Channels;

public class UploadedTrackService {

    private final static String FileSuffixGpx  = ".gpx";
    private final static String FileSuffixJson = ".json";

    static public class TrackType {
        public final String nameTemplate;
        public final String mimeType;

        public TrackType(String aFileSuffix, String aMimeType) {
            nameTemplate = "%s"+aFileSuffix;
            mimeType     = aMimeType;
        }

        static public TrackType fromFileName(String name) {
            if (name.endsWith(FileSuffixGpx)) {
                return GpxFileType;
            } else if (name.endsWith(FileSuffixJson)) {
                return JsonFileType;
            }
            return UnknownFileType;
        }
    }
    static public final TrackType GpxFileType     = new TrackType(FileSuffixGpx,  "text/xml");
    static public final TrackType JsonFileType    = new TrackType(FileSuffixJson, "application/json");
    static public final TrackType UnknownFileType = new TrackType("",    "text/plain");

    static private final String GiftTracksFolder = "anonymous/tracks/";
    static private final int BUFFER_SIZE = 2048;

    @Autowired
    private GcsSettings settings;

    @Autowired
    private GcsService gcsService;

    public void put(InputStream uploadedTrack, TrackType trackType) throws IOException {
        String fileName = String.format(trackType.nameTemplate, ZikesIdManager.generateId());
        GcsFilename giftFromUserFilename = new GcsFilename(settings.getBucket(), GiftTracksFolder + fileName);
        GcsOutputChannel giftOutputChannel =
            gcsService.createOrReplace(giftFromUserFilename, new GcsFileOptions.Builder()
                .mimeType(trackType.mimeType)
                .acl("bucket-owner-full-control")
                .build()
            );

        try(OutputStream giftOutput = Channels.newOutputStream(giftOutputChannel)) {
            copy(
                uploadedTrack,
                giftOutput
            );
        } finally {
            giftOutputChannel.close();
        }
    }

    private void copy(InputStream input, OutputStream o1) throws IOException {
        byte[] buffer = new byte[BUFFER_SIZE];
        int bytesRead = input.read(buffer);
        while (bytesRead != -1) {
            o1.write(buffer, 0, bytesRead);
            bytesRead = input.read(buffer);
        }
    }
}
