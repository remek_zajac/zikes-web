package com.zikes.api.exceptions.handlers;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.zikes.api.exceptions.GcsAclAccessException;
import com.zikes.api.exceptions.ServiceException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.acls.model.NotFoundException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

@ControllerAdvice
public class ExceptionControllerAdvice {
    private static final Logger logger = Logger.getLogger(ExceptionControllerAdvice.class.getSimpleName());

    private void logException(Exception ex) {
        logger.log(Level.WARNING, "Caught exception", ex);
    }

    @ExceptionHandler(ServiceException.class)
    public ResponseEntity handleServiceException(ServiceException ex) {
        logException(ex);

        JsonNodeFactory nodeFactory = JsonNodeFactory.instance;
        ObjectNode exception = nodeFactory.objectNode();
        exception.put("title", ex.getStatus().getReasonPhrase());
        exception.put("status", ex.getStatus().value());
        exception.put("message", ex.getMessage());

        return ResponseEntity.status(ex.getStatus()).contentType(MediaType.APPLICATION_JSON).body(exception.toString());
    }

    @ExceptionHandler({AccessDeniedException.class, GcsAclAccessException.class})
    public ResponseEntity handleAccessDeniedException(final Exception ex, final WebRequest request) {
        logException(ex);

        JsonNodeFactory nodeFactory = JsonNodeFactory.instance;
        ObjectNode exception = nodeFactory.objectNode();
        exception.put("title", HttpStatus.FORBIDDEN.getReasonPhrase());
        exception.put("status", HttpStatus.FORBIDDEN.value());
        exception.put("message", ex.getMessage());

        return ResponseEntity.status(HttpStatus.FORBIDDEN).contentType(MediaType.APPLICATION_JSON).body(exception.toString());
    }

    @ExceptionHandler({NoHandlerFoundException.class, NotFoundException.class, FileNotFoundException.class})
    public ResponseEntity handleResourceNotFoundException(final Exception ex, final WebRequest request) {
        logException(ex);

        JsonNodeFactory nodeFactory = JsonNodeFactory.instance;
        ObjectNode exception = nodeFactory.objectNode();
        exception.put("title", HttpStatus.NOT_FOUND.getReasonPhrase());
        exception.put("status", HttpStatus.NOT_FOUND.value());
        exception.put("message", ex.getMessage());

        return ResponseEntity.status(HttpStatus.NOT_FOUND).contentType(MediaType.APPLICATION_JSON).body(exception.toString());
    }

    @ExceptionHandler({MethodArgumentNotValidException.class, TypeMismatchException.class, IllegalArgumentException.class, IllegalStateException.class})
    public ResponseEntity handleBadArgumentException(final Exception ex, final WebRequest request) {
        logException(ex);

        JsonNodeFactory nodeFactory = JsonNodeFactory.instance;
        ObjectNode exception = nodeFactory.objectNode();
        exception.put("title", HttpStatus.BAD_REQUEST.getReasonPhrase());
        exception.put("status", HttpStatus.BAD_REQUEST.value());

        if (ex instanceof MethodArgumentNotValidException) {
            StringBuilder message = new StringBuilder();
            for (FieldError error : ((MethodArgumentNotValidException) ex).getBindingResult().getFieldErrors()) {
                message.append("[").append(error.getField()).append("] - ").append(error.getDefaultMessage()).append(".");
            }
            exception.put("message", message.toString());
        } else {
            exception.put("message", ex.getMessage());
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).contentType(MediaType.APPLICATION_JSON).body(exception.toString());
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity handleUncaughtException(final Exception ex, final WebRequest request) {
        logException(ex);

        JsonNodeFactory nodeFactory = JsonNodeFactory.instance;
        ObjectNode exception = nodeFactory.objectNode();
        exception.put("title", HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
        exception.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value());
        exception.put("message", ex.getMessage());

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).contentType(MediaType.APPLICATION_JSON).body(exception.toString());
    }
}