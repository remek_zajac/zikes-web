package com.zikes.api.client;

import com.google.appengine.api.urlfetch.HTTPRequest;

public interface ZikesClient {
    public String fetch(HTTPRequest request) throws ZikesClientException;
}
