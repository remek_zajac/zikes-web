package com.zikes.api.oauth2;

public class OAuthHeaders {
    public static final String OAUTH_PROVIDER = "Authorization-Provider";
    public static final String OAUTH_TOKEN = "Authorization";
    public static final String OAUTH_MOCK_USERID = "Authorization-Mock-UserID";
}
