package com.zikes.api.config;

import com.google.common.base.Splitter;
import com.google.common.collect.Sets;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.Set;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "cors")
public class CorsSettings {
    private String allowedOriginsCsv;

    private boolean allowSubdomains;
    private Set<String> allowedOrigins;

    @PostConstruct
    public void initIt() throws Exception {
        allowedOrigins = Sets.newHashSet(Splitter.on(",").split(allowedOriginsCsv));
    }

    public boolean isAllowSubdomains() {
        return allowSubdomains;
    }

    public void setAllowSubdomains(boolean allowSubdomains) {
        this.allowSubdomains = allowSubdomains;
    }

    public String getAllowedOriginsCsv() {
        return allowedOriginsCsv;
    }

    public void setAllowedOriginsCsv(String allowedOriginsCsv) {
        this.allowedOriginsCsv = allowedOriginsCsv;
    }

    public Set<String> getAllowedOrigins() {
        return allowedOrigins;
    }

    public void setAllowedOrigins(Set<String> allowedOrigins) {
        this.allowedOrigins = allowedOrigins;
    }
}
