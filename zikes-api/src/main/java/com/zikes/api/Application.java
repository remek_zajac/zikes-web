package com.zikes.api;

import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.zikes.api.client.ZikesClient;
import com.zikes.api.client.ZikesLiveClient;
import com.zikes.api.client.ZikesMockClient;
import com.zikes.api.config.CorsSettings;
import com.zikes.api.config.ZikesSettings;
import com.zikes.api.config.profiles.LiveProfile;
import com.zikes.api.config.profiles.MockProfile;
import com.zikes.api.model.Journey;
import com.zikes.api.model.Preferences;
import com.zikes.api.storage.gcs.AccessControlledResourceService;
import com.zikes.api.storage.gcs.UploadedTrackService;
import com.zikes.api.storage.gcs.impl.JourneyLocatorImpl;
import com.zikes.api.storage.gcs.impl.JourneyReaderImpl;
import com.zikes.api.storage.gcs.impl.PreferencesLocatorImpl;
import com.zikes.api.storage.gcs.impl.PreferencesReaderImpl;
import org.jsondoc.spring.boot.starter.EnableJSONDoc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.web.MultipartAutoConfiguration;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import org.springframework.context.annotation.*;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import javax.servlet.Filter;
import java.util.Properties;

@Configuration
@Import(ZikesSettings.class)
@ComponentScan
@EnableAutoConfiguration(exclude = {WebMvcAutoConfiguration.class, MultipartAutoConfiguration.class})
@EnableJSONDoc
@EnableWebMvc
@RestController
public class Application {

    @Autowired
    private ZikesSettings settings;

    @Autowired
    private CorsSettings corsSettings;

    @Value("${info.version}")
    private String version;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @RequestMapping("/version")
    public String getVersion() {
        return version;
    }

    @Bean
    public WebMvcConfigurer webConfigurer() {
        return new WebMvcConfigurerAdapter() {

            @Override
            public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
                configurer.defaultContentType(MediaType.APPLICATION_JSON);
            }
        };
    }

    @Bean(name = "corsFilter")
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin(corsSettings.getAllowedOriginsCsv());
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }

    @Bean(name = "gcsService")
    public GcsService gcsService() {
        return GcsServiceFactory.createGcsService();
    }

    @Bean(name = "plannedJourneyService")
    public AccessControlledResourceService<Journey> plannedJourneyService() {
        return new AccessControlledResourceService<>(JourneyLocatorImpl.plan(), new JourneyReaderImpl());
    }

    @Bean(name = "uploadedJourneyService")
    public AccessControlledResourceService<Journey> uploadedJourneyService() {
        return new AccessControlledResourceService<>(JourneyLocatorImpl.uploaded(), new JourneyReaderImpl());
    }

    @Bean(name = "preferencesService")
    public AccessControlledResourceService<Preferences> preferencesService() {
        return new AccessControlledResourceService<>(new PreferencesLocatorImpl(), new PreferencesReaderImpl());
    }

    @Bean(name = "uploadedTrackService")
    public UploadedTrackService uploadedTrackService() {
        return new UploadedTrackService();
    }

    @Bean
    @Conditional(MockProfile.class)
    public ZikesClient devZikesClientService() throws Exception {
        return new ZikesMockClient(settings);
    }

    @Bean
    @Conditional(LiveProfile.class)
    public ZikesClient liveZikesClientService() throws Exception {
        return new ZikesLiveClient(settings);
    }
}
