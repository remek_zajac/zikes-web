package com.zikes.api.endpoints;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.zikes.api.model.Journey;
import com.zikes.api.model.Metadata;
import com.zikes.api.oauth2.OAuthAuthentication;
import com.zikes.api.storage.ZikesIdManager;
import com.zikes.api.storage.gcs.AccessControlledResourceService;
import com.zikes.api.storage.gcs.UploadedTrackService;
import org.apache.commons.lang3.StringEscapeUtils;
import org.joda.time.Instant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@RestController
@RequestMapping("/trackUpload")
public class TrackUploadEndpoint {

    private static final String MediaTypeZIP        = "application/zip";
    private static final String MediaTypeXML        = "text/xml";
    private static final String MediaTypePlainText  = "text/plain";
    private static final String MediaTypeJSON       = "application/json";
    private static final String MediaTypeJSON_TF8   = "application/json;charset=UTF-8";

    @Autowired
    private UploadedTrackService uploadedAnonymousJourneyService;

    @Autowired
    @Qualifier("uploadedJourneyService")
    private AccessControlledResourceService<Journey> uploadedJourneyService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity post(
            @RequestHeader(value="Content-Type") String contentType,
            InputStream in) throws IOException {
        Long zikesUserId = null;
        if (SecurityContextHolder.getContext().getAuthentication() instanceof OAuthAuthentication) {
            OAuthAuthentication oauth = (OAuthAuthentication)SecurityContextHolder.getContext().getAuthentication();
            zikesUserId = oauth.getZikesUser().getZikesId();
        }
        switch (contentType) {
            case MediaTypeZIP:
                try(ZipInputStream zin = new ZipInputStream(in)) {
                    ZipEntry zentry;
                    while ((zentry = zin.getNextEntry()) != null) {
                        post(
                            zin,
                            zikesUserId,
                            UploadedTrackService.TrackType.fromFileName(zentry.getName())
                        );
                    }
                }
                break;
            case MediaTypeXML:
                post(in, zikesUserId, UploadedTrackService.GpxFileType);
            case MediaTypePlainText:
                post(in, zikesUserId, UploadedTrackService.UnknownFileType);
            case MediaTypeJSON:
            case MediaTypeJSON_TF8:
                post(in, zikesUserId, UploadedTrackService.JsonFileType);
                break;
            default:
                return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().build();
    }

    private void post(InputStream in, Long zikesUser, UploadedTrackService.TrackType trackType) throws IOException {
        if (zikesUser != null && trackType == UploadedTrackService.JsonFileType) {
            ObjectMapper objectMapper = new ObjectMapper();
            Journey journey = objectMapper.readValue(
                in, Journey.class
            );
            Metadata metadata = journey.getMetadata();

            metadata.setId(ZikesIdManager.generateId());
            metadata.setOwner(zikesUser);
            metadata.setCreated(Instant.now().getMillis());
            metadata.setLastModified(metadata.getCreated());
            uploadedJourneyService.put(journey);
            in = new ByteArrayInputStream(journey.getContent().toString().getBytes("UTF-8"));
        }
        uploadedAnonymousJourneyService.put(in, trackType);
    }
}
