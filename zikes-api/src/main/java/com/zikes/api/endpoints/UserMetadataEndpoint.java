package com.zikes.api.endpoints;

import com.zikes.api.exceptions.ServiceException;
import com.zikes.api.model.ZikesUser;
import com.zikes.api.oauth2.OAuthAuthentication;
import com.zikes.api.storage.ds.OfyService;
import org.joda.time.Instant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.io.IOException;

@RestController
@RequestMapping("/users/metadata")
@Secured({"ROLE_USER"})
public class UserMetadataEndpoint {
    @Autowired
    @Qualifier("objectifyService")
    private OfyService objectifyService;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ZikesUser get() throws ServiceException, IOException {
        OAuthAuthentication oauth = (OAuthAuthentication) SecurityContextHolder.getContext().getAuthentication();

        return oauth.getZikesUser();
    }

    @RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ZikesUser update(@Valid @RequestBody ZikesUser user) throws ServiceException, IOException {
        OAuthAuthentication oauth = (OAuthAuthentication) SecurityContextHolder.getContext().getAuthentication();

        //only allow opaque data modification
        oauth.getZikesUser().setOpaque(user.getOpaque());
        oauth.getZikesUser().setLastModified(Instant.now().getMillis());

        objectifyService.ofy().save().entity(oauth.getZikesUser()).now();

        return oauth.getZikesUser();
    }
}
