package com.zikes.api.endpoints.proxy;

import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.zikes.api.config.ZikesSettings;
import com.zikes.api.exceptions.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.Callable;

@RestController
@RequestMapping("/meta")
public class MetaEndpointBase extends ZikesServerProxyEndpointBase {
    @Autowired
    private ZikesSettings settings;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity get() throws ServiceException {
        String fromRoutingServer = proxyZikes2String(new Callable<HTTPRequest>() {
            @Override
            public HTTPRequest call() throws Exception {
                return new HTTPRequest(new URL(settings.getUrl() + "/meta.json"), HTTPMethod.GET);
            }
        });
        try {
            String defaultRoutingPrefs = Resources.toString(
                Resources.getResource("defaultRoutingPrefs.json"),
                Charsets.UTF_8
            );
            return new ResponseEntity(
                    "{ \"map\" : " + fromRoutingServer + ", \"defaultRoutingPrefs\": " + defaultRoutingPrefs + "}",
                    HttpStatus.OK
            );
        } catch (IOException e) {
            throw new ServiceException("Server is misconfigured");
        }
    }
}
