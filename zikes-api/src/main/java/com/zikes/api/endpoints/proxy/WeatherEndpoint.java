package com.zikes.api.endpoints.proxy;

import com.google.appengine.api.urlfetch.*;
import com.zikes.api.exceptions.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

@RestController
@RequestMapping("/weather")
public class WeatherEndpoint {

    @Configuration
    @EnableConfigurationProperties
    @ConfigurationProperties(prefix = "openweather")
    public static class WeatherSettings {

        private String appid;
        private String url;

        public  String getAppid() { return appid; }
        public  void setAppid(String _appid ) { appid = _appid; }
        public  String getUrl() { return url; }
        public  void setUrl(String _url ) { url = _url; }
    }


    @Autowired
    private WeatherSettings settings;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({"ROLE_USER"})
    public byte[] get(@RequestParam("lat") String lat, @RequestParam("lng") String lng) throws ServiceException, IOException {
        URLFetchService service = URLFetchServiceFactory.getURLFetchService();
        try {
            HTTPRequest request = new HTTPRequest(
                new URL(settings.getUrl() + "?" + "lat="+lat+"&lon="+lng+"&appid="+settings.getAppid()),
                HTTPMethod.GET
            );
            return service.fetch(request).getContent();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
