'use strict';

var fs = require("fs");
var config = require('../config');
var path = require('path');
var gulp = require('gulp');
var cache = require('gulp-cache');
var changed = require('gulp-changed');
var imagemin = require('gulp-imagemin');
var rename = require('gulp-rename');
var rev = require('gulp-rev');
var size = require('gulp-size');
var transform = require('gulp-transform');

// Build
gulp.task('build', ['html', 'browserify', 'mobile', 'appendages', 'mobileServiceWorker']);

gulp.task('appendages', function() {
	return gulp.src([
			'app/css/**/*',
			'app/templates/**/*',
			'app/graphics/**/*'],
			{base: 'app'}
		)
		.pipe(gulp.dest(config.dist));
});


gulp.task('mobileServiceWorker', function() {
	return gulp.src('app/mobile/serviceWorker.js', {base: 'app'})
		.pipe(transform(function(contents) {
			return contents + fs.readFileSync('./app/scripts/lib/indexedDB.js', 'utf8');
		 }, {encoding: 'utf8'}))
		.pipe(transform(function(contents) {
			return contents + "\n\n//reinstall timestamp: "+new Date().getTime();
		 }, {encoding: 'utf8'}))
		.pipe(gulp.dest(config.dist));
});
