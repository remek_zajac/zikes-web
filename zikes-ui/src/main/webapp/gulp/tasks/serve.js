'use strict';

var config = require('../config');
var gulp   = require('gulp');
var fs     = require('fs');

// Connect
gulp.task('connect', function () {
  var connect = require('connect');
  var app = connect()
    .use(require('connect-livereload')({ port: config.livereloadPort }))
    .use('/', connect.static('.tmp'))
    .use(connect.directory('app'));

  require('http').createServer(app)
    .listen(config.port)
    .on('listening', function () {
      console.log('Started connect web server on http://localhost:' + config.port);
    });

  //openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365
  require("http-proxy").createServer({
    ssl: {
      key: fs.readFileSync(process.env.HOME + "/.ssh/ssl-self/key.pem", "utf8"),
      cert: fs.readFileSync(process.env.HOME + "/.ssh/ssl-self/cert.pem", "utf8"),
      passphrase: "zikes"
    },
    target: "http://localhost:"+config.port,
    secure: true
  }).listen(8443);
});

gulp.task('serve', ['connect', 'browserify'], function () {
  require('opn')('http://localhost:' + config.port);
});
