'use strict';

var config = require('../config');
var gulp   = require('gulp');
var rev    = require('gulp-rev');
var minify = require('gulp-minify');

// Build
gulp.task('doDist', ['build'], function() {
  return gulp.src(config.dist+'/**/*')
    .pipe(gulp.dest('dist'))
    .pipe(rev())
    .pipe(gulp.dest('dist'))
    .pipe(rev.manifest())
    .pipe(gulp.dest('dist'));
});

gulp.task('compress', ['doDist'], function() {
  return gulp.src(config.dist+'/**/mobile/main.js')
    .pipe(minify({
        ext:{
            src:'-debug.js',
            min:'.js'
        }
    }))
    .pipe(gulp.dest('dist'))
});

gulp.task('dist', ['clean'], function () {
    return gulp.start('doDist');
});
