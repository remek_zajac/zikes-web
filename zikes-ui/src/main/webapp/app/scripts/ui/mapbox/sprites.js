/**
 * scripts/ui/mapbox/sprites.js
 *
 * Map sprites specific to mapboxgl stack
 */

'use strict';
const turfHelpers = require("turf-helpers");
const turf       = {
    point       : turfHelpers.point,
    lineString  : turfHelpers.lineString
}

const geometry = require("../../lib/geometry.js");
const utils    = require("../../lib/utils.js");

var sprites = (function() {

    /**
    * Mapbox works on lnglat (x,y) whereas we've started with latlng.
    * which one is better is hard to tell, but we'll sticking with
    * what we have and thus all client calls are expected with latlng
    */
    function latlng2lnglat(coords, everyPtCb) {
        if (!Array.isArray(coords)) {
            coords = [coords.lng, coords.lat];
            !everyPtCb || everyPtCb(coords);
            return coords;
        }
        if (!Array.isArray(coords[0]) && typeof coords[0] == "number") {
            var res = [coords[1], coords[0]].concat(coords.slice(2));
            !everyPtCb || everyPtCb(res);
            return res;
        }
        return coords.map(function(elem) {
            return latlng2lnglat(elem, everyPtCb);
        });
    }


    var theMap       = undefined;
    var currentHover = undefined;
    var draghandler  = undefined;
    var sprites      = {};
    function newSprite(map, sprite) {
        sprites[sprite.mLayer.id] = sprite;
        if (!theMap) {
            function cloneEvent(e) {
                return {
                    type : e.type,
                    lngLat: e.lngLat,
                    originalEvent: e.originalEvent,
                    point: e.point,
                    containerPoint: e.containerPoint,
                    target: e.target
                };
            }

            function dispatchEvent(type, e) {
                var features = theMap.queryRenderedFeatures(e.point, {layers: Object.keys(sprites)});
                if (features && features.length) {
                    var top = features.sort()[0];
                    var target = sprites[top.layer.id];
                    target.fire(type, e);
                    return target;
                }
            }

            function dispatchDragEvent(e) {
                draghandler ? draghandler(e) : dispatchEvent(e.type, e);
            }
            theMap = map;
            theMap.on("mousedown", dispatchDragEvent);
            theMap.on("mouseup",   dispatchDragEvent);
            theMap.on("mousemove", function(e) {
                e.containerPoint = e.containerPoint || new geometry.Point(e.point);
                if (draghandler) {
                    draghandler("mousemove", e);
                } else {
                    var target = dispatchEvent("mousemove", e);
                    if (target) {
                        if (currentHover != target) {
                            if (currentHover) {
                                currentHover.fire("mouseleave", cloneEvent(e));
                                currentHover.fire("mouseout", cloneEvent(e));
                            }
                            currentHover = target;
                            currentHover.fire("mouseenter", cloneEvent(e));
                            currentHover.fire("mouseover", cloneEvent(e));
                        }
                    } else if (currentHover) {
                        currentHover.fire("mouseleave", cloneEvent(e));
                        currentHover.fire("mouseout", cloneEvent(e));
                        currentHover = undefined;
                    }
                }
            });
            theMap.on("mouseleave", function(e) {
                if (currentHover) {
                    currentHover.fire("mouseleave", cloneEvent(e));
                    currentHover.fire("mouseout", cloneEvent(e));
                    currentHover = undefined;
                }
                if (draghandler) {
                    draghandler(cloneEvent(e))
                }
            });
        }

    }

    function removeSprite(map, sprite) {
        delete sprites[sprite.mLayer.id];
    }

    function extendBounds(point) {
        point = new mapboxgl.LngLat(point[0], point[1]);
        if (!this.mBounds) {
            this.mBounds = new mapboxgl.LngLatBounds();
        }
        this.mBounds.extend(point);
    }






    var KSpriteId = 0;
    class Sprite extends utils.Subscribable {
        constructor(map, options) {
            super();
            this.mMap = map;
            this.options = options;
        }

        setLayer(layer) {
            layer.id = ""+KSpriteId++ + "_" + this.constructor.name;
            this.mLayer = layer;
        }

        id() {
            return this.mLayer && this.mLayer.id;
        }

        show() {
            this.mMap.addLayer(this.mLayer);
            newSprite(this.mMap, this);
            return this;
        }

        hide() {
            this.mMap.removeLayer(this.id());
            removeSprite(this.mMap, this);
            return this;
        }

        delete() {
            return this.hide();
        }

        getBounds() {
            return this.mBounds;
        }

        bringToFront() {
            //hmmmm TODO
        }
    }

    class Polygon extends Sprite {
        constructor(map, coordinates, style) {
            super(map, style);
            this.setLayer({
                'type': 'fill',
                'source': {
                    'type': 'geojson',
                    'data': {
                        'type': 'Feature',
                        'geometry': {
                            'type': 'Polygon',
                            'coordinates': latlng2lnglat(coordinates, extendBounds.bind(this))
                        }
                    }
                },
                'layout': {},
                'paint': {
                    'fill-color'  : style.fill.color,
                    'fill-opacity': style.fill.opacity
                }
            });
        }
    }

    class Polyline extends Sprite {
        constructor(map, coordinates, style) {
            super(map, style);
            utils.assert(coordinates.length > 1, "Polyline must have more than one point");
            this.setLatLngs(coordinates);
            var paint = {
                "line-color"  : style.color,
                "line-width"  : style.width,
                "line-opacity": style.opacity || 1,
                "line-blur"   : style.blur || 0
            }
            if (style.dashArray) {
                paint["line-dasharray"] = style.dashArray;
            }
            this.setLayer({
                "type": "line",
                "source": {
                    "type": "geojson",
                    "data": this.mLineString
                },
                "layout": {
                    "line-join": style.lineJoin || "round",
                    "line-cap":  style.lineCap  || "round"
                },
                "paint": paint
            });
        }

        setLatLngs(coordinates) {
            delete this["mBounds"];
            this.mCoordinates = coordinates;
            this.mLineString = turf.lineString(
                latlng2lnglat(coordinates, extendBounds.bind(this))
            );
            var source = this.mMap.getSource(this.id());
            if (source) {
                source.setData(this.mLineString);
            } else if (this.mLayer) {
                this.mLayer.source.data = this.mLineString;
            }
        }

        getLatLngs() {
            return this.mCoordinates;
        }

        projectOnto(what) {
            return new geometry.GeoLineString(this.mCoordinates).projectOnto(what, true);
        }

        on(eventType, cb) {
            if (eventType == "mouseenter" || eventType == "mouseover") {
                var origCb = cb;
                var self   = this;
                cb = function(e) {
                    e.target  = self;
                    e.nearest = self.projectOnto(e.lngLat);
                    origCb(e);
                }
            }
            super.on(eventType, cb);
        }
    }


    class Marker {

        constructor(map, coordinates, style) {
            this.mMap    = map;
            this.options = style || {};
            if (this.options.icon) {
                var div = document.createElement('div');
                if (this.options.icon.class) {
                    div.className    = this.options.icon.class;
                }
                div.style.width  = this.options.icon.size[0] + 'px';
                div.style.height = this.options.icon.size[1] + 'px';
                if (this.options.icon.html) {
                    div.innerHTML = this.options.icon.html;
                } else if (this.options.icon.url) {
                    div.innerHTML = '<img src="'+this.options.icon.url+'"/>'
                }
                if (this.options && "opacity" in style) {
                    $(div).css({opacity: this.options.opacity});
                }
                var offset = [-this.options.icon.size[0]/2, -this.options.icon.size[1]/2];
                if (this.options.icon.anchor) {
                    offset = [-this.options.icon.anchor[0], -this.options.icon.anchor[1]];
                }
                this.mMarker = new mapboxgl.Marker(
                    div, {offset: offset}
                );
            } else {
                this.mMarker = new mapboxgl.Marker();
            }
            this.mMarker.setLngLat(
                latlng2lnglat(coordinates, extendBounds.bind(this))
            );

            if (this.options.draggable) {
                var self = this;
                var canvas = this.mMap.getCanvasContainer();
                div.addEventListener("mouseenter", function() {
                    canvas.style.cursor = "move";
                });
                div.addEventListener("mouseleave", function() {
                    canvas.style.cursor = "";
                });
                div.addEventListener("mousedown", function() {
                    if (!draghandler) {
                        self.mMap.dragPan.disable();
                        canvas.style.cursor = "grabbing";
                        self.mBeingDragged  = true;
                        draghandler = function(e) {
                            if (e.type == "mousemove") {
                                self.mMarker.setLngLat(e.lngLat);
                            } else if (e.type == "mouseup") {
                                self.mMap.dragPan.enable();
                                canvas.style.cursor = "";
                                delete self["mBeingDragged"];
                                draghandler = undefined;
                            }
                        }
                    }
                });
            }
        }

        show() {
            var self = this;
            if (this.mAdded) {
                $(this.mMarker.getElement()).animate({opacity:1});
            } else {
                this.mMarker.addTo(this.mMap);
                this.mAdded = true;
                this.on("click", function() {
                    !self.mPopup || self.mPopup.show();
                });
            }
            return this;
        }

        getLatLng() {
            return new geometry.LatLng(
                this.mMarker.getLngLat().lat, this.mMarker.getLngLat().lng
            );
        }

        hide() {
            $(this.mMarker.getElement()).animate({opacity:0});
            !this.mPopup || this.mPopup.hide();
        }

        delete() {
            delete this["mAdded"];
            this.mMarker.remove();
            !this.mPopup || this.mPopup.hide();
        }

        setLatLng(latlng) {
            this.mMarker.setLngLat(latlng);
        }

        setIcon(icon) {
            if (icon.class && icon.class != this.options.icon.class) {
                this.mMarker.getElement().classList.add(icon.class);
                this.mMarker.getElement().classList.remove(this.options.icon.class);
            }
            if (icon.size) {
                this.mMarker.getElement().style.width  = icon.size[0] + 'px';
                this.mMarker.getElement().style.height = icon.size[1] + 'px';
            }
            if (icon.html) {
                this.mMarker.getElement().innerHTML = icon.html;
            }
            this.options.icon = icon;
        }

        on() {
            var $icon = $(this.getIcon());
            $icon.on.apply($icon, arguments);
        }

        off() {
            var $icon = $(this.getIcon());
            $icon.off.apply($icon, arguments);
            return this;
        }

        getIcon() {
            return this.mAdded && this.mMarker.getElement();
        }

        bindPopup(popup) {
            this.mPopup = popup.setOptions({
                offset: this.options.icon.popupAnchor
            });
            return this;
        }

        openPopup() {
            this.mPopup && this.mPopup.setLatLng(this.getLatLng()).show();
            return this;
        }

        closePopup() {
            this.mPopup && this.mPopup.hide();
            return this;
        }

        getPopup(popup) {
            return this.mPopup;
        }
    }



    class Popup {
        constructor(map, options) {
            this.mMap = map;
            this.mPopup = new mapboxgl.Popup(options);
        }

        setContent() {
            this.mPopup.setHTML.apply(this.mPopup, arguments);
            return this;
        }

        setLatLng() {
            this.mPopup.setLngLat.apply(this.mPopup, arguments);
            return this;
        }

        show() {
            this.mPopup.addTo(this.mMap);
            if ("opacity" in this.mPopup.options) {
                this.getDiv().parent().css("opacity", this.mPopup.options.opacity);
            }
            if ("backgroundColor" in this.mPopup.options) {
                this.getDiv().css("background-color", this.mPopup.options.backgroundColor);
                //this.getDiv().parent().find(".mapboxgl-popup-tip").css("border-top-color", this.mPopup.options.backgroundColor);
                //TODO? won't paint the tip as it changes on map pans.
            }
            return this;
        }

        hide() {
            this.mPopup.remove();
        }

        getDiv() {
            return $(this.mPopup._content);
        }

        getCloseButton() {
            return $(this.mPopup._closeButton);
        }

        setOptions(options) {
            this.mPopup.options = utils.mergeSimpleObjects(this.mPopup.options, options);
            return this;
        }
    }

    return {
        Polygon  : Polygon,
        Polyline : Polyline,
        Marker   : Marker,
        Popup    : Popup,
        bindMap  : function(map) {
            return {
                Polygon  : Polygon.bind(null, map),
                Marker   : Marker.bind(null, map),
                Polyline : Polyline.bind(null, map),
                Popup    : Popup.bind(null, map)
            }
        }
    }

})();

module.exports = sprites;