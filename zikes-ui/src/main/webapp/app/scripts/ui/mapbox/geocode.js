/**
 * scripts/ui/geosearch.js
 */

'use strict';

const geometry    = require("../../lib/geometry.js");
const http        = require("../../lib/http.js");
const geocodeBase = require("../../lib/geocode.js")


module.exports = function(accessToken, map) {
    return {
        ui : function(searchBoxDiv, createContextMenu) {
            const geocoder = new MapboxGeocoder({
                accessToken: accessToken,
                placeholder: "find a place",
                flyTo : false
            });
            document.getElementById("geo-search-box").appendChild(geocoder.onAdd(map));
            searchBoxDiv = searchBoxDiv.find("input")
            searchBoxDiv.blur(function() {
                searchBoxDiv.val('');
            });
            searchBoxDiv.focus(function() {
                searchBoxDiv.val('');
            });
            geocoder.on("result", (result) => {
                setTimeout(function() {
                    result = result.result
                    if (result.geometry) {
                        let bboxExtent = result.bbox && Math.max(
                            Math.abs(result.bbox[0]-result.bbox[2]),
                            Math.abs(result.bbox[1]-result.bbox[3])
                        ) || 0;
                        let position = new geometry.LatLng(
                            result.geometry.coordinates[1],
                            result.geometry.coordinates[0])

                        if (bboxExtent > 0.2 ) {
                            let bounds = new geometry.LatLngBounds();
                            bounds.extend(new geometry.LatLng(
                                result.bbox[1], result.bbox[0])
                            );
                            bounds.extend(new geometry.LatLng(
                                result.bbox[3], result.bbox[2])
                            );
                            map.fitBounds(bounds, {animate:true});
                        } else {
                            map.setView({
                                position : position,
                                zoom     : 17,
                                animate: true
                            });
                        }
                        if (bboxExtent < 0.5) {
                            var title = result.place_name;
                            title = title.split(", ");
                            title[0] = "<b>"+title[0]+"</b>";
                            title = title.join(", ");
                            let ctxMenu = createContextMenu(position);
                            ctxMenu && ctxMenu.title(title).show(position);
                        }
                        searchBoxDiv.val('');
                    }
                }, 500);
            })
        },
        reverse : function(latlng) {
            if (Array.isArray(latlng)) {
                latlng = new geometry.LatLng(latlng[0], latlng[1]);
            }
            // returns an array containing (optional):
            const localityTypes = ["country", "region", "district", "place", "locality", "neighborhood", "address", "poi"];
            let query = "https://api.mapbox.com/geocoding/v5/mapbox.places/"+latlng.lng+","+latlng.lat+".json?access_token="+accessToken
            return http.get(query)
            .then(function(response) {
                let fill = function(features) {
                    return localityTypes.map((localityType) => {
                        let feature = features.find((feature) => {
                            return feature.id.startsWith(localityType);
                        });
                        if (feature) {
                            if (localityType == "address" && feature.place_name.length > 0) {
                                feature.text = feature.place_name.split(",")[0]
                            }
                            return feature.text;
                        }
                    })
                }
                let result = fill(response.features);
                return result;
            });
        },
        humanReadable: geocodeBase.humanReadable
    }
}
