/**
 * scripts/ui/warnedSegments.js
 *
 * This draws track segment warnings, like:
 * - that there are stairs or ferries or restricted sections (biycle forbidden)
 */

'use strict';
const geometry   = require("../../lib/geometry.js");
const track      = require("../../lib/track.js");
const messaging  = require("./messaging.js");

const warned = (function() {



    const KIgnorePedestrianSectionsBelowMts = 25;
    const KIgnoreSectionsBelowMts           = 15;
    const KProbablyPedestranisedHwyTypes = new Set([
        "path",
        "pedestrian",
        "footway"
    ]);
    const KWarnAboutHighways = new Set([
        "ferry", "steps"
    ]);
    const KProbablyOnewayHwyTypes        = new Set([
        "service",
        "unclassified",
        "road",
        "primary",
        "primary_link",
        "secondary",
        "secondary_link",
        "tertiary",
        "tertiary_link",
    ]);
    const KIconSize = [23, 23];
    function warningIcon(section) {
        let html = '<span class="glyphicon glyphicon-minus-sign"></span>';
        let cls  = "forbiddenIcon";
        let special = section.mHwy.special();
        let trackColour = "#FF0000";

        if (!section.mForbidden && special && special.icon) {
            html = '<img src="'+special.icon+'" style="width:'+KIconSize[0]+'px;height:'+KIconSize[1]+'px"/>'
            cls  = "warningIcon"
            trackColour = "#ff4000";
        }
        return {
            class: cls,
            html: html,
            size: KIconSize,
            popupAnchor: [0, -10],
            trackColour: trackColour
        };
    }


    var restrictedSectionsMessage = undefined;
    class DrawnSections {
        constructor(sections, parent, map) {
            this.mSections = sections.filter(function(section) {
                var distanceMts = section.distanceMts();
                return section.mHwy.name() == "ferry" ||
                       !(distanceMts < KIgnoreSectionsBelowMts ||
                         (KProbablyPedestranisedHwyTypes.has(section.mHwy.name()) && distanceMts < KIgnorePedestrianSectionsBelowMts)
                        );
            })
            .map(function(section) {
                return new DrawnSection(section, parent, map);
            });
        }

        draw() {
            this.mSections.forEach(function(section) {
                section.draw();
                if (!restrictedSectionsMessage) {
                    restrictedSectionsMessage = messaging.popup(
                         '<span class="lead">Marked with <span class="small glyphicon glyphicon-minus-sign"></span> are route fragments with <b>restricted traffic</b>.<br></span>'
                        +'<span class="small">We advise that you inspect (click on) them carefully.</span>',
                        {once:"restrictedTraffic"}
                    ).enqueue();
                }
            });
            return this;
        }

        clear() {
            this.mSections.forEach(function(section) {
                section.clear();
            });
        }

    }



    class DrawnSection {
        constructor(section, parent, map) {
            this.mParent    = parent;
            this.mMap       = map;
            this.mSection   = section;
            this.mZoomendCb = this.invalidate.bind(this);
        }

        draw() {
            var self = this;
            this.clear();
            let icon = warningIcon(this.mSection);
            this.mPoly && this.clear();
            this.mPoly = new this.mMap.sprites.Polyline(this.mSection.mLatLngs, {
                width: 10,
                color : icon.trackColour
            }).show();
            this.mWarningMarker = new this.mMap.sprites.Marker(this.mSection.midPoint(), {
                icon      : icon,
                draggable : false
            }).show();
            this.mWarningMarker.bindPopup(
                new this.mMap.sprites.Popup().setContent(
                    this.popupContent(
                        this.mWarningMarker.getLatLng()
                    )
                )
            );
            this.mWarningMarker.on("popupopen", function() {
                var popupDiv = $(self.mWarningMarker.getPopup().getDiv());
                popupDiv.find("#showOnStreetView").click(function() {
                    self.mMap.streetView.show(
                        self.mWarningMarker.getLatLng(),
                        self.mSection.mLatLngs[self.mSection.mLatLngs.length-1]
                    );
                    self.mWarningMarker.closePopup();
                });
            });
            this.mMap.on("zoomend", this.mZoomendCb);
        }

        invalidate() {
            function areClose(div1, div2) {
                if (div1 && div2) {
                    div1 = div1.getBoundingClientRect();
                    div1 = new geometry.Point(div1.left, div1.top);
                    div2 = div2.getBoundingClientRect();
                    div2 = new geometry.Point(div2.left, div2.top);
                    return div1.distanceTo(div2) < 20;
                }
                return true;
            }
            var head = this.mParent.mParent.head();
            var tail = this.mParent.tail();
            if (areClose(this.mWarningMarker.getIcon(), head.startMarker.getIcon()) ||
                areClose(this.mWarningMarker.getIcon(), tail.startMarker.getIcon())) {
                this.hide();
            } else {
                this.show();
            }
        };

        hide() {
            this.mWarningMarker.hide();
        };

        show() {
            this.mWarningMarker.show();
        };

        clear() {
            this.mPoly && this.mPoly.delete();
            this.mPoly = undefined;
            this.mWarningMarker && this.mWarningMarker.delete();
            this.mWarningMarker = undefined;
            this.mMap.off("zoomend", this.mZoomendCb);
        }


        popupContent(latlng) {
            const hwyName = this.mSection.mHwy.name();
            const dbLink  = '<a href="http://www.openstreetmap.org/#map=19/'+latlng.lat+'/'+latlng.lng+'&layers=D" target="_blank">Inspect this stretch in the OpenStreetMap database</a>'
            let heading, body;
            if (hwyName == "ferry") {
                heading = '<p style="margin: 0;"><b>Ferry Crossing</b></p>'
                body    = '<p style="margin: 4px 0px;">We don\'t know who the operator is or how frequently does the ferry run.</p>'
                        + '<p style="margin: 4px 0px;">' + dbLink + ' to learn more </p>';
            } else {
                heading = '<p style="margin: 0;"><b>Restricted Section [highway: '+hwyName+']</b></p>'
                var probably = '<p class="small text-muted" style="margin: 4px 0px;"> The highway type suggests '
                if (KProbablyOnewayHwyTypes.has(hwyName)) {
                    probably += '<b>this stretch may be one-way</b>, '
                } else if (KProbablyPedestranisedHwyTypes.has(hwyName)) {
                    probably += '<b>this stretch may be pedestrianised</b>, '
                } else {
                    probably += '<b>bicycles may be explicitly forbidden</b>, ';
                }
                probably += 'but there could be other reasons and we cannot tell you for sure. Unless you know this area, we encourage that you:</p>';
                var actionList = '<ul class="unstyled small text-muted" style="padding-left: 1.2em;">'
                                 +'<li><a id="showOnStreetView" href="#">Check StreetView</a></li>'
                                 +'<li>'+dbLink+'.</li></ul>';
                var reason  = '<p class="small text-muted" style="margin: 4px 0px;">You are technically not permitted to cycle through this stretch, but we\'ve offered it because the \'relax restrictions\' slider (once) asked us to do so or because we had no other choice. </p>'+probably+actionList
                body    = '<p class="text-danger" style="margin: 4px 0px;"><b>You may need to dismount.</b></p>' + reason;
            }
            return '<div style="margin:-5px 0">'+heading+body+'</div>';
        }

    }







    class Section {
        constructor(hwy, forbidden) {
            this.mForbidden = forbidden;
            this.mHwy       = hwy;
            this.mLatLngs   = [];
        }

        add(latlng) {
            this.mLatLngs.push(latlng);
            return this;
        }

        distanceMts(latlng) {
            return this.mLatLngs[this.mLatLngs.length-1].frmStrt
                  -this.mLatLngs[0].frmStrt;
        }

        midPoint() {
            function interpolate(startLatLng, endLatLng, atDistance) {
                var dd = endLatLng.frmStrt
                       - startLatLng.frmStrt;
                atDistance -= startLatLng.frmStrt;
                if (atDistance >= dd) {
                    return endLatLng;
                } else if (atDistance <= 0) {
                    return startLatLng;
                }
                var factor = 1.0*atDistance/dd;
                return new geometry.LatLng(
                    startLatLng.lat + factor*(endLatLng.lat - startLatLng.lat),
                    startLatLng.lng + factor*(endLatLng.lng - startLatLng.lng)
                );
            }
            if (this.mLatLngs.length < 2) {
                return;
            }
            var totalDistance = this.mLatLngs[this.mLatLngs.length-1].frmStrt
                              - this.mLatLngs[0].frmStrt;
            var halfDistance  = totalDistance/2;
            var seekDistance  = this.mLatLngs[0].frmStrt + halfDistance;
            for (var i = 1; i < this.mLatLngs.length; i++) {
                if (this.mLatLngs[i].frmStrt == seekDistance) {
                    return this.mLatLngs[i];
                } else if (this.mLatLngs[i].frmStrt > seekDistance) {
                    return interpolate(this.mLatLngs[i-1], this.mLatLngs[i], seekDistance);
                }
            }
        }
    }


    class TrackPlugin extends track.Plugin {

        constructor(highways, track) {
            super(track, "warn");
            this.mHighways = highways;
            this.mSections = [];
            this.mActive   = false;
        }

        clone(track) {
            return new TrackPlugin(track);
        }

        append(latlng) {
            let hwy  = undefined;
            let warn = undefined;
            if (latlng.d3c && latlng.d3c.h != undefined) {
                hwy  = this.mHighways.highway(latlng.d3c.h);
                warn = latlng.d3c.a == "forbidden" || KWarnAboutHighways.has(hwy.name());
                if (warn) {
                    if (!this.mActive) {
                        this.mSections.push(
                            new Section(hwy, latlng.d3c.a == "forbidden")
                        );
                    }
                    this.mActive = true;
                }
            }
            if (this.mActive) {
                var lastSection = this.mSections.back();
                lastSection.add(latlng);

                if (hwy) {
                    if (!warn) {
                        this.mActive = false;
                    } else if (hwy.id() != lastSection.mHwy.id()) {
                        this.mSections.push(
                            new Section(hwy, latlng.d3c.a == "forbidden")
                        );
                        this.mSections.back().add(latlng);
                    }
                }
            }
            return super.append(latlng);
        }

        show(parent, map) {
            this.mDrawn = new DrawnSections(this.mSections, parent, map).draw();
        }

        clear() {
            if (this.mDrawn) {
                this.mDrawn.clear();
                delete this["mDrawn"];
            }
        }

    }

    TrackPlugin.create = function(track) {
        return new TrackPlugin(track)
    }


    return {
        track       : {
            Plugin  : TrackPlugin
        }
    };

})();

module.exports = warned;