/**
 * scripts/ui/common/waypointsSection.js
 *
 * Base functionality of a single way segment that has intermediate waypoints.
 */
const when         = require("when");

const utils        = require("../../lib/utils.js");
const geometry     = require("../../lib/geometry.js");

const _module = (function() {


    /*
     *
     * Base functionality of a single way segment that has intermediate waypoints.
     *
     */
    class Section extends require("./sectionBase.js").SectionBase {

        constructor(map, params) {
            super(map, params);
        }

        delete() {
            this.mTrack && this.mTrack.warn.clear();
            return super.delete(arguments);
        }


        redraw() {
            this.mTrack && this.mTrack.warn.clear();
            super.redraw();
        };

        /*
         * Distribute the json representation of a route's fragment (routing result or product of earlier toJSON) into the
         * sections following 'this'. In a general case, a route request has been issued between two existing
         * sections and number intermediates (optional via points). Now the result has come as single array of latlng
         * coordinates and, should via points been specified in the request, we need to find them between
         * this and 'end'. They are marked with a 't' (for terminal)
         */
        fromJSON(allWaypoints) {
            const self = this;
            let sections = allWaypoints.reduce(function(sections, wpt, idx) {
                wpt      = geometry.LatLng.create(wpt);
                let d3c  = wpt.d3c;
                sections.back().append(wpt);

                if (d3c && d3c.t) {
                    delete d3c["t"];
                    sections.push(sections.pop().wrap());
                    sections.push(self._newTrack())
                }
                return sections;
            }, [this._newTrack()]);
            sections.push(sections.pop().wrap());
            this.setSections(sections);
        };

        setSections(sections) {
            this.clear();
            this.mTrack && this.mTrack.warn.clear();
            this.mTrack  = sections[0];
            if (sections.length > 1) {
                this.mNext.setSections(sections.splice(1))
            }
        };

        draw(force) {
            if (super.draw(force)) {
                this.mTrack.warn.show(this, this.mMap);
            }
        };


        prepareRoute(routesPromise, end) {
            var self = this;
            return when(routesPromise)
            .then(function(route) {
                self.fromJSON(route.waypoints);
            });
        };

        toJSON() {
            let result = super.toJSON();
            if (this.waypoints()) {
                result["waypoints"] = this.waypoints().map(function(pt) {
                    return pt.toArray();
                });
            }
            return result;
        };

        append(section) {
            var self = this;
            super.append(section, function(current) {
                if (current.projection && self.waypoints()) {
                    var appendToPrev  = undefined;
                    var prependToNext = undefined;
                    var splitAt = current.projection.segment.closest.distanceMts < 20 && current.projection.segment.closest.index;
                    var waypoints = self.waypoints();
                    var tail      = undefined;
                    if (splitAt != false) {
                        //projection is actually one of the waypoints, make sure it's on both segments
                        tail = self.waypoints()[splitAt].clone();
                        waypoints.splice(splitAt, 0, tail);
                    } else {
                        splitAt   = current.projection.segment.start.index+1;
                        tail      = current.projection.projection.clone();
                        waypoints.splice(splitAt, 0, tail, tail.clone());
                    }
                    tail.d3c   = tail.d3c || {};
                    tail.d3c.t = true; //terminator
                    delete current["projection"];
                    self.fromJSON(waypoints);
                } else {
                    self.clear();
                }
            });
        };
    }



    return { Section : Section };
})();

module.exports = _module;