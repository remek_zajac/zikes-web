/**
 * scripts/ui/streetView.js
 *
 * This manages the route item in the Journey panel
 */

'use strict';

const when          = require("when");
when["sequence"]  = require('when/sequence');
const mustache      = require("mustache");
const googleAPI     = require("../../lib/google.quarantined.js");
const geometry      = require("../../lib/geometry.js");

const streetView = (function() {

    var dialogWindowTemplate = when.defer();
    $.get("/templates/ui.template.html", function(template, textStatus, jqXhr) {
        dialogWindowTemplate.resolve($(template).filter("#streetView_tmpl").html());
    });

    function googlePoint2Str(point) {
        return point ? "["+point.lat()+","+point.lng()+"]" : "undefined";
    }

    function latlngEqual(a,b) {
        if (a && b) {
            var a = a instanceof L.LatLng ? [a.lat,a.lng] : [a.lat(),a.lng()];
            var b = b instanceof L.LatLng ? [b.lat,b.lng] : [b.lat(),b.lng()];
            return a[0] == b[0] && a[1] == b[1];
        }
        return false;
    }

    function toGoogle(latlng) {
        return latlng && !(latlng instanceof google.maps.LatLng) ?
            new google.maps.LatLng({lat: latlng.lat, lng: latlng.lng}) : undefined;
    }

    function toLPoint(point) {
        if (Array.isArray(point)) {
            return new L.LatLng(point);
        }
        return point;
    }



    var lastHeading = 0;
    class RoutePoint {
        constructor(sectionIterator) {
            this.mSection = sectionIterator;
        }


        heading() {
            var current = toGoogle(this.current());
            var next    = toGoogle(this.peekNext({minMetres:20}));
            if (current && next) {
                lastHeading = google.maps.geometry.spherical.computeHeading(
                    current,next
                );
            }
            return lastHeading;
        }

        current() {
            return  this.mSection.current()
        }

        next(metres) {
            this.mSection.next(metres);
            return this.current();
        }

        peekNext(metres) {
            return this.mSection.peekNext(metres);
        }

        walk(stepCb, stoppedCb, options) {
            delete this["mStopped"];
            var self = this;
            options  = options || {
                frameMs  : 2000,
                speedKph : 60
            }
            if (options.maxIterations != undefined) {
                --options.maxIterations;
            } else {
                options.maxIterations = 50;
            }
            if (options.maxIterations > 0 &&
                this.next({metres:(options.speedKph*1000)/(3600000/options.frameMs)})
                ) {
                var timeBefore = new Date().getTime();
                when(stepCb(this.current(), this.heading()))
                .then(function() {
                    return when.delay(
                        Math.max(0, options.frameMs - (new Date().getTime() - timeBefore))
                    );
                })
                .then(function() {
                    if (!self.mStopped) {
                        self.walk(stepCb,stoppedCb,options);
                    } else {
                        self.stop(stoppedCb);
                    }
                });
            } else {
                this.stop(stoppedCb);
            }
        }

        stop(stoppedCb) {
            this.mStopped = true;
            stoppedCb && stoppedCb();
        }

    }










    var throttleUpdatesToMs = 1000;
    var KKoneSize = 40;
    class StreetView {

        constructor(map) {
            this.mMap = map;
        }

        update(what) {
            this.shown() && this.show(what);
        }

        show(what, headingForPoint) {
            var self = this;
            var now = new Date().getTime();
            var dt  = now - this.mLastUpdateMs;
            if (this.mDeferred) {
                clearTimeout(this.mDeferred);
                delete self["mDeferred"];
            }
            if (dt < throttleUpdatesToMs) {
                this.mDeferred = setTimeout(function() {
                    self.show(what);
                    delete self["mDeferred"];
                }, dt);
                return;
            }
            this.mLastUpdateMs = now;
            if (this.mRoutePoint) {
                this.mRoutePoint.stop();
            }

            what = toLPoint(what);
            var heading = headingForPoint ?
                google.maps.geometry.spherical.computeHeading(
                    toGoogle(what), toGoogle(toLPoint(headingForPoint))
                ) : 0;
            if ("next" in what) {
                this.mRoutePoint = new RoutePoint(what);
                what             = this.mRoutePoint.current();
                heading          = this.mRoutePoint.heading();
            }

            return when(this.render(what,heading)).then(function() {
                return self.doShow(what, heading);
            });
        }


        render(where, heading) {
            if (this.mDiv) {
                this.mDiv.removeClass("hidden");
                return;
            }
            var self = this;
            when(dialogWindowTemplate.promise)
            .then(function(template) {
                function listenForUpdates() {
                    var streetViewDiv = $("#streetViewWindow #streetViewBody");
                    var staticDiv     = $("#streetViewWindow #static");

                    var marker = undefined;
                    self.mDiv.find(".close").click(function() {
                        self.mDiv.addClass("hidden");
                        marker.delete();
                        self.mRoutePoint && self.mRoutePoint.stop();
                    });

                    var markerIcon = {
                        url:    "graphics/cone.png",
                        size:   [KKoneSize, KKoneSize],
                        anchor: [20, 40]
                    };

                    function updateMarker() {
                        !marker || marker.delete();
                        if (self.mDiv.hasClass("hidden")) {
                            return;
                        }
                        var streetViewLocation = self.mPanorama.getLocation();
                        var position           = self.mPanorama.getPosition();
                        if (streetViewLocation &&
                            latlngEqual(streetViewLocation.latLng, position)) {
                            staticDiv.addClass("hidden");
                            streetViewDiv.removeClass("hidden");
                        } else {
                            streetViewDiv.addClass("hidden");
                            staticDiv.removeClass("hidden");
                        }

                        position = new geometry.LatLng(position.lat(), position.lng());
                        var markerPosition = position.distanceTo(self.mRequestedPosition.where) < 30 ?
                            self.mRequestedPosition.where :
                            position;
                        marker = new self.mMap.sprites.Marker(
                            markerPosition,
                            {
                                icon: markerIcon,
                                draggable : false,
                                opacity: 0.8,
                                rotationAngle: self.mPanorama.getPov().heading
                            }
                        ).show();

                        var markerBounds     = new geometry.Bounds(marker.getIcon().getBoundingClientRect());
                        var streetViewBounds = new geometry.Bounds(self.mDiv[0].getBoundingClientRect()).expand(KKoneSize);
                        if (!self.mMap.getBounds().contains(markerPosition) ||
                             streetViewBounds.contains(markerBounds)) {
                            self.mMap.setView({position: markerPosition});
                        }
                        if (!(self.mRequestedPosition.reached.promise.inspect().state == "fulfilled")) {
                            self.mRequestedPosition.reached.resolve(true);
                        }
                    }

                    var KStaticTimeoutMs = 2000;
                    var staticTimeout    = undefined;
                    function panoChanged() {
                        var streetViewLocation = self.mPanorama.getLocation();
                        var position           = self.mPanorama.getPosition();
                        clearTimeout(staticTimeout);

                        if (streetViewLocation &&
                            latlngEqual(streetViewLocation.latLng, position)) {
                            updateMarker();
                        } else {
                            staticTimeout = setTimeout(function() {
                                updateMarker();
                            }, KStaticTimeoutMs);
                        }
                    }

                    self.mPanorama.addListener('position_changed', panoChanged);
                    self.mPanorama.addListener('pov_changed', panoChanged);
                }

                $("body").append(
                    mustache.render(template)
                );
                self.mDiv = $("#streetViewWindow");
                var streetViewDiv = $("#streetViewWindow #streetViewBody");
                self.mDiv.show();
                self.mPanorama = new google.maps.StreetViewPanorama(
                    streetViewDiv[0],
                    {
                      position: where,
                      pov: {heading: heading, pitch: 0},
                      zoom: 1
                    }
                );

                var walkButton = self.mDiv.find("#walk");
                var stopButton = self.mDiv.find("#stop");
                walkButton.click(function() {
                    walkButton.addClass("hidden");
                    stopButton.removeClass("hidden");
                    self.mRoutePoint.walk(
                        self.doShow.bind(self),
                        function() {
                            stopButton.addClass("hidden");
                            walkButton.removeClass("hidden");
                        }
                    );
                });
                stopButton.click(function() {
                    self.mRoutePoint.stop();
                });

                listenForUpdates(self);
            });
        }

        doShow(where, heading) {
            this.mRequestedPosition = {
                where   : where,
                reached : when.defer()
            }
            this.mPanorama.setOptions({
                position: toGoogle(where),
                pov: {heading: heading, pitch: 0}
            });
            return this.mRequestedPosition.reached.promise;
        }

        shown() {
            return this.mDiv ? !this.mDiv.hasClass("hidden") : false;
        }
    }


    return StreetView

})();

module.exports = streetView;