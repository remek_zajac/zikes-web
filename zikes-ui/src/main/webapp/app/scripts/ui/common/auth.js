/**
 * scripts/ui/auth.js
 *
 * Handles authentication (with ui)
 */

'use strict';

const when     = require("when");
const hello    = require("hellojs");
const mustache = require("mustache");

const http     = require("../../lib/http.js");
const utils    = require("../../lib/utils.js");

const auth = (function() {

    const networks = {
        facebook: "955510821203918",
        google: "787067721578-396usv4alu7u9rjfnf0mi2pa6m7r8jps.apps.googleusercontent.com"
    }

    const authDialogTemplate = when.defer();
    $.get("/templates/ui.template.html", function(template, textStatus, jqXhr) {
        const html = $(template).filter("#authDialogTemplate_tmpl").html()
        authDialogTemplate.resolve(html);
    });


    const KLSKey                  = "auth.js";
    const KTokenValidityMarginSec = 100;
    class AuthenticatedUser {
        constructor(eventDispatcher, ls) {
            this.eventDispatcher = eventDispatcher;
            ls = ls || JSON.parse(window.localStorage.getItem(KLSKey));
            if (ls) {
                this.network      = ls.network;
                this.authResponse = ls.authResponse;
                this.userDetails  = ls.userDetails;
            }
        }

        tokenValid() {
            if (!(this.authResponse && this.authResponse.access_token)) {
                return false;
            }

            var currentTime   = (new Date()).getTime() / 1000;
            var timeRemaining = this.authResponse.expires - currentTime;
            if (timeRemaining < KTokenValidityMarginSec) {
                return false;
            }
            return true;
        }

        toStore() {
            window.localStorage.setItem(KLSKey,
                JSON.stringify({
                    network      : utils.assert(this.network,      "Network not set"),
                    authResponse : utils.assert(this.authResponse, "authResponse not set"),
                    userDetails  : utils.assert(this.userDetails,  "userDetails not set"),
                    ctor         : this.constructor.name
                })
            );
        }

        login(force) {
            utils.assert(this.network);
            return !force && this.tokenValid() && this;
        }

        logout() {
            window.localStorage.removeItem(KLSKey);
            delete this["userDetails"];
            delete this["authResponse"];
            delete this["network"];
        }
    }

    class HelloAuthenticatedUser extends AuthenticatedUser {

        constructor(eventDispatcher, ls) {
            super(eventDispatcher, ls);
            const self = this;
            hello.init(networks, {
                redirect_uri: '/auth_redirect.html'
            });
            hello.on("auth.login", function(response) {     //could be initiated (spontanously) by hello.init or by explicit login
                self.authResponse = response.authResponse;
                self.login().then(function() {              //if initiated by hello.init we need to re-run login to fetch user data
                    self.eventDispatcher.fire("login", {user: self});
                })
                .otherwise(function(e) {
                    console.error("The token we have been assured is valid turns out stale, we need to force you to hard-authenticate again. Sorry about that", e);
                    authenticatedUser.login(true);
                });
            });

            hello.on("auth.logout", function(r) {
                self.eventDispatcher.fire("logout");
            });
        }

        login(force) {
            const self = this;
            return when(super.login() || when.promise(function(resolve, reject) {
                let options = {
                    display : "popup",
                    force   : null
                }
                if (!force) {
                    if (self.network != "facebook") {
                        options.display = "none";
                    }
                }

                when.any([hello(self.network).login(options), when.delay(3000)])
                .then(function(response) {
                    if (response) {
                        self.authResponse = response.authResponse;
                        resolve();
                    } else if (force) {
                        reject(new Error("No auth response"));
                    } else {
                        resolve();
                    }
                }, reject);
            }))
            .then(function(tokenWasValid) {
                return when((tokenWasValid && self.userDetails) || when.promise(function(resolve, reject) {
                    hello(self.network).api('/me')
                    .then(function(userDetails) {
                        self.userDetails = userDetails;
                        resolve();
                    }, reject);
                }));
            })
            .then(function(userDataWereValid) {
                userDataWereValid || self.toStore();
                return self;
            })
            .catch(function(err) {
                console.error("foo: ", err);
            });
        }

        logout() {
            this.network && hello.logout(this.network);
            super.logout();
        }
    }


    const KFbBaseUrl           = "https://graph.facebook.com/v2.7/me?fields=email,first_name,last_name,name,timezone,verified&suppress_response_codes=true&access_token="
    const KFbAssumeExpireInSec = 60*60*24; //facebook doesn't tell us when its token expires, so we'll see what this does.
    class PhoneGapAuthenticatedUser extends AuthenticatedUser {
        constructor(eventDispatcher, ls) {
            super(eventDispatcher, ls);
            const self   = this;
            this.doLogin = {
                "facebook" : function() {
                    return when.promise(function(resolve, reject) {
                        window.CordovaFacebook.login({
                            permissions: ['public_profile'],
                            onSuccess: function(response) {
                                if (response.accessToken && response.userID) {
                                    self.authResponse = {
                                        access_token : response.accessToken,
                                        expires      : Math.round((new Date().getTime()/1000)+KFbAssumeExpireInSec),
                                        network      : "facebook"
                                    }
                                    self.userDetails = {
                                        thumbnail    : 'https://graph.facebook.com/' + response.userID + '/picture',
                                        id           : response.userID
                                    }
                                    resolve();
                                } else {
                                    reject(new Error("No token returned"))
                                }
                            },
                            onFailure: reject
                        })
                    })
                    .then(function() {
                        return new http.HttpRequest(
                            KFbBaseUrl + self.authResponse.access_token
                        ).get()
                        .then(function(response) {
                            self.userDetails.name  = response.name;
                            self.userDetails.email = response.email;
                        });
                    });
                },
                "google" : function() {
                    return when.promise(function(resolve, reject) {
                        window.plugins.googleplus.login({webClientId:networks.google}, function(response) {
                            self.authResponse = {
                                access_token : response.accessToken,
                                expires      : response.expires,
                                network      : "google"
                            }
                            self.userDetails = {
                                name         : response.displayName,
                                thumbnail    : response.imageUrl,
                                email        : response.email,
                                id           : response.userId
                            }
                            resolve();
                        }, reject);
                    })
                }
            }
        }

        login() {
            const self = this;
            return  when(super.login() || this.doLogin[this.network]())
            .then(function(tokenWasValid) {
                if (!tokenWasValid) {
                    self.toStore();
                }
                self.eventDispatcher.fire("login", {user: self});
                return self;
            });
        }

        logout() {
            window.plugins.googleplus.logout();
            super.logout();
            this.eventDispatcher.fire("logout");
        }
    }

    class UIAuth extends utils.Subscribable {
        constructor() {
            super();
            const self = this;
            this.resolved = when.promise(function(resolve, reject) {
                $(document).ready(function(){
                    let ls = JSON.parse(window.localStorage.getItem(KLSKey));
                    if (ls && ls.ctor && ls.ctor == "HelloAuthenticatedUser") {
                        resolve( new HelloAuthenticatedUser(self, ls));
                    } else {
                        when().delay(5000).then(function() { //it's unfortunate, but in order to determine the environment (or use it!), we need to let it load
                            if (window.plugins && window.plugins.googleplus) {
                                resolve(new PhoneGapAuthenticatedUser(self));
                                return;
                            }
                            resolve(new HelloAuthenticatedUser(self));
                        });
                    }
                });
            }).then(function(resolved) {
                resolved.network && resolved.login(); //speculative
                return resolved;
            });
        }

        fire(eventType, event) {
            if (eventType == "login" && !this.loggedIn) {
                var userNavItem = $("#userNavItem");
                userNavItem.find("#userName").html(event.user.userDetails.name);
                userNavItem.find("#userAvatar").attr('src', event.user.userDetails.thumbnail);
                $("#loginNavItem").addClass("hide");
                userNavItem.removeClass("hide");
                this.loggedIn = true;
                return super.fire(eventType, event);
            } else if (eventType == "logout" && this.loggedIn) {
                $("#loginNavItem").removeClass("hide");
                $("#userNavItem").addClass("hide");
                delete this["loggedIn"];
                return super.fire(eventType, event);
            }
        }

        _chooseNetwork(prompt) {
            return prompt && when(authDialogTemplate.promise)
            .then(function(template) {
                return when.promise(function(resolve, reject) {
                    $(document.body).append(
                        mustache.render(
                            template, {
                                prompt:prompt
                            }
                        )
                    );
                    var modal = $("#authModal").modal({"backdrop" : "static"});
                    modal.find("#google").click(function() {
                        modal.modal("hide");
                        resolve("google");
                    });
                    modal.find("#facebook").click(function() {
                        modal.modal("hide");
                        resolve("facebook");
                    });
                    modal.on('hidden.bs.modal', function (e) {
                        resolve();
                        modal.remove();
                    });
                });
            })
        }

        login(prompt) {
            const self = this;
            return when(this.resolved).then(function(resolved) {
                return when(resolved.network || self._chooseNetwork(prompt))
                .then(function(network) {
                    resolved.network = network;
                    return resolved.network && resolved.login(prompt);
                });
            });
        }

        logout() {
            return when(this.resolved).then(function(resolved) {
                return resolved.logout();
            });
        }
    }
    $("#loginNavItem").click(function() {
        auth.login("Choose Network");
    });

    $("#logoutNavItem").click(function() {
        auth.logout();
    });


    const auth = new UIAuth();
    return auth;
})();

module.exports = auth;