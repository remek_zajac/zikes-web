/**
 * scripts/ui/common/mapRoute.js
 *
 * This draws the route on the map and takes care of the interactions
 * with it.
 */

'use strict';

const when             = require("when");

const trackModule      = require("../../lib/track.js");
const utils            = require("../../lib/utils.js");
const geometry         = require("../../lib/geometry.js");

const sections         = require("./sections.js");
const drawnSegment     = require("./drawnSegment.js");

const route = (function() {

    const naviIconSize = [23, 23];
    const KDefaultIcons =  {
        to   : {
            class: "toFromIcon",
            html: '<div class="glyphicon glyphicon-log-in"></div>',
            size: naviIconSize
        },
        from : {
            class: "toFromIcon",
            html: '<div class="glyphicon glyphicon-log-out"></div>',
            size: naviIconSize
        },
        via  : {
            class: "toFromIcon",
            size: [15,15]
        }
    };

    class Route {

        constructor(parent, options = {}) {
            this.mHead      = undefined;
            this.mHasFrom   = false;
            this.mParent    = parent;
            this.mIcons     = options.icons || KDefaultIcons;
            this.readonly   = !parent.route;
        }

        /*
         * Route.invalidate is the main ´lifecycle´ entry of a Route. It is called by:
         * - client (parent) - to request rerouting (as routing preferences have changed)
         * - subordinate sections - when they observe ui interaction that needs to be
         *   acted on (a marker has been moved and the section needs re-routing)
         *
         * Sections cannot request routing for themselves - they won't be able to optimise
         * a call that spans multiple, often disjoint, sections. This call is made at the onset
         * and when complete the function arranges for two stage update:
         * - calls invalidate on all sections - this is the opportunity for them to finalise
         *   their elevation profiles (request elevation from the server). A collective elevation
         *   profile must be amased before calling draw (to know how with what colour range to draw)
         * - calls draw on all sections.
         */
        invalidate(options) {
            function needsRouting(section, force) {
                return "prepareRoute" in section && section.isStale();
            }

            options     = options || {};
            const self  = this;

            function prepareRoute() {
                let routesPrepared    = [];
                if (!self.readonly) {
                    let tail              = self.mHead;
                    let head              = undefined;
                    let routeRqMilestones = [];
                    while (tail) {
                        //Some sections may need (re)routing (they were dragged) and if they do,
                        //we need to keep adding their new coordinates to routeRqMilestones
                        //for as long as we encounter stale nodes and thus compose a single
                        //consistent [from,[via...],to] request.
                        if (options.reroute || needsRouting(tail)) {
                            if (!head) {
                                //remember the first section (which will receive the route request result)
                                head = tail;
                            }
                            routeRqMilestones.push([tail.startMarker.getLatLng().lat, tail.startMarker.getLatLng().lng]);
                        } else if (routeRqMilestones.length) {
                            //If the stale nodes are interleaved with not stale ones, we need to issue
                            //more than one routing request (for detached sections).
                            routeRqMilestones.push([tail.startMarker.getLatLng().lat, tail.startMarker.getLatLng().lng]);
                            routesPrepared.push(
                                head.prepareRoute(self.mParent.route(routeRqMilestones), tail)
                            );
                            routeRqMilestones = [];
                            head = undefined;
                        }
                        tail = tail.mNext;
                    }
                    //and the last request
                    if (routeRqMilestones.length > 1) {
                        routesPrepared.push(
                            head.prepareRoute(self.mParent.route(routeRqMilestones), self.mHead.tail())
                        );
                    }
                }
                return when.all(routesPrepared);
            }

            return prepareRoute()
            .then(function() {
                return when.all(
                    self.mapSections(function(section) {
                        return section.invalidate();
                    })
                );
            })
            .then(function() {
                //generate new profile
                self.mProfile  = new trackModule.Tracks();
                self.mapSections(function(section) {
                    section.mNext && self.mProfile.append(section.profile());
                });
                self.mProfile = self.mProfile.wrap();

                //redraw (if say, the track changed from rugged<->smooth it needs forceful redrawing)
                let shownElevation = self.showsElevation();
                self.style(true);
                let forceRedraw = options.redraw || (self.showsElevation() ? !shownElevation : shownElevation);
                self.mapSections(function(section) {
                    section.draw(forceRedraw);
                });

                return self.mParent.invalidate();
            });
        };

        distanceMts() {
            return Math.round((this.mProfile && this.mProfile.distanceMts) || 0);
        };

        getProfile() {
            return this.mProfile;
        };

        style(sectionType) {
            if (!this.mStyle || sectionType == true) {
                var profile     = this.getProfile();
                var parentStyle = this.mParent.style();
                var deft = {
                    width: 5,
                    opacity: 0.7,
                    smoothFactor: 3,
                    lineCap: "butt",
                    color : parentStyle.color.base,
                    horizonIcons : {
                        from   : this.mIcons.from,
                        to     : this.mIcons.to
                    }
                };
                if (parentStyle.showElevation && profile && profile.ele.rugged) {
                    deft.schema = {
                        reach      : "spot",
                        datapoint  : ["d3c", "e"],
                        thresholds : drawnSegment.thresholds(
                            [{value: profile.ele.min.value, color: parentStyle.color.base},
                             {value: profile.ele.max.value, color: parentStyle.color.max}],
                            7
                        )
                    }
                }
                this.mStyle = {
                    default           : deft,
                    RubberbandSection : utils.mergeSimpleObjects(
                        utils.cloneSimpleObject(deft), {
                            dashArray : "5, 5"
                        }
                    )
                };
            }
            if (sectionType in this.mStyle) {
                return this.mStyle[sectionType];
            }
            return this.mStyle.default;
        };

        showsElevation() {
            return this.mStyle && this.mStyle.default.schema;
        }

        head(newHead) {
            if (!newHead) {
                //getter
                return this.mHead;
            }
            if (this.mHead && newHead === this.mHead.tail()) {
                //if the new head is the same as the current 'to' - we have no 'from'
                this.mHasFrom = false;
            }
            this.mHead = newHead;
            return this.mHead;
        };

        /**
         * Setter and getter for this.from.
         * @latlng  - array of floats carrying the new coordinates for this.from.
         *            when undefined, the method behaves as a getter.
         * @returns - (setter) true - if the given latlng differs from current (false otherwise)
         *            (getter) head Section object (if exists)
         */
        from(latlng) {
            if (!latlng) {
                //getter
                if (this.mHead) {
                    if (this.mHead != this.mHead.tail() || this.mHasFrom) {
                        return this.mHead.startMarker.getLatLng();
                    }
                }
                return undefined;
            }
            //setter...
            var currentFrom = this.from();
            if (currentFrom) {
                if (currentFrom == latlng) {
                    return false;
                }
                this.mHead.startMarker.setLatLng(latlng);
            }
            else {
                this.mHead = new sections.Section(this.mParent.map, {
                    next     : this.mHead,
                    icon     : this.mIcons.from,
                    latlng   : latlng,
                    parent   : this
                });
                if (this.mHead.mNext) {
                    this.mHead.mNext.mPrev = this.mHead;
                }
            }
            this.mHasFrom = true;
            return true;
        };

        /**
         * Setter and getter for this.to.
         * @latlng  - array of floats carrying the new coordinates for this.to.
         *            when undefined, the method behaves as a getter.
         * @returns - (setter) true - if the given latlng differs from current (false otherwise)
         *            (getter) tail Section object (if exists)
         */
        to(latlng)
        {
            if (!latlng) {
                //getter
                if (this.mHead) {
                    if (this.mHead != this.mHead.tail() || !this.mHasFrom) {
                        return this.mHead.tail().startMarker.getLatLng();
                    }
                }
                return undefined;
            }
            //setter...
            var currentTo = this.to();
            if (currentTo) {
                if (currentTo == latlng) {
                    return false;
                }
                this.mHead.tail().startMarker.setLatLng(latlng);
            } else {
                var to = new sections.Section(this.mParent.map, {
                    icon     : this.mIcons.to,
                    latlng   : latlng,
                    parent   : this
                });
                if (!this.mHead) {
                    this.mHead = to;
                } else {
                    to.mPrev = this.mHead.tail();
                    this.mHead.tail().mNext = to;
                }
            }
            return true;
        };

        /**
         * Adds a via point.
         * @latlng  - array of floats carrying the new coordinates for the new via.
         */
        via(latlng) {
            if (!latlng) {
                return;
            }
            const projection = this.projectOnto(latlng, Infinity);
            if (projection) {
                projection.segment.section.append(
                    new sections.Section(this.mParent.map, {
                        icon     : this.mIcons.via,
                        latlng   : latlng,
                        parent   : this
                    })
                );
            }
        }

        isPending() {
            return !(this.mHead && this.mHead.mNext);
        };

        isUnititialised() {
            return !this.mHead;
        };

        otherRoutes(cb) {
            return this.mParent.otherJoureyItems(cb);
        };

        highlight() {
        }

        projectOnto(location, toleranceMts) {
            var result = undefined;
            this.mapSections(function(section) {
                var projection = section.projectOnto(location, toleranceMts);
                if (projection && (!result || result.distanceMts > projection.distanceMts)) {
                    result = projection;
                }
            });
            return result;
        };

        toJSON() {
            return this.mapSections(function(section) {
                return section.toJSON();
            });
        };

        fromJSON(sectionsJSON) {
            this.delete();
            for (var i = 0; i < sectionsJSON.length; i++) {
                var icon = this.mIcons.via;
                if (!this.mHead) {
                    icon = this.mIcons.from;
                } else if (i == sectionsJSON.length-1) {
                    icon = this.mIcons.to;
                }
                var ctorParams = {
                    icon     : icon,
                    latlng   : sectionsJSON[i].from,
                    parent   : this
                };
                var section = new sections[sectionsJSON[i].ctor || "Section"](
                    this.mParent.map, ctorParams
                );
                if (!this.mHead) {
                    this.mHead = section
                } else {
                    section.mPrev = this.mHead.tail();
                    section.mPrev.mNext = section;
                    "fromJSON" in section.mPrev && section.mPrev.fromJSON(sectionsJSON[i-1].waypoints);
                }
            }
            return this.invalidate();
        };

        showDirectionMarkers(show) {
            this.mapSections(function(section) {
                section.showDirectionMarkers(show);
            });
        };

        getBounds() {
            var bounds = new geometry.LatLngBounds();
            this.mapSections(function(section) {
                bounds.extend(section.getBounds());
            });
            return bounds;
        };

        delete() {
            var undo;
            if (this.mHead) {
                undo = this.mapSections(function(section) {
                    return section.delete();
                });
            }
            this.mHead    = undefined;
            this.mHasFrom = false;
            return undo;
        };

        mapSections(f) {
            var result = [];
            var current = this.mHead;
            while (current) {
                result.push(f(current));
                current = current.mNext;
            }
            return result;
        };


        waypointIterator() {
            return new sections.WaypointIterator(this.mHead);
        }
    }

    return {
        Route         : Route,
        KDefaultIcons : KDefaultIcons
    }

})();

module.exports = route;