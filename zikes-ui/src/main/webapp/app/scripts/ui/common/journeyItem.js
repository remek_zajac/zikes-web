/**
 * scripts/ui/common/journeyItem.js
 *
 * Base mixin for Journey Items
 */

'use strict';

const when         = require("when");
const mustache     = require("mustache");
const geometry     = require("../../lib/geometry.js");
const geocode      = require("../../lib/geocode.js");
const utils        = require("../../lib/utils.js");
const routeProfile = require("./routeProfile.js");

const journeyItem = (function() {

    class JourneyItemStub {}

    /*
     * A JourneyItem is part of a journey (e.g.: Route or POI). A JourneyItem has a direct relationship with its parent,
     * a Journey object. It typically has on-map and off-map UI representation.
     *
     * The JourneyItem class below is common between desktop and mobile and it allows for leaf objects (Route and POI) to be
     * composed with multiple inheritance. As desktop Route and POI share a desktop-specific JourneyItem and the JourneyItem
     * below.
     *
     * The multiple inheritance isn't supported in javascript per se, but it's mimicked. So effectivelly this is the case:
     *
     *     desktop/JourneyItem
     *             ^
     *             |
     *     common/JourneyItem
     *             ^
     *     common/Route
     *             ^
     *             |
     *     desktop/Route
     *
     * What common/JourneyItem encapsulates is the relationship/delagtion with the on-map UI element and the
     */
    var journeyItemId = 0;
    let JourneyItem = (superclass = JourneyItemStub) => class extends superclass {

        constructor(options) {
            super(options);
            this.options   = options;
            this.mId       = "journeyItemId_"+journeyItemId++;
            this.mParent   = options.parent;
            this.map       = options.map;
            this.mParams   = {
                givenName    : undefined,
                description  : ""
            }
        }

        name() {
            return this.mParams.givenName;
        }

        centre(options={}) {
            options = utils.mergeSimpleObjects({
                maxZoom : 17,
                animate : true,
                padding : 30
            }, options);

            this.map.fitBounds(
                this.getBounds(),
                options
            );
        }

        delete() {
            if (this.mParent) {
                this.mParent.deleteItem(this);
            }
            return !super.delete || super.delete();
        }

        /*
         * If not already rendered and DOM nesting provided (as this.options.itemsDiv):
         *   Renders off-map representation, adds to DOM, puts common $divs into this.mDivs and calls this.rendered()
         */
        render(templates, templateName, templateParams={}) {
            if (this.mDivs != undefined || !this.options.itemsDiv) {
                return when(true);
            }
            const self = this;
            this.mDivs = false;
            templateParams["id"]   = this.mId;
            templateParams["name"] = self.name();
            return when(templates)
            .then(function(templates) {
                self.options.itemsDiv.append(mustache.render(templates[templateName], templateParams));
                let panel  = self.options.itemsDiv.find("#"+self.mId);
                if (!panel.length) {
                    panel  = self.options.itemsDiv;
                }
                self.mDivs = {
                    panel         : panel,
                    name          : panel.find(".name"),
                    description   : panel.find(".description")
                }
                self.mDivs.description.html(self.mParams.description);
                return self.rendered();
            });
        }

        rendered() {
            return when(!super.rendered || super.rendered(...arguments));
        }

        doInvalidate() {
            return !super.doInvalidate || super.doInvalidate();
        }

        invalidate() {
            var self = this;
            if (!this.mInvalidatePosted) {
                this.mInvalidatePosted = this.render()
                .delay(300) //delay to catch quick followups
                .then(function() {
                    self.doInvalidate();
                    delete self["mInvalidatePosted"];
                    self.mParent && self.mParent.invalidate();
                });
            }
            return this.mInvalidatePosted;
        }
    };

    const KMaxZoomForPopup = 9;
    const KBkgColor = "#fff5cc";
    let POI = (superclass) => class extends JourneyItem(superclass) {

        constructor() {
            super(...arguments);
        }

        toJSON() {
            return { poi : this.mParams };
        }

        fromJSON(json) {
            this.mParams = json;
            return this.invalidate();
        }

        delete() {
            !this.mLmMarker || this.mLmMarker.delete();
            return super.delete();
        }

        getBounds() {
            if (this.mParams.latlng) {
                return new geometry.LatLngBounds(
                    new geometry.LatLng(this.mParams.latlng[0], this.mParams.latlng[1]),
                    new geometry.LatLng(this.mParams.latlng[0], this.mParams.latlng[1])
                );
            }
        }

        name() {
            var result = super.name();
            if (!result) {
                if (this.mParams.geocode) {
                    result = geocode.humanReadable(
                        this.mParams.geocode, 3
                    );
                } else {
                    result = "New Point of Interest";
                }
            }
            return result;
        }

        popupContent() {
            var description = "";
            if (this.mParams.description) {
                description = '<p class="small text-muted" style="margin: 0;color:black">'+this.mParams.description+'</p>'
            }
            return '<div style="margin:-5px 0"><p style="margin: 0; color:red"><b>'+this.name()+'</b></p>'+description+'</div>';
        }

        resetPopup() {
            if (this.mLmMarker) {
                this.mLmMarker.getPopup().setContent(
                    this.popupContent()
                );
                if ((this.mParams.givenName ||
                     this.mParams.description) &&
                    this.map.getZoom() > KMaxZoomForPopup &&
                    !this.mParams.hidePopup) {
                    this.mLmMarker.openPopup();
                } else {
                    this.mLmMarker.closePopup();
                }
            }
        }

        render() {
            var self = this;
            if (this.mParams.latlng) {
                return super.render(...arguments)
                .then(function() {
                    if (self.mLmMarker) {
                        return;
                    }
                    self.mLmMarker = new self.map.sprites.Marker(self.mParams.latlng, {
                        draggable : true,
                        icon : {
                            url: '//cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.0/images/marker-icon.png',
                            size: [20,36],
                            anchor: [10, 36],
                            popupAnchor: [0, -24]
                        }
                    }).show();
                    self.mLmMarker.bindPopup(
                        new self.map.sprites.Popup({
                            autoClose : false,
                            closeOnClick : false,
                            autoPan : false,
                            opacity : 0.7,
                            backgroundColor: KBkgColor
                        })
                    );
                    self.resetPopup();
                });
            }
        }
    }

    let Route = (superclass) => class extends JourneyItem(superclass) {

        constructor() {
            super(...arguments);
            this.mCentreOnRoute = true;
        }

        doInvalidate() {
            super.doInvalidate();
            if (!this.mRouteOnMap.isPending() &&
                 this.mCentreOnRoute &&
                 this.getBounds().isValid()
            ) {
                this.centre();
                delete this["mCentreOnRoute"];
            }
            this.mDivs.routeStats.distance.text(utils.mtsToString(this.distanceMts()));
            if (this.mParams.v) {
                if (this.mParams.v.avg) {
                    this.mDivs.routeStats.speed.avg.text(
                        Math.round(utils.mps2Kph(this.mParams.v.avg))
                    );
                    if (this.mParams.v.max) {
                        this.mDivs.routeStats.speed.max.text(
                            Math.round(utils.mps2Kph(this.mParams.v.max.value))
                        );
                    }
                    this.mDivs.routeStats.speed.unit.text("km/h");
                    this.mDivs.routeStats.speed.removeClass("hide");
                } else {
                    this.mDivs.routeStats.speed.addClass("hide");
                }
            }
            if (this.mParams.mov && this.mParams.mov.durationSec) {
                this.mDivs.routeStats.duration.html('<span style="margin-right:4px">'+utils.secondsToHrs(this.mParams.mov.durationSec)+'</span><span class="glyphicon glyphicon glyphicon-time"/>');
            }
            let geoProfile = this.mRouteOnMap.getProfile();
            if (geoProfile && (!this.mPixProfile || this.mPixProfile.geoProfile() != geoProfile)) {
                this.mPixProfile = new routeProfile.PixRouteProfile(
                    geoProfile,
                    this.map.highways,
                    this.mDivs
                );
            }
            this.mPixProfile && this.mPixProfile.draw();
            this.mDivs.dEle.text(this.dEleContent());
        }

        from(latlng) {
            if (!latlng) {
                if (this.mParams.from) {
                    return this.mParams.from;
                }
                latlng = this.mRouteOnMap.from() || (this.map.geoLocator && this.map.geoLocator.position());
                return latlng && {
                    latlng : latlng
                };
            } else if (this.mParams.from && latlng.lat == this.mParams.from.latlng.lat && latlng.lng == this.mParams.from.latlng.lng) {
                return
            }
            this.mParams.from = {latlng : latlng};
            if (this.mRouteOnMap.from(latlng)) {
                this.mRouteOnMap.invalidate();
            }
            return this.mParams.from;
        }

        to(latlng) {
            if (!latlng) {
                if (this.mParams.to) {
                    return this.mParams.to;
                }
                return this.mRouteOnMap.to() && {
                    latlng : this.mRouteOnMap.to()
                };
            } else if (this.mParams.to && latlng.lat == this.mParams.to.latlng.lat && latlng.lng == this.mParams.to.latlng.lng) {
                return
            }
            if (this.from()) {
                this.mRouteOnMap.from(this.from().latlng);
            }
            this.mParams.to = {latlng : latlng};
            if (this.mRouteOnMap.to(latlng)) {
                this.mRouteOnMap.invalidate();
            }
            return this.mParams.to;
        }

        dEleContent() {
            if (this.mPixProfile && !isNaN(this.mPixProfile.geoProfile().ele.d)) {
                return  "\u0394" + Math.round(this.mPixProfile.geoProfile().ele.d) + "m "
                        + "\u2197" + Math.round(this.mPixProfile.geoProfile().ele.gain) + "m";
            }
            return "";
        }

        distanceMts() {
            return this.mRouteOnMap.distanceMts();
        }

        isPending() {
            return this.mRouteOnMap.isPending();
        }

        isUnititialised() {
            return this.mRouteOnMap.isUnititialised();
        }

        route() {
            this.mRouteOnMap.invalidate({reroute:true});
        }

        showsElevation() {
            return this.mRouteOnMap.showsElevation();
        }

        toJSON() {
            return {
                route : {
                    meta    : this.mParams,
                    sections: this.mRouteOnMap.toJSON()
                }
            };
        }

        fromJSON(json) {
            this.mCentreOnRoute = false;
            this.mParams = json.meta;
            return this.mRouteOnMap.fromJSON(json.sections);
        }

        getBounds() {
            return this.mRouteOnMap.getBounds();
        }

        rendered() {
            let panel                  = this.mDivs.panel;
            this.mDivs.profileCanvas   = panel.find(".routeProfileCanvas").get(0);
            this.mDivs.highlightCanvas = panel.find(".highlightProfileCanvas").get(0);
            let routeStatsDiv          = panel.find(".routeStats");
            let speedDiv               = routeStatsDiv.find(".speed");
            speedDiv["avg"]            = speedDiv.find(".avg");
            speedDiv["max"]            = speedDiv.find(".max");
            speedDiv["unit"]           = speedDiv.find(".unit");

            this.mDivs.routeStats = {
                distance : routeStatsDiv.find(".distance"),
                speed    : speedDiv,
                duration : panel.find(".routeStats .duration")
            };
            this.mDivs["dEle"]         = panel.find(".routeEleDelta");
            return super.rendered();
        }

        render(templates, templateName, templateParams={}) {
            templateParams.distanceMts = utils.mtsToString(this.distanceMts());
            return super.render(templates, templateName, templateParams);
        }

        name() {
            var result = super.name();
            if (!result) {
                var fromTo = [
                    this.mParams.from && this.mParams.from.geocode,
                    this.mParams.to && this.mParams.to.geocode
                ];
                if (fromTo[0] || fromTo[1]) {
                    result = geocode.humanReadable(fromTo)
                    .map((e) => { return e || "" })
                    .join(" -> ");
                } else {
                    result = "New Route";
                }
            }
            return result;
        }
    };



    return {
        JourneyItem            : JourneyItem,
        Route                  : Route,
        POI                    : POI
    };

})();

module.exports = journeyItem;