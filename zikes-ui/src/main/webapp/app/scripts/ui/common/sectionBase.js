/**
 * scripts/ui/common/drawnSegment.js
 *
 * Base functionality of a single way segment, i.e.:  a node in a linked list of SectionBase.
 */

const utils           = require("../../lib/utils.js");
const geometry        = require("../../lib/geometry.js");
const unredo          = require("../../lib/unredo.js");
const trackModule     = require("../../lib/track.js");
const hwyModule       = require("../../lib/highways.js");
const wayMarker       = require("./wayMarker.js");
const drawnSegment    = require("./drawnSegment.js");
const warnSegments    = require("./warnedSegments.js");


const _module = (function() {

    /*
     *
     * Base functionality of a single way segment, i.e.:  a node in a linked list of SectionBase.
     *
     */
    class SectionBase {

        constructor(map, params) {
            if (!map) {
                return; //prototype instantiation
            }
            const self          = this;
            this.mMap         = map;
            this.mParent      = params.parent;
            this.mPrev        = params.prev;
            this.mNext        = params.next;
            this.startMarker  = new wayMarker.WayMarker(
                map,
                params.latlng, {
                    icon      : params.icon,
                    opacity   : 0.7,
                    draggable : params.parent.readonly ? false : true
                }
            );
            this.on();
        }

        _newTrack() {
            return new trackModule.Track([
                trackModule.Elevation.create.bind(null, ["d3c","e"]),
                hwyModule.track.Plugin.create.bind(null, ["d3c","h"]),
                warnSegments.track.Plugin.create.bind(null, this.mMap.highways)
            ]);
        }

        profile() {
            return this.mTrack;
        }

        waypoints() {
            return this.mTrack && this.mTrack.points;
        };

        distanceMts() {
            return (this.mTrack && this.mTrack.distanceMts) || 0;
        };

        on() {
            const self = this;
            if (!this.mOn) {
                this.mOn = true;
                this.startMarker.undelete();
                unredo.push(
                    new unredo.Checkpoint(function() {
                        if (!self.isTemp() && !self.isOrphan()) {
                            self.delete();
                            self.mParent.invalidate();
                        } else {
                            return false;
                        }
                    }, this)
                );
                this.startMarker.on("dragend", function() {
                    if (self.isTemp()) {
                        self.mPrev.append(self.orphan());
                    } else {
                        var prevLocation = self.startMarker.mPrevLatLng;
                        unredo.push(
                            new unredo.Checkpoint(function() {
                                self.startMarker.setLatLng(prevLocation);
                                self.mParent.invalidate();
                            })
                        );
                        if ((self.mPrev || self.mNext ) &&
                           !(self.mPrev && self.mNext)) {
                            //we'll merge to the tail
                            //start assuming this is the tail that we've just move
                            var closeNeithbours = [];
                            var saveIfClose = function(other) {
                                if (other && other.startMarker.pixDistanceFrom(self.startMarker) < 8) {
                                    closeNeithbours.push(other);
                                }
                            }
                            var end = function(ofWhat) {return ofWhat.mHead; }
                            if (!self.mPrev) {
                                //this is head that we've just moved
                                end = function(ofWhat) {return ofWhat.mHead.tail(); }
                            }
                            self.mParent.otherRoutes(function(other) {
                                if (!other.isPending()) {
                                    saveIfClose(end(other));
                                }
                            });
                            if (closeNeithbours.length == 1) {
                                var recipient = self.mParent;
                                var newTail   = closeNeithbours[0];
                                if (!self.mPrev) {
                                    recipient = newTail.mParent;
                                    newTail   = self;
                                }
                                if ("merge" in recipient) {
                                    recipient.merge(newTail);
                                }
                            }
                        }
                    }
                    self.mParent.invalidate();
                }).on("mouseover", function(e) {
                    if (!self.isTemp()) {
                        self.mParent.highlight(
                            self.distanceOffsetFromStartMts(),
                            true
                        );
                    }
                    if ("mouseover" in self.mParent) {
                        e.section = self;
                        self.mParent.mouseover(e);
                    }
                }).on("mouseout", function(e) {
                    if (!self.isTemp()) {
                        self.mParent.highlight(false);
                    }
                }).on("click", function(e) {
                    if ("contextmenu" in self.mParent) {
                        e.section = self;
                        self.mParent.contextmenu(e);
                    }
                }).on("contextmenu", function(e) {
                    if ("contextmenu" in self.mParent) {
                        e.section = self;
                        self.mParent.contextmenu(e);
                    }
                });
                this.mMap.on("zoomend", zoomend, this);
            }
            return this;
        }

        delete() {
            const self = this;
            var undo;
            if (!this.isTemp()) {
                if (this.mNext) {
                    this.mNext.mPrev = this.mPrev;
                }

                if (this.mPrev) {
                    this.mPrev.mNext = this.mNext;
                    this.mPrev.clear();
                } else if (this.mNext) {
                    this.mParent.head(this.mNext);
                } else {
                    this.mParent.mHead = undefined;
                }
                undo = new unredo.Checkpoint(function() {
                    if (!self.isOrphan()) {
                        if (self.mPrev) {
                            self.mPrev.append(self.orphan());
                        } else if (self.mNext) {
                            var next = self.mNext;
                            self.mNext = undefined;
                            self.mParent.head(self);
                            self.append(next);
                        } else {
                            self.mParent.head(self);
                        }
                        self.on();
                    } else {
                        return false;
                    }
                });
            } else {
                unredo.delete(function(cp) {
                    return cp.context() === self
                });
            }
            this.clear();
            this.startMarker.delete();
            this.mMap.off("zoomend", zoomend, this);
            delete this["mOn"];
            return undo;
        };

        clear() {
            this.redraw();
            delete this["mTrack"];
        };

        redraw() {
            !this.mDrawnSegment || this.mDrawnSegment.clear();
            this.mDrawnSegment = undefined;
        };

        showDirectionMarkers(show) {
            !this.mDrawnSegment || this.mDrawnSegment.showDirectionMarkers(show);
        };

        tail() {
            if (this.mNext) {
                return this.mNext.tail();
            }
            return this;
        };

        head() {
            if (this.mPrev) {
                return this.mPrev.head();
            }
            return this;
        };

        //temp is such that it points at its prev and next but them don't point
        //at it
        isTemp() {
            return !((!this.mNext || this.mNext.mPrev == this) &&
                     (!this.mPrev || this.mPrev.mNext == this));

        };

        orphan() {
            this.mNext   = undefined;
            this.mPrev   = undefined;
            this.mParent = undefined;
            return this;
        };

        isOrphan() {
            return !this.mParent;
        };

        isStale() {
            return this.mNext &&
                   !(this.isOrphan() || this.isTemp()) &&
                   (this.startMarker.hasMoved() ||
                    this.mNext.startMarker.hasMoved() ||
                    !this.waypoints());
        };

        split(projection) {
            var wedgedSection = new this.constructor(
                this.mMap, {
                    parent : this.mParent,
                    icon   : this.mParent.mIcons.via,
                    latlng : projection ? projection.projection : this.startMarker.getLatLng()
                }
            );
            if (projection) {
                wedgedSection.projection = projection;
            }
            this.append(wedgedSection);
            return wedgedSection;
        }

        cut(projection) {
            var wedgedSection = this.split(projection);
            var tail = wedgedSection.tail();
            while(tail !== wedgedSection) {
                tail.delete();
                tail = wedgedSection.tail();
            }
            wedgedSection.clear();
            return wedgedSection;
        }

        append(section, forEachAppendedCb) {
            utils.assert(this.tail() != section.tail(), "can't append waymarkers sharing the same tail");

            //bi-list insert
            //affix head
            if (section.mPrev) {
                section.mPrev.mNext = undefined;
            }
            var next = this.mNext;
            this.mNext = section;
            section.mPrev = this;

            if (section.mParent && section.mParent.mHead === section) {
                if (section.mParent === this.mParent) {
                    section.mParent.mHead = this.head();
                } else {
                    section.mParent.mHead = undefined;
                }
            }

            var current = section;
            while(current) {
                current.mParent = this.mParent;
                !forEachAppendedCb || forEachAppendedCb(current);
                current.redraw();
                current = current.mNext;
            }


            //affix tail
            var sectionTail = section.tail();
            if (next) {
                next.mPrev = sectionTail;
                sectionTail.mNext = next;
            }

            this.redraw();
        };

        move(newParent) {
            utils.assert(!newParent.mHead, "I don't know what to do with the old head of this guy");
            if (this.mPrev) {
                this.mPrev.mNext = undefined;
                this.mPrev.clear();
            }
            this.mPrev = undefined;
            newParent.head(this);
            var current = this;
            while (current) {
                current.mParent = newParent;
                current = current.mNext;
            }
        };

        distanceOffsetFromStartMts() {
            var result = 0;
            var current = this.mPrev;
            while (current) {
                result += current.distanceMts()
                current = current.mPrev;
            }
            return result;
        };

        getBounds() {
            if (this.mDrawnSegment) {
                return this.mDrawnSegment.getBounds();
            }
        }

        toJSON() {
            return {
                from : this.startMarker.toJSON(),
                ctor : this.constructor.name
            }
        };

        invalidate() {
            if (this.mNext && this.mPrev) {
                this.startMarker.setIcon(this.mParent.mIcons.via);
            } else if (this.mNext) {
                this.startMarker.setIcon(this.mParent.mIcons.from);
            } else if (this.mPrev) {
                this.startMarker.setIcon(this.mParent.mIcons.to);
            }
        };

        draw(force) {
            function mouseMove(e) {
                if (lastHoverEvent.containerPoint.distanceTo(e.containerPoint) > KPixTolerance) {
                    if (tempMarker) {
                        if (!tempMarker.startMarker.beingDragged) {
                            tempMarker.delete();
                        }
                        tempMarker = undefined;
                    }
                    self.mMap.off("mousemove", mouseMove);
                    self.mParent.highlight(false);
                }
            };
            const self = this;

            force = force || !this.mDrawnSegment || this.isStale();
            this.startMarker.clearMoved();

            if (!this.mNext || force) {
                !this.mDrawnSegment || this.mDrawnSegment.clear();
            }
            if (this.mNext && force) {
                this.mDrawnSegment = new drawnSegment.Polyline(
                    this.waypoints(),
                    this.mMap,
                    this.mParent.style(this.constructor.name)
                ).show();
                this.mDrawnSegment.bringToFront();

                var tempMarker          = undefined;
                var lastHoverEvent      = undefined;
                var KPixTolerance       = 10;
                this.mDrawnSegment.on('mouseover', function(e) {
                    var segmentStart = e.nearest.segment.start.point;
                    self.mParent.highlight(
                        segmentStart.frmStrt +
                        e.nearest.projection.distanceTo(segmentStart)+
                        self.distanceOffsetFromStartMts(),
                        true
                    );
                    lastHoverEvent = e;
                    if (tempMarker) {
                        tempMarker.startMarker.setLatLng(e.nearest.projection);
                    } else {
                        tempMarker = new self.constructor(self.mMap, {
                            icon     : self.mParent.mIcons.via,
                            prev     : self,
                            next     : self.mNext,
                            latlng   : e.nearest.projection,
                            parent   : self.mParent
                        });
                        self.mMap.on("mousemove", mouseMove);
                    }
                    tempMarker.projection = e.nearest;
                });
                return true;
            }
        };

        highlight(highlight) {
            if (this.mDrawnSegment) {
                if (highlight) {
                    this.mDrawnSegment.highlight({
                        opacity: 1,
                        width: 7
                    });
                } else {
                    this.mDrawnSegment.highlight();
                }
            }
        };

        projectOnto(what, toleranceMts) {
            if (what && this.mDrawnSegment) {
                if (what instanceof SectionBase) {
                    what = what.startMarker;
                }
                if (what instanceof wayMarker.WayMarker) {
                    what = what.mLmMarker;
                }
                if ("getLatLng" in what) {
                    what = what.getLatLng();
                }
                let force  = false;
                if (toleranceMts != undefined) {
                    if (toleranceMts != Infinity &&
                        !this.getBounds().clone().expand({metres: 2 * toleranceMts}).contains(what)) {
                        return;
                    }
                    force = true;
                }
                var result = this.mDrawnSegment.projectOnto(what, force);
                if (toleranceMts != undefined && result.distanceMts > toleranceMts) {
                    return;
                }
                if (result) {
                    var segmentStart = result.segment.start.point;
                    result.distanceFromStart = segmentStart.frmStrt +
                        result.projection.distanceTo(segmentStart) +
                        this.distanceOffsetFromStartMts();
                    result.segment.section = this;
                }
                return result;
            }
        };

    }



    function zoomend() {
        function areClose(div1, div2) {
            if (div1 && div2) {
                div1 = div1.getBoundingClientRect();
                div1 = new geometry.Point(div1.left, div1.top);
                div2 = div2.getBoundingClientRect();
                div2 = new geometry.Point(div2.left, div2.top);
                return div1.distanceTo(div2) < 20;
            }
            return true;
        }

        function visiblePrev() {
            var current = this.mPrev;
            while(current) {
                if (current.startMarker.getIcon()) {
                    return current;
                }
                current = current.mPrev;
            }
        }

        if (this.mNext) {
            var animate = {animate:400};
            if (!this.mPrev) {
                var tail = this.tail();
                if (areClose(this.startMarker.getIcon(), tail.startMarker.getIcon())) {
                    this.startMarker.hide(animate);
                    tail.startMarker.hide(animate);
                } else {
                    this.startMarker.show(animate);
                    tail.startMarker.show(animate);
                }
            } else {
                var prev = visiblePrev.call(this);
                if (!prev || areClose(this.startMarker.getIcon(), prev.startMarker.getIcon())) {
                    this.startMarker.hide(animate);
                } else {
                    this.startMarker.show(animate);
                }
            }
        }
    }








    class WaypointIterator {

        constructor(section, wayPointIndex, prevResult) {
            this.mSection        = section;
            if (wayPointIndex != undefined) {
                this.mWayPointIdx = wayPointIndex;
            }
            this.mPreviousResult = prevResult;
        }

        current(ignorePrev) {
            if (!ignorePrev && this.mPreviousResult) {
                return this.mPreviousResult;
            }
            if (!("mSection" in this)) {
                return undefined;
            }
            if ("mWayPointIdx" in this && this.mSection.waypoints()) {
                return this.mSection.waypoints()[this.mWayPointIdx];
            }
            return this.mSection.startMarker.getLatLng();
        }

        back() {
            if (this.mSection) {
                this.mSection     = this.mSection.tail();
                delete this["mWayPointIdx"];
            }
            return this.current();
        }

        next(offset) {
            const self = this;
            function next() {
                if (self.mSection.isTemp()) {
                    self.mWayPointIdx = self.mSection.mPrev.projectOnto(
                        self.mSection.startMarker
                    ).segment.start.index;
                    self.mSection  = self.mSection.mPrev;
                }
                if ("mWayPointIdx" in self) {
                    if (self.mSection.waypoints() &&
                        self.mWayPointIdx == self.mSection.waypoints().length - 1) {
                        //last point of the section
                        if (self.mSection.mNext && self.mSection.mNext.waypoints()) {
                            self.mSection = self.mSection.mNext;
                            delete self["mWayPointIdx"];
                        } else {
                            //fell off the cliff, iterator invalid now
                            delete self["mWayPointIdx"];
                            delete self["mSection"];
                            return undefined;
                        }
                    } else {
                        ++self.mWayPointIdx;
                    }
                } else if (self.mSection.waypoints()) {
                    self.mWayPointIdx = 0;
                } else {
                    delete self["mWayPointIdx"];
                    delete self["mSection"];
                    return undefined;
                }
                return self.current(true);
            }

            offset = offset || 1;
            var metres = 0;
            var projectionTolerance = undefined;
            if (typeof offset === 'object') {
                if (offset.metres != undefined) {
                    utils.assert(offset.metres >= 0, "offset.metres must be greater than zero");
                    metres = Math.max(0, offset.metres);
                    projectionTolerance = offset.projectionTolerance == undefined ? 40 : offset.projectionTolerance;
                } else if (offset.minMetres != undefined) {
                    metres = offset.minMetres;
                } else {
                    console.error("Wrong offset!!");
                    console.dir(offset);
                }
                offset = metres ? 0 : 1;
            }
            var previous = this.mPreviousResult;
            this.mPreviousResult = undefined;
            var result   = this.current(true);
            if (!result) {
                return undefined;
            }
            if (previous) {
                metres -= previous.distanceTo(result);
                offset -= 1;
            }
            for (;offset > 0 || metres > 0; offset--) {
                previous = result;
                result   = next();
                if (!result) {
                    this.mPreviousResult = previous;
                    return this.mPreviousResult;
                }
                metres -= previous.distanceTo(result);
            }
            metres = Math.abs(metres);
            if (projectionTolerance != undefined && (metres > projectionTolerance)) {
                //asked for some metres (not minMetres), but we've now overshot and are too far behind the current result
                if (result.distanceTo(previous) < projectionTolerance) {
                    //we've overshot by a smaller distance than projectionTolerance, so return previous
                    this.mPreviousResult = result = previous;
                } else {
                    //project
                    this.mPreviousResult = result = new geometry.GeoLine(result, previous).offsetMts(metres);
                }
            } else {
                delete this["mPreviousResult"];
            }
            return result;
        }

        peekNext(offset) {
            return new WaypointIterator(
                this.mSection,
                this.mWayPointIdx,
                this.mPreviousResult
            ).next(offset);
        }

        dump(offset) {
            var result = [];
            while(this.current()) {
                result.push(this.current());
                this.next(offset);
            }
            return result;
        }
    }




    return { SectionBase : SectionBase,
             WaypointIterator : WaypointIterator };
})();

module.exports = _module;