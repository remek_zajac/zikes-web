/**
 * scripts/ui/geoLocator.js
 */

'use strict';
const when          = require("when");
const http          = require("../../lib/http.js");
const geometry      = require("../../lib/geometry.js");
const utils         = require("../../lib/utils.js");
const movement      = require("../../lib/movement.js");
const db            = require("../../lib/indexedDB.js");
const zikesBackend  = require("../../lib/zikesBackend.js");

const geoLocator = (function() {

    function geoIP() {
        return http.myIp()
        .then((ip) => {
            return http.get("//freegeoip.net/json/"+ip);
        })
        .then(function(response) {
            return {
                position : new geometry.LatLng(response.latitude, response.longitude),
                country  : response.country_name
            }
        });
    }

    const KLastLocationLocalStorageKey = "lastPosition";
    function lastPosition() {
        var position = window.localStorage.getItem(KLastLocationLocalStorageKey);
        if (position) {
            return new geometry.LatLng.create(
                JSON.parse(position)
            );
        }
    }

    function getCurrentPosition() {
        var result = when.defer();
        //well: http://stackoverflow.com/questions/3397585/navigator-geolocation-getcurrentposition-sometimes-works-sometimes-doesnt
        navigator.geolocation.getCurrentPosition(function () {}, function () {}, {});
        function doGet(trials) {
            navigator.geolocation.getCurrentPosition(
                function(position) { //success
                    result.resolve(position);
                },
                function(err) { //failure
                    if (!trials) {
                        result.reject(err);
                    }
                    doGet(--trials)
                },
                {
                    enableHighAccuracy: true,
                    timeout           : 100,
                    maximumAge        : Infinity
                }
            );
        }
        doGet(10);
        return result.promise;
    }

    var persistThrottleTimeout;
    const KMaximumAgeMs          = 30000;
    const KMaxSnapToCorrectedMts = 15;
    class GeoLocator extends utils.Subscribable {

        constructor(options = {
            minDistanceMts:20,
            minIntervalSec:15
        }) {
            super();
            this.mOptions = options;
            this.mOptions.minIntervalMs  = this.mOptions.minIntervalSec ? this.mOptions.minIntervalSec*1000 : Infinity;
            this.mOptions.minDistanceMts = this.mOptions.minDistanceMts || 1;
        }

        listen() {
            let self = this;
            function lost() {
                if (self.mPosition) {
                    self.fire("lost");
                    delete self["mPosition"];
                }
            }

            function update(position) {
                if (!self.mSimulatedPoints) {
                    self.update(geometry.LatLng.create({
                        lat : position.coords.latitude,
                        lng : position.coords.longitude,
                        d3c : {
                            ts : position.timestamp,
                            v  : position.speed
                        }
                    }));
                }
            }

            let ready;
            if (window.cordova) {
                let permissions = cordova.plugins.permissions;
                if (permissions) {
                    ready = when.promise(function(resolve, reject) {
                        permissions.requestPermissions(
                            [permissions.ACCESS_FINE_LOCATION, permissions.ACCESS_COARSE_LOCATION],
                            function() {
                                resolve();
                            }, function(e) {
                                reject(e);
                            }
                        );
                    });
                }
            }
            function updateAndWatch(position) {
                update(position);
                setTimeout(function() {
                    if (self.mPosition && self.mPosition.d3c.ts != position.timestamp ) {
                        return;
                    }
                    getCurrentPosition()
                    .then(updateAndWatch)
                    .otherwise(lost);
                }, 10000);
            }
            when(ready).then(function() {
                navigator.geolocation.watchPosition(
                    updateAndWatch,
                    console.error,
                    {
                        enableHighAccuracy: true,
                        maximumAge        : KMaximumAgeMs
                    }
                );
            });

            setTimeout(()=> {
                let coarsePosition = self.mPosition && {
                    position : self.mPosition,
                    fine : true
                };
                when(coarsePosition || geoIP())
                .then((coarsePosition) => {
                    self.fire("coarse", coarsePosition);
                })
            },2000);
            return this;
        }

        update(position) {
            let self     = this;
            if (this.mPosition &&
                this.mPosition.distanceTo(position) < this.mOptions.minDistanceMts &&
                position.d3c.ts - this.mPosition.d3c.ts     < this.mOptions.minIntervalMs) {
                return;
            }

            when(this.mCorrectCb ? this.mCorrectCb(position) : position)
            .then(function(corrected) {
                let acquired   = !self.mPosition;
                self.mPosition = corrected;
                if (corrected.routeProjection && corrected.routeProjection.snapped) {
                    if (corrected.routeProjection.distanceMts < KMaxSnapToCorrectedMts) {
                        self.mPosition.lat = corrected.routeProjection.snapped.lat;
                        self.mPosition.lng = corrected.routeProjection.snapped.lng;
                    } else {
                        self.mPosition.d3c._corrected = corrected.routeProjection.snapped;
                    }
                }

                self.fire("position", { position: self.mPosition });
                !acquired || self.fire("acquired", { position: self.mPosition });
            });

            if (!persistThrottleTimeout) {
                persistThrottleTimeout = setTimeout(function() {
                    let positionToSave = JSON.stringify((self.mPosition || position).toArray());
                    window.localStorage.setItem(KLastLocationLocalStorageKey, positionToSave);
                    persistThrottleTimeout = undefined;
                }, 30000);
            }
        }

        position(allowStale) {
            return this.mPosition || (allowStale && lastPosition());
        }

        correct(cb) {
            utils.assert(!("mCorrectCb" in this), "Only one corrector allowed");
            this.mCorrectCb = cb;
        }

        on(eventType, cb) {
            if (eventType == "acquired" && this.mPosition) {
                cb({
                    type: "acquired",
                    position: this.mPosition
                })
            }
            super.on(eventType, cb);
        }

        simulate(points, options) {
            if (this.mSimulatedPoints) {
                console.log("Already running simulation");
                return;
            }
            function createPt(pt) {
                if (pt) {
                    let result = geometry.LatLng.create(pt);
                    result.d3c = utils.cloneSimpleObject(result.d3c || {});
                    return result;
                }
            }
            options               = options             || {};
            options.distortMts    = options.distortMts != undefined ? options.distortMts : 0;
            let first = createPt(points[0]);
            if (options.distortMts) {
                options.distortDeg = new geometry.GeoVector(
                    first,
                    first.offsetCourse(options.distortMts, Math.PI/4)
                );
            } else {
                options.distortDeg = options.distortDeg || new geometry.GeoVector(first, first);
            }

            options.durationSec   = options.durationSec || Infinity;
            if (!options.intervalMs) {
                options.speedKph  = options.speedKph    || 35;
                options.speedMps  = utils.kph2Mps(options.speedKph);
            }

            let done              = when.defer();
            let self              = this;
            const timeStart       = new Date().getTime();
            this.mSimulatedPoints = points;
            let index = 0;
            console.log("Starting to simulate gelocation with options "+JSON.stringify(options));

            function finish() {
                self.mSimulatedPoints = undefined;
                console.log("Finished simulating gelocation, fed "+(index-1)+" points.");
                done.resolve();
                return;
            }

            function emit(current) {
                //emit current
                if (!current) { return finish(); }
                current.d3c.ts = current.d3c.ts || new Date().getTime();
                self.update(current);

                //prepare next
                let next = createPt(self.mSimulatedPoints[index++]);
                if (!next || (options.durationSec && (new Date().getTime()-timeStart)/1000 > options.durationSec) ) {
                    //if any
                    return finish();
                }

                next.d3c.ts = next.d3c.ts || current.d3c.ts + 1000*current.distanceTo(next)/options.speedMps;
                let pauseMs    = options.quick ? 0 : next.d3c.ts - current.d3c.ts;
                setTimeout(function() { emit(next); }, pauseMs);
            }

            emit(createPt(self.mSimulatedPoints[index++]));
            return done.promise;
        }
    }

    class TrackDB extends db.IndexedDB {

        constructor() {
            super(
                "tracksDB",
                [new db.IndexedDBObjStore("track", {keyPath: "meta.mov.ts.start"})]
            );
        }
    }
    const trackDB = new TrackDB();


    //</DEBUG>
    const KKeepDebugTracksForDays = 5;
    const KKeepDebugTracksForMs   = KKeepDebugTracksForDays*24*60*60*1000;
    class DebugAllTrackDB extends db.IndexedDB {

        constructor() {
            super(
                "debugAllTracksDB",
                [new db.IndexedDBObjStore("track", {keyPath: "startedts"})]
            );
            const self = this;
            this.mRegister = [];
            this.stores.track.clear([0, new Date().getTime()-KKeepDebugTracksForMs]);
            $("#uploadDebugTracks").click(function() {
                self.upload();
            });
        }

        append(position) {
            this.mRegister.push(position);
            if (this.mRegister.length % 10 == 0) {
                this.stores.track.put({
                    startedts : this.mRegister[0].d3c.ts,
                    points    : this.mRegister.map(function(p) {
                        return p.toArray();
                    })
                });
            }
        }

        list() {
            this.stores.track.allKeys().then(function(keys) {
                console.log("Debug tracks:");
                keys.forEach(function(ts) {
                    console.log(new Date(ts).toLocaleString() + " => " + ts);
                });
            });
        }

        upload() {
            const self = this;
            return this.stores.track.all()
            .then(function(tracks) {
                let consolidatedTracks = new movement.TimeDistanceTracks({
                    ms      : Infinity,
                    mts     : Infinity
                });
                tracks.forEach(function(trk) {
                    consolidatedTracks.append(trk);
                });
                consolidatedTracks.wrap();
                if (consolidatedTracks.length)  {
                    zikesBackend.journeys.taken.upload({
                        metadata : zikesBackend.newMeta({
                            type  : "rawRecording",
                            debug : true,
                            from  : consolidatedTracks.front().front().toArray(),
                            to    : consolidatedTracks.back().back().toArray()
                        }),
                        journey   : {
                            raw  : consolidatedTracks.toJSON()
                        }
                    }).then(function() {
                        self.clear();
                    });
                }
            });
        }

        clear() {
            this.stores.track.clear();
        }

        print(ts) {
            if (!ts) {
                console.log("Give us a timestamp, use .list()");
                return;
            }
            this.stores.track.get(ts).then(function(track) {
                console.log(JSON.stringify(track));
            });
        }

    }
    window.debugAllTracksDB = new DebugAllTrackDB();
    //</DEBUG>


    class GeoLocatorMov extends GeoLocator {

        constructor(options) {
            super(options);
            const self = this;
            this.mRegister = new movement.Track().on(self.fire.bind(self));
            this.on("position", function(e) {
                self.mRegister.append(e.position);
                !window.debugAllTracksDB || window.debugAllTracksDB.append(e.position);
            });
        }

        simulate() {
            this.mRegister.wrap(); //to avoid timestamp issues
            return super.simulate(...arguments);
        }

        /*
         * Collect (for upload) tracks recorded to date
         */
        collectTracks(tracks, filter = {
            maxSpeed       : utils.kph2Mps(50),
            minAvgSpeed    : utils.kph2Mps(8),
            minDistanceMts : 1900,
            trackGlue      : movement.TrackGlue
        }) {
            if (!tracks.length) {
                return;
            }
            let consolidatedTracks = new movement.TimeDistanceTracks(filter.trackGlue);
            tracks.forEach(function(trk) {
                consolidatedTracks.append(trk);
            });
            consolidatedTracks.wrap();
            let tail; //track collection may happen at an arbitrary moment of time
                      //and there could be a tail that is too short at this moment, but
                      //the rider may continue to grow it, so let's not process it yet, but
                      //lets lets return it so that it can be preserved.
            consolidatedTracks = consolidatedTracks.filter(function(trk, idx) {
                if (trk.distanceMts <= filter.minDistanceMts) {
                    if (idx == consolidatedTracks.length-1 &&  //last track
                       (new Date().getTime() - trk.back().d3c.ts)/(1000*60) < filter.trackGlue.ms) { // that could still grow
                        tail = trk;
                    }
                    return false;
                }
                utils.assert(trk.mov.v.max,              "No max speed present");
                utils.assert(trk.mov.v.avg != undefined, "No avg speed present");

                return trk.mov.v.max.value < filter.maxSpeed &&
                       trk.mov.v.avg       > filter.minAvgSpeed;
            });
            let rawJurney;
            if (consolidatedTracks.length)  {
                rawJurney = {
                    metadata : zikesBackend.newMeta({
                        type : "rawRecording",
                        from : consolidatedTracks.front().front().toArray(),
                        to   : consolidatedTracks.back().back().toArray()
                    }),
                    journey   : {
                        raw  : consolidatedTracks.toJSON()
                    }
                }
            }
            return [rawJurney, tail];
        }

    }

    class GeoLocatorUI extends GeoLocatorMov {

        constructor(map, options) {
            super(options);
            let self = this;
            let positionsFed = 0;
            function saveRegister(register) {
                let capturedTrack = register.toJSON();
                !capturedTrack || trackDB.stores.track.put(capturedTrack);
                positionsFed = 0;
            }
            this.on("position", function(e) {
                if (self.mIcon) {
                    self.mIcon.delete();
                }
                self.mIcon = new map.sprites.Marker(
                    e.position.d3c._corrected || e.position, {
                        icon : {
                            class: "positionIcon",
                            size: [20,20]
                        }
                    }
                ).show();

                if (self.mRegister.leading() && (++positionsFed % 10) == 0) {
                    saveRegister(self.mRegister.leading())
                }
            });
            this.on("stopped", function(e) {saveRegister(e.track)});
            this.uploadTracks();
            when(map.loaded).then(function() {
                self.listen();
            });
        }

        uploadTracks(tracks) {
            const self = this;
            if (!tracks) {
                return trackDB.stores.track.all()
                .then(function(tracks) {
                    return self.uploadTracks(tracks)
                })
                .then(function(tail) {
                    return trackDB.stores.track.clear(tail ? [0, tail.front().d3c.ts-1] : undefined)
                })
            }
            return when(this.collectTracks(tracks))
            .then(function(journeyAndTail) {
                if (!journeyAndTail) {
                    return;
                }
                return when(journeyAndTail[0] && zikesBackend.journeys.taken.upload(journeyAndTail[0]))
                .then(function() {
                    return journeyAndTail[1];
                });
            });
        }
    }

    return {
        GeoLocatorUI    : GeoLocatorUI,
        GeoLocatorMov   : GeoLocatorMov,
        GeoLocator      : GeoLocator,
        lastPosition    : lastPosition
    }

})();

module.exports = geoLocator;