/**
 * scripts/ui/common/messaging.js
 *
 * This prepares and manages the Zikes's UI.
 */

'use strict';

const when     = require("when");
const mustache = require("mustache");
const utils    = require("../../lib/utils.js");

const messaging = (function() {

    /*
     * Message animations
     */
    function _shakeOnce(distance, duration) {
        var finished = when.defer();
        this.animate({"margin-left":(distance*-1)}, duration/4)
        .animate({"margin-left":distance}, duration/2)
        .animate({"margin-left":0}, duration/4, function() {
            finished.resolve();
        });
        return finished.promise;
    };

    function _shake(shakes, distance, duration) {
        var self = this;
        if (shakes == 0) {
            return;
        }
        return _shakeOnce.bind(this)(distance, duration/shakes)
        .then(function() {
            return _shake.bind(self)(shakes-1, distance - distance/shakes, duration - duration/shakes);
        });
    }

    function _fadeIn() {
        var finished = when.defer();
        this.animate({opacity: 1}, function() {
            finished.resolve();
        });
        return finished.promise;
    }

    function _fadeOut() {
        var finished = when.defer();
        this.animate({opacity: 0}, function() {
            finished.resolve();
        });
        return finished.promise;
    }

    function _fadeInNShake() {
        var self = this;
        return _fadeIn.bind(this)()
        .then(function() {
            return _shake.bind(self)(7,20,800)
        });
    }

    function _loop(div, animation, sleep) {
        var self = this;
        if (!this.hiding()) {
            return animation()
            .then(function() {
                if (sleep) {
                    return when.any([when().delay(sleep), self.whenHiding()]);
                }
            })
            .then(function() {
                if (!self.hiding()) {
                    return _fadeOut.bind(div)();
                }
            })
            .then(function() {
                return _loop.bind(self,div)(animation,sleep);
            });
        }
    }



    /*
     * A message queue associated with a specific display area.
     * Some may be exclusive, some may allow messages to coexist,
     * carousel, stack, etc.
     */
    function MessageQ(div) {
        this.mDiv = div;
        this.mQ   = [];
    }

    MessageQ.prototype.currentMessage = function() {
        return this.mQ.length ? this.mQ[this.mQ.length-1] : undefined;
    }

    MessageQ.prototype.find = function(cb) {
        return this.mQ.find(cb);
    }



    function SingleSlotMessageQ(div) {
        MessageQ.call(this, div);
    }
    SingleSlotMessageQ.prototype = new MessageQ();
    SingleSlotMessageQ.prototype.constructor = SingleSlotMessageQ;


    SingleSlotMessageQ.prototype.deque = function() {
        var self = this;
        var bestCandidateIdx = undefined;
        for (var i = this.mQ.length-1; i >= 0; i--) {
            var message = this.mQ[i];
            if (bestCandidateIdx == undefined || this.mQ[bestCandidateIdx].mPrority < message.mPrority) {
                bestCandidateIdx = i;
            }
        }
        if (bestCandidateIdx != undefined) {
            if (this.mQ[bestCandidateIdx] != this.currentMessage()) {
                //move the message to be shown to the top of the queue
                this.mQ.push(
                    this.mQ.splice(bestCandidateIdx, 1)[0]
                );
            }
            var message2Show = this.currentMessage();
            message2Show.render()
            message2Show.whenHidden()
            .then(function() {
                self.mQ.pop();
                self.mDiv.text("");
                self.deque();
            });
        }
    }

    SingleSlotMessageQ.prototype.enqueue = function(message) {
        if (!message.hidden()) {
            this.mQ.unshift(message);
            if (this.mQ.length == 1) {
                this.deque();
            } else if (this.currentMessage().mPrority < message.mPrority || this.currentMessage().mOptions.preemptMe) {
                this.currentMessage().hide();
            }
        }
    }


    var popupTemplate = when.defer();
    $.get("/templates/ui.template.html", function(template, textStatus, jqXhr) {
        var html = $(template).filter("#popupMessageTemplate_tmpl").html()
        popupTemplate.resolve(html);
    });
    function PopupMessageQ(div) {
        MessageQ.call(this, div);
        this.mLastId    = 0;
    }
    PopupMessageQ.prototype = new MessageQ();
    PopupMessageQ.prototype.constructor = PopupMessageQ;

    PopupMessageQ.prototype.enqueue = function(message) {
        var self = this;
        if (!message.hidden()) {
            var previousMessage = this.currentMessage();
            this.mQ.unshift(message);
            when(popupTemplate.promise)
            .then(function(template) {
                var id = self.mLastId++;
                self.mDiv.append(mustache.render(template, {
                    id: id
                }));
                var div = $("#popup_"+id);

                var previousMessageShown = previousMessage ? previousMessage.whenShown() : undefined;
                when(previousMessageShown)
                .then(function() {
                    message.render(div);
                    self.adjustHeight();
                });
                message.whenHidden()
                .then(function() {
                    for (var i = 0; i < self.mQ.length; i++) {
                        if (self.mQ[i] == message) {
                            self.mQ.splice(i,1);
                            break;
                        }
                    }
                    self.adjustHeight();
                });
            });
        }
    }

    PopupMessageQ.prototype.adjustHeight = function() {
        var finished = when.defer();
        var height = KBottomPadding;
        for (var i = 0; i < this.mQ.length; i++) {
            height += this.mQ[i].height();
        }
        this.mDiv.animate({"margin-top": -1*(height+30)}, function() {
            finished.resolve();
        });
        return finished.promise;
    }

    const KBottomMessageQ = new SingleSlotMessageQ($("#userMessage"));
    const KPopUpMessageQ  = new PopupMessageQ($("#modlessDialogWrapper"));
    const KBottomPadding  = Math.abs(
        parseInt(
            utils.substring(
                KPopUpMessageQ.mDiv.css("margin-top") || "0px", 0, -2
            )
        )
    );




    function localStorage(content) {
        var storageKey = "zikesMessages";
        if (content) {
            //setter
            window.localStorage.setItem(storageKey, JSON.stringify(content));
        } else {
            return JSON.parse(
                window.localStorage.getItem(storageKey) || "{}"
            );
        }
    }


    /*
     * Baseclass for all messaging
     */
    function MessageBase(text, queue, options) {
        var self = this;
        this.mText    = text;
        this.mHiding  = when.defer();
        this.mShown   = when.defer();
        this.mHidden  = when.defer();
        this.mQ       = queue;
        this.mOptions = options || {};
        if (this.mOptions.once) {
            var history   = localStorage();
            var messageId = self.mOptions.once;
            if (messageId in history) {
                this.mHiding.resolve();
                this.mHidden.resolve();
            } else {
                this.whenHidden().then(function() {
                    history = localStorage();
                    history[messageId] = true;
                    localStorage(history);
                });
            }
        }
    }

    MessageBase.prototype.enqueue = function(timeout = 0) {
        var self = this;
        when().delay(timeout).then(function() {
            self.mQ.enqueue(self);
        });
        return this;
    }

    MessageBase.prototype.whenHidden = function() {
        return this.mHidden.promise;
    }

    MessageBase.prototype.whenHiding = function() {
        return this.mHiding.promise;
    }

    MessageBase.prototype.hiding = function() {
        return this.mHiding.promise.inspect().state == "fulfilled";
    }

    MessageBase.prototype.hidden = function() {
        return this.mHidden.promise.inspect().state == "fulfilled";
    }

    MessageBase.prototype.whenShown = function() {
        return this.mShown.promise;
    }

    MessageBase.prototype.hide = function() {
        if (!this.hiding()) {
            this.mHiding.resolve();
            var self = this;
            this.mAnimating = _fadeOut.bind(this.mDiv)();
            when(this.mAnimating)
            .then(function() {
                self.mHidden.resolve();
            });
        }
        return this.whenHidden();
    }





    function BasicMessage(text, options) {
        MessageBase.call(this, text, KBottomMessageQ, options);
        this.mDiv = this.mQ.mDiv;
        this.mPrority   = 2;
        this.mAnimation = _loop.bind(
            this,
            this.mDiv,
            _fadeInNShake.bind(this.mDiv),
            (this.mOptions.loopSec || 10) * 1000
        );
        this.mCss       = [["color", "#ebebeb"]];
    }
    BasicMessage.prototype = new MessageBase();
    BasicMessage.prototype.constructor = BasicMessage;

    MessageBase.prototype.render = function() {
        var self = this;
        if (this.hiding()) {
            return;
        }
        this.mDiv.html(self.mText);
        for (var i = 0; i < this.mCss.length; i++) {
            this.mDiv.css(this.mCss[i][0], this.mCss[i][1]);
        }
        if (this.mAnimation) {
            this.mAnimating = this.mAnimation();
        }
        this.mShown.resolve();
        var timeoutSec = this.mOptions.timeoutSec != undefined ? this.mOptions.timeoutSec : this.mText.length*70;
        if (timeoutSec > 0) {
            setTimeout(function() {
                self.hide();
            }, timeoutSec);
        }
    }

    function WarningMessage(text, options) {
        BasicMessage.call(this, text, options);
        this.mPrority   = 3;
        this.mCss       = [["color", "#ff8000"]];
    }
    WarningMessage.prototype = new BasicMessage();
    WarningMessage.prototype.constructor = WarningMessage;


    function InfoMessage(text, options) {
        BasicMessage.call(this, text, options);
        this.mOptions["preemptMe"] = true;
        this.mPrority   = 1;
        this.mCss       = [["color", "#999999"]];
        this.mAnimation = _fadeIn.bind(this.mDiv);
    }
    InfoMessage.prototype = new BasicMessage();
    InfoMessage.prototype.constructor = InfoMessage;


    function WaitingMessage(text, options) {
        BasicMessage.call(this, text, options);
        this.mOptions.timeoutSec = 0;
        this.mPrority   = 0;
        this.mAnimation = _loop.bind(
            this,
            this.mDiv,
            _fadeInNShake.bind(this.mDiv),
            3000
        );
        this.mCss       = [["color", "#ebebeb"]];
    }
    WaitingMessage.prototype = new BasicMessage();
    WaitingMessage.prototype.constructor = WaitingMessage;


    function ErrorMessage(text, options) {
        BasicMessage.call(this, text, options);
        this.mPrority   = 4;
        this.mCss       = [["color", "#ff8080"]];
        if (this.mOptions.timeoutSec == undefined) {
            this.mOptions.timeoutSec = text.length*120;
        }
    }
    ErrorMessage.prototype = new BasicMessage();
    ErrorMessage.prototype.constructor = ErrorMessage;




    function PopupMessage(text, options) {
        MessageBase.call(this, text, KPopUpMessageQ, options);
        this.mCss       = [["color", "#ebebeb"]];
        this.mAnimation = _fadeIn;
    }
    PopupMessage.prototype = new MessageBase();
    PopupMessage.prototype.constructor = PopupMessage;

    PopupMessage.prototype.render = function(div) {
        this.mDiv = div;
        var self = this;
        if (this.hiding()) {
            return;
        }
        var body = this.mDiv.find(".modlessDialogBody");
        body.html(self.mText);
        for (var i = 0; i < this.mCss.length; i++) {
            this.mDiv.css(this.mCss[i][0], this.mCss[i][1]);
        }
        if (this.mAnimation) {
            this.mAnimating = this.mAnimation.bind(this.mDiv)();
        }
        when(this.mAnimating)
        .then(function(){
            self.mShown.resolve();
        });

        var button = this.mDiv.find(".modelessButton")
        if (this.mOptions.indismissable) {
            button.addClass("hidden");
        } else {
            button = button.find(".btn");
            setTimeout(function() {
                button[0].focus();
            }, 400);
            button.click(function() {
                button[0].blur();
                self.hide();
            });
            button.keypress(function(ev) {
                button[0].blur();
                self.hide();
            })
            if (this.mOptions.closeLabel) {
                button.html(this.mOptions.closeLabel);
            }
        }
        if (this.mOptions.timeoutSec) {
            setTimeout(function() {
                self.hide();
            }, 1000*this.mOptions.timeoutSec);
        }
    }

    PopupMessage.prototype.height = function() {
        if (this.mDiv) {
            return this.mDiv.outerHeight(true/*includeMargin*/) + (this.mOptions.extraHeight || 0);
        }
        return 0;
    }

    PopupMessage.prototype.hide = function() {
        var self = this;
        MessageBase.prototype.hide.call(this);
        this.whenHidden()
        .then(function(){
            !self.mDiv || self.mDiv.remove();
        });
    }

    return {
        message : function(text, options) { return new BasicMessage(text, options); },
        info    : function(text, options) { return new InfoMessage(text, options); },
        warn    : function(text, options) { return new WarningMessage(text, options); },
        wait    : function(text, options) { return new WaitingMessage(text, options); },
        error   : function(text, options) { return new ErrorMessage(text, options); },
        popup   : function(text, options) { return new PopupMessage(text, options); },
        q       : function(type) {
            if (type == "popup") {
                return KPopUpMessageQ;
            } else if (type == "bottom") {
                return KBottomMessageQ;
            }
            return [];
        }
    }

})();

module.exports  = messaging;