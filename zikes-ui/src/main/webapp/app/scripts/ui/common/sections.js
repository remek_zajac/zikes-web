module.exports = {
	Section  		  : require("./waypointsSection.js").Section,
	RubberbandSection : require("./rubberbandSection.js").RubberbandSection,
	WaypointIterator  : require("./sectionBase.js").WaypointIterator
};