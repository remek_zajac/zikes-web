/**
 * scripts/ui/common/contextMenu.js
 *
 * This prepares the map's context menu
 */

'use strict';

const messaging   = require("./messaging.js");
const messages    = require("./messages.js");

const contextMenu = (function() {

    class ContextMenu {
        constructor(map, journey, options, latlng) {
            this.options       = options || {};
            this.options.width = options.width || 20;
            this.options.class = this.options.class || {};
            this.actions       = [];
            this.map           = map;
            this.latlng        = latlng
            this.journey       = journey;
            this.mTitle        = "";
        }

        from(options) {
            var self = this;
            this.action({
                id     : "from",
                display: "Start",
                action : function() {
                    self.journey.pendingRoute().from(self.latlng);
                }
            }, options);
            return this;
        }

        to(options) {
            var self = this;
            this.action({
                id     : "to",
                display: "Finish",
                action : function() {
                    self.journey.pendingRoute().to(self.latlng);
                }
            }, options);
            return this;
        }

        via(options) {
            var self = this;
            this.action({
                id     : "via",
                display: "Route Via",
                action : function() {
                    self.journey.pendingRoute().via(self.latlng);
                }
            }, options);
            return this;
        }

        rubberband(options) {
            var self = this;
            this.action({
                id     : "rubberband",
                display: "Insert Rubberband",
                action : undefined
            }, options);
            return this;
        }

        poi(options) {
            var self = this;
            this.action({
                id     : "poi",
                display: "New Point of Interest",
                action : function() {
                    self.journey.newPOI(self.latlng);
                }
            }, options);
            return this;
        }

        save(options) {
            var self = this;
            this.action({
                id     : "save",
                display: "Save",
                action : undefined
            }, options);
            return this;
        }

        edit(options) {
            var self = this;
            this.action({
                id     : "edit",
                display: "Edit",
                action : undefined
            }, options);
            return this;
        }

        delete(options) {
            var self = this;
            this.action({
                id     : "delete",
                display: "Delete",
                action : undefined
            }, options);
            return this;
        }

        split(options) {
            var self = this;
            this.action({
                id     : "splt",
                display: "Split",
                action : undefined
            }, options);
            return this;
        }

        inspect(options) {
            var self = this;
            this.action({
                id   : "inspect",
                html : '<div id="inspect"><a href="http://www.openstreetmap.org/#map=19/'+self.latlng.lat+'/'+self.latlng.lng+'&layers=D" target="_blank">Inspect in OSM</a></div>',
                separator: true
            });
            return this;
        }

        streetView(options) {
            if ("streetView" in this.map) {
                var self = this;
                var route = options ? options.route : undefined;
                if ( route && this.map.streetView.shown()) {
                    return this;
                }
                this.action({
                    id     : "streetView",
                    display: "Street View",
                    action : function() {
                        self.map.streetView.show(route || self.latlng);
                    }
                }, options);
            }

            return this;
        }

        separator() {
            this.action({
                html : "<hr style=\"margin:2px 0px 12px 0px\">",
                separator: true
            });
            return this;
        }

        title(title) {
            if (title.length > this.options.width) {
                var components = title.split(", ");
                var middleIdx  = components.length/2;
                title = components.slice(0,middleIdx).join(", ")
                    + ",<br>"
                    + components.slice(middleIdx).join(", ");
            }
            var clazz   = this.options.class.title ? ' class="'+this.options.class.title+'"' : "";
            this.mTitle = '<h5'+clazz+'>'+title+'</h5>';
            return this;
        }

        action(template, options) {
            var copiedOptions = {};
            for (var key in options) {
                copiedOptions[key] = options[key];
            }
            for (var key in template) {
                copiedOptions[key] = copiedOptions[key] || template[key];
            }
            for (var i = 0; i < this.actions.length; i++) {
                if (this.actions[i].id && this.actions[i].id == copiedOptions.id) {
                    if (copiedOptions.delete) {
                        this.actions.splice(i,1);
                    } else {
                        this.actions[i] = copiedOptions;
                    }
                    return;
                }
            }
            if (!copiedOptions.delete) {
                this.actions.push(copiedOptions);
            }
            return this;
        }

        html() {
            return this.mTitle + this.actions.map(function(elem,i,arr) {
                if ((i == arr.length-1 || i == 0) && elem.separator == true) {
                    return "";
                }
                var inner = elem.display;
                if (elem.bold) {
                    inner = "<b>"+inner+"</b>";
                }
                return elem.html || '<div id="'+elem.id+'"><a href="#">'+inner+'</a></div>'
            }).join("");
        }

        show() {
            if (this.map.busy() || this.options.disabled) {
                return false;
            }
            if (!(this.map.withinCoverage(this.latlng) || window.location.href.includes("localhost"))) {
                messaging.popup(
                     '<span class="lead warn">Sorry! We don\'t* have coverage here<br></span>'
                    +'<span>'+messages.coverage+'<br></span>'
                    +'<span class="small text-muted">*Unless you clicked really close to the edge of our kingdom</span>', {
                        timeoutSec : 8
                    }
                ).enqueue();
            }

            return true;
        }
    }

    class ContextMenuFactory {
        constructor() {
            this.arguments = Array.from(arguments);
            this.clazz = this.arguments.shift();
            this.options = this.arguments.back();
            this.actions = [];
            if (this.options.constructor) {
                this.options = {};
                this.arguments.push(this.options);
            }
        }

        new(latlng) {
            let result = (
            new (Function.prototype.bind.apply(
                this.clazz, [null].concat(this.arguments).concat([latlng])
                ))
            ).streetView().separator();
            this.actions.forEach((action) => {
                result.action(action);
            });
            result.poi();
            return result.journey.pendingRoute().fillContextMenu(result);
        }

        disable() {
            this.options.disabled = true;
        }

        enable() {
            const self = this;
            setTimeout(() => {
                delete self.options["disabled"];
            }, 500)
        }

        add(action) {
            this.actions.push(action);
            return this;
        }
    }

    return {
        ContextMenu : ContextMenu,
        ContextMenuFactory : ContextMenuFactory
    };

})();

module.exports = contextMenu;