/**
 * scripts/ui/common/journeys.js
 */

'use strict';
var when              = require("when");
when["sequence"]      = require('when/sequence');
const geocode         = require("../../lib/geocode.js")
const zikesBackend    = require("../../lib/zikesBackend.js");
const utils           = require("../../lib/utils.js");
const geometry        = require("../../lib/geometry.js");
const routingPrefs    = require("../routingPreference.js");
const messaging       = require("./messaging.js");

const journeyItem     = require("./journeyItem.js");
const TakenJourneys   = require("./takenJourneys.js").TakenJourneys;


const journeys = (function() {

    class JourneyListBase {

        constructor(metaDivsCb, listDivsCb) {
            this.mMetaDivsCb = metaDivsCb;
            this.mListDivCb  = listDivsCb;
        }

        append(metadata, cb) {
            this.mMetas.push({
                metadata : metadata,
                cb       : cb
            });
        }

        startPopulate() {
            this.mMetas  = [];
            this.mListDivCb().remove();
            this.mMetaDivsCb().children().addClass("hide");
            this.mMetaDivsCb().find(".busy").removeClass("hide");
        }

        finishPopulate(type, eachCb) {
            this.mMetaDivsCb().children().addClass("hide");
            let self = this;
            this.mMetas.sort(function(a,b) {
                return a.metadata.lastModified > b.metadata.lastModified ? -1 : 1
            }).forEach(eachCb.bind(this));
            if (this.mListDivCb().length == 0) {
                this.mMetaDivsCb().find(".empty."+type).removeClass("hide");
            } else {
                this.mMetaDivsCb().find(".nonEmpty."+type).removeClass("hide");
            }
        }

        remove(id) {
            this.mListDivCb("#"+id).remove();
        }
    }


    class JourneyHtmlList extends JourneyListBase {
        constructor(listDiv) {
            super(
                function() { return listDiv; },
                function(sel) { return listDiv.children(sel).not(".busy, .empty, .nonEmpty"); }
            )
            this.mListDiv = listDiv;
        }

        finishPopulate(type) {
            super.finishPopulate(type, function(entry) {
                this.mListDiv.append('<li id="'+entry.metadata.id+'"><a href="#" data-toggle="collapse" data-target=".navbar-collapse">'+entry.metadata.opaque.name+'</a></li>');
                this.mListDiv.find("#"+entry.metadata.id).click(function() {
                    entry.cb("open");
                });
            });
        }
    }


    /*
     * A list of journeys (planned or taken)
     */
    class Journeys {

        constructor(map, list, currentJourney) {
            this.map      = map;
            this.mList    = list;
            this.mJourney = currentJourney;

            const self = this;
            if (utils.urlParams.journey && utils.urlParams.user) {
                when(this.map.loaded).then(function() {
                    return zikesBackend.journeys[utils.urlParams.type].get(utils.urlParams.journey, utils.urlParams.user);
                }).then(function(journeyJSON) {
                    self.mJourney.fromJSON(journeyJSON);
                    self.mJourney.orphan();
                }).otherwise(function(e) {
                    var errorMessage = undefined;
                    if (e.status == 403) {
                        errorMessage = messaging.popup(
                             '<span class="lead">The author of this journey does not authorise you to see it.</span>'
                        )
                    } else if (e.status == 404) {
                        errorMessage = messaging.popup(
                             '<span class="lead">This journey does not exist (might have been deleted).</span>'
                        )
                    } else {
                        errorMessage = messaging.popup(
                             '<span class="lead">An error occurred accessing this journey.<br></span>'
                            +'<span >The server sais: '+JSON.stringify(e)+'</span>'
                        )
                    }
                    errorMessage.enqueue()
                    .whenHidden().then(function() {
                        self.mJourney.hide();
                    });
                });
            }
        }

        show(type) {
            var self = this;
            this.mList.startPopulate();
            return zikesBackend.journeys[type].list()
            .then(function(journeyMetas) {
                return type == "taken" ? new TakenJourneys(self.map).process(journeyMetas) : journeyMetas;
            })
            .then(function(journeyMetas) {
                journeyMetas = journeyMetas.sort(function(a,b) {
                    return (a.lastModified > b.lastModified) ? -1 : 1;
                });
                return journeyMetas.forEach(function(journeyMeta) {
                    self.mList.append(journeyMeta, function(what) {
                        if (what == "open") {
                            zikesBackend.journeys[type].get(journeyMeta.id)
                            .then(function(journeyJSON) {
                                self.mJourney.fromJSON(journeyJSON);
                            });
                        } else if (what == "delete") {
                            zikesBackend.journeys[type].delete(journeyMeta.id)
                            .then(function() {
                                self.mList.remove(journeyMeta.id);
                            });
                        }
                    });
                });
            })
            .ensure(function() {
                self.mList.finishPopulate(type);
            });
        }

        current() {
            return this.mJourney;
        }
    }


    /*
     * A base class for all journeys reflecting only that journeys are
     * consists of routes and pois.
     * No awareness of UI/persistence/change
     */
    class UILessJourney extends utils.Subscribable {

        constructor() {
            super();
            this.mItems     = [];
            this.mMetadata  = zikesBackend.newMeta();
        }

        fromJSON(json) {
            var self = this;
            this.mMetadata = undefined;
            this.mItems.forEach(function(item) {
                item.delete();
            });
            this.mMetadata = json.metadata;
            utils.assert(this.mItems.length == 0);
            return when.sequence(
                json.journey.items.map(function(itemJSON) {
                    return function() {
                        var item = undefined;
                        if ("route" in itemJSON) {
                            item = self.newRoute();
                            itemJSON = itemJSON["route"];
                        } else if ("poi" in itemJSON) {
                            item = self.newPOI();
                            itemJSON = itemJSON["poi"];
                        } else {
                            assert(false, "unknown journey item type: "+ JSON.stringify(itemJSON))
                            return;
                        }
                        self.newItem(item);
                        return item.fromJSON(itemJSON);
                    }
                })
            );
        }

        toJSON() {
            this.mMetadata.opaque.type = this.mMetadata.opaque.type || "planned"; //TODO, backwards compatibility (April, 2017)
            return {
                metadata: this.mMetadata,
                journey: {
                    items: this.mItems.map(function(item) {
                        return item.toJSON();
                    })
                }
            }
        }

        forEachItem(cb, what) {
            this.mapItems(cb, what);
        }

        mapItems(cb, what) {
            var result = [];
            this.mItems.forEach(function(item) {
                if (!what ||
                    item.constructor.name == what ||
                    (typeof what === "function" && item instanceof what)
                    ) {
                    result.push(cb(item));
                }
            });
            return result;
        }

        findItem(cb, what) {
            return this.mItems.find(function(item) {
                if (!what ||
                    item.constructor.name == what ||
                    (typeof what === "function" && item instanceof what)
                    ) {
                    return cb(item);
                }
            });
        }

        newItem(item) {
            this.mItems.push(item);
            return item;
        }

        name() {
            let result = "New Journey";
            if (this.mMetadata.opaque.givenName) {
                result = this.mMetadata.opaque.givenName;
            } else {
                let from, to;
                this.forEachItem(function(item) {
                    if (item.from && item.to) {
                        from = from      || item.from();
                        to   = item.to() || to;
                    }
                });
                if (from && from.geocode && to && to.geocode) {
                    result = geocode.humanReadable([from.geocode, to.geocode]).join(" -> ");
                    if (from.latlng[2] && from.latlng[2].ts) {
                        result = new Date(from.latlng[2].ts).toLocaleDateString() + ": " + result;
                    }
                }
            }

            this.mMetadata.opaque.name = result;
            return result;
        }

        description() {
            return this.mMetadata.opaque.description || "";
        }
    }





    /*
     *
     * A base-class for Journeys common between mobile and desktop
     *
     */
    class Journey extends UILessJourney {

        constructor(map, journeyDiv, RoutingPreference = routingPrefs.RoutingPreference) {
            super();
            this.map         = map;
            this.mJourneyDiv = journeyDiv;
            var self = this;
            when(map.loaded).then(function() {
                self.mRoutingPreferences = new RoutingPreference(
                    map,
                    self.route.bind(self)
                );
                map.geocode.ui($("#geo-search-box"), self.contextmenu.new.bind(self.contextmenu))
                self.loaded && self.loaded();
            });

            this.map.on("contextmenu", function(e) {
                self.contextmenu && self.contextmenu.new(e.latlng).show();
            });
        }

        route(milestones) {
            if (this.mMetadata.opaque.type == "taken") {
                return;
            }
            if (milestones) {
                //this is request from the UI towards the server
                var self = this;
                this.map.busy(true);
                return zikesBackend.requestRoute(
                    milestones,
                    this.mRoutingPreferences.generatePreferences()
                ).ensure(function() {
                    self.map.busy(false);
                });
            } else {
                //this is request towards the UI - to reroute as circumstances changed
                var shownRoute = this.shownRoute();
                if (shownRoute) {
                    shownRoute.route();
                }
            }
        }

        centre() {
            if (this.mItems.length) {
                var bounds = this.mItems.reduce(function(cum, item) {
                    return cum.extend(item.getBounds())
                }, this.mItems[0].getBounds());
                if (bounds.isValid()) {
                    return this.map.fitBounds(
                        bounds,
                        {
                            padding : 25,
                            animate : true
                        }
                    );
                }
            }
            return when();
        }

        show() {
            return this.centre();
        }

        fromJSON(json) {
            const self = this;
            this.invalidate = function() {} //ignore any invalidates flowing during deserialisation
            return super.fromJSON(json)
            .then(function() {
                delete self["invalidate"];
                self.show();
                return self.invalidate({dontSave:true});
            });
        }

        invalidate() {
            var self = this;
            if (this.mPendingRoute && !this.mPendingRoute.isPending()) {
                this.mItems.push(this.mPendingRoute);
                this.mPendingRoute = undefined;
            }

            let distanceMts = 0;
            this.forEachItem(function(route) {
                distanceMts   += route.distanceMts();
            }, "Route");
            this.mJourneyDiv.find("#journeyName").text(this.name());
            this.mJourneyDiv.find("#journeyDistance").text(utils.mtsToString(distanceMts));

            this.forEachItem(function(route) {
                if (route.isPending()) {
                    if (!route.isUnititialised() && (!self.mPendingRoute || self.mPendingRoute.isUnititialised())) {
                        self.deleteItem(route, true);
                        self.mPendingRoute = route;
                    } else {
                        route.delete();
                    }
                }
            }, "Route");
        }

        deleteItem(item, dontInvalidate) {
            if (item === this.mPendingRoute) {
                this.mPendingRoute = undefined;
            } else {
                for (var i = 0; i < this.mItems.length; i++) {
                    if (this.mItems[i] === item) {
                        this.mItems.splice(i,1);
                        break;
                    }
                }
            }
            if (!dontInvalidate) {
                this.invalidate();
            }
        }

        pendingRoute() {
            var shownRoute = this.shownRoute();
            if (shownRoute && shownRoute.isPending()) {
                return shownRoute;
            }
            if (!this.mPendingRoute) {
                this.mPendingRoute = this.newRoute();
            }
            return this.mPendingRoute;
        }

        shownRoute() {
            var onlyOne = this.mItems.length == 1;
            return this.findItem(function(route) {
                return "isShown" in route ? route.isShown() : onlyOne;
            }, "Route");
        }

        save() {
            var self = this;
            if (!this.mSavePosted) {
                this.mSavePosted = when.delay(600)
                .then(function() {
                    delete self["mSavePosted"];
                    return zikesBackend.journeys[self.mMetadata.opaque.type || "planned"].save( //TODO, backwards compatibility (April, 2017)
                        self.toJSON()
                    );
                })
                .then(function(metadata) {
                    self.mMetadata = metadata;
                    self.invalidate({dontSave:true});
                });
            }
            return this.mSavePosted;
        }

        delete() {
            if (this.mMetadata.id) {
                zikesBackend.journeys[this.mMetadata.opaque.type || "planned"].delete( //TODO, backwards compatibility (April, 2017)
                    this.mMetadata.id
                );
            }
            this.hide();
        }

        orphan() {
            this.mMetadata = zikesBackend.newMeta();
        }

        hide() {
            this.orphan();
            let items = this.mItems;
            this.mItems = [];
            items.forEach(function(item) {
                item.delete();
            });
            window.history.replaceState(null, null, window.location.pathname);
            this.invalidate();
        }
    }


    return {
        Journeys        : Journeys,
        Journey         : Journey,
        JourneyHtmlList : JourneyHtmlList,
        JourneyListBase : JourneyListBase
    }

})();

module.exports = journeys;