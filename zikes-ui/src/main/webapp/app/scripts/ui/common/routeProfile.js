/**
 * scripts/ui/routeProfile.js
 *
 * This, given a GeoRouteProfile, can prepare and draw this profile on canvas
 */

'use strict';

var geometry      = require("../../lib/geometry.js");

var routeProfile = (function() {

    const KMinProfileCountourHeightPix = 18;
    const KHwySpectrumHeightPercent    = 7;
    const KXPixMaxStep                 = 6;
    const KXPixMinStep                 = 3;
    var drawnProfile                   = false;

    class PixRouteProfile {

        constructor(tracks, highways, divs) {
            const self              = this;
            this.mTracks            = tracks;
            this.mDivs              = divs;
            this.mFlattenBy         = Math.min(
                1.0*tracks.ele.d/200, 1
            )
            this.mPixWidth          = this.mDivs.profileCanvas.width;
            this.mXFactor           = 1.0*this.mPixWidth/tracks.distanceMts;

            this.mPixHight          = this.mDivs.profileCanvas.height;
            this.mHwySpectrumHeight = Math.round(KHwySpectrumHeightPercent*this.mPixHight/100);
            var raisedYWindow       = (this.mPixHight-this.mFlattenBy*KMinProfileCountourHeightPix);
            this.mPixYAxis          = raisedYWindow/2;
            this.mGeoYAxis          = tracks.ele.mid;
            this.mYFactor           = 1;
            if (tracks.ele.d > 0) {
                this.mYFactor       = raisedYWindow/tracks.ele.d;
            }
            this.mPixProfile = this.mTracks.rollup({
                min : this.mTracks.distanceMts/(this.mPixWidth/KXPixMinStep),
                max : this.mTracks.distanceMts/(this.mPixWidth/KXPixMaxStep)
            }).iterator().forward().map(function(pt) {
                return {
                    x       : self.pixx(pt.frmStrt),
                    y       : self.pixy(pt.d3c.e),
                    highway : highways.highway(pt.d3c.h)
                }
            });
            this.highlight(false);
        }

        pixy(eleMts) {
            var dEle = eleMts - this.mGeoYAxis;
            return Math.round(
                this.mPixYAxis-(dEle*this.mFlattenBy*this.mYFactor)
            );
        }

        pixx(distanceMts) {
            return Math.round(this.mXFactor*distanceMts);
        }


        pixyFromX(x) {
            var left  = this.mPixProfile[0];
            var right = undefined;
            this.mPixProfile.every(function(dp) {
                if (dp.x >= x) {
                    right = dp;
                    return false;
                }
                left = dp;
                return true;
            });
            if (x==left.x || right==undefined) {
                return left.y;
            } else if (x==right.x) {
                return right.y;
            }
            return Math.round(
                geometry.linearApprox(
                    left.y, right.y, right.x-left.x, x-left.x
                )
            );
        }

        geoProfile() {
            return this.mTracks;
        }

        draw() {
            const self = this;
            let canvas = this.mDivs.profileCanvas;
            let c2 = canvas.getContext("2d");
            c2.clearRect(0, 0, canvas.width, canvas.height);
            if (this.mPixProfile == undefined) {
                return;
            }
            c2.fillStyle = "#5bc0de";
            c2.beginPath();
            c2.moveTo(this.mPixProfile[0].x, this.mPixProfile[0].y);
            this.mPixProfile.forEach(function(pt) {
                c2.lineTo(pt.x, pt.y);
            });
            c2.lineTo(canvas.width, canvas.height-this.mHwySpectrumHeight);
            c2.lineTo(0,canvas.height-this.mHwySpectrumHeight);
            c2.closePath();
            c2.fill();

            var prev;
            this.mPixProfile.forEach(function(pt) {
                if (prev) {
                    c2.beginPath();
                    c2.rect(prev.x, canvas.height-self.mHwySpectrumHeight, pt.x-prev.x, canvas.height);
                    c2.fillStyle = prev.highway.color();
                    c2.fill();
                }
                if (pt.highway) {
                    prev = pt;
                }
            });
            return this;
        }

        highlight(milestoneMts, clearedCb, drawCb) {
            var canvas = this.mDivs.highlightCanvas;
            if (!canvas) {
                return;
            }
            var c2 = canvas.getContext("2d");
            var isMilestoneNumber = !isNaN(parseFloat(milestoneMts)) && isFinite(milestoneMts)
            if (!isMilestoneNumber || !this.mPixProfile) {
                drawnProfile = false;
                setTimeout(function() {
                    if (!drawnProfile) {
                        c2.clearRect(0, 0, canvas.width, canvas.height);
                        clearedCb && clearedCb();
                    }
                }, 1000);
            } else {
                drawnProfile = true;
                if (drawCb) {
                    drawCb.call(this, c2, canvas);
                } else {
                    c2.clearRect(0, 0, canvas.width, canvas.height);
                    var bottom           = canvas.height-20;
                    var x                = this.pixx(milestoneMts);
                    var y                = this.pixyFromX(x);
                    c2.beginPath();
                    c2.moveTo(x-1, y);
                    c2.lineTo(x+1, y);
                    c2.lineTo(x+1, bottom-1);
                    c2.lineTo(x-1, bottom-1);
                    c2.fillStyle = "#ebebeb";
                    c2.closePath();
                    c2.fill();
                    var halfCanvasWidth = canvas.width/2;
                    var dEleX = Math.max(
                        Math.min(
                            x-halfCanvasWidth,
                            halfCanvasWidth-22
                        ), 22-halfCanvasWidth
                    ) - 2;
                    if (this.mDivs.dEle) {
                        this.mDivs.dEle.css(
                            "margin-left",
                            dEleX+"px"
                        );
                        this.mDivs.dEle.text(
                            Math.round(
                                this.mTracks.ele.getAt(milestoneMts)
                            ) + "m"
                        );
                    }
                }
            }
        }

        progress(milestoneMts) {
            this.highlight(milestoneMts, undefined, function(c2, canvas) {
                var x = this.pixx(milestoneMts);
                c2.clearRect(0, 0, canvas.width, canvas.height);
                c2.lineWidth = 6;
                c2.beginPath();
                let markerPoint;
                let pixPoint;
                c2.moveTo(this.mPixProfile[0].x, this.mPixProfile[0].y);
                for (var i = 1; i < this.mPixProfile.length; i++ ) {
                    pixPoint = this.mPixProfile[i];
                    pixPoint = {
                        x : pixPoint.x,
                        y : pixPoint.y+4
                    }
                    if (pixPoint.x > x && !markerPoint) {
                        markerPoint = pixPoint = {
                            x : x,
                            y : this.pixyFromX(x)+4
                        }
                        c2.lineTo(pixPoint.x, pixPoint.y);
                        c2.strokeStyle = "#ffffff";
                        c2.stroke();
                        c2.beginPath();
                        c2.moveTo(pixPoint.x, pixPoint.y);
                    } else {
                        c2.lineTo(pixPoint.x, pixPoint.y);
                    }
                }
                c2.lineWidth = 5;
                c2.strokeStyle = "#074869";
                c2.stroke();

                markerPoint = markerPoint || pixPoint;
                c2.beginPath();
                c2.arc(markerPoint.x, markerPoint.y, 7, 0, 2 * Math.PI, false);
                c2.strokeStyle = "#ffffff";
                c2.fillStyle = "#074869";
                c2.fill();
                c2.lineWidth = 4;
                c2.stroke();
            });
        }
    }

    return {
        PixRouteProfile: PixRouteProfile
    }

})();

module.exports = routeProfile;