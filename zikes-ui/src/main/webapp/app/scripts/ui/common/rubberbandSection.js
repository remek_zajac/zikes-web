/**
 * scripts/ui/common/RubberbandSection.js
 *
 * Base functionality of a single way segment that has intermediate waypoints.
 */

const when       = require("when");

const utils      = require("../../lib/utils.js");
const mapquest   = require("../../lib/mapquest.js");
const geometry   = require("../../lib/geometry.js");

const _module = (function() {

    /*
     * Sections that spread a straight (bird flight/rubberband)  line between this and next
     */
    class RubberbandSection extends require("./sectionBase.js").SectionBase {

        constructor(map, params) {
            var endRubberband = params.appendBefore;
            if (endRubberband) {
                !endRubberband.isTemp() || endRubberband.mPrev.append(endRubberband.orphan().on());
                super(map, {
                    icon   : endRubberband.mParent.mIcons.via,
                    latlng : endRubberband.startMarker.getLatLng(),
                    parent : params.parent
                });
                endRubberband.mPrev.append(this);
            } else {
                super(map, params);
            }
        }

        invalidate() {
            const self = this;
            const super_invalidate = super.invalidate.bind(this);
            function ensureElevations(markers) {
                var missing = markers.filter(function(marker) {
                    marker.latlng.d3c = marker.latlng.d3c || {};
                    marker.latlng.frmStrt = marker.latlng.frmStrt || 0;

                    if (!("e" in marker.latlng.d3c)) {
                        if (marker.approxFrom) {
                            if (marker.approxFrom.distanceTo(marker.latlng) < KMaxApproxDistanceMts) {
                                marker.latlng.d3c.e = marker.approxFrom.d3c.e;
                                return false;
                            }
                        }
                        return true;
                    }
                }).map(function(marker) {return marker.latlng; });

                if (missing.length) {
                    var requestLatLngs = missing.filter(function(latlng) {
                        if (!latlng.d3c._e_promise) {
                            latlng.d3c._e_promise = when.defer();
                            return true;
                        }
                        //request already pending
                        return false;
                    });
                    mapquest.elevation(requestLatLngs)
                    .then(function(elevations) {
                        for (var i = 0; i < requestLatLngs.length; i++) {
                            var latlng = requestLatLngs[i];
                            latlng.d3c.e = elevations[i];
                            latlng.d3c._e_promise.resolve();
                            delete latlng.d3c["_e_promise"];
                        }
                    });
                }
                return when.all(
                    missing.map(function(m) { return m.d3c._e_promise.promise; })
                );
            }

            if (this.mNext) {
                return ensureElevations([{
                    latlng: this.startMarker.getLatLng(),
                    approxFrom: this.mPrev && this.mPrev.waypoints() && this.mPrev.waypoints().back(function(dp) {return !!dp.point;}).point
                },{
                    latlng: this.mNext.startMarker.getLatLng(),
                    approxFrom: this.mNext.waypoints() && this.mNext.waypoints().front(function(dp) {return !!dp.point;}).point
                }]).then(function() {
                    self.mTrack = self._newTrack();
                    self.mTrack.append(
                        self.startMarker.getLatLng()
                    );
                    self.mTrack.append(
                        geometry.LatLng.create(self.mNext.startMarker.getLatLng())
                    );
                    self.mTrack.wrap();
                    return super_invalidate();
                });
            }
            return super.invalidate();
        };
    }
    const KMaxApproxDistanceMts = 45;

    return { RubberbandSection : RubberbandSection };

})();

module.exports = _module;