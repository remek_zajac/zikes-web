/**
 * scripts/common/messages.js
 *
 * User Messages
 */

'use strict';

const messages = (function() {
    const coverage = 'We\'re renting a small (32GB) server in Germany and can\'t yet afford serving the whole world. What we don\'t serve we cover with a pinkish overlay.';
    return {
        coverage  : coverage
    }
})();

module.exports  = messages;
