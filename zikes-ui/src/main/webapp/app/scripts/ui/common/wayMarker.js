
var geometry = require("../../lib/geometry.js");

var _module = (function() {

    const KInvisibleIcon = {
        class: "hidden",
        size: [0,0]
    }

    /*
     *
     * Basic functionality of a waymarker (a to/from/via point)
     *
     */
    class WayMarker {

        constructor(map, latlng, params) {
            params.icon = params.icon || KInvisibleIcon;
            this.mLmMarker = new map.sprites.Marker(geometry.LatLng.create(latlng), params);
            if (latlng.length > 2) {
                this.mLmMarker.getLatLng().d3c = latlng[2];
            }
            this.show();
        }

        delete() {
            this.mLmMarker.off();
            this.mLmMarker.delete();
        };

        undelete() {
            this.show();
        };

        hide(options) {
            this.mLmMarker.hide(options);
        };

        show(options) {
            let subscribe = !this.mLmMarker.getIcon();
            this.mLmMarker.show(options);
            if (subscribe && this.mLmMarker.options.draggable) {
                const self = this;
                let prevLatLng;
                this.mLmMarker.on("mousedown", () => {
                    self.beingDragged = true;
                    prevLatLng = self.getLatLng();
                });
                this.mLmMarker.on("mouseup", (event) => {
                    event.originalEvent.preventDefault();
                });
                this.mLmMarker.on("dragend", (event) => {
                    if (self.beingDragged && prevLatLng && self.getLatLng() !== prevLatLng) {
                        self.mPrevLatLng = prevLatLng;
                    }
                    delete self["beingDragged"];
                });
            }
        };

        on() {
            this.mLmMarker.on.apply(this.mLmMarker, arguments);
            return this;
        };

        clearMoved() {
            delete this["mPrevLatLng"];
        };

        setIcon(icon) {
            this.mLmMarker.setIcon(icon || KInvisibleIcon);
        };

        getIcon() {
            return this.mLmMarker.getIcon();
        };

        getLatLng() {
            return this.mLmMarker.getLatLng();
        };

        setLatLng(latlng) {
            this.mPrevLatLng = this.getLatLng();
            this.mLmMarker.setLatLng(latlng);
        };

        pixDistanceFrom(other) {
            var pixa = this.mLmMarker.getIcon().getBoundingClientRect();
            pixa = new geometry.Point(pixa.left, pixa.top);
            var pixb = other.mLmMarker.getIcon().getBoundingClientRect();
            pixb = new geometry.Point(pixb.left, pixb.top);
            return pixa.distanceTo(pixb);
        };

        toJSON() {
            return this.getLatLng().toArray();
        };

        hasMoved() {
            return !!this.mPrevLatLng;
        };


    }


    return {
        WayMarker : WayMarker
    };
})();

module.exports = _module;