/**
 * scripts/ui/common/journeys.js
 */

'use strict';
var when              = require("when");
when["sequence"]      = require('when/sequence');
const mapquest        = require("../../lib/mapquest.js")
const zikesBackend    = require("../../lib/zikesBackend.js");
const utils           = require("../../lib/utils.js");
const geometry        = require("../../lib/geometry.js");
const movement        = require("../../lib/movement.js");

const TakenJourneys = (function() {

    const KMaxElePoints    = 150;
    const KMinEvery        = 5;
    const KJourneyGlue = {
        ms      : utils.mins2ms(24*60), //journeys will not merge if separated by more than this many millis
        mts     : Infinity              //journeys will not merge if separated by more than this many metres
    };

    function isCloseEnough(glue, to, from) {
        return (from[2].ts - to[2].ts) < glue.ms &&
                geometry.LatLng.create(from).distanceTo(geometry.LatLng.create(to)) < glue.mts;
    }

    function decorateTrack(track) { //product of trackModule.Track.toJSON()
        track          = movement.track.create().fromJSON(track);
        let every      = Math.max(KMinEvery, Math.round(track.length / KMaxElePoints));
        let eleLatLngs = [];
        for (var i = 0; i < track.points.length; i++) {
            if ((i % every) == 0 || i == track.points.length-1) {
                track.points[i]._eleIdx = i;
                eleLatLngs.push(track.points[i]);
            }
        }
        return mapquest.elevation(eleLatLngs).then(function(eles) {
            eleLatLngs.forEach(function(pt, idx) {
                track.points[pt._eleIdx].d3c.e = eles[idx];
                delete pt["_eleIdx"];
            });
        }).then(function() {
            return track;
        });
    }


    function updateJourneyMetadata(journey, geocode) {
        let from   = journey.journey.items.front().route.meta.from;
        let to     = journey.journey.items.back().route.meta.to;
        let opaque = {
            name : new Date(from.latlng[2].ts).toLocaleDateString()
                   + ": "+ geocode.humanReadable([from.geocode, to.geocode]).join(" -> "),
            to       : to.latlng,
            from     : from.latlng,
            type     : "taken",
            autosave : true
        }
        if (journey.metadata) {
            journey.metadata.opaque = opaque;
        } else {
            journey.metadata = zikesBackend.newMeta(opaque);
        }
        return journey;
    }


    function mergeJourney(tracks, journey, geocode) {
        return when.all(
            new movement.TimeDistanceTracks(movement.TrackGlue).append(tracks.tracks).wrap().tracks.map(function(track) {
                track    = track.toJSON();
                let from = track.points.front();
                let to   = track.points.back();
                return when.all([
                    geocode.reverse(from),
                    geocode.reverse(to)
                ]).then(function(geocodes) {
                    return {
                        route : {
                            meta : utils.mergeSimpleObjects(track.meta, {
                                from : {
                                    latlng  : from,
                                    geocode : geocodes[0]
                                },
                                to : {
                                    latlng  : to,
                                    geocode : geocodes[1]
                                },
                            }),
                            sections: [{
                                from : from,
                                waypoints : track.points
                            }, {
                                from : to,
                            }]
                        }
                    };
                })
            })
        ).then(function(routes) {
            //construct and save the journey
            journey = journey || {
                journey : {
                    items : []
                }
            };
            routes.forEach(function(route) {
                journey.journey.items.push(route);;
            });
            updateJourneyMetadata(journey, geocode);
            return zikesBackend.journeys.taken.save(journey);
        });
    }

    /*
     * Transforms "rawRecording" journeys into "taken" journeys. As the user navigates with their mobile, the
     * app collects waypoints and timestamps and it does so in a potentially unconnected enviroment. The app
     * therefore cannot work out the waypoints elevations, nor reverse-geocode them. Neither can it work
     * out whether a route or journey are finished (or whether the user will still continue). A "rawJourney"
     * is thus a bare-bone recording that needs to be transformed into a "taken" journey, i.e.: decorated
     * with addresses visited, elevations, calories burnt, etc.
     */
    class TakenJourneys {

        constructor(map) {
            this.map = map;
        }

        process(journeyMetas) {
            const self = this;

            //chronological order based on recorded timestamps
            journeyMetas = journeyMetas.sort(function(a,b) {
                return (a.opaque.from[2].ts < b.opaque.from[2].ts) ? -1 : 1;
            }).map(function(m) { return [m]; }); //and produce a one-elment chain for each entry

            /*
             * Every (initially single-element) chain is a kernel for a potentially larger journey.
             * Journeys can grow when their new routes (or new route segments) appear on the server
             * later. This can happen when the user stops for lunch, the app wraps and uploads the
             * journey to date and then the user continues.
             *
             * In essence then the server may present us with a chain/cascade of "rawRecording" sub-journeys
             * that we should here merge into one.
             *
             * Now it can happen that a some of such "rawRecordings" have managed to morph into a "taken"
             * journey already (because the user invoked this very code to marvel their milage to date),
             * but owing to the fact that they can only be at one place at one time, it can't happen
             * that a single journey had morphed into a number of "taken" journeys. What can happen thus
             * is that a chain can be prefixed with one and only one "taken" journey.
             */
            for (var i = 0; i < journeyMetas.length; i++) {
                let hostChain = journeyMetas[i];
                for (var j = i+1; j < journeyMetas.length;) {
                    let headOfGuestChain = journeyMetas[j].front();
                    if (headOfGuestChain.opaque.type == "rawRecording") {
                        //will only consider rawRecordings for concatenation.
                        let tailOfHostChain = hostChain.back();
                        if (isCloseEnough(KJourneyGlue, tailOfHostChain.opaque.to, headOfGuestChain.opaque.from)) {
                            journeyMetas[i] = hostChain.concat(journeyMetas.splice(j,1)[0]);
                            continue;
                        }
                    }
                    j++;
                }
            }

            /*
             * Given a chain of journeys, this function merges them into one or more (a "rawRecording" may
             * span multiple journeys). The chain's head can potentially be a "taken" journey already, in
             * which case the followups will be merged into it. Otherwise a new "taken" journeys will be created.
             */
            function mergeFollowups(chainOfJourneys) {
                let rawIds2Delete = [];
                return when.all( //we'll extract all trackModule.Tracks for merging, each journey can have a couple
                    chainOfJourneys.reduce(function(tracks, journey, idx) {
                        if (journey.metadata.opaque.type == "rawRecording") {
                            rawIds2Delete.push(journey.metadata.id);
                            journey.metadata.id = undefined;
                            tracks = tracks.concat(
                                journey.journey.raw.map(decorateTrack)
                            );
                        } else {
                            //this is the "taken" head of the group, pop its tail track for potential merging
                            utils.assert(idx == 0, "Got non-head taken journey!");
                            let route    = journey.journey.items.pop().route;
                            utils.assert(route.sections.length == 2, "Unexpected multi-section taken route"); //we never prepare these
                            route.points = route.sections[0].waypoints;
                            tracks.push(
                                movement.track.create().fromJSON(route)
                            );
                        }
                        return tracks;
                    }, [])
                ).then(function(tracks) {
                    //isolate journeys according to the KJourneyGlue
                    return when.all(
                        new movement.TimeDistanceTracks(KJourneyGlue).append(tracks).wrap().tracks.map(function(track, idx) {
                            let into;
                            if (idx == 0) {
                                into = chainOfJourneys.front();
                                into = into.metadata.opaque.type == "rawRecording" ? undefined : into;
                            }
                            return mergeJourney(track, into, self.map.geocode);
                        })
                    );
                }).then(function(journeyMetas) {
                    rawIds2Delete.forEach(function(id) {
                        zikesBackend.journeys.taken.delete(id);
                    });
                    return journeyMetas;
                });
            }

            //we process each group sequentially to create a chronology of server creation timestamps
            //in agreement with the journey chronology;
            return when.sequence(journeyMetas.map(function(chainOfJourneyMetas) {
                if (chainOfJourneyMetas.length == 1 && chainOfJourneyMetas[0].opaque.type != "rawRecording") {
                    //the group has only one element and it needs no processing
                    return function() { return chainOfJourneyMetas[0] };
                }
                return function() {
                    return when.all(chainOfJourneyMetas.map(function(journeyMeta) {
                        //we'll retrieve each journey in the group in full for merging
                        return zikesBackend.journeys.taken.get(journeyMeta.id);
                    })).then(mergeFollowups);
                };
            })).then(utils.flatten);
        }
    }

    return {
        TakenJourneys : TakenJourneys
    }

})();

module.exports = TakenJourneys;