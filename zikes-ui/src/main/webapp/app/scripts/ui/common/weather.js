/**
 * scripts/ui/common/weather.js
 */

'use strict';
const when     = require("when");
const http     = require("../../lib/http.js");
const zikesAPI = require("../../lib/zikesBackend.js");
const utils    = require("../../lib/utils.js");

const weather = (function() {

    const KSampleRadiusMts  = 5000;
    const KSampleIntervalMs = 5*1000;
    const KHour             = 60*60*1000;
    const K3Hours           = 3*KHour;
    const K24Hours          = 24*KHour;
    const KUrl              = "/weather/?lat={{lat}}&lng={{lon}}";
    const KBadWeatherFrom   = 4;
    const KWeatherCodes     = {
        "01d" : {bad:0, img: "sunny"},
        "01n" : {bad:0, img: "sunny"},

        "02d" : {bad:1, img: "part-cloudy"},
        "02n" : {bad:1, img: "part-cloudy"},

        "03d" : {bad:2, img: "cloudy"},
        "03n" : {bad:2, img: "cloudy"},

        "04d" : {bad:3, img: "cloudy"},
        "04n" : {bad:3, img: "cloudy"},

        "09d" : {bad:4, img: "rain-cloud-sun"},
        "09n" : {bad:4, img: "rain-cloud-sun"},

        "10d" : {bad:5, img: "rain"},
        "10n" : {bad:5, img: "rain"},

        "11d" : {bad:6, img: "thunderstorm"},
        "11n" : {bad:6, img: "thunderstorm"},

        "13d" : {bad:7, img: "snow"},
        "13n" : {bad:7, img: "snow"},

        "50d" : {bad:4, img: "cloud"}, //fog
        "50n" : {bad:4, img: "cloud"}
    }

    function isBadWeather(forecast) {
        return KWeatherCodes[forecast.icon || forecast.forecast.icon].bad >= KBadWeatherFrom;
    }

    var AllForecasts = [];
    function closeEnoughMilestone(neededForTs, targetLatLng, distanceFromStart) {
        return AllForecasts.reduce(function(closest, milestone) {
            let distanceFromTarget = targetLatLng.distanceTo(milestone.target);
            if (distanceFromTarget < KSampleRadiusMts && (!closest || closest.distanceFromTarget > distanceFromTarget)) {
                closest = {
                    forecast           : closeEnoughForecast(milestone, neededForTs),
                    base               : milestone,
                    distanceFromTarget : distanceFromTarget,
                    distanceFromStart  : distanceFromStart,
                    target             : targetLatLng,
                    neededForTs        : neededForTs
                }
            }
            return closest;
        }, undefined);
    }

    function closeEnoughForecast(milestone, neededForTs) {
        return milestone.forecasts.reduce(function(closest, timestone) {
            let dt = Math.abs(timestone.timestamp - neededForTs);
            if ((dt < K3Hours) && (!closest || Math.abs(closest.timestamp - neededForTs) > dt)) {
                return timestone;
            }
            return closest;
        }, undefined);
    }


    const KMinRainVolume = 1;
    function timestamp2icon(timestone) {
        let icon = timestone.weather[0].icon;
        let code = parseInt(icon.substr(0,2));
        if (code == 9 || code == 10) {
            //openweathermaps are a bit paranoid, so lets adjust by the rain volume
            let rainVolume = (timestone.rain && timestone.rain["3h"]) || 0;
            if (rainVolume < KMinRainVolume) {
                //so it says rain, but the expected volume is so low, that we'll un-paranoidise it
                let cloudPercent = (timestone.clouds && timestone.clouds.all) || 0;
                if (cloudPercent > 50) {
                    icon = "03d";
                } else if (cloudPercent > 25) {
                    icon = "02d";
                } else {
                    icon = "01d";
                }
            }
        }
        return icon;
    }

    function collectSampleForPoint(point) {
        let url = KUrl.replace("{{lat}}", point.lat).replace("{{lon}}", point.lng);
        return zikesAPI.ifAuthenticated(url, "get").then(function(response) {
            if (response && response.cod == 200 && response.list && response.list.length > 0) {
                let acceptBeforeSec = (new Date().getTime() + K24Hours)/1000;
                return response.list.filter(function(timestone) {
                    return (timestone.dt < acceptBeforeSec) && timestone.weather && timestone.weather.length;
                }).map(function(timestone) {
                    return {
                        timestamp : timestone.dt*1000,
                        icon      : timestamp2icon(timestone)
                    }
                }).sort(function(a,b) {
                    return a.timestamp < b.timestamp ? -1 : 1
                });
            }
        });
    }



    class Weather {

        constructor(route, div=$("#weather")) {
            this.mRoute = route;
            this.mDiv = div;
        }

        show(distanceFromStart, speedMps=utils.kph2Mps(19)) {
            if (!(this.mDiv && this.mRoute)) { return; }
            distanceFromStart = Math.round(distanceFromStart);
            if (this.mMilestoneForecasts) {
                return this.update(distanceFromStart);
            }

            let it                   = this.mRoute.waypointIterator();
            let now                  = new Date().getTime();
            it.next({
                metres              : distanceFromStart + KSampleRadiusMts,
                projectionTolerance : Infinity
            });
            let current              = it.current();
            let cumDistanceMts       = current.frmStrt;
            let stepMts              = Math.max(3*KSampleRadiusMts/2, (this.mRoute.distanceMts() - cumDistanceMts)/4);

            //filter outdated forcasts
            AllForecasts = AllForecasts.filter(function(milestone) {
                let furthest3Hourly = milestone.forecasts[milestone.forecasts.length-1];
                return furthest3Hourly && furthest3Hourly.timestamp > now;
            });
            delete this["mShowMilestone"];
            this.mMilestoneForecasts = [];

            while (current) {
                let neededForTs         = now + 1000*(cumDistanceMts - distanceFromStart)/speedMps;
                let milestoneForecast   = closeEnoughMilestone(
                    neededForTs, current, cumDistanceMts
                );
                if (!milestoneForecast) {
                    milestoneForecast = {
                        target      : current,
                        forecasts   : []
                    };
                    AllForecasts.push(milestoneForecast);
                    milestoneForecast = {
                        base                : milestoneForecast,
                        neededForTs         : neededForTs,
                        distanceFromTarget  : 0,
                        distanceFromStart   : cumDistanceMts,
                        target              : current
                    }
                }
                this.mMilestoneForecasts.push(milestoneForecast);
                it.next({
                    metres              : stepMts,
                    projectionTolerance : Infinity
                });
                current         = it.current();
                cumDistanceMts  = Math.round(
                    current && current.d3c ? current.frmStrt : cumDistanceMts + stepMts
                );
            }
            const self = this;
            const forcastRequests = this.mMilestoneForecasts.filter(function(milestone) {
                let furthest3Hourly = milestone.base.forecasts[milestone.base.forecasts.length-1];
                return (!furthest3Hourly || furthest3Hourly.timestamp < milestone.neededForTs);
            });
            if (forcastRequests.length) {
                when.all(
                    forcastRequests.map(function(milestone) {
                        return collectSampleForPoint(milestone.base.target)
                        .then(function(forecasts) {
                            if (forecasts) {
                                console.log(
                                    "Got forecast for "+milestone.distanceFromStart+"mts from start "+JSON.stringify(milestone.base.target.toArray())+" and "
                                    +new Date(milestone.neededForTs-new Date().getTime()).toLocaleTimeString([], {timeZone:"UTC"}) + " from now"
                                );
                                milestone.base.forecasts = forecasts;
                            }
                            return forecasts;
                        });
                    })
                ).then(function(results) {
                    if (!results || !results.length || !results[0]) {
                        console.log("Didn't get any forecast - user not logged in?");
                    } else {
                        self.update(distanceFromStart);
                    }
                });
            } else {
                this.update(distanceFromStart);
            }
        }

        milestone2Show(distanceFromStart) {
            let remainingMilestones = this.mMilestoneForecasts.map(function(milestone) {
                milestone.forecast = milestone.forecast || closeEnoughForecast(milestone.base, milestone.neededForTs);
                return milestone;
            }).filter(function(milestone) {
                return milestone.forecast;
            })
            remainingMilestones.filter(function(milestone, idx) {
                return milestone.distanceFromStart > distanceFromStart || idx == remainingMilestones.length-1;
            });
            if (remainingMilestones.length) {
                let first = remainingMilestones[0];
                if (remainingMilestones.length == 1) {
                    return first;
                }
                if (isBadWeather(first)) {
                    //if the first is bad weather - lets try to show if the weather consistently improves later
                    let result = first;
                    for (var i = remainingMilestones.length-1; i > 0; i--) {
                        let milestone = remainingMilestones[i];
                        if (isBadWeather(milestone)) {
                            break;
                        }
                        result = milestone;
                    }
                    return result;
                } else {
                    //if the first is good weather - lets try to show if the weather worsens at any point
                    return remainingMilestones.find(function(milestone) {
                        return isBadWeather(milestone);
                    }) || first;
                }
            }
        }

        update(distanceFromStart) {
            const self      = this;
            let now         = new Date().getTime();
            if (!this.mShowMilestone || distanceFromStart-this.mShowMilestone.distanceFromStart > KSampleRadiusMts/2) {
                delete this["mLegend"];
                this.mDiv.empty();
                this.mShowMilestone = this.milestone2Show(distanceFromStart);
                if (this.mShowMilestone) {
                    let showCode = KWeatherCodes[this.mShowMilestone.forecast.icon];
                    let showImg  = showCode.img;
                    this.mDiv.append('<img class="shadow" src="/graphics/weather/'+showImg+'-shadow.svg"/>');
                    this.mDiv.append('<img class="icon" src="/graphics/weather/'+showImg+'.svg"/>');
                    this.mDiv.removeClass("hide");
                    this.mLegend = true;
                }
            }

            if (this.mLegend) {
                this.mDiv.find(".legend").remove();
                if (this.mShowMilestone.distanceFromStart - distanceFromStart > 1.5*KSampleRadiusMts ) {
                    distanceFromStart = distanceFromStart || this.mDistanceFromStartMts;
                    let decorate = {
                        time     : utils.secondsToHrs(
                            Math.max(0,(this.mShowMilestone.neededForTs-new Date().getTime())/1000),
                            {short:true}
                        ),
                        distance : utils.mtsToString(
                            Math.max(0,this.mShowMilestone.distanceFromStart-distanceFromStart)
                        )
                    }
                    this.mDiv.append(
                        '<div class="small legend"> in: '+decorate.time+'<br> & '+decorate.distance+'</div>'
                    );
                }
            }
        }

        hide() {
            if (this.mDiv) {
                this.mDiv.addClass("hide");
                this.mDiv.empty();
            }
            delete this["mShowMilestone"];
            delete this["mMilestoneForecasts"];
        }
    }


    return {
        Weather : Weather
    }

})();

module.exports = weather;
