/**
 * scripts/ui/common/drawnSegment.js
 *
 * This can draw a multi-coloured polyline on the given map
 */

'use strict';

var chroma   = require("chroma-js");

var geometry = require("../../lib/geometry.js");
var utils    = require("../../lib/utils.js");

var drawn = (function() {

    function createGradient(svg,id,stops){
      var svgNS = svg.namespaceURI;
      var grad  = document.createElementNS(svgNS,'radialGradient');
      grad.setAttribute('id',id);
      for (var i=0;i<stops.length;i++){
        var attrs = stops[i];
        var stop = document.createElementNS(svgNS,'stop');
        for (var attr in attrs){
          if (attrs.hasOwnProperty(attr)) stop.setAttribute(attr,attrs[attr]);
        }
        grad.appendChild(stop);
      }

      var defs = svg.querySelector('defs') ||
          svg.insertBefore(document.createElementNS(svgNS,'defs'), svg.firstChild);
      return defs.appendChild(grad);
    }

    function createGradients(thresholds, id) {
        var prevColor = undefined;
        for (var i = 0; i < thresholds.lenght; i++) {
            if (prevColor) {
                createGradient($('svg')[0],""+id+"_"+threshold[0],[
                    {offset:'0%',  'stop-color':prevColor},
                    {offset:'100%','stop-color':threshold[1]}
                ]);
            }
            prevColor = threshold[1];
        }
    }

    function preparePolylines(waypoints, style) {
        var self = this;
        if (!style.schema) {
            return [
                new this.mMap.sprites.Polyline(waypoints, {
                    width: style.width,
                    opacity: style.opacity,
                    smoothFactor: style.smoothFactor,
                    lineCap : style.lineCap,
                    dashArray : style.dashArray,
                    color : style.color,
                    waypointsIdx : {from : 0, to: waypoints.length-1}
                })
            ];
        }
        var result = [];

        function newDatapoint(datapoint, idx) {
            if (datapoint == undefined) {
                return;
            }
            var color = (style.schema.thresholds.find(function(th) {
                return datapoint <= th[0];
            }) || style.schema.thresholds[style.schema.thresholds.length-1])[1];
            var fromIdx = 0;

            if (result.length > 0) {
                var prev = result[result.length-1];
                prev.toIdx = idx;
                if (prev.color == color || idx == waypoints.length-1) {
                    return;
                }
                fromIdx = prev.toIdx;
            }
            result.push({
                fromIdx : fromIdx,
                toIdx   : idx,
                color   : color
            });
        }

        var datapoint = undefined;
        for (var i = 0; i < waypoints.length; i++) {
            datapoint = style.schema.datapoint.reduce(function(cum, e) {
                return cum ? cum[e] : undefined
            }, waypoints[i])
            newDatapoint(datapoint,i);
        }
        if (datapoint == undefined) {
            var previous = result[result.length-1];
            var fromIdx    = previous ? previous.fromIdx : 0;
            var color    = previous ? previous.color : style.color;
            result.push({
                fromIdx : fromIdx,
                toIdx   : waypoints.length-1,
                color   : color
            });
        }
        return result.map(function(polyParams) {
            return new self.mMap.sprites.Polyline(
                waypoints.slice(polyParams.fromIdx, polyParams.toIdx+1), {
                    width        : style.width,
                    opacity      : style.opacity,
                    smoothFactor : style.smoothFactor,
                    lineCap      : style.lineCap,
                    dashArray    : style.dashArray,
                    color        : polyParams.color,
                    waypointsIdx : {from : polyParams.fromIdx, to: polyParams.toIdx}
                }
            );
        });
    }

    /**
    style : {
        width: 6,
        opacity: 0.5,
        lineCap : "butt",
        smoothFactor: 3,
        color : "#0003F0",
        schema: {
            reach      : "spot",
            datapoint  : ["d3c", "e"],
            thresholds : drawnSegment.eleThresholds(
                {ele: profile.ele.min, color: "#00FF00"},
                {ele: profile.ele.max, color: "#FF0000"},
                6
            )
        },
        horizonIcons : {
            from   : fromIcon,
            to     : toIcon
        }
    }
    */

    class Polyline {
        constructor(waypoints, map, style) {
            this.mStyle = style;
            this.mMap = map;
            this.mDirectionMarker = new DirectionMarker(this);
            this.mPolys = preparePolylines.bind(this)(
                waypoints, style
            );
            var self = this;
            this.mResetDirectionMarker = function resetDirectionMarker() {
                self.mDirectionMarker.invalidate();
            };
            this.mClearDirectionMarker = function resetDirectionMarker() {
                self.mDirectionMarker.clear();
            };
        }

        showDirectionMarkers(show) {
            return this.mDirectionMarker.show(show);
        }

        show() {
            var self = this;
            this.mPolys.forEach(function(poly) {
                poly.show();
            });

            this.mMap.on("moveend",   this.mResetDirectionMarker);
            this.mMap.on("viewreset", this.mResetDirectionMarker);
            this.mMap.on("movestart", this.mClearDirectionMarker);
            return this;
        }

        on(eventType, cb) {
            var overridenCb = cb;
            var self = this;
            if (eventType == "mouseover") {
                overridenCb = function(e) {
                    e.nearest.segment.start.index += e.target.options.waypointsIdx.from;
                    e.nearest.segment.end.index   += e.target.options.waypointsIdx.from;
                    e.target = self;
                    cb(e);
                }
            }
            var self = this;
            this.mPolys.forEach(function(poly) {
                poly.on(eventType, overridenCb);
            });
        }

        fireEvent(ev, options) {
            let nearestDistance = Infinity;
            this.mPolys.reduce(function(nearestPoly, poly) {
                let nearestLayerPoint = poly.projectOnto(options.latlng);
                if (nearestLayerPoint && nearestLayerPoint.distanceMts < nearestDistance) {
                    nearestDistance = nearestLayerPoint.distanceMts;
                    return poly;
                }
                return nearestPoly;
            }).fireEvent(ev, options);
        }

        projectOnto(what) {
            if (what) {
                return this.mPolys.reduce(function(bestProjection, poly) {
                    var projection = poly.projectOnto(what);
                    if (projection && (!bestProjection || bestProjection.distanceMts > projection.distanceMts)) {
                        bestProjection = projection;
                        bestProjection.segment.start.index += poly.options.waypointsIdx.from;
                        bestProjection.segment.end.index   += poly.options.waypointsIdx.from;
                    }
                    return bestProjection;
                }, undefined);
            }
        }

        clear() {
            var self = this;
            this.mPolys.forEach(function(poly) {
                poly.delete();
            });
            this.mDirectionMarker.clear();
            this.mMap.off("moveend",   this.mResetDirectionMarker);
            this.mMap.off("viewreset", this.mResetDirectionMarker);
            this.mMap.off("movestart", this.mClearDirectionMarker);
            return this;
        }


        bringToFront() {
            this.mPolys.forEach(function(poly) {
                poly.bringToFront();
            });
            return this;
        }

        getBounds() {
            return this.mPolys.reduce(function(bounds, poly) {
                return bounds.extend(poly.getBounds());
            }, new geometry.LatLngBounds());
        }

        highlight(highlightStyle) {
            this.mPolys.forEach(function(poly) {
                if (highlightStyle) {
                    if (poly.prevStyle) {
                        return;
                    }
                    poly.prevStyle   = utils.cloneSimpleObject(poly.options);
                    var newStyle     = utils.cloneSimpleObject(poly.options);
                    if (highlightStyle.color) {
                        newStyle.color = chroma.mix(newStyle.color, highlightStyle.color, 0.7).hex()
                    }
                    newStyle.width  = highlightStyle.width  || newStyle.width;
                    newStyle.opacity = highlightStyle.opacity || newStyle.opacity;

                    poly.setStyle(newStyle);
                } else if (poly.prevStyle) {
                    poly.setStyle(poly.prevStyle);
                    delete poly["prevStyle"];
                }
            });
        }

        setStyle(style) {
            this.mPolys.forEach(function(poly) {
                utils.mergeSimpleObjects(poly.options, style);
                poly.setStyle(poly.options);
            });
        }
    }






    const KOffsetFromEdgePix = 20;
    class DirectionMarker {
        constructor(forPolyLine) {
            this.mPolyLine  = forPolyLine;
            this.mMap       = this.mPolyLine.mMap;
            this.mMarkers   = [];
        }

        clear() {
            this.mMarkers.forEach(function(marker) { marker.delete(); });
            this.mMarkers = [];
        }

        show(show) {
            if (show != undefined) {
                if (show) {
                    delete this["mHidden"];
                } else {
                    this.mHidden = true;
                }
                this.invalidate();
            }
            return !this.mHidden;
        }

        invalidate() {
            this.clear();
            if (this.mHidden) {
                return;
            }
            var self = this;
            var mapContainerPixBounds = this.mMap.getContainer().getBoundingClientRect();
            var mapBounds = this.mMap.getBounds();
            var lastPolyLatLngs = this.mPolyLine.mPolys[this.mPolyLine.mPolys.length-1].getLatLngs();
            var toFromVisibility   = [
                mapBounds.contains(this.mPolyLine.mPolys[0].getLatLngs()[0]),
                mapBounds.contains(lastPolyLatLngs[lastPolyLatLngs.length-1])
            ];

            function show(vanishingSegment, icon) {
                var closerWest   = Math.abs(vanishingSegment.mStart.lng - mapBounds.getWest()) <
                                   Math.abs(vanishingSegment.mStart.lng - mapBounds.getEast());
                var closerNorth  = Math.abs(vanishingSegment.mStart.lat - mapBounds.getNorth()) <
                                   Math.abs(vanishingSegment.mStart.lat - mapBounds.getSouth());
                var closestEdges = [
                    closerWest ? {
                        edge : new geometry.GeoLine(
                            mapBounds.getNorthWest(),
                            mapBounds.getSouthWest()
                        ), ox : icon.size[0]/2 - KOffsetFromEdgePix}
                        :
                        {edge : new geometry.GeoLine(
                            mapBounds.getNorthEast(),
                            mapBounds.getSouthEast()
                        ), ox : icon.size[0]/2 + KOffsetFromEdgePix },
                    closerNorth ?
                        {edge : new geometry.GeoLine(
                            mapBounds.getNorthWest(),
                            mapBounds.getNorthEast()
                        ), oy : icon.size[1]/2 - KOffsetFromEdgePix }
                        :
                        {edge : new geometry.GeoLine(
                            mapBounds.getSouthWest(),
                            mapBounds.getSouthEast()
                        ), oy : icon.size[1]/2 + KOffsetFromEdgePix}
                ];
                closestEdges.map(function(edge) {
                    var intersection = edge.edge.intersect(vanishingSegment);
                    if (intersection) {
                        self.mMarkers.push(
                            new self.mMap.sprites.Marker(
                                intersection, {
                                    icon      : {
                                        class  : icon.class,
                                        html   : icon.html,
                                        size   : icon.size,
                                        anchor : [
                                            edge.ox || icon.size[0]/2 + (Math.sign(vanishingSegment.mEnd.lng - intersection.lng) * KOffsetFromEdgePix),
                                            edge.oy || icon.size[1]/2 + (Math.sign(intersection.lat - vanishingSegment.mEnd.lat) * KOffsetFromEdgePix)
                                        ]
                                    },
                                    opacity   : 0.4,
                                    clickable : false,
                                    draggable : false
                                }
                            ).show()
                        );
                    }
                })
            }

            if (!toFromVisibility[0] && !toFromVisibility[1]) {
                var firstVisibleSegment = undefined;
                var lastVisibleSegment  = undefined;
                this.mPolyLine.mPolys.forEach(function(poly) {
                    for (var i = 1; i < poly.getLatLngs().length; i++) {
                        if (mapBounds.contains(poly.getLatLngs()[i])) {
                            if (!firstVisibleSegment) {
                                firstVisibleSegment = new geometry.GeoLine(
                                    poly.getLatLngs()[i-1],
                                    poly.getLatLngs()[i]
                                );
                            }
                        } else if (firstVisibleSegment && !lastVisibleSegment) {
                            lastVisibleSegment = new geometry.GeoLine(
                                poly.getLatLngs()[i-1],
                                poly.getLatLngs()[i]
                            ).flip();
                        }
                    };
                });

                firstVisibleSegment && show(firstVisibleSegment, this.mPolyLine.mStyle.horizonIcons.from);
                lastVisibleSegment && show(lastVisibleSegment, this.mPolyLine.mStyle.horizonIcons.to);
            }
        }
    }


    function thresholds42(min, max, steps) {
        var step = (max.value - min.value)/(steps-1);
        var result = [];
        for (var i = 0; i < steps-1; i++) {
            result.push([
                min.value + i*step,
                chroma.mix(min.color, max.color, 1.0*i/steps).hex()
            ]);
        }
        result.push([max.value, max.color]);
        return result;
    }

    return {
        Polyline: Polyline,
        thresholds : function(stops, resultion) {
            var result = [];
            var resultion42 = Math.round(resultion/(stops.length-1))+1;
            for (var i = 1; i < stops.length; i++) {
                if (result.length > 0) {
                    result.pop(); //otherwise they will repeat between stops
                }
                result = result.concat(
                    thresholds42(stops[i-1], stops[i], resultion42)
                );
            }
            return result;
        }
    }

})();

module.exports = drawn;