/**
 * scripts/ui/journeyItem.js
 *
 * Base class for Journey Items
 */

'use strict';

const when          = require("when");
const linkifyHtml   = require("linkifyjs/html");

const journeyItem = (function() {

    var templates = when.defer();
    $.get("templates/journey.template.html", function(template, textStatus, jqXhr) {
        templates.resolve({
            Journey : $(template).filter("#journey_tmpl").html(),
            Route   : $(template).filter("#route_tmpl").html(),
            POI     : $(template).filter("#poi_tmpl").html(),
            colours : $(template).filter("#color_picker_tmpl").html()
        });
    });


    class JourneyItem {

        description() {
            return this.mParams.description || "";
        }

        isDeleted() {
            return this.mDivs == "deleted";
        }

        delete() {
            if (this.mDivs) {
                $( "#"+this.mId).remove();
                this.mDivs = "deleted";
                return true;
            }
        }

        show() {
            var self = this;
            var rendered = true;
            if (!this.mDivs) {
                rendered = this.render();
            }
            return when(rendered)
            .then(function() {
                self.mDivs.collapse.collapse("show");
            });
        }

        isShown() {
            if (this.mDivs) {
                var openPanel = this.mDivs.panel.find(".collapse.in");
                return openPanel.length == 1;
            }
            return false;
        }

        otherItems(cb, itemType) {
            var self = this;
            this.mParent.forEachItem(function(item) {
                if (self.mId != item.mId) {
                    cb(item);
                }
            }, itemType);
        }

        hide() {
            this.mDivs.collapse.collapse("hide");
        }

        doInvalidate() {
            if (!this.mParams.givenName) {
                this.mDivs.name.text(this.name());
            }
        }

        rendered() {
            const self = this;
            let panel  = this.mDivs.panel;
            this.mDivs.collapse     = panel.find(".collapse");
            this.mDivs.delete       = panel.find("#delete");
            this.mDivs.centreIcon   = panel.find("#centreIcon");
            this.mDivs.busyIcon     = panel.find("#busyIcon");

            function closeOthers() {
                //for some reason, we need to programmatically enforce single open panel
                $("#journey .collapse.in").each(function() {
                    var collapsible = $(this);
                    if (collapsible.parent()[0] != panel[0]) {
                        collapsible.collapse("hide");
                    }
                });
            }
            panel.on("show.bs.collapse", closeOthers);
            panel.on("shown.bs.collapse", closeOthers);
            $('[data-toggle="tooltip"]').tooltip();
            this.mDivs.delete.click(function() {
                self.delete();
            });
            this.mDivs.centreIcon.click(function() {
                self.centre();
            });

            /**
             * Handle description field
             */
            this.mDivs.description.focus(function() {
                if (!self.mParams.description) {
                    $(this).text("");
                }
            }).blur(function() {
                if ($(this).html() == "") {
                    $(this).text("description");
                    $(this).addClass("placeHolderText");
                } else {
                    $(this).removeClass("placeHolderText");
                    $(this).html(
                        linkifyHtml($(this).text(), {
                            attributes: {
                                contenteditable: "false"
                            },
                            format: function (value, type) {
                                // value is the link, type is 'url', 'email', etc.
                                if (type === 'url' && value.length > 30) {
                                    value = value.slice(0, 30) + '…';
                                }
                                return value;
                            }
                        })
                    );
                }
                if (self.mParams.description != $(this).html()) {
                    self.mParams.description = $(this).html();
                    self.invalidate();
                }
            });


            /**
             * Handle name field
             */
            this.mDivs.name.blur(function() {
                $(this).html($(this).text()); //remove markup
                if (self.name() != $(this).text()) {
                    //something got editted
                    if ($(this).text() == "") {
                        self.mParams.givenName = undefined;
                    } else {
                        self.mParams.givenName = $(this).text();
                    }
                    self.invalidate();
                }
            }).keypress(function (e) {
              if (e.keyCode == 10 || e.keyCode == 13) {
                e.preventDefault();
                self.mDivs.name.trigger("blur");
              }
            })
            this.show();
        }
    }

    return {
        JourneyItem : JourneyItem,
        templates   : templates.promise
    };

})();

module.exports = journeyItem;