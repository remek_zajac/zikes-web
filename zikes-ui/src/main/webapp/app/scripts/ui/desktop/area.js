/**
 * scripts/ui/desktop/area.js
 *
 * This, edittable (circular for now) area
 */

'use strict';
const when      = require("when");
const baseArea  = require("../../lib/area.js");
const utils     = require("../../lib/utils.js");
const messaging = require("../common/messaging.js");


var area = (function() {

    class Area extends utils.Subscribable {

        constructor(map, geometry) {
            super();
            const self = this;
            this.map   = map;
            this.circle = new this.map.sprites.Circle(
                geometry.centre,
                geometry.radiusMts || 0, {
                    color: "orange"
                }
            )
            this.editMarker = new this.map.sprites.Marker(
                geometry.centre, {
                    icon: {
                        class: "warningIcon",
                        html: '<div class="glyphicon glyphicon-edit"></div>',
                        size: [23, 23]
                    },
                    draggable:true
                }
            ).show()

            this.editMarker.on("dragend", (e) => {
                self.circle.setLatLng(self.editMarker.getLatLng());
                self.fire("startEdit");
            });
            this.editMarker.on("dragstart", () => {
                self.fire("endEdit");
            });
        }

        on(eventType, handler) {
            if (eventType == "startEdit" || eventType == "endEdit") {
                super.on(eventType, handler);
            } else {
                this.editMarker.on(eventType, handler);
            }
            return this;
        }

        show() {
            this.circle.show()
            return this;
        }

        delete() {
            this.circle.delete();
            this.editMarker.delete();
        }

        getLatLng() {
            return this.editMarker.getLatLng();
        }

        edit() {
            this.fire("startEdit");
            function onMouseMove(e) {
                self.circle.setRadiusMts(
                    e.latlng.distanceTo(
                        self.circle.getLatLng()
                    )
                );
            }
            const self = this;
            return when.promise((resolve, reject)=> {
                self.map.on("mousemove", onMouseMove);
                self.map.once("click", () => {
                    self.map.off("mousemove", onMouseMove);
                    self.fire("endEdit");
                    resolve();
                });
            });
        }

        geometry() {
            return {
                centre : this.circle.getLatLng().toArray(),
                radiusMts : this.circle.getRadiusMts()
            }
        }

    }

    class AreaPrefernece extends baseArea.CirclesPreference {
        constructor(map) {
            super();
            this.map = map;
            this.areas = new utils.EventSubscriptionGroup();
        }

        circles() {
            return Array.from(this.areas.group);
        }

        add(geometry) {
            messaging.popup(
                 '<span class="lead">Note that <b>avoid areas</b> will only be saved when created against a named preference profile</span>',
                {once:"avoidareas"}
            ).enqueue();
            if (!geometry.centre) {
                geometry = {
                    centre : geometry
                }
            }
            let result = new Area(this.map, geometry).show();
            this.areas.add(result);
            return result;
        }

        delete(area) {
            this.areas.delete(area);
            area.delete();
        }

        on() {
            this.areas.on(...arguments);
            return this;
        }

        off() {
            this.areas.off(...arguments);
            return this;
        }

    }

    return {
        Area: Area,
        AreaPrefernece, AreaPrefernece
    }

})();

module.exports = area;