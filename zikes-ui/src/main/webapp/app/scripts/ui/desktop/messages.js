/**
 * scripts/ui/messages.js
 *
 * User Messages
 */

'use strict';

const messaging       = require("../common/messaging.js");
const coverageMessage = require("../common/messages.js").coverage;

const messages = (function() {

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        const KMobileUrl = "https://zikes-web-client.appspot.com/mobile/";
        if (window.location.search) {
            window.location.replace(KMobileUrl+window.location.search);
        }
        let message = '<span class="small">This website is for<br></span><span class="lead"><b>planning</b> bicycle journeys.</span><br>'
                    + '<span class="small">And planning needs a bigger screen. For riding the journeys, ';
            if (/Android|Opera Mini/i.test(navigator.userAgent)) {
                message += 'we\'re happy to oblige, <a href="https://play.google.com/store/apps/details?id=net.zikes">we have an app</a>.</span>'
            } else {
                message += 'we have an Android app, but regrettably, its iOS counterpart is still in the works.</span><br>'
                        +  '<span class="small">Perhaps you will have some luck <a href="'+KMobileUrl+'">previewing it</a> in your browser.</span>'
            }


        messaging.popup(message, {indismissable:true}).enqueue();
        $(".navbar").hide();
        $("body").removeClass("room-for-navbar");
        $("#mainWorkArea").css("padding", "0");
        return
    }
    const fastidiousDefinition = '<span>a :  having high and often capricious standards :  difficult to please <i>critics … so fastidious that they can talk only to a small circle of initiates — Granville Hicks</i><br></span>'
                              +'<span>b :  showing or demanding excessive delicacy or care<br></span>'
                              +'<span>c :  reflecting a meticulous, sensitive, or demanding attitude <i>fastidious workmanship</i><br><br></span>'
                              +'<span class=&quot;small text-warning&quot;>Merriam Webster</span>'
    const fastidious = '<a data-html="true" data-container="body" data-toggle="popover" data-trigger="hover" title="\\fa-ˈsti-dē-əs, fə-\\" data-content="'+fastidiousDefinition+'">fastidious</a>'

    const welcomeMessage    = messaging.popup(
         '<span class="lead"><b>Zikes</b> is here so you can be '+fastidious+' about which way you ride your bicycle when you fancy riding it.<br></span>'
        +'<span class="lead"><b><a id="showMeHow" href="#">Show me how</a></b><br></span>'
        +'<span class="small text-muted">'+coverageMessage+'.<br>'
        +'We will use these popups rarely and try communicating with you unintrusivelly via the panel directly below.</span>',
        {once:"welcome"}
    );

    welcomeMessage.whenShown()
    .then(function() {
        $("[data-toggle=popover]").popover();
        $("#showMeHow").click(function() {
            welcomeMessage.hide();
            require("./tutorial.js").basicTutorial();
        });
    })

    $("#showTutorial").click(function() {
        require("./tutorial.js").basicTutorial();
    });

    return {
        welcomeMessage  : welcomeMessage
    }

})();

module.exports  = messages;
