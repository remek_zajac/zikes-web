/**
 * scripts/ui/desktop/map.js
 *
 * This prepares and manages the leaflet map element
 */

'use strict';

const when        = require("when");
when.delay        = require("when/delay");
                    require("../../external/leaflet-singleclick.js");

const zikesApi    = require("../../lib/zikesBackend.js");
const utils       = require("../../lib/utils.js");
const highways    = require("../../lib/highways.js");

const messaging   = require("../common/messaging.js");
const StreetView  = require("../common/streetView.js");
const geoLocator  = require("../common/geoLocator.js");

const sprites     = require("../leaflet/sprites.js");
const messages    = require("./messages.js");

const map = (function() {

    const mapElement = $("#map");
    const mapWrap    = $("#mapWrap")

    const initialCentre = new L.LatLng(49.977722, 14.064453);
    const attribution   = '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors';

    const mapboxToken = "pk.eyJ1IjoicmVtc3RlciIsImEiOiJjanA3M3ZycGgxZzk0M3ZwZXV5aW9lanh1In0.4DScp7g_9SCbMdi67aefhA"
    const mapboxTileLayer = L.tileLayer(
        "https://api.mapbox.com/styles/v1/mapbox/streets-v10/tiles/256/{z}/{x}/{y}?access_token="+mapboxToken, {
        maxZoom: 18,
        attribution: "© <a href='https://www.mapbox.com/about/maps/'>Mapbox</a> © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a> <strong><a href='https://www.mapbox.com/map-feedback/' target='_blank'>Improve this map</a></strong>"
    });
    const openCycleMap    = L.tileLayer(
        "http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png", {
        maxZoom: 18,
        attribution: attribution + ',<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>'
    });
    const googleSatView = L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
    });

    const baseMaps = {
        "Mapbox"       : mapboxTileLayer,
        "OpenCycleMap" : openCycleMap,
        "Google Aerial": googleSatView
    };
    var mainTileLayer   = mapboxTileLayer;
    const map = L.map('map', {
      center: initialCentre,
      zoom: 5,
      layers: [mainTileLayer]
    });
    L.control.layers(baseMaps).addTo(map);
    const connectionMessage = messaging.wait(
        "Contacting backend... It sometimes needs waking up. This normally takes less than 20s."
    );
    const mapOnOff = new utils.OverridenSubsciption(map);

    function mapOnOnce(method, eventtype, cb, context) {
        eventtype = Array.isArray(eventtype) ? eventtype : [eventtype];
        eventtype.map(function(eventtype) {
            if (eventtype == "contextmenu") {
                mapOnOff[method]({
                    eventType: eventtype,
                    cb       : cb,
                    context  : context
                }, { "singleclick" : function(e) {
                        context ? cb.call(context, e) : cb(e)
                    }
                });
            }
            mapOnOff[method]({
                eventType: eventtype,
                cb       : cb,
                context  : context
            }, function(e) {
                    context ? cb.call(context, e) : cb(e)
            });
        });
    }

    const mapAbstraction = {
        expand          : expand,
        shrink          : shrink,
        isShrunk        : isShrunk,
        sprites         : sprites.bindMap(map),
        busy            : busy,
        on              : mapOnOnce.bind(null, "on"),
        once            : mapOnOnce.bind(null, "once"),
        off             : mapOnOff.off.bind(mapOnOff),
        fireEvent       : function(eventtype, options) {
            map.fireEvent(eventtype, options);
        },
        getZoom         : map.getZoom.bind(map),
        getCenter       : map.getCenter.bind(map),
        getContainer    : map.getContainer.bind(map),
        getBounds       : map.getBounds.bind(map),
        fitBounds       : function(bounds, options) {
            if (options && "padding" in options) {
                utils.assert(typeof options.padding === "number", "Padding accepted as a number (of pixels around the bounds)");
                options.padding = [options.padding, options.padding];
            }
            map.fitBounds(bounds, options);
        },
        setView         : function(options) {
            map.setView(
                options.position || map.getCenter(),
                options.zoom     || map.getZoom(),
                options
            );
        },
        project         : map.latLngToContainerPoint.bind(map),
        unproject       : map.containerPointToLatLng.bind(map),
    };
    mapAbstraction.geocode    = require("../mapbox/geocode.js")(mapboxToken, mapAbstraction);
    //mapAbstraction.streetView = new StreetView(mapAbstraction); disabled due to breach of terms - maybe you can add google maps?
    mapAbstraction.geoLocator = new geoLocator.GeoLocatorUI(mapAbstraction);

    mapAbstraction.loaded = zikesApi.meta().then(function(meta) {
        busy(false);
        connectionMessage.hide();
        var coverageOverlay = [[[89, -179],[89, 179],[-89, 179],[-89, -179]]];
        mapAbstraction.highways = new highways.Highways(meta.map.highways);
        coverageOverlay = coverageOverlay.concat(meta.map.coverage);
        var polygon = L.polygon(
            coverageOverlay,
            {
                color: "red",
                weight: 0,
                clickable: false
            }
        ).addTo(map);

        mapAbstraction.withinCoverage = function(latlng) {
            return utils.pips(latlng, meta.map.coverage);
        }

        if (!messages.welcomeMessage.hidden() && Object.keys(utils.urlParams).length == 0) {
            return when.delay(1000)
            .then(function() {
                messages.welcomeMessage.enqueue();
            });
        }
    })
    .otherwise(function() {
        connectionMessage.hide();
        messaging.error(
            "We're sorry, but our routing server appears to be down :( There is nothing we can do for you right now.",
            {timeoutSec:0}
        ).enqueue();
        throw "We're doomed!"
    })
    .then(function() {
        mapAbstraction.geoLocator.on("coarse", (e) => {
            var currentCentre = map.getCenter();
            if (mapAbstraction.withinCoverage(e.position)) {
                if (currentCentre.lat == initialCentre.lat && currentCentre.lon == initialCentre.lon) {
                    map.setView(
                        e.position,
                        e.fine ? 12 : 8,
                        { animate : true }
                    );
                    return when.delay(1200);//let the view settle
                }
            } else {
                messaging.warn(
                    "We're sorry, but it appears, where you are, we don't offer coverage :( We only have a small server."
                ).enqueue();
            }
        });
        require("./mousemovement.js").show(mapAbstraction);
    });

    when.delay(1000).then(() => {
        connectionMessage.enqueue();
        if (!mapAbstraction.highways) {
            busy(true);
        }
    });

    map.on('dblclick', (e) => {
        map.setView(
            e.latlng,
            map.getZoom()+1, {
                animate: true
            }
        );
    });

    function expand() {
        if (mapWrap.hasClass("shrunk")) {
            var done = when.defer();
            mapWrap.removeClass('shrunk').animate({'margin-left':'0px'}, function() {
                map.invalidateSize();
                done.resolve();
            });
            return done.promise;
        }
        return when(true);
    }

    function shrink() {
        if (!mapWrap.hasClass("shrunk")) {
            var done = when.defer();
            mapWrap.addClass('shrunk').animate({'margin-left':'305px'}, function() {
                map.invalidateSize();
                done.resolve();
            });
            return done.promise;
        }
        return when(true);
    }

    function isShrunk() {
        return mapWrap.hasClass("shrunk");
    }

    function busy(busy) {
        if (busy === undefined) {
            return mapElement.hasClass("busy");
        }
        if (busy) {
            if (busy == true) {
                busy = "wait";
            }
            mapElement.css("cursor", busy);
        } else {
            mapElement.css("cursor", "");
        }
    }

    return mapAbstraction;

})();


module.exports   = map;
