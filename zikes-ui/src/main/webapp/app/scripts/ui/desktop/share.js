/**
 * scripts/ui/shareDialog.js
 *
 * This shows a dialog with a url to share and lets user copy the url
 */

'use strict';

var when     = require("when");
var mustache = require("mustache");

var share = (function() {

    var shareDialogTemplate = when.defer();
    $.get("/templates/ui.template.html", function(template, textStatus, jqXhr) {
        var html = $(template).filter("#shareDialogTemplate_tmpl").html()
        shareDialogTemplate.resolve(html);
    });

    function Dialog(url, accessMeta, accessUpdatedCb) {
        this.mUrl = url;
        this.mAccessMeta = accessMeta;
        this.mAccessUpdatedCb = accessUpdatedCb;
    }

    Dialog.prototype.invalidate = function() {
        if (this.mAccessMeta.ro == "") {
            this.mDivs.status.text("This jurney is currently private and nobody but you can see it.");
            this.mDivs.hint.text("By pressing the 'Share' button below you will turn it public and anyone with the above link will be able to view or copy it. Nobody but you will ever be able to modify it.");
            this.mDivs.result.text("The link has been copied to your cliboard, pass it, email it to anyone you like.");
            this.mDivs.actionName.text("Share");
        } else {
            this.mDivs.status.text("This jurney is currently public and anyone with the link can view or copy it.");
            this.mDivs.hint.text("Copy the link (right-click) and pass it to anyone you like or press 'Unshare' to turn this journey private again.");
            this.mDivs.result.text("The journey is now private and nobody but you can see it.");
            this.mDivs.actionName.text("Unshare");
        }
    }

    Dialog.prototype.show = function() {
        var self = this;
        when(shareDialogTemplate.promise).then(function(template) {
            $("body").append(
                mustache.render(template,
                {
                    url: self.mUrl
                })
            );
            var mainDiv = $("#shareDialog");
            self.mDivs = {
                status  : mainDiv.find("#status"),
                hint    : mainDiv.find("#hint"),
                result  : mainDiv.find("#result"),
                url     : mainDiv.find("#url"),
                toggleShare: mainDiv.find("#toggleShare"),
                actionName: mainDiv.find("#actionName"),
                busyIcon: mainDiv.find("#busyIcon"),
                main    : mainDiv
            }
            self.invalidate();
            self.mDivs.toggleShare.click(function() {
                if (self.mAccessMeta.ro == "") {
                    self.mAccessMeta.ro = "public";
                } else {
                    self.mAccessMeta.ro = "";
                }
                self.mDivs.busyIcon.removeClass("hide");
                self.mAccessUpdatedCb(self.mAccessMeta)
                .then(function(newAccessMeta) {
                    self.mAccessMeta = newAccessMeta;
                    self.mDivs.busyIcon.addClass("hide");
                    self.invalidate();
                });
            });
            mainDiv.find(".close").click(function() {
                self.close();
            });
        });
    }

    Dialog.prototype.close = function() {
        if (this.mDivs) {
            this.mDivs.main.remove();
        }
    }


    return {
        Dialog    : Dialog
    };

})();

module.exports = share;