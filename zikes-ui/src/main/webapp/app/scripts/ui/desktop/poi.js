/**
 * scripts/ui/desktop/poi.js
 *
 * This manages the route item in the Journey panel
 */

'use strict';

const when         = require("when");
when["sequence"]   = require('when/sequence');

const utils        = require("../../lib/utils.js");
const unredo       = require("../../lib/unredo.js");
const geometry     = require("../../lib/geometry.js");

const desktopJI    = require("./journeyItem.js");
const BasePOI      = require("../common/journeyItem.js").POI(desktopJI.JourneyItem);


const poi = (function() {

    class POI extends BasePOI {

        constructor(options) {
            super(options);
            var self = this;
            this.latlng(options.latlng);
            this.map.on("zoomend", function() {
                self.resetPopup();
            });
            unredo.push(new unredo.Checkpoint(function() {
                self.delete();
            }));
        }

        doInvalidate() {
            if (!this.mParams.latlng) {
                this.delete();
                return;
            }
            super.doInvalidate();
        }


        fromJSON(json) {
            this.show = function() { //prevent expanding accordion when deserialising
                delete self["show"];
                return when(true);
            }
            return super.fromJSON(json)
        }

        latlng(latlng) {
            var self = this;
            if (latlng) {
                !this.mLmMarker || this.mLmMarker.delete()
                this.mLmMarker = undefined;
                if (latlng instanceof geometry.LatLng) {
                    latlng = [latlng.lat, latlng.lng]
                }
                this.mParams.latlng = latlng;
                return when(self.mParams.geocode || this.map.geocode.reverse(self.mParams.latlng))
                .then((geocoded) => {
                    self.mParams.geocode = geocoded;
                    return self.invalidate();
                });
            }
            return this.mParams.latlng;
        }

        toGpx() {
            var result = '<wpt lat="'+this.mParams.latlng[0]+'" lon="'+this.mParams.latlng[1]+'">\n';
            result += '<name>'+utils.escapeXML(this.name())+'</name>\n';
            result += '<desc>'+utils.escapeXML(this.description())+'</desc>\n';
            result += '</wpt>';
            return result;
        }

        render() {
            var self = this;
            return super.render(
                desktopJI.templates,
                "POI", {
                    parent: "journey"
                }
            ).then(function() {
                if (!self.mLmMarker) {
                    return;
                }
                self.mLmMarker.on("popupopen", function() {
                    delete self.mParams["hidePopup"];
                    self.mLmMarker.getPopup().getCloseButton().click(function() {
                        self.mParams.hidePopup = true;
                    });
                });

                self.mDivs.name.on('blur', function() {
                    self.resetPopup();
                });
                self.mDivs.description.on('blur', function() {
                    self.resetPopup();
                });
                self.mLmMarker.on("dragend", function() {
                    self.mParams.geocode = undefined;
                    self.latlng(self.mLmMarker.getLatLng());
                });
            });
        }

        delete(dontUndo) {
            super.delete();
            if (!dontUndo) {
                var self = this;
                unredo.push(new unredo.Checkpoint(function() {
                    self.mDivs = undefined;
                    self.mLmMarker.show();
                    self.mParent.newItem(self);
                    self.invalidate();
                }));
            }
        }
    }

    return { POI : POI };

})();

module.exports = poi;
