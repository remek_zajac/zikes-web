/**
 * scripts/ui/desktop/area.js
 *
 * This, edittable (circular for now) area
 */

'use strict';
const when      = require("when");
const mapquest  = require("../../lib/mapquest.js");

var mousemovement = (function() {

    const mousepositionLatLng = $("#mouseposition .latlng");
    const mouseElevationBtn = $("#mouseposition .btn");
    const mousepositionElevation = $("#mouseposition .elevation");
    const stabilityCycleMs = 700;
    const autoFetchesNo = 12;

    class EleButton {
        constructor(map) {
            const self = this;
            this.map = map;
            mouseElevationBtn.show();
            mouseElevationBtn.on("click", () => {
                self.fetchesRemaining = autoFetchesNo;
                let lpStable = undefined;
                let lpStableTimes = 0;
                mouseElevationBtn.hide();
                let interval = setInterval(() => {
                    if (self.fetchesRemaining == 0) {
                        clearInterval(interval);
                        return;
                    }
                    if (self.lastMousePosition && lpStable == self.lastMousePosition) {
                        if (lpStableTimes < 4) {
                            mouseElevationBtn.fadeToggle(stabilityCycleMs);
                            lpStableTimes++;
                        }
                    } else {
                        lpStableTimes = 0;
                    }
                    if (lpStableTimes == 4) {
                        lpStableTimes++;
                        mapquest.elevation([lpStable]).then((e) => {
                            let ele = (e[0] >= -500 ? e[0] : "? ")
                            mousepositionElevation.text(ele + "m");
                            mouseElevationBtn.hide();
                            self.fetchesRemaining--;
                        });
                    }
                    lpStable = self.lastMousePosition;
                },
                stabilityCycleMs);
            });
        }

        mousemove(latlng) {
            mousepositionElevation.text("");
            this.lastMousePosition = latlng;
            if (this.fetchesRemaining == 0) {
                mouseElevationBtn.fadeIn();
            }
        }
    }

    return {
        show: function(map) {
            var eleButton = undefined;
            map.on('mousemove', (e) => {
                if (e.latlng) {
                    mousepositionLatLng.text(e.latlng.lat.toFixed(6) + ", " + e.latlng.lng.toFixed(6));
                    eleButton || (eleButton = new EleButton(map));
                } else {
                    mousepositionLatLng.text("");
                }
                eleButton && eleButton.mousemove(e.latlng);
            });
        }
    }

})();

module.exports = mousemovement;