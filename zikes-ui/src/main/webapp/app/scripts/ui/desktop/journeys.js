/**
 * scripts/ui/desktop/journeys.js
 *
 * This manages the journeys panel
 */

'use strict';

const when              = require("when");
when["sequence"]        = require('when/sequence');
const mustache          = require("mustache");
const Sortable          = require("sortablejs");

const fileSaver         = require("../../external/fileSaver.js");

const utils             = require("../../lib/utils.js");
const unredo            = require("../../lib/unredo.js");

const commonJourneys    = require("../common/journeys.js");
const messaging         = require("../common/messaging.js");

const Route             = require("./panelRoute.js").Route;
const POI               = require("./poi.js").POI;
const share             = require("./share.js");
const contextmenu       = require("./contextMenu.js");
const area              = require("./area.js");

const journeyTemplates  = require("./journeyItem.js").templates;

const journeys = (function() {


    class PanelJourneyList extends commonJourneys.JourneyListBase {

        constructor(panel) {
            super(
                function()    { return panel.find("#meta"); },
                function(sel) { return self.mList.children(sel); }
            )
            let self    = this;
            this.mList  = panel.find("#list");
        }

        finishPopulate(kind) {
            const super_finishPopulate = super.finishPopulate.bind(this);
            when(journeyTemplates)
            .then(function(templates) {
                const template = templates.Journey;
                super_finishPopulate(kind, function(entry) {
                    this.mList.append(mustache.render(template, {
                        id: entry.metadata.id,
                        description: entry.metadata.opaque.description || "description",
                        name: entry.metadata.opaque.name
                    }));
                    var journeyDiv = $("#"+entry.metadata.id);

                    journeyDiv.find("#open").click(function() {
                        entry.cb("open");
                    });
                    journeyDiv.find("#delete").click(function() {
                        entry.cb("delete");
                    });
                });
            });
        }
    }




    class Journeys extends commonJourneys.Journeys {

        constructor(map, journeysPanelDiv, journeyPanelDiv) {
            super(
                map,
                new PanelJourneyList(journeysPanelDiv),
                new Journey(map, journeyPanelDiv)
            );
            const self = this;
            this.mJourney.on("hidden", function(e) {
                e.caller == self || (self.mShown ? self.show(self.mShown) : map.expand());
            });
        }

        show(type) {
            var self = this;
            return super.show(this.mShown = type)
            .then(function() {
                return when.all([
                    self.map.shrink(),
                    self.mJourney.hide({
                        caller : self
                    })
                ]);
            });
        }

        hide() {
            delete this["mShown"];
            return this.map.expand();
        }
    }










    var KElevationMessage = undefined;
    class Journey extends commonJourneys.Journey {

        constructor(map, panelDiv) {
            super(map, panelDiv);

            var self = this;
            this.mJourneyDiv   = panelDiv;
            this.mItemsDiv     = panelDiv.find("#journeyItems");
            this.mClosables    = [];
            this.contextmenu   = new contextmenu.ContextMenuFactory(contextmenu.ContextMenu, map, this);

            this.mActions = {
                download : new Button($("#journeyDownload"), function() {
                    self.download();
                }),
                save : new Button($("#journeySave"), function() {
                    self.autoSave();
                }),
                share : new Button($("#journeyShare"), function() {
                    var l = window.location;
                    var port = l.port == 80 ? "" : ":"+l.port;
                    var dialog = new share.Dialog(
                        l.protocol+"//"+l.hostname+port+"?user="+self.mMetadata.userId+"&journey="+self.mMetadata.id+"&type="+(self.mMetadata.opaque.type || "planned"),
                        self.mMetadata.access,
                        function(accessMeta) {
                            self.mMetadata.access = accessMeta;
                            return self.autoSave()
                            .then(function() {
                                return self.mMetadata.access;
                            });
                        }
                    );
                    self.mClosables.push(dialog);
                    dialog.show();
                }),
                close : new Button($("#journeyClose"), function() {
                    self.hide();
                })
            }

            this.mJourneyDiv.find("#journeyName").blur(function() {
                if (self.name() != $(this).text()) {
                    //something got editted
                    if ($(this).text() == "") {
                        self.mMetadata.opaque.givenName = undefined;
                    } else {
                        self.mMetadata.opaque.givenName = $(this).text();
                    }
                    self.invalidate();
                }
            });

            new Sortable(
                this.mItemsDiv[0], {
                    handle: ".draghandle",
                    onUpdate : function(evt) {
                        //we need to save that new order
                        var moved = self.mItems.splice(evt.oldIndex, 1);
                        self.mItems.splice(evt.newIndex, 0, moved[0]);
                        self.invalidate();
                    }
                }
            );
        }

        loaded() {
            const self = this;
            const areaPreference = new area.AreaPrefernece(this.map)
            .on("startEdit", this.contextmenu.disable.bind(this.contextmenu))
            .on("endEdit", () => {
                self.contextmenu.enable();
                self.mRoutingPreferences.invalidate();
            })
            .on("click", (e)=> {
                self.contextmenu.new(e.target.getLatLng())
                .separator().delete({action: () => {
                    areaPreference.delete(e.target);
                    self.mRoutingPreferences.invalidate();
                }}).edit({action: () => {
                    e.target.edit();
                }}).show();
            });
            this.mRoutingPreferences.add(areaPreference);
            this.contextmenu.add({
                id     : "avoid",
                display: "Avoid",
                action : (menu) => {
                    areaPreference.add(menu.latlng).edit();
                }
            })
        }

        invalidate(options = {}) {
            super.invalidate(options);
            const self = this;

            if (!KElevationMessage) {
                let showsElevation = false;
                this.forEachItem(function(route) {
                    showsElevation = showsElevation || route.showsElevation();
                }, Route);
                if (showsElevation) {
                    let routeOrRoutes = this.mItems.length > 1 ? {subject : "Some routes", verb : "are"} : {subject : "The route", verb: "is"};
                    KElevationMessage = messaging.popup(
                         '<span class="lead">'+routeOrRoutes.subject+' that\'s just appeared '+routeOrRoutes.verb+' rugged and thus coloured to highlight the <b>elevation</b>.<br></span>'
                        +'<span class="small text-muted">Which, you should note, is <b>different from climb</b> - we tried showing climb instead, but it didn\'t look informative (too many flips).<br></span>'
                        +'<span class="small">Every route has its individual colour spectrum (unrelated to other routes in the journey).</span>',
                        {once:"elevation"}
                    ).enqueue();
                }
            }

            if (this.mItems.length) {
                if (this.mItems.length == 1) {
                    var theOnlyItem = this.mItems[0]
                    if (theOnlyItem instanceof Route && theOnlyItem.isPending()) {
                        return;
                    } else {
                        theOnlyItem.show();
                    }
                }
                this.show();
                when.sequence([
                    function() {
                        return self.mActions.save.show(
                            !(self.mMetadata.opaque.autosave || self.mMetadata.opaque.type == "taken")
                        );
                    },
                    function() {
                        return self.mActions.share.show(!!self.mMetadata.id);
                    }
                ]);
                if (this.mMetadata.opaque.autosave && !options.dontSave) {
                    this.save();
                }

            } else if (!self.mPendingRoute || self.mPendingRoute.isUnititialised()) {
                self.hide();
                if (this.mMetadata.opaque.autosave && !options.dontSave) {
                    this.delete();
                }
            }
        }

        show(eventData) {
            var journeyDiv = $("#journeyWrapper");
            if (!journeyDiv.hasClass("hidden")) {
                return when(true);
            }
            super.show();
            if (!("mShowing" in this)) {
                journeyDiv.removeClass("hidden");
                var self = this;
                this.mShowing = this.map.shrink()
                .then(function() {
                    var finished = when.defer();
                    journeyDiv.animate({"left" : "0px"}, function() {
                        finished.resolve();
                    });
                    return when(finished.resolve)
                    .then(function() {
                        self.fire("shown", {target : self}, eventData);
                        delete self["mShowing"];
                    });
                });
            }
            return this.mShowing;
        }

        hide(eventData) {
            var super_hide = super.hide.bind(this);
            var journeyDiv = $("#journeyWrapper");
            if (journeyDiv.hasClass("hidden")) {
                return when(true);
            }
            if (!("mHiding" in this)) {
                var self = this;
                var animatedAway = when.defer();
                $("#journeyWrapper").animate({"left" : "-300px"}, function() {
                    journeyDiv.addClass("hidden");
                    animatedAway.resolve();
                });
                this.mHiding = when(animatedAway.promise).then(function() {
                    super_hide();
                    self.fire("hidden", {target : self}, eventData);
                    delete self["mHiding"];
                });
            }
            this.mClosables.map(function(closable) {
                closable.close();
            })
            return this.mHiding;
        }

        newPOI(latlng) {
            var res = new POI({
                map     : this.map,
                parent  : this,
                itemsDiv: this.mItemsDiv,
                latlng  : latlng
            });
            if (latlng) {
                this.newItem(res);
            }
            return res;
        }

        newRoute() {
            return new Route({
                map     : this.map,
                parent  : this,
                itemsDiv: this.mItemsDiv,
                readonly: this.mMetadata.opaque.type == "taken"
            });
        }

        download() {
            if (this.mItems.length) {
                var result = '<?xml version="1.0" encoding="UTF-8"?>\n'
                result    += '<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="zikes.website" version="1.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">\n';
                result    += '<metadata><name>'+utils.escapeXML(this.name())+'</name><desc>'+utils.escapeXML(this.description())+'</desc></metadata>\n';
                //http://www.topografix.com/GPX/1/1/#element_gpx stipulates order and loose wpts should come first.
                //regrettably some devices anoyingly don't work when this order isn't obeyed. Hence, pois first, then routes
                this.forEachItem(function(poi) {
                    result += poi.toGpx();
                }, POI);
                this.forEachItem(function(route) {
                    result += route.toGpx();
                }, Route);
                result += "</gpx>";
                var blob = new Blob([result], {type: "text/xml"});
                fileSaver.saveAs(blob, this.name()+".gpx");
            }
        }


        fromJSON(json) {
            var self = this;
            unredo.reset(); //we're loaded a new document
            return super.fromJSON(json)
            .then(function() {
                if (self.mItems.length == 1) {
                    self.mItems[0].show();
                }
            });
        }

        autoSave() {
            this.mMetadata.opaque.autosave = true;
            return this.save();
        }
    }


    class Button {
        constructor(div, onClickCb) {
            this.mDiv = div;
            if (onClickCb) {
                this.click(onClickCb);
            }
        }

        show(flag) {
            var delayMs = 1000;
            flag = (flag == undefined) ? true : flag;
            var result = when.defer();
            if (flag) {
                this.mDiv.fadeIn(delayMs, function() {
                    result.resolve();
                });
            } else {
                this.mDiv.fadeOut(delayMs, function() {
                    result.resolve();
                });
            }
            return result.promise.delay(delayMs);
        }

        hide() {
            return this.show(false);
        }

        click(cb) {
            return this.mDiv.click(cb);
        }
    }


    return {
        Journeys: Journeys
    }

})();

module.exports = journeys;