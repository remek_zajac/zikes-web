/**
 * scripts/ui/tutorial.js
 *
 * This, given a GeoRouteProfile, can prepare and draw this profile on canvas
 */

'use strict';
const when      = require("when");
when.delay      = require("when/delay");
const utils     = require("../../lib/utils.js");
const mouseSim  = require("../../lib/mouseSim.js");
const messaging = require("../common/messaging.js");
const map       = require("./map.js");

const tutorial = (function() {

    const KeyEnter = 13;
    const KeyDown  = 40;

    function wait(cb, retries, delayMs) {
        var resource = cb();
        if (resource) {
            return when(resource);
        }
        retries = retries || 5;
        if (retries < 0) {
            throw Error("Timeout awaiting resource");
        }
        delayMs = delayMs || 200;
        return when.delay(delayMs).then(function() {
            return wait(cb, retries-1, delayMs*2);
        });
    }

    function $wait(jq) {
        return wait(function() {
            var ele = $(jq);
            if (ele.length > 0) {
                return ele;
            }
        })
    }

    function sendKeys(string, div, retries) {
        function recursive(string, div) {
            if (string.length > 0) {
                if (string.length < 8) {
                    simulateKeyEvent(string.charCodeAt(0), div[0]);
                }
                var newContent = div.val()+string[0];
                div.val(newContent);
                return when.delay(40)
                .then(function() {
                    return recursive(string.slice(1), div)
                });
            }
        }
        div.val("");

        return recursive(string, div).then(function() {
             return wait(function() {
                var pacContainer = $(".suggestions")
                if (pacContainer.css("display") != "none") {
                    return pacContainer
                }
             });
        });
    }


    function geoSearch(query, mousePtr) {
        var searchBox = $("#geo-search-box").find("input");
        searchBox.trigger("focus");

        return mousePtr.move(searchBox)
        .then(function() {
            return sendKeys(query, searchBox);
        })
        .delay(500)
        .then(function() {
            simulateKeyEvent(KeyEnter, searchBox[0]);
        })
        .delay(1000);
    }

    function simulateKeyEvent(code, div) {
        var evt = document.createEvent('Event');
        evt.initEvent('keydown', true, true);
        evt.keyCode = code;
        div.dispatchEvent(evt);
    }

    var KMouseWithHeight = 32;
    function containerToClient(containerPoint) {
        return new L.point(
            containerPoint.x + map.getContainer().getBoundingClientRect().left,
            containerPoint.y + map.getContainer().getBoundingClientRect().top
        );
    }
    var mapCentre = L.point(
        map.getContainer().getBoundingClientRect().left + (map.getContainer().getBoundingClientRect().right - map.getContainer().getBoundingClientRect().left)/2,
        map.getContainer().getBoundingClientRect().top + (map.getContainer().getBoundingClientRect().bottom - map.getContainer().getBoundingClientRect().top)/2
    )

    function BasicTutorial() {
        this.mMouseDiv = '<img id="tutorialMouseCursor" src="graphics/hand_icon.png" style="width:'+KMouseWithHeight+'px;height:'+KMouseWithHeight+'px;position:absolute;left:'+mapCentre.x+'px;top:'+mapCentre.y+'px;z-index:401"/>';
    }

    var KFrom     = "Camden Arts Centre";
    var KTo       = "Gospel Oak Overground";
    var KDragVia  = new L.LatLng(51.5507759412,-0.166999414062);
    var KToLatLng = new L.LatLng(51.5549990907,-0.151065168571);

    BasicTutorial.prototype.run = function() {
        var self = this;

        when.delay(500)
        .then(function() {
            map.busy("none");
            return messaging.popup(
                 "<span class=\"lead\"><b>Zikes</b> is for planning bicycle journeys.<br></span>"
                 + "<span>Journeys have legs and points of interests - say - to remind you where you might want to stop for lunch.</span><br>"
                 + "<span class=\"lead\">Let's plot a basic route, a leg in a journey. It will take less than a minute.</span><br>",
                 { closeLabel : "Continue" }
            ).enqueue()
             .whenHidden()
        })
        .then(function() {
            $("body").append(self.mMouseDiv);
            self.mMousePtr = new mouseSim.SimulatedMouse({
                mouseDiv : $("#tutorialMouseCursor"),
                mouseAnchor : L.point(12,0),
                delaysMs : {
                    arrive: 800,
                    postClick: 500
                }
            });
            return self.mMousePtr.shake(10, 20, 1500);
        })
        .then(function() {
            return geoSearch(KFrom, self.mMousePtr);
        })
        .then(function() {
            return self.mMousePtr.click(
                $wait(".leaflet-popup-content #from a")
            );
        })
        .then(function() {
            return geoSearch(KTo, self.mMousePtr)
        })
        .then(function() {
            return messaging.popup(
                 "<span class=\"lead\">We don't want to actually roll into the station. Just next to it will do.<br></span>",
                 {timeoutSec: 4, indismissable: true}
            ).enqueue()
             .whenShown()
        })
        .delay(500)
        .then(function() {
            var containerPointToClick;
            var oldContextMenu;
            return when($wait(".leaflet-popup-content #from"))
            .delay(500)
            .then(function() {
                oldContextMenu = $(".leaflet-popup-content #from")[0];
                containerPointToClick = map.project(KToLatLng);
                return self.mMousePtr.click(
                    containerToClient(containerPointToClick),
                    {postClick : 0}
                )
            })
            .then(function() {
                return  map.fireEvent("click", {
                  latlng: KToLatLng
                });
            })
            .then(function() {
                return wait(function() {
                    var newContextMenu = $(".leaflet-popup-content #from");
                    if (newContextMenu.length == 1) {
                        newContextMenu = newContextMenu[0];
                        if (oldContextMenu != newContextMenu) {
                            return true
                        }
                    }
                });
            })
            .then(function() {
                return self.mMousePtr.click($wait(".leaflet-popup-content #to a"));
            })
            .delay(300);
        })
        .then(function() {
            return wait(function() {
                if (global.zikes.journeys.current().mItems.length > 0) {
                    self.mRoute = global.zikes.journeys.current().mItems[0];
                    return self.mRoute;
                }
            }).delay(1000)
        })
        .then(function() {
            self.mMousePtr.hide();
            var elevationMessage = messaging.q("popup").find(function(m) {
                return m.mOptions.once == "elevation"
            });
            if (elevationMessage) {
                return elevationMessage.whenHidden()
                .then(function() {
                    return messaging.popup(
                        '<span class="lead">But what if we didn\'t like rugged routes?</span>',
                        {timeoutSec: 3, indismissable: true}
                    ).enqueue();
                });
            } else {
                var KArkwrightSteepLink="https://www.google.co.uk/maps/@51.5508812,-0.18347,3a,75y,10.86h,83.36t/data=!3m6!1e1!3m4!1scVotCYrfvVYoWlRhop2Igw!2e0!7i13312!8i6656";
                return messaging.popup(
                     '<span class="lead">We have the first route.<br></span>'
                     +'<span>For those who know or who just eyed its elevation profile, the route sends us climbing the <a href="'+KArkwrightSteepLink+'" target="_blank">relentlessly steep Arkwright Rd.</a><br></span>'
                     +'<span class="lead">But what if we didn\'t like climbing?</span>',
                     { closeLabel : "Continue" }
                ).enqueue()
                 .whenHidden();
             }
        })
        .delay(1000)
        .then(function() {
            self.mMousePtr.show();
            var prevDistanceMts = self.mRoute.distanceMts();
            return self.mMousePtr.drag(
                $("#toolsAccordion #climb .slider-handle"),
                {vector : L.point(-50,0)}
            ).then(function() {
                return prevDistanceMts;
            })
        })
        .then(function(prevDistanceMts) {
            return wait(function() {
                return self.mRoute.distanceMts() != prevDistanceMts;
            });
        })
        .then(function() {
            self.mRoute.centre();
        })
        .delay(1000)
        .then(function() {
            return messaging.popup(
                 '<span class="lead">What if we preferred going up Belsize Avenue?<br></span>',
                 {timeoutSec: 4, indismissable: true}
            ).enqueue()
        })
        .delay(2000)
        .then(function() { return self.dragPath(KDragVia);})
        .then(function() {
            var finalMessage = messaging.popup(
                 '<span class="lead">Well, that\'s it. As we continue adding to this tutorial, it should get you started.<br></span>'
                 +'<span>Play with the other sliders. Drag the route around. Inspect it, scrutinise it by '
                 +'<a id="streetViewTutorial" href="#">riding it virtually</a>.<br></span>'
                 +'<span>Once satisfied, save it. Name it your way if you like, the headings are all editable. '
                 +'Once saved, fasten your phone (or garmin device) to your bike and <a href="https://sites.google.com/view/zikesmobile/" target=\"_blank\">ride it for real</a>.<br></span>'
                 +'You can run this tutorial again by choosing <b>About->Tutorial</b> from the top navigation bar menu.</span>'
            );
            finalMessage.enqueue()
             .whenShown()
             .then(function() {
                $("#streetViewTutorial").click(function() {
                    when(self.mViaMarker.fireEvent("click", {
                      latlng: self.mViaMarker.getLatLng()
                    }))
                    .delay(500)
                    .then(function() {
                        return self.mMousePtr.click(
                            $wait($(".leaflet-popup-content #streetView a"))
                        );
                    });
                });
            });
            return finalMessage.whenHidden();
        })
        .otherwise(function(e) {
            console.error(e);
            return messaging.popup(
                 '<span class="lead">Ouh bugger. This tutorial has just tumbled.<br></span>'
                 +'<span>..as it normally doesn\'t. It is not a recording, it is a relativelly delicate simulation of user actions, which relies on responsive, external resources and it has not been easy to write.<br></span>'
                 +'<spam>We\'re nonetheless embarassed. You can run it again, if you care, by choosing <b>About->Tutorial</b> from the top navigation bar menu.</span>')
            .enqueue();
        })
        .finally(function() {
            self.mMousePtr.mMouseDiv.remove();
            map.busy(false);
        });
    }

    BasicTutorial.prototype.dragPath = function(dstLatLng) {
        var self = this;
        var section = self.mRoute.mRouteOnMap.mHead;
        var midIdx  = Math.round(section.waypoints().length/2);
        var srcLatlngPoint = section.waypoints()[midIdx];
        var mapRect = $("#map")[0].getBoundingClientRect();
        function containerToClient(containerCoords) {
            return new L.point(
                containerCoords.x + mapRect.left,
                containerCoords.y + mapRect.top
            )
        }

        var dragFrom  = containerToClient(
            map.project(srcLatlngPoint)
        );
        var dragTo    = containerToClient(
            map.project(dstLatLng)
        );
        map.on("layeradd", function (layer) {
            if (layer.layer._icon && layer.layer._icon.classList.contains("toFromIcon")) {
                self.mViaMarker = layer.layer;
            }
        });

        return this.mMousePtr.move(dragFrom)
        .then(function() {
            section.mDrawnSegment.fireEvent("mouseover", {
              latlng: srcLatlngPoint
            });
        })
        .delay(400)
        .then(function() {
            return self.mMousePtr.drag(function(e) {
                self.mViaMarker._icon.dispatchEvent(e);
            }, dragTo);
        })
    }

    return {
        basicTutorial : function() {
            new BasicTutorial().run();
        }
    }

})();

module.exports = tutorial;