/**
 * scripts/ui/desktop/mapRoute.js
 *
 * This draws the route on the map and takes care of the interactions
 * with it.
 */

'use strict';

const when             = require("when");

const utils            = require("../../lib/utils.js");
const unredo           = require("../../lib/unredo.js");

const sections         = require("../common/sections.js");
const messaging        = require("../common/messaging.js");
const commonMapRoute   = require("../common/mapRoute.js");

const route = (function() {

    var MRubberbandMessage = undefined;
    class Route extends commonMapRoute.Route {

        constructor(parent) {
            super(parent)
        }

        highlight(highlight, onlyParent) {
            var self = this;
            function doHighlight() {
                if (!onlyParent) {
                    self.mapSections(function(section) {
                        section.highlight(highlight);
                    });
                }
                if (typeof highlight === 'number' || highlight == false) {
                    self.mParent.highlight(highlight);
                }
            }
            if (highlight == false) {
                clearTimeout(this.mHighlightTimeout)
                delete this["mHighlightTimeout"];
                doHighlight();
            }
            this.mHighlightTimeout = setTimeout(function() {
                doHighlight();
            }, 300);
        };

        delete() {
            var undo = super.delete();
            if (undo) {
                undo = undo.reverse()
                var self = this;
                undo = new unredo.Checkpoint(undo).push(function() {
                    self.invalidate();
                });
            }
            return undo;
        };

        gpxWayPts(wpttype) {
            wpttype = wpttype || "trkpt";
            var result = "";
            this.mapSections(function(section) {
                if (section.waypoints()) {
                    var waypoint;
                    var ele = "";
                    for (var i = 0; i < section.waypoints().length; i++) {
                        waypoint = section.waypoints()[i];
                        if (waypoint.d3c && waypoint.d3c.e != undefined) {
                            ele = '<ele>'+waypoint.d3c.e+'</ele>';
                        }
                        result += '<'+wpttype+' lat="'+waypoint.lat+'" lon="'+waypoint.lng+'">'+ele+'</'+wpttype+'>\n';
                        ele = "";
                    }
                }
            });
            return result;
        }

        contextmenu(e) {
            if (!("contextmenu" in this.mParent)) {
                return;
            }
            var menu = this.mParent.contextmenu(e.latlng);
            menu.streetView({delete:true});
            var self = this;
            if (!e.section.mNext) {
                //last
                menu.from({display: "Continue From"});
            }
            menu.separator();
            if (e.section.mPrev &&
                !(e.section instanceof sections.RubberbandSection) &&
                !(e.section.mPrev instanceof sections.RubberbandSection)) {
                menu.rubberband({
                    action: function() {
                        new sections.RubberbandSection(self.mParent.map, {
                            appendBefore : e.section,
                            icon         : self.mIcons.via,
                            parent       : self
                        });
                        if (!MRubberbandMessage) {
                            MRubberbandMessage = messaging.popup(
                                 '<span class="lead">So you\'ve been curious what those <b>rubberbands</b> were.<br></span>'
                                +'<span class="small">They are a means of inserting arbitrary bird-flight sections into a route. Sections that don\'t follow roads and thus sections Zikes couldn\'t have offered. <br></span>'
                                +'<span class="small text-muted">Perhaps you\'d like to make a <a href="http://zikes.website/?user=TMBx&journey=Zbhk_poeQgumCyGcr5wxNw&type=planned" target="_blank">ferry crossing</a> part of a journey\'s leg. Perhaps you want to cheat on a train a bit or plought your bike through a field. <br>'
                                +'Perhaps there is a newly built ramp that you know of but Zikes does not.<br></span>'
                                +'<span class="lead">Your first rubberband is very short. <b>Stretch it</b> a bit (move that new marker) to see how it behaves.</span>',
                                {once:"rubberband"}
                            ).enqueue();
                        }
                        self.invalidate();
                    }
                });
            }
            if (!e.section.isTemp()) {
                menu.delete({action: function() {
                    unredo.push(
                        new unredo.Checkpoint(
                            e.section.delete()
                        ).push(function() {
                            self.invalidate();
                        })
                    );
                    self.invalidate();
                }});
            }
            menu.inspect();
            if (this.mParent.map.streetView && !this.mParent.map.streetView.shown()) {
                menu = menu.streetView({
                    route: new sections.WaypointIterator(e.section)
                })
            }
            if (e.section.mPrev && e.section.mNext && "new" in self.mParent) {
                menu.split({action: function() {
                    var newHead = e.section;
                    var newHeadWasTemp = newHead.isTemp();
                    !newHead.isTemp() || newHead.mPrev.append(newHead.orphan().on());
                    var newTail = new newHead.constructor(self.mParent.map, {
                        icon     : self.mIcons.to,
                        latlng   : newHead.startMarker.getLatLng(),
                        parent   : self
                    });
                    newHead.mPrev.append(newTail);
                    var newRoute = self.mParent.new();
                    newHead.move(newRoute);
                    self.invalidate();
                    newRoute.invalidate();

                    unredo.push(
                        new unredo.Checkpoint(function() {
                            newTail.delete();
                            newTail.mPrev.append(newHead);
                            if (newHeadWasTemp) {
                                newHead.delete();
                            }
                            self.invalidate();
                        })
                    );
                }});
            }
            menu.show();
        };

        mouseover(e) {
            if (this.mParent.map.streetView) {
                var self = this;
                setTimeout(function() {
                    if (e.section.startMarker.getIcon()) {
                        self.mParent.map.streetView.update(
                            new sections.WaypointIterator(e.section)
                        );
                    }
                }, 300);
            }
        };

        merge(newTail) {
            function doMerge() {
                var oldRoute = newTail.mParent;
                unredo.push(
                    new unredo.Checkpoint(oldTail.delete())
                    .push(function() {
                        self.invalidate();
                        newTail.move(oldRoute);
                        oldRoute.invalidate();
                    })
                );
                oldTail.mPrev.append(newTail);
                self.invalidate();
            }
            var self = this;
            var oldTail = self.mHead.tail();
            var c = document.createElement('div');
            c.innerHTML='<a href="#">Merge</a>';
            var popup = new this.mParent.map.sprites.Popup()
            .setLatLng(oldTail.startMarker.getLatLng())
            .setContent(c)
            .show();
            $(c).find("a").click(function() {
                popup.delete();
                doMerge();
            });
            setTimeout(function() {
                popup.delete({animate:2000});
            }, 1000);
        };

    }



    return {
        Route : Route
    }

})();

module.exports = route;