/**
 * scripts/ui/desktop/contextMenu.js
 *
 * This prepares and shows the map's conext menu.
 */

'use strict';

const baseContextMenu = require("../common/contextMenu.js")

const ContextMenu = (function() {

    class ContextMenu extends baseContextMenu.ContextMenu {
        constructor(map, journey, options, latlng) {
            options.class = {
                title: "text-muted"
            }
            super(map, journey, options, latlng);
        }

        show() {
            const self = this;
            if (super.show()) {
                var c = document.createElement('div');
                c.innerHTML= this.html();
                var popup = new this.map.sprites.Popup()
                .setLatLng(this.latlng)
                .setContent(c)
                .show();

                this.actions.map(function(elem) {
                    $(c).find("#"+elem.id).click(function() {
                        elem.action && elem.action(self);
                        popup.delete();
                    });
                });
            }
            return this;
        }
    }

    return ContextMenu;
})();

module.exports = {
    ContextMenu : ContextMenu,
    ContextMenuFactory : baseContextMenu.ContextMenuFactory
}