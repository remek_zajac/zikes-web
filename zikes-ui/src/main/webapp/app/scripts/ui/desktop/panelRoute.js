/**
 * scripts/ui/desktop/panelRoute.js
 *
 * This manages the route item in the Journey panel
 */

'use strict';

const when          = require("when");
const mustache      = require("mustache");

const utils         = require("../../lib/utils.js");
const unredo        = require("../../lib/unredo.js");

const messaging     = require("../common/messaging.js");
const desktopJI     = require("./journeyItem.js");
const BaseRoute     = require("../common/journeyItem.js").Route(desktopJI.JourneyItem);

const mapRoute      = require("./mapRoute.js");

const panelRoute = (function() {

    const colours = [
      { base : "#0000FF", min: "#000066", max: "#ff0000"},
      { base : "#006699", min: "#002233", max: "#ff0000"},
      { base : "#cc00cc", min: "#330033", max: "#ffff00"},
      { base : "#cc0000", min: "#330000", max: "#ffff00"},
      { base : "#663300", min: "#000000", max: "#ffff00"},
      { base : "#006600", min: "#000000", max: "#ffff00"}
    ]

    function colorTemplateParams() {
        var result = {};
        var keys = Object.keys(colours);
        for (var i = 0; i < keys.length; i++) {
            result["c"+i] = colours[i].base;
        }
        return result;
    }

    function resetDelta(self) {
        if (self.isDeleted()) {
            return;
        }
        self.mDivs.dEle.text(self.dEleContent());
        self.mDivs.dEle.css("margin-left", "0px");
    }

    class Route extends BaseRoute {

        constructor(options) {
            super(options);

            var self = this;
            this.mRouteOnMap = new mapRoute.Route({
                contextmenu      : this.mParent.contextmenu.new.bind(this.mParent.contextmenu),
                map              : this.map,
                style            : self.style.bind(self),
                otherJoureyItems : function(cb) {
                    self.otherItems(function(item) {cb(item.mRouteOnMap);}, Route);
                },
                new              : function(newRouteOnMap) {
                    var newRoute = new Route(options);
                    newRoute.mCentreOnRoute = false;
                    newRoute.mParams.style = utils.cloneSimpleObject(self.mParams.style);
                    self.mParent.newItem(newRoute);
                    return newRoute.mRouteOnMap;
                },
                highlight        : function(milestoneMts) {
                    if (!isNaN(parseFloat(milestoneMts)) && isFinite(milestoneMts)) {
                        self.show();
                    }
                    if (self.mPixProfile) {
                        self.mPixProfile.highlight(
                            milestoneMts,
                            function() {
                                resetDelta(self);
                            }
                        );
                    }
                },
                invalidate       : function() {
                    if (self.isDeleted()) {
                        return;
                    }
                    return self.render().then(function() {
                        self.from(self.mRouteOnMap.from());
                        self.to(self.mRouteOnMap.to());
                        return self.invalidate();
                    })
                },
                route          : options.readonly ? undefined : function(milestones) {
                    var beforeMts = new Date().getTime();
                    self.busy(true);
                    var routed = self.mParent.route(milestones);

                    routed.then(function(route) {
                        var serverRoutingMs  = route.meta.routing_us/1000;
                        var networkLatencyMs = (new Date().getTime() - beforeMts)-serverRoutingMs;
                        var visitedJunctions = route.meta.routingRequests.reduce(function(cum, cur) {return cum+cur.visitedJunctions}, 0);
                        messaging.info(
                            "To plot this route we visited "+utils.largeNumberToStr(visitedJunctions)
                             +" junctions and it took us "+utils.millisToString(serverRoutingMs)+" to do so. It took us additional "
                             +utils.millisToString(networkLatencyMs)+" to send this result accross the wire."
                        ).enqueue();
                    })
                    .catch(function(err) {
                        var message = undefined;
                        if (typeof err.reason === 'object') {
                            if (err.reason.code == "RoutingException::NearestPointNotFound") {
                                message = "Unable to find a road/path within "+err.reason.attemptedToleranceMts+"mts from where you clicked. Wiggle that a bit and position it nearer a way please.";
                                self.map.setView({
                                    position: err.reason.culprit,
                                    zoom    : 13,
                                    animate : true
                                });
                            } else if (err.reason.code == "RoutingException::ServerOverload") {
                                message = "We've analysed " + utils.largeNumberToStr(err.reason.junctionsLimitExceeded) + " junctions for you and had to to bail to protect our small server from swapping RAM. Try plotting a shorter route.";
                            } else if (err.reason.code == "RoutingException::NoRoute" ) {
                                message = "We're sorry, but we could not find a route between the points you requested.";
                            }
                        }
                        messaging.error(
                            message || "We're sorry, there's been an unusual problem. Our server says :'"+err.message+"'"
                        ).enqueue();
                    })
                    .ensure(function() {
                        self.busy(false);
                    }).done();
                    return routed;
                }
            });
        }


        style() {
            return utils.mergeSimpleObjects(
                this.mParams.style,
                { showElevation : true,
                  color : colours[0] },
                true
            );
        }

        fillContextMenu(ctxMenu) {
            if (this.from()) {
                //from set
                ctxMenu.from().to({
                    bold: true
                });
            } else {
                ctxMenu.to().from({
                    bold: true
                });
            }
            return ctxMenu;
        }

        from(latlng) {
            return latlng && this.updateGeocodedLocation(super.from(latlng)) || super.from();
        }

        to(latlng) {
            return latlng && this.updateGeocodedLocation(super.to(latlng)) || super.to();
        }

        updateGeocodedLocation(field) {
            let self = this;
            field && this.map.geocode.reverse(field.latlng)
            .then(function(geocode) {
                field.geocode = geocode;
                self.invalidate();
            });
            return field
        }

        delete() {
            if (super.delete()) {
                var self = this;
                unredo.push(
                    new unredo.Checkpoint(function() {
                        self.mDivs = undefined;
                        self.mParent.newItem(self);
                    }).push(
                        this.mRouteOnMap.delete()
                    )
                );
            }
        }

        doInvalidate() {
            if (!this.mRouteOnMap || this.mRouteOnMap.isUnititialised()) {
                this.delete();
                return;
            }
            super.doInvalidate();
        }


        busy(busy) {
            if (this.mDivs) {
                if (busy) {
                    this.mDivs.centreIcon.addClass("hide");
                    this.mDivs.busyIcon.removeClass("hide")
                } else {
                    this.mDivs.busyIcon.addClass("hide");
                    this.mDivs.centreIcon.removeClass("hide")
                }
            }
        }

        toGpx() {
            var result = '<trk>\n<name>'+utils.escapeXML(this.name())+'</name>\n';
            result += '<trkseg>\n'+ this.mRouteOnMap.gpxWayPts() + '\n</trkseg>\n';
            result += '\n</trk>';
            return result;
        }

        rendered() {
            const self = this;
            return super.rendered()
            .then(function() { return desktopJI.templates; })
            .then(function(templates) {
                self.mDivs["changeColour"] = self.mDivs.panel.find("#changeColor a");
                self.mDivs.panel.hover(
                    function() { self.mRouteOnMap.highlight(true)},
                    function() { self.mRouteOnMap.highlight(false)}
                );
                self.mDivs.description.html(self.mParams.description || "description");

                var popover = self.mDivs.name.popover({
                    trigger: 'manual',
                    html:    true,
                    placement: 'bottom',
                    title:   "route colour",
                    content:  mustache.render(
                        templates.colours,
                        utils.mergeSimpleObjects(
                            colorTemplateParams(), {
                                checked : self.style().showElevation ? "checked" : ""
                            }
                        )
                    )
                });
                self.mDivs.changeColour.click(function(e) {
                    function hide() {
                        popover.popover("hide");
                        $('body').off('click', dismissIfClickedOutside);
                    }
                    function dismissIfClickedOutside(e) {
                        var popoverDiv = self.mDivs.name.parent().find(".popover");
                        if (popoverDiv.length && !$(e.target).parents(".popover").length) {
                            hide();
                        }
                    }
                    $('body').on('click', dismissIfClickedOutside);
                    setTimeout(function() {
                        popover.popover("show");
                        var checkbox = self.mDivs.name.parent().find("input");
                        checkbox.prop('checked', !!self.style().showElevation);
                        self.mDivs.name.parent().find(".colorSwatchItem-clickable").click(function(e) {
                            hide();
                            var color = colours[parseInt($(e.target).attr("id").slice(1))];
                            self.mParams.style = utils.mergeSimpleObjects(self.mParams.style, {color : color});
                            self.mRouteOnMap.invalidate({redraw:true});
                            self.mDivs.changeColour.find(".colorSwatchItem").css("background", color.base);
                        });
                        checkbox.click(function(e) {
                            self.mParams.style = utils.mergeSimpleObjects(
                                self.mParams.style, {showElevation : $(e.target).is(":checked")}
                            );
                            self.mRouteOnMap.invalidate({redraw:true});
                        });
                    }, 0);
                });
            });
        }

        render() {
            return super.render(
                desktopJI.templates,
                "Route", {
                    color: this.style().color.base,
                    parent: "journey"
                }
            );
        }
    }

    return { Route : Route };

})();

module.exports = panelRoute;