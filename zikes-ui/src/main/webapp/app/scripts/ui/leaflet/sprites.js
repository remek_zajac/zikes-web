/**
 * scripts/ui/leaflet/sprites.js
 *
 * Map sprites specific to leaflet stack
 */

'use strict';
const when          = require("when");
const rotatedMarker = require("leaflet-rotatedmarker");
const geometry      = require("../../lib/geometry.js");
const utils         = require("../../lib/utils.js");

var sprites = (function() {

    class Sprite {
        constructor(map, layer) {
            this.mMap     = map;
            this.mLayer   = layer;
            var self = this;
            Object.defineProperty(this, 'options', {
              get: function() { return this.mLayer.options; },
              enumerable: true,
              configurable: false
            });
        }

        hide(options) {
            if ("getDiv" in this) {
                if (this.getDiv()) {
                    if (options && options.animate) {
                        var self = this;
                        var done = when.defer();
                        $(this.getDiv()).animate({opacity: 0}, function() {
                             done.resolve(self);
                        });
                        return done.promise;
                    }
                    $(this.getDiv()).css({opacity: 0});
                }
            } else {
                this.mMap.removeLayer(this.mLayer);
            }
            return this;
        }

        show(options) {
            if ("getDiv" in this && this.getDiv()) {
                if (options && options.animate) {
                    var self = this;
                    var done = when.defer();
                    $(this.getDiv()).animate({opacity: 1}, function() {
                         done.resolve(self);
                    });
                    return done.promise;
                }
                $(this.getDiv()).css({opacity: 1});
            } else {
                this.mMap.addLayer(this.mLayer)
            }
            return this;
        }

        delete(options) {
            if (options) {
                var self = this;
                return when(this.hide(options)).then(function() {
                    self.mMap.removeLayer(self.mLayer);
                });
            }
            this.mMap.removeLayer(this.mLayer);
        }

        getBounds() {
            return this.mLayer.getBounds();
        }

        on() {
            this.mLayer.on.apply(this.mLayer, arguments);
            return this;
        }

        off() {
            this.mLayer.off.apply(this.mLayer, arguments);
            return this;
        }

        bringToFront() {
            if ("bringToFront" in this.mLayer) {
                this.mLayer.bringToFront();
            } else if ("_bringToFront" in this.mLayer) {
                this.mLayer._bringToFront();
            }
        }

        fireEvent(eventType, options) {
            options.containerPoint = options.containerPoint || this.mMap.latLngToContainerPoint(options.latlng);
            options.layerPoint = options.layerPoint || this.mMap.latLngToLayerPoint(options.latlng);
            this.mLayer.fireEvent(eventType, options);
        }
    }

    class Polygon extends Sprite {
        constructor(map, coordinates, style) {
            super(map, new L.Polygon(coordinates, style));
        }
    }

    class Circle extends Sprite {
        constructor(map, coordinates, radiusMts, style) {
            style = style || {};
            style.radius = radiusMts;
            super(map, new L.Circle(coordinates, style));
        }

        setRadiusMts(radius) {
            this.mLayer.setRadius(radius);
        }

        getRadiusMts() {
            return this.mLayer.getRadius();
        }

        setLatLng(latlng) {
            this.mLayer.setLatLng(latlng);
        }

        getLatLng() {
            return this.mLayer.getLatLng();
        }
    }

    class Polyline extends Sprite {

        constructor(map, coordinates, style) {
            style.weight = style.width || style.weight || 4;
            super(map, new L.Polyline(coordinates, style));
            this.mOnOff = new utils.OverridenSubsciption({
                on  : super.on.bind(this),
                off : super.off.bind(this)
            });
        }

        setLatLngs(coordinates) {
            this.mLayer.setLatLngs(coordinates);
        }

        getLatLngs() {
            return this.mLayer.getLatLngs();
        }

        setStyle(style) {
            style.weight = style.width || style.weight || 4;
            this.mLayer.setStyle(style);
        }

        projectOnto(what) {
            return new geometry.GeoLineString(this.getLatLngs()).projectOnto(what, true);
        }

        on(eventType, cb, context) {
            var self = this;
            if (eventType == "mouseenter" || eventType == "mouseover") {
                this.mOnOff.on({
                    eventType : eventType,
                    cb        : cb,
                    context   : context
                }, function(e) {
                    e.nearest = self.projectOnto(e.latlng);
                    e.target = self;
                    context ? cb.call(context, e) : cb(e);
                });
                return this;
            }
            return super.on(eventType, cb);
        }

        off(eventType, cb, context) {
            this.mOnOff.off({
                eventType : eventType,
                cb        : cb,
                context   : context
            });
            return this;
        }
    }




    function _toLeafletIconStyle(iconStyle) {
        var result;
        if (iconStyle.url) {
            result =  L.icon({
                iconUrl     : iconStyle.url,
                iconSize    : iconStyle.size,
                iconAnchor  : iconStyle.anchor,
                popupAnchor : iconStyle.popupAnchor
            });
        } else if (iconStyle.class) {
            var params = {
                className   : iconStyle.class,
                iconSize    : iconStyle.size,
                iconAnchor  : iconStyle.anchor,
                popupAnchor : iconStyle.popupAnchor
            }
            if (iconStyle.html) {
                params.html = iconStyle.html;
            }
            result = L.divIcon(params);
        } else {
            utils.assert(false, "Unable to construct an icon style from: ");
            console.dir(iconStyle);
        }
        return result;
    }

    class Marker extends Sprite {

        constructor(map, coordinates, style) {
            super(map, L.marker(coordinates, {
                opacity : style.opacity,
                draggable: style.draggable,
                clickable: "clickable" in style ? style.clickable : true,
                icon : _toLeafletIconStyle(style.icon),
                rotationAngle: style.rotationAngle || 0
            }));
            this.mCoordinates = coordinates;
            this.mOnOff = new utils.OverridenSubsciption({
                on  : super.on.bind(this),
                off : super.off.bind(this)
            });
        }

        setIcon(icon) {
            this.mLayer.setIcon(
                _toLeafletIconStyle(icon)
            );
        }


        getIcon() {
            return this.mLayer._icon;
        }

        getDiv() {
            return this.getIcon();
        }

        show(options) {
            var res = super.show(options);
            if (!this.options.clickable) {
                var self = this;
                when(res).then(function() {
                     $(self.getIcon()).removeClass("leaflet-interactive");
                });
            }
            return res;
        }

        getLatLng() {
            return this.mLayer.getLatLng();
        }

        setLatLng(latlng) {
            this.mLayer.setLatLng(latlng);
        }

        on(eventType, cb, context) {
            this.mOnOff.on({
                eventType : eventType,
                cb        : cb,
                context   : context
            }, function(e) {
                setTimeout(function() { //workaround for https://github.com/Leaflet/Leaflet/issues/5067
                    context ? cb.call(context, e) : cb(e);
                }, 0);
            });
            return this;
        }

        off(eventType, cb, context) {
            this.mOnOff.off({
                eventType : eventType,
                cb        : cb,
                context   : context
            });
            return this;
        }

        bindPopup(popup) {
            this.mPopup = popup;
            this.mLayer.bindPopup(popup.mLayer);
        }

        getPopup() {
            return this.mPopup;
        }

        openPopup() {
            this.mLayer.openPopup();
            return this;
        }

        closePopup() {
            this.mLayer.closePopup();
            return this;
        }
    }

    class Popup extends Sprite {
        constructor(map, options) {
            super(map, new L.Popup(options));
            if (options && ("opacity" in options || "backgroundColor" in options)) {
                var self = this;
                this.mLayer.on("add", function() {
                    var popupDiv = $(self.mLayer._wrapper);
                    if ("opacity" in options) {
                        popupDiv.parent().css("opacity", options.opacity);
                    }
                    if ("backgroundColor" in options) {
                        popupDiv.css("background-color", options.backgroundColor);
                        $(self.mLayer._tip).css("background-color", options.backgroundColor);
                    }
                });
            }
        }

        setContent() {
            this.mLayer.setContent.apply(this.mLayer, arguments);
            return this;
        }

        setLatLng() {
            this.mLayer.setLatLng.apply(this.mLayer, arguments);
            return this;
        }

        show() {
            this.mLayer.openOn(this.mMap);
            return this;
        }

        getDiv() {
            return this.mLayer._wrapper.parentNode;
        }

        getCloseButton() {
            return $(this.mLayer._closeButton);
        }
    }

    return {
        Polygon  : Polygon,
        Polyline : Polyline,
        Marker   : Marker,
        Popup    : Popup,
        Circle   : Circle,
        bindMap  : function(map) {
            return {
                Polygon  : Polygon.bind(null, map),
                Marker   : Marker.bind(null, map),
                Polyline : Polyline.bind(null, map),
                Popup    : Popup.bind(null, map),
                Circle   : Circle.bind(null, map)
            }
        }
    }

})();

module.exports = sprites;