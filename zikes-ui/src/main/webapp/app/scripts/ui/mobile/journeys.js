/**
 * scripts/ui/mobile/journeys.js
 */

'use strict';

const when           = require("when");
const commonJourneys = require("../common/journeys.js");
const messaging      = require("../common/messaging.js");

const Windrose       = require("./windrose.js").Windrose;
const navigated      = require("./navigated.js");
const contextmenu    = require("./contextMenu.js");
const offline        = require("./offline.js");

const journeys = (function() {

    class Journeys extends commonJourneys.Journeys {
        constructor(map, list, routePanel, contextMenuPopupPanel) {
            super(map, list, new Journey(map, routePanel, contextMenuPopupPanel));
        }
    }

    class RoutingPreferences extends require("../routingPreference.js").RoutingPreference {

        constructor(map, updateCb) {
            super(map, updateCb);
            var self = this;
            this.mPanel = $("#toolsPanel");
            this.mOpenButton = this.mPanel.find("#open");
            this.mOpenButton.animate({"opacity": 1}, 800);
            this.mOpenButton.click(this.show.bind(this));
        }

        hide() {
            if (this.shown()) {
                this.mPanel.animate({"margin-top": "0px"}, 800);
                this.mOpenButton.animate({"opacity": 1}, 800);
            }
        }

        show() {
            if (!this.shown()) {
                this.mPanel.animate({"margin-top": -this.mPanel.innerHeight()+"px"}, 800);
                this.mOpenButton.animate({"opacity": 0}, 800);
            }
        }

        shown() {
            return this.mPanel.css("margin-top") != "0px";
        }

        render(panel) {
            var self = this;
            super.render(panel)
            .then(function() {
                self.mPreferences.forEach(function(slider) {
                    slider.expand();
                });
            });
        }
    }


    const KMessageEnqueueTimeoutMs = 1500;
    var   MSimulateJourneyMessage;
    var   MWindroseMessage;
    class Journey extends commonJourneys.Journey {

        constructor(map, divs) {
            super(map, divs.journey, RoutingPreferences);
            var self = this;
            this.mWindrose           = new Windrose(map);
            this.mDivs               = divs;
            this.mDivs.journey       = {
                panel      : this.mDivs.journey,
                header     : divs.journey.find("#journeyHeader"),
                items      : divs.journey.find("#journeyItems")
            }
            this.mDivs.journey.minMargin = 4+self.mDivs.journey.items.innerHeight()-self.mDivs.journey.panel.innerHeight();
            this.mSimulateNavItem        = $("#simulateNavItem");
            this.mClearMap               = $("#clearMap");
            this.contextmenu             = new contextmenu.ContextMenuFactory(contextmenu.ContextMenu, map, this.mDivs.contextMenu, this);

            offline.enabled().then(function(enabled) {
                if (enabled) {
                    self.mOfflinePrepare    = $("#offlinePrepare");
                    self.mOfflinePrepare.click(function() {
                        self.minimise();
                        var iterators = self.mapItems(function(route) {
                            return function() { return route.mRouteOnMap.waypointIterator();}
                        }, navigated.Route);

                        new offline.TileCache(self.map).prepare(
                            iterators,
                            [13, 16]
                        );
                    });
                }
            });
            this.mSimulateNavItem.click(function() {
                var simulateRoute = self.shownRoute();
                if (simulateRoute) {
                    self.minimise();
                    self.map.geoLocator.simulate(
                        simulateRoute.mRouteOnMap.waypointIterator().dump({
                            metres: 40,
                            projectionTolerance : 0
                        })
                    );
                    !MSimulateJourneyMessage || MSimulateJourneyMessage.hide();
                    if (!MWindroseMessage) {
                        MWindroseMessage = messaging.popup(
                             '<span class="lead">Tap the compass (top right).</span><br>'
                            +'<span class="small">(a few times) to enter or exit the navigation mode.</span>',
                            {once:"windrose"}
                        ).enqueue(KMessageEnqueueTimeoutMs);
                        self.mWindrose.on("click", function() {
                            MWindroseMessage.hide();
                        });
                    }
                }
            });
            this.mClearMap.click(function() {
                self.hide();
            });
            this.mDivs.journey.header.click(function() {
                self.mDivs.journey.panel.animate({"margin-top":-self.mDivs.journey.panel.innerHeight()+"px"}, function() {
                    if (self.mItems.length > 1 && self.mDivs.journey.items.scrollLeft() == 0) {
                        self.mDivs.journey.items.animate({scrollLeft:100}, function() {
                            self.mDivs.journey.items.animate({scrollLeft:0})
                        });
                    }
                });
            });
            let scrollTimer;
            this.mDivs.journey.items.scroll(function() {
                clearTimeout(scrollTimer);
                scrollTimer = setTimeout(function() {
                    let childrenNo  = self.mDivs.journey.items.children().length;
                    if (childrenNo) {
                        let stepWidth   = window.innerWidth-7; //TODO - magic fudge!
                        let fraction    = self.mDivs.journey.items.scrollLeft()/(childrenNo*stepWidth);
                        self.mShownItem = Math.round(fraction*childrenNo);
                        self.mDivs.journey.items.animate({scrollLeft:self.mShownItem*stepWidth}, function() {
                            self.mShownItem.centre({offset:[0,-self.mDivs.journey.panel.innerHeight()/2]});
                            self.invalidate();
                        });
                        self.mShownItem = self.mItems[self.mShownItem];
                    }
                }, 300)
            });
            map.on("mousedown", function() {
                $('.navbar-collapse').collapse('hide');
                if (self.mRoutingPreferences) {
                    self.mRoutingPreferences.hide();
                }
                self.minimise();
            });
        }

        newRoute() {
            return new navigated.Route({
                map      : this.map,
                parent   : this,
                routeNav : this.mDivs.routeNav,
                itemsDiv : this.mDivs.journey.items
            });
        }

        newPOI() {
            return new navigated.POI({
                map      : this.map,
                parent   : this,
                itemsDiv : this.mDivs.journey.items
            });
        }

        projectOnto(location, toleranceMts) {
            var result;
            this.findItem(function(route) {
                result = route.projectOnto(location, toleranceMts);
                return result;
            }, navigated.Route);
            return result;
        }

        pendingRoute() { //mobile journeys only have pending routes
            return this.shownRoute() || super.pendingRoute();
        }

        invalidate() {
            super.invalidate();
            var self = this;
            if (this.mRoutingPreferences && this.mRoutingPreferences.shown()) {
                clearTimeout(this.mHidePrefsTimeout);
                this.mHidePrefsTimeout = setTimeout(function() {
                    self.mRoutingPreferences.hide();
                }, 4000);
            }
            if (this.mItems.length) {
                this.mClearMap.removeClass("hide");
                this.mOfflinePrepare && this.mOfflinePrepare.removeClass("hide");
            } else {
                this.mClearMap.addClass("hide");
                this.mOfflinePrepare && this.mOfflinePrepare.addClass("hide");
            }
            if (this.shownRoute()) {
                this.mSimulateNavItem.removeClass("hide");
            } else {
                MSimulateJourneyMessage && MSimulateJourneyMessage.hide();
                this.mSimulateNavItem.addClass("hide");
            }

            if (!MSimulateJourneyMessage) {
                self.forEachItem(function(route) {
                    if (!route.isPending() && !MSimulateJourneyMessage) {
                        MSimulateJourneyMessage = messaging.popup(
                             '<span>Curious how this works fastened to your bike?</span><br>'
                            +'<span>Simulate the ride (check the menu).</span>',
                            {once:"simulateJourney"}
                        ).enqueue(KMessageEnqueueTimeoutMs);
                    }
                }, navigated.Route);
            }
        }

        show() {
            const self = this;
            this.mShownItem = this.mItems[0];
            return super.show()
            .then(function() {
                return when.promise(function(resolve) {
                    self.mDivs.journey.panel.animate({"margin-top":-self.mDivs.journey.panel.innerHeight()+"px"}, function() {
                        self.minimise().then(resolve);
                    });
                })
            })
        }

        minimise() {
            if (this.mDivs && this.mDivs.journey.panel.css("margin-top") != "0px") {
                const self = this;
                return when.promise(function(resolve) {
                    self.mDivs.journey.panel.animate({"margin-top":self.mDivs.journey.minMargin+"px"}, 800, function() {
                        resolve();
                    });
                });
            }
        }

        hide() {
            this.mDivs.journey.items.empty();
            const self = this;
            when.promise(function(resolve) {
                self.mDivs.journey.panel.animate({"margin-top":0}, function() {
                    resolve();
                })
            });
            delete this["mShownItem"];
            return super.hide();
        }

        shownRoute() {
            if (this.mItems.length == 1 && this.mItems.front().constructor.name == "Route") {
                return this.mItems.front();
            }
            return this.mShownItem && this.mShownItem.constructor.name == "Route" && this.mShownItem;
        }
    }

    return {
        Journeys        : Journeys,
        JourneyHtmlList : commonJourneys.JourneyHtmlList
    }

})();

module.exports = journeys;