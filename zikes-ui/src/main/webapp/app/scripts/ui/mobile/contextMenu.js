/**
 * scripts/ui/mobile/contextMenu.js
 *
 * This prepares and shows the map's conext menu.
 */

'use strict';
const when        = require("when");
const baseContextMenu = require("../common/contextMenu.js")


const ContextMenu = (function() {

    const contextMenuMarker = {
        url    : '//cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.0/images/marker-icon.png',
        size   : [24, 40],
        anchor : [12, 40]
    };

    /*
     * We want to register event handlers only once despite we
     * want the ContextMenu to be re-instantiable, hence this singleton
     */
    class StateHandler {

        constructor() {
            this.mDivs = {};
        }

        show(ctxMenu) {
            var self = this;
            if (!(ctxMenu.paneldiv in this.mDivs)) {
                this.mDivs[ctxMenu.paneldiv.attr("id")] = ctxMenu.paneldiv;
                ctxMenu.paneldiv.find(".close").click(function() {
                    self.hide();
                });
                ctxMenu.paneldiv.on("mousedown", function(e) {
                    e.stopPropagation();
                    return false;
                });
            }
            if (!this.mMapHandler) {
                this.mMapHandler = true;
                ctxMenu.map.on("mousedown", function(e) {
                    self.hide();
                });
            }
            return when(this.hide())
            .then(function() {
                self.mCurrentCtxMenu = ctxMenu;
            });
        }

        hide() {
            var self = this;
            if (this.mCurrentCtxMenu) {
                return this.mCurrentCtxMenu.hide()
                .then(function() {
                    self.mCurrentCtxMenu = undefined;
                });
            }
        }
    }
    const SStateHandler = new StateHandler();


    const KAnimationDurationMs = 800;
    class ContextMenu extends baseContextMenu.ContextMenu {
        constructor(map, panel, journey, options, latlng) {
            options.width = panel.innerWidth()
            super(map, journey, options,latlng);
            this.paneldiv  = panel;
        }

        hide() {
            const self = this;
            return when().delay(500)
            .then(function() {
                return when.promise(function(resolve) {
                    self.paneldiv.animate({"margin-top": "0px"}, KAnimationDurationMs, function() {
                        resolve();
                    });
                    !self.marker || self.marker.delete();
                });
            });
        }

        show() {
            var self = this;
            if (!super.show()) {
                return when(true);
            }
            return when(SStateHandler.show(this))
            .then(function() {
                var done = when.defer();
                self.marker = new self.map.sprites.Marker(self.latlng, {
                    icon: contextMenuMarker
                }).show();

                var body  = self.paneldiv.find(".modal-body");
                body.empty();
                body.html(self.html());
                self.actions.map(function(elem) {
                    body.find("#"+elem.id).one("mouseup", function(e) {
                        elem.action();
                        self.hide();
                    });
                });

                self.paneldiv.animate({"margin-top": -self.paneldiv.innerHeight()+"px"}, KAnimationDurationMs, function() {
                    done.resolve();
                });
                return done.promise;
            });
        }

        default() {
            return this.journey.pendingRoute().fillContextMenu(this);
        }
    }

    return ContextMenu;
})();

module.exports = {
    ContextMenu : ContextMenu,
    ContextMenuFactory : baseContextMenu.ContextMenuFactory
}
