/**
 * scripts/main.js
 *
 * This is the starting point for your application.
 * Take a look at http://browserify.org/ for more info
 */

'use strict';

global.zikes = {};
var journeys = require("./journeys.js");
//the bootstrap for the mobile browser
global.zikes.journeys = new journeys.Journeys( //this is to allow html refer to some entry points
	require("./map.js"),
    new journeys.JourneyHtmlList($("#plannedJourneysList")), {
    	journey     : $("#journey"),
	    routeNav    : $("#routeNav"),
    	contextMenu : $("#contextMenuPopup")
    }
);
