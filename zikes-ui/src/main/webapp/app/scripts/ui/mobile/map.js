/**
 * scripts/ui/mobile/map.js
 *
 * This prepares and manages the leaflet map element
 */

'use strict';

const when       = require("when");
when.delay       = require("when/delay");

const zikesApi   = require("../../lib/zikesBackend.js");
const utils      = require("../../lib/utils.js");
const highways   = require("../../lib/highways.js");

const messaging  = require("../common/messaging.js");
const sprites    = require("../mapbox/sprites.js");

const geoLocator = require("../common/geoLocator.js");
const browser    = require("./browser.js");


const map = (function() {

    function fullScreen() {
        function triggerAndWait(triggerFn, eventToAwait) {
            function _await() {
                finished.resolve();
                document.removeEventListener(eventToAwait, _await);
                map.resize();
            }
            let finished = when.defer();
            fullscreenTransitionFinished = finished.promise;
            document.addEventListener(eventToAwait, _await)
            triggerFn();
            return finished.promise;
        }
        var fullscreenTransitionFinished = when(true);
        var domElement = {} || window.cordova || $("#mapWrap")[0];
        function compose(request, exit, element, event) {
            return {
                enable   : triggerAndWait.bind(null, domElement[request].bind(domElement), event),
                exit     : triggerAndWait.bind(null, document[exit].bind(document), event),
                changing : function() { return !(fullscreenTransitionFinished.inspect().state == "fulfilled"); },
                enabled  : function() { return !!document[element]; }
            };
        }
        if (domElement.requestFullScreen) {
            return compose("requestFullScreen", "exitFullscreen", "fullscreenElement", "fullscreenchange");
        } else if (domElement.mozRequestFullScreen) {
            return compose("mozRequestFullScreen", "mozCancelFullScreen", "mozFullScreen", "mozfullscreenchange");
        } else if (domElement.webkitRequestFullScreen) {
            return compose("webkitRequestFullScreen", "webkitExitFullscreen", "webkitFullscreenElement", "webkitfullscreenchange");
        } else if (domElement.msRequestFullscreen) {
            return compose("msRequestFullscreen", "msExitFullscreen", "msFullscreenElement", "MSFullscreenChange");
        } else {
            //basic fullscreen for iOS and cordova/phone-gap
            var $nav = $("nav");
            var $mainMobileWorkArea = $("#mainMobileWorkArea");
            var enabled = true;
            var journey = $("#journey")
            var journeyMargin;
            return {
                enable : function() {
                    if (!enabled) {
                        let finished = when.defer();
                        fullscreenTransitionFinished = finished.promise;
                        $nav.animate({"margin-top":"-"+$nav.innerHeight()+"px"}, function() {
                            map.resize();
                            finished.resolve();
                        });
                        journeyMargin = journey.css("margin-top");
                        journey.css({"margin-top":"0px"});
                        $mainMobileWorkArea.animate({height: "100%"});
                        enabled = true;
                        window.powermanagement && window.powermanagement.acquire();
                        return finished.promise;
                    }
                },
                exit   : function() {
                    if (enabled) {
                        let finished = when.defer();
                        fullscreenTransitionFinished = finished.promise;
                        $nav.animate({"margin-top":"0"}, function() {
                            map.resize();
                            finished.resolve();
                        });
                        $mainMobileWorkArea.removeAttr("style");
                        journeyMargin && journey.css({"margin-top":journeyMargin});
                        journeyMargin = undefined;
                        enabled = false;
                        window.powermanagement && window.powermanagement.release();
                        return finished.promise;
                    }
                },
                enabled : function() { return enabled; },
                changing : function() {
                    return !fullscreenTransitionFinished.inspect().state == "fulfilled";
                }
            };
        }
    }

    const mapElement = $("#map");
    var center       = geoLocator.lastPosition();
    center           = center || [17.0314, 51.1106];

    mapboxgl.accessToken = 'pk.eyJ1IjoicmVtc3RlciIsImEiOiJjaXF6MnlrYXUwMDY3aTVubmxxdWN2M2htIn0.8FBrAn804OlX9QYW-FRVWA'
    const map = new mapboxgl.Map({
        container  : 'map',
        zoom       : 10,
        center     : center,
        style      : 'mapbox://styles/mapbox/outdoors-v10',
        bearingSnap: 10,
        dragRotate : false //https://github.com/mapbox/mapbox-gl-js/issues/4297
    });

    var mapboxLoadedEvent = when.defer();
    map.on('load', function() {
        mapboxLoadedEvent.resolve();
    });
    var mapboxLoaded = when.any([mapboxLoadedEvent.promise, when.delay(3000)]); //in offline scenarios, map will never load;

    function busy(busy) { busy ? $("#mapBusy").removeClass("hidden") : $("#mapBusy").addClass("hidden");}
    busy(true);
    var connectionMessage = messaging.wait(
        "Contacting backend... It sometimes needs waking up. This normally takes less than 20s."
    );

    var mapOnOff = new utils.OverridenSubsciption(map);
    var mapAbstraction = {
        busy            : busy,
        accessToken     : mapboxgl.accessToken,
        sprites         : sprites.bindMap(map),
        on              : function(eventtype, cb, context) {
            eventtype = Array.isArray(eventtype) ? eventtype : [eventtype];
            eventtype.map(function(eventtype) {
                if (eventtype == "contextmenu" && !browser.Firefox) { //see: https://github.com/mapbox/mapbox-gl-js/issues/4334
                    function clearTmMv(e) {
                        if (tmStart.tm &&
                            Math.abs(e.point.x-tmStart.pt.x)+Math.abs(e.point.y-tmStart.pt.y) > 5) {
                            clearTm();
                        }
                    }
                    function clearTm() {
                        clearTimeout(tmStart.tm);
                        tmStart = {};;
                    }
                    function setTm(e) {
                        clearTimeout(tmStart.tm);
                        tmStart = {
                            tm : setTimeout(function() {
                                e.latlng = e.lngLat;
                                tmStart = {};;
                                cb(e);
                            }, 1000),
                            pt : e.point
                        };
                    }
                    let tmStart = {};
                    mapOnOff.on({
                        eventType: eventtype,
                        cb       : cb,
                        context  : context
                    }, {
                        "touchstart" :  setTm,
                        "mousedown"  :  setTm,
                        "touchmove"  :  clearTmMv,
                        "mousemove"  :  clearTmMv,
                        "touchend"   :  clearTm,
                        "mouseup"    :  clearTm
                    });
                } else {
                    mapOnOff.on({
                        eventType: eventtype,
                        cb       : cb,
                        context  : context
                    }, function(e) {
                        if (!e.originalEvent || $(e.originalEvent.target).hasClass("mapboxgl-canvas")) {
                            if (e.lngLat) {
                                e.latlng = e.lngLat;
                            }
                            context ? cb.call(context, e) : cb(e);
                        }
                    });
                }
            });
        },
        off             : mapOnOff.off.bind(mapOnOff),
        getCenter       : map.getCenter.bind(map),
        getZoom         : map.getZoom.bind(map),
        getContainer    : map.getContainer.bind(map),
        getBounds       : map.getBounds.bind(map),
        fitBounds       : function(bounds, options, eventData) {
            eventData = eventData || {};
            eventData.fitBounds = bounds;
            let finished = when.promise(function(resolve) {
                function onfinished(e) {
                    if (e.fitBounds === bounds) {
                        resolve();
                    }
                }
                map.once("zoomend", onfinished);
            });
            map.fitBounds(bounds, options, eventData);
            return finished;
        },
        setView         : function(options, eventData) {
            options.center = options.position || map.getCenter();
            options.zoom   = options.zoom     || map.getZoom();
            if (options.centerOffset) {
                let transform = new map.transform.constructor();
                transform.resize(map.transform.width, map.transform.height);
                transform.zoom    = options.zoom;
                transform.bearing = options.bearing;
                transform.pitch   = options.pitch;
                transform.center  = options.center;
                let screenBottom  = transform.pointLocation(
                    new mapboxgl.Point(map.transform.width/2, map.transform.height)
                );
                options.center = new mapboxgl.LngLat(
                    options.center.lng - (screenBottom.lng - options.center.lng)/2,
                    options.center.lat - (screenBottom.lat - options.center.lat)/2
                );
            }
            eventData = eventData || {};
            eventData.setView = options;
            let finished = when.promise(function(resolve) {
                function onfinished(e) {
                    if (e.setView === options) {
                        resolve();
                    }
                }
                map.once("zoomend", onfinished);
            });
            if (options.animate) {
                if (typeof options.animate === "number") {
                    options.duration = options.animate;
                }
                map.flyTo(options,  eventData);
            } else {
                map.jumpTo(options, eventData);
            }
            return finished;
        },
        isMoving        : map.isMoving.bind(map),
        project         : map.project.bind(map),
        unproject       : map.unproject.bind(map),
        getBearing      : map.getBearing.bind(map)
    };
    mapAbstraction.geocode = require("../mapbox/geocode.js")(mapboxgl.accessToken, mapAbstraction),
    mapAbstraction.loaded = when.all([mapboxLoaded, browser.ready]).then(function() {
        return zikesApi.meta();
    }).then(function(meta) {
        connectionMessage.hide();
        var coverageOverlay = [[[89, -179],[89, 179],[-89, 179],[-89, -179]]];
        mapAbstraction.highways = new highways.Highways(meta.map.highways);
        coverageOverlay = coverageOverlay.concat(meta.map.coverage);
        var polygon = new sprites.Polygon(
            map,
            coverageOverlay,
            {
                fill : {
                    color: "red",
                    opacity: 0.3
                },
                weight: 0
            }
        ).show();
        mapAbstraction.fullScreen = fullScreen();
        mapAbstraction.withinCoverage = function(latlng) {
            return utils.pips(latlng, meta.map.coverage);
        }
        return meta.map.coverage;
    })
    .otherwise(function() {
        connectionMessage.hide();
        messaging.error(
            "We're sorry, but our routing server appears to be down :( There is nothing we can do for you right now.",
            {timeoutSec:0}
        ).enqueue();
        throw "We're doomed!"
    }).ensure(function() {
        busy(false);
    });
    mapAbstraction.geoLocator = new geoLocator.GeoLocatorUI(mapAbstraction);
    return mapAbstraction;

})();

module.exports   = map;