/**
 * scripts/ui/mobile/offline.js
 */

'use strict';

const when        = require("when");
when.pipeline     = require('when/pipeline');
const osm         = require("../../lib/osm.js");
const tileCacheDb = require("../../lib/indexedDB.js").tileCacheDb;
const geometry    = require("../../lib/geometry.js");

const messaging   = require("../common/messaging.js");

const offline = (function() {

    class TileWalker {

        constructor(waypointIteratorFactory, minMaxZoom, perimtererMts) {
            this.mWaypointIteratorFactory = waypointIteratorFactory;
            this.mMinMaxZoom = minMaxZoom;
            this.mPerimeterMts = perimtererMts;
        }

        walk(cb) {
            var self = this;
            function _walk() {
                var point = pointIterator.current();
                if (!point) {
                    finished.resolve();
                    return;
                }
                var pointFinished = [];
                if (prevPoint) {
                    for (var z = self.mMinMaxZoom[0]; z <= self.mMinMaxZoom[1]; z++) {
                        pointFinished.push(
                            self._processTileVector(
                                z,
                                prevPoint,
                                point,
                                cb
                            )
                        );
                    }
                    prevPoint = point;
                } else {
                    prevPoint = point;
                    self.mGeoPerimeterCompDistance = point.comparableDistance(point.offsetCourse(self.mPerimeterMts, 0));
                }

                when.all(pointFinished).then(function() {
                    pointIterator.next();
                    _walk();
                });
            }
            var prevPoint;
            var pointIterator = this.mWaypointIteratorFactory();
            var finished = when.defer();
            _walk();
            return finished.promise;
        }

        _processTileVector(zoom, fromPoint, toPoint, cb) {
            var fromTile = osm.deg2num(fromPoint, zoom);
            var toTile   = osm.deg2num(toPoint,zoom);
            if (fromTile.equals(toTile)) {
                return this._processTile(fromTile, [toPoint], cb);
            }
            var dx = toTile.x - fromTile.x;
            var dy = toTile.y - fromTile.y;
            if (Math.abs(dx) > 1 || Math.abs(dy) > 1) {
                var midPoint = new geometry.LatLng(
                    fromPoint.lat + (toPoint.lat - fromPoint.lat)/2,
                    fromPoint.lng + (toPoint.lng - fromPoint.lng)/2
                );
                return when.all([
                    this._processTileVector(
                        zoom,
                        fromPoint,
                        midPoint,
                        cb
                    ),
                    this._processTileVector(
                        zoom,
                        midPoint,
                        toPoint,
                        cb
                    )
                ]);
            }
            return when.all([
                this._processTile(fromTile, [toPoint], cb),
                this._processTile(toTile, [toPoint], cb)
            ]);
        }

        _processTile(tile, refPoints, cb) {
            var self      = this;
            var openset   = {};
            var finished  = [];
            var closedset = new Set();
            openset[tile.toString()] = tile;
            while(Object.keys(openset).length) {
                let current;
                for (let key in openset) {
                    current = {
                        key : key,
                        val : openset[key]
                    }
                    break;
                }
                delete openset[current.key];
                closedset.add(current.key);
                finished.push(cb(current.val));
                let neighbours = current.val.neighbours();
                neighbours.forEach(function(neighbour) {
                    let neighbourKey = neighbour.toString();
                    if (closedset.has(neighbourKey)) {
                        return;
                    }
                    if (!refPoints.find(function(refPoint) {
                        return neighbour.polygon().find(function(edge) {
                            return refPoint.comparableDistance(edge) < self.mGeoPerimeterCompDistance;
                        });
                    })) {
                        return;
                    }
                    openset[neighbourKey] = neighbour;
                })
            }
            return when.all(finished);
        }
    }


    const KTileServerUrl  = "https://a.tiles.mapbox.com/v4/mapbox.mapbox-terrain-v2,mapbox.mapbox-streets-v7/{{z}}/{{x}}/{{y}}.vector.pbf?access_token=";
    const KTrimAboveTiles = 50;
    var   KOfflineMessage;
    class TileCache {

        constructor(map) {
            this.map = map;
        }

        prepare(waypointIteratorFactories, minMaxZoom) {
            var polyOpacity         = 0.5/(1+minMaxZoom[1]-minMaxZoom[0]);
            var downloadedPolyStyle = { fill : { color: "blue", opacity: polyOpacity }};
            var pendingPolyStyle    = { fill : { color: "grey", opacity: polyOpacity }};
            var self                = this;
            var polygons            = {};
            var unusedTiles         = new Set();
            when().then(function() {
                if (!KOfflineMessage) {
                    KOfflineMessage = messaging.popup(
                         '<span class="lead">Caching map data along the journey.</span><br>'
                        +'<span class="small">We don\'t know how much your phone can take so we won\'t constrict this request.</span><br>'
                        +'<span class="small">We may however purge the map data you cached earlier to manage available storage. In practice then:</span><br>'
                        +'<span class="lead">One journey cached at a time!</span>',
                        {"once": "offlineCacheMaintenance"}
                    ).enqueue();
                    return KOfflineMessage.whenHidden();
                }
            }).then(function() {
                self.map.busy(true);
                return tileCacheDb.stores.tile.allKeys();
            }).then(function(preexitingTiles) {
                unusedTiles = new Set(preexitingTiles);
                return when.pipeline(
                    waypointIteratorFactories.map(function(waypointIteratorFactory) {
                        return function() {
                            return new TileWalker(waypointIteratorFactory, minMaxZoom, 400).walk(function(tile) {
                                let key = tileCacheDb.stores.tile.key(tile);
                                if (key in polygons) {
                                    return;
                                }
                                polygons[key] = true;
                                return tileCacheDb.stores.tile.has(key)
                                .then(function(has) {
                                    let tilePoly;
                                    if (has) {
                                        tilePoly = new self.map.sprites.Polygon(
                                            [tile.polygon()], downloadedPolyStyle
                                        ).show();
                                    } else {
                                        tilePoly = new self.map.sprites.Polygon(
                                            [tile.polygon()], pendingPolyStyle
                                        ).show();
                                        tilePoly.tile = tile;
                                    }
                                    polygons[key] = tilePoly;
                                    unusedTiles.delete(key);
                                });
                            });
                        }
                    })
                );
            }).then(function() {
                let noOfUsedTiles   = Object.keys(polygons).length;
                let noOfUnusedTiles = unusedTiles.size;
                let allWouldBeTiles = noOfUnusedTiles + noOfUsedTiles;
                if (allWouldBeTiles > KTrimAboveTiles) {
                    return when.all(
                        Array.from(unusedTiles.keys()).map(function(key) {
                            return tileCacheDb.stores.tile.delete(key);
                        })
                    );
                }
            }).then(function() {
                return when.all(
                    Object.keys(polygons).map(function(key) {
                        let polygon = polygons[key];
                        if (polygon.tile) {
                            let url = KTileServerUrl
                            .replace("{{z}}", polygon.tile.z)
                            .replace("{{x}}", polygon.tile.x)
                            .replace("{{y}}", polygon.tile.y) + self.map.accessToken;
                            return fetch(url)
                            .then(function(response) {
                                if (response.statusText == "OK") {
                                    return response.arrayBuffer()
                                    .then(function(arrayBuffer) {
                                        return tileCacheDb.stores.tile.put(key, arrayBuffer);
                                    })
                                    .then(function() {
                                        polygon.delete();
                                        polygons[key] = new self.map.sprites.Polygon(
                                            [polygon.tile.polygon()], downloadedPolyStyle
                                        ).show();
                                    });
                                } else {
                                    throw new Error("Unable to fetch a tile, status: "+response.statusText);
                                }
                            });
                        }
                    })
                );
            }).ensure(function() {
                self.map.busy(false);
            }).then(function(){
                return messaging.popup(
                     '<span class="lead">Bon Voyage!</span><br>'
                    +'<span>All map data along the selected journey now cached.</span>'
                ).enqueue().whenHidden();
            }).otherwise(function(err) {
                return messaging.popup(
                     '<span class="lead text-danger"><b>Failed to cache some data!</b></span><br>'
                    +'<span>Reason: "'+err.message+'"</span>'
                ).enqueue().whenHidden();
            }).ensure(function() {
                for (var polygon in polygons) {
                    polygons[polygon].delete();
                }
            });
        }

    }

    return {
        TileCache : TileCache,
        enabled   : function() {
            return when(tileCacheDb.ready())
            .then(function() {
                return !!navigator.serviceWorker
            }).otherwise(function(err) {
                return false;
            });
        }
    }

})();

module.exports = offline
