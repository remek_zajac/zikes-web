/**
 * scripts/ui/mobile/browser.js
 *
 * client side aspect of the service worker implementation
 * all aspects related to browser support.
 */

'use strict';

const utils      = require("../../lib/utils.js");
const messaging  = require("../common/messaging.js");

const browser = (function() {

    var result = utils.browserFeaturesFromUserAgent(navigator.userAgent);
    result.ready = undefined;

    const KRecommended    = {
        Chrome  : "53",
        Firefox : "51",
        Safari  : "10"
    };
    const recommendedBrowsers = result.safari ? "Safari "+KRecommended.Safari+".0+" : "Chrome "+KRecommended.Chrome+".0+ or FireFox "+KRecommended.Firefox+".0+";
    const outdated = result.browser in KRecommended && !result[result.browser].version || result[result.browser].version < KRecommended[result.browser];
    const KMessageEnqueueTimeout = 1000;
    if (outdated) {
        messaging.popup(
             '<span class="lead text-danger"><b>Your browser is out of date.</b></span><br>'
            +'<span class="small">We recommend that you switch to '+recommendedBrowsers+' otherwise things may not work.</span><br>',
            +'<span class="small">If you think we\'re wrong, please send us the screenshot <a href="https://bitbucket.org/remek_zajac/zikes-web/issues?status=new&status=open" target="_blank">here</a></span><br>',
            +'<span class="small text-info">'+navigator.userAgent+'</span>'
        ).enqueue(KMessageEnqueueTimeout);
    } else if (!result.browser) {
        messaging.popup(
             '<span class="lead text-danger"><b>We\'ve not tested on this browser.</b></span><br>'
            +'<span class="small">We recommend that you switch to '+recommendedBrowsers+', but, you never know, things may just work.</span><br>',
            +'<span class="small">If you think you are running a browser we recommend, please send us the screenshot <a href="https://bitbucket.org/remek_zajac/zikes-web/issues?status=new&status=open" target="_blank">here</a></span><br>',
            +'<span class="small text-info">'+navigator.userAgent+'</span>',
            {once: "uknownBrowser"}
        ).enqueue(KMessageEnqueueTimeout);
    }

    if ('serviceWorker' in navigator) {
        result.ready = new Promise(function(resolve, reject) {
            let installingDiv;
            const registrationPoll = setInterval(function() {
                navigator.serviceWorker.getRegistration().then(function(r) {
                    if (!installingDiv) {
                        installingDiv = $('<div style="text-align:center;font-size:1.5em;">').html('Installing');
                        $("#mapBusy .content").append(installingDiv);
                    }
                });
            }, 500);
            navigator.serviceWorker.ready.then(function() {
                !installingDiv || installingDiv.remove();
                clearInterval(registrationPoll);
                resolve();
            });
            window.addEventListener('load', function() {
                navigator.serviceWorker.register('/mobile/serviceWorker.js').then(function(registration) {
                    console.log('ServiceWorker registration successful with scope: ', registration.scope);
                    registration.update();
                }).catch(function(err) {
                    console.log('ServiceWorker registration failed: ', err);
                    !installingDiv || installingDiv.remove();
                    clearInterval(registrationPoll);
                    resolve();
                });
            });
        });
    } else  {
        if (result.iOS) {
            messaging.popup(
                 '<span class="lead">Your beloved Apple device lacks support for <b>service workers</b>.</span><br>'
                +'<span>Without which we can\'t offer you the offline experience.</span><br>'
                +'<span class="small">iOS is <a href="https://www.google.com/search?q=safari+is+the+new+internet+explorer" target="_blank">known</a> to lag behind the cutting edge web and at this point we can only recommend that you:<br>'
                +'- Live without the offline experience.<br>- Use the Android phone you keep in your drawer.<br>- Await us release the native iOS app.</span>',
                {once:"missingServiceWorkers"}
            ).enqueue(KMessageEnqueueTimeout);
        } else {
            messaging.popup(
                 '<span class="lead">Your browser lacks support for <b>service workers</b>.</span><br>'
                +'<span>Without which we can\'t offer you the offline experience.</span><br>',
                +'<span class="small">Your browser is either outdated or exotic. We recommend that you switch to '+recommendedBrowsers+'</span>',
                {once:"missingServiceWorkers"}
            ).enqueue(KMessageEnqueueTimeout);
        }
    }

    /*
     * Highjack href clicks to open them on '_blank' if running in a system browser,
     * or '_system' if running in cordova (will open in system browser)
     */
    $(document).on('click', 'a[href^=http], a[href^=https]', function(e){
        e.preventDefault();
        window.open($(this).attr('href'), window.cordova ? '_system' : '_blank', 'location=no');
    });

    window.zikes.sliderEnableTouch = result.mobile;
    return result;

})();

module.exports = browser;
