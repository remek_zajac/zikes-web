/**
 * scripts/ui/mobile/battery.js
 */

'use strict';

const batteryModule = (function() {

    const KLevelMargin = 0.15;
    var div;
    var battery;
    function updateBatteryUI() {
        if (!(div && battery)) {
            return
        }
        div.children().addClass("hidden");
        if (battery.charging) {
            div.find(".charging").removeClass("hidden");
        } else {
            div.find(".charging").addClass("hidden");
        }
        if (battery.level > 0.75+KLevelMargin) {
            div.find(".full").removeClass("hidden");
        } else if (battery.level > 0.5+KLevelMargin) {
            div.find(".three-quarters").removeClass("hidden");
        } else if (battery.level > 0.25+KLevelMargin) {
            div.find(".half").removeClass("hidden");
        } else if (battery.level > 0+KLevelMargin) {
            div.find(".quarter").removeClass("hidden");
        } else {
            div.find(".empty").removeClass("hidden");
        }
    }

    function initAndUpdateBatteryUI(trials=4) {
        if (navigator.getBattery) {
            if (battery) {
                updateBatteryUI();
                return;
            }
            if (trials) {
                setTimeout(function() {
                    initAndUpdateBatteryUI(trials-1)
                }, 2000);
            }
            return navigator.getBattery().then(function(_battery) {
                if (battery) {
                    return;
                }
                battery = _battery;
                battery.addEventListener('chargingchange', updateBatteryUI);
                battery.addEventListener('levelchange', updateBatteryUI);
                updateBatteryUI();
            });
        }
    }

    return {
        setDiv : function(_div) {
            div = _div;
            initAndUpdateBatteryUI();
        }
    }

})();

module.exports = batteryModule.setDiv;
