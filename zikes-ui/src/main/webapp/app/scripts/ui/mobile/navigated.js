/**
 */

'use strict';

const when          = require("when");
const mustache      = require("mustache");
const utils         = require("../../lib/utils.js");
const mapRoute      = require("../common/mapRoute.js");
const routeProfile  = require("../common/routeProfile.js");
const common        = require("../common/journeyItem.js");
const Weather       = require("../common/weather.js").Weather;
const batteryDiv    = require("./battery.js");

const navigated = (function() {

    const templates = when.promise(function(resolve) {
        $.get("/templates/route.mobile.template.html", function(template, textStatus, jqXhr) {
            resolve({
                Navigated   : $(template).filter("#navigated_route_tmpl").html(),
                Route       : $(template).filter("#route_tmpl").html(),
                POI         : $(template).filter("#poi_tmpl").html()
            });
        });
    });

    const KMobileIcons = {
        from : mapRoute.KDefaultIcons.from,
        to   : mapRoute.KDefaultIcons.to
    }

    const KAssumeAvgSpeedMps_TODO = utils.kph2Mps(18);
    class NavigatedRoute extends common.Route() {

        constructor(route, panel) {
            super({
                itemsDiv:panel,
                map:route.map
            });
            this.mRouteOnMap = route.mRouteOnMap;
            this.mPanelDiv   = panel;
            this.mParams     = route.mParams;
            delete this["mCentreOnRoute"];
        }

        doInvalidate() {
            this.mWeather = new Weather(this.mRouteOnMap);
            this.mRouteOnMap.showDirectionMarkers(false);
            return super.doInvalidate();
        }

        rendered() {
            super.rendered();
            const self = this;
            let panel  = this.mDivs.panel;
            this.mDivs.fromOrigin         = panel.find(".fromOrigin");
            this.mDivs.speed              = panel.find(".speed");
            this.mDivs.toDestination      = panel.find(".toDestination");
            this.mDivs.timeToDestination  = panel.find(".timeToDestination");
            this.mDivs.clock              = panel.find(".clock");
            this.mDivs.battery            = panel.find(".battery");
            this.mDivs.profileCanvas.width   = window.innerWidth;
            this.mDivs.highlightCanvas.width = window.innerWidth;
            this.mPanelDiv.animate({"margin-top": -this.mPanelDiv.innerHeight()+"px"}, 800);
            let ticker = setInterval(function() {
                if (self.mDivs) {
                    self.mDivs.clock.text(new Date().toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'}));
                } else {
                    clearInterval(ticker);
                }
            }, 1000);
            batteryDiv(this.mDivs.battery);
            this.invalidate();
        }

        show(distanceFromStart, speedMps) {
            const self = this;
            super.render(
                templates,
                "Navigated"
            ).then(function() {
                self.mPixProfile && self.mPixProfile.progress(distanceFromStart);
                self.mWeather    && self.mWeather.show(distanceFromStart, KAssumeAvgSpeedMps_TODO);

                self.mDivs.fromOrigin.text(
                    utils.mtsToString(distanceFromStart)
                );
                let distanceToDstMts = self.distanceMts()-distanceFromStart;
                self.mDivs.toDestination.text(
                    utils.mtsToString(distanceToDstMts)
                );
                let timeToDestination = Math.round(distanceToDstMts/KAssumeAvgSpeedMps_TODO);
                self.mDivs.timeToDestination.text(
                    timeToDestination ? utils.secondsToHrs(timeToDestination) : ""
                );
                self.mDivs.speed.text(
                    Math.round(utils.mps2Kph(speedMps || 0))+"km/h"
                );
                var deltaChar = "\u0394";
                var content = "";
                if (self.mPixProfile && !isNaN(self.mPixProfile.geoProfile().ele.d)) {
                    content = "\u0394" + Math.round(self.mPixProfile.geoProfile().ele.d) + "m "
                            + "\u2197" + Math.round(self.mPixProfile.geoProfile().ele.gain) + "m";
                }
                self.mDivs.dEle.text(content);
            });
        }

        hide() {
            const self = this;
            this.mPanelDiv.animate({"margin-top": "0px"}, 800, function() {
                self.mPanelDiv.empty();
                delete self["mDivs"];
                delete self["mPixProfile"];
            });
            !this.mWeather || this.mWeather.hide();
            this.mRouteOnMap.showDirectionMarkers(true);
        }
    }


    class Route extends common.Route() {
        constructor(options) {
            super(options);
            this.mRouteOnMap  = new mapRoute.Route({
                map : options.map,
                style: function() {
                    return {
                        showElevation : true,
                        color : { base : "#0000FF", min: "#000066", max: "#ff0000" }
                    };
                },
                invalidate : this.invalidate.bind(this),
                highlight  : function(distance) {},
                route      : function(milestones) {
                    return options.parent.route(milestones);
                }
            }, {
                icons : KMobileIcons
            });
        }

        getBounds() {
            return this.mRouteOnMap.getBounds();
        }

        reroute(lastGoodProjection, escapePath) {
            const self = this;
            if (lastGoodProjection && lastGoodProjection.segment) {
                let failover = this.mRouteOnMap.toJSON();
                let to       = this.mRouteOnMap.to();
                let after    = lastGoodProjection.segment.section.cut(lastGoodProjection);
                after.startMarker.setLatLng(escapePath.back());
                after.split();
                this.mRouteOnMap.to(to);
                return this.mRouteOnMap.invalidate().
                catch((err) => {
                    this.mRouteOnMap.fromJSON(failover);
                    throw err;
                });
            }
        }

        projectOnto(location, toleranceMts) {
            var result = this.mRouteOnMap.projectOnto(location, toleranceMts);
            if (result) {
                result.route = this;
            }
            return result;
        }

        navigate(distanceFromStart, speedMps) {
            if (speedMps != undefined) {
                this.mNavigated = this.mNavigated || new NavigatedRoute(this, this.options.routeNav);
                this.mNavigated.show(distanceFromStart, speedMps);
            } else if (this.mNavigated) {
                this.mNavigated.hide();
                this.mNavigated = undefined;
            }
        }

        fillContextMenu(ctxMenu) {
            if (this.from()) {
                if (this.to()) {
                    ctxMenu.from().via().to();
                } else {
                    ctxMenu.from().to({
                        bold: true
                    });
                }
            } else {
                ctxMenu.to().from({
                    bold: true
                });
            }
            return ctxMenu;
        }

        via(latlng) {
            utils.assert(this.from() && this.to(), "to and from must be set to add a via");
            this.mRouteOnMap.via(latlng)
            this.mRouteOnMap.invalidate();
        }

        delete() {
            this.mRouteOnMap.delete();
            super.delete();
        }

        invalidate() {
            !this.mNavigated || this.mNavigated.invalidate();
            return super.invalidate();
        }

        render() {
            return super.render(templates, "Route");
        }

        rendered() {
            super.rendered();
            const self = this;
            let panel  = this.mDivs.panel;
            if (!this.mParams.description) {
                self.mDivs.description.remove();
            }
            this.mDivs.profileCanvas.width  = $(this.mDivs.profileCanvas).parent().innerWidth();
        }
    }


    class POI extends common.POI() {
        constructor(options) {
            super(options);
        }

        render() {
            return super.render(templates, "POI");
        }
    }


    return {
        Route : Route,
        POI   : POI
    };

})();

module.exports = navigated;