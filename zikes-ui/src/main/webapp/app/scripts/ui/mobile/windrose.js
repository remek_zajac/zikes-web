/**
 * scripts/ui/mobile/windrose.js
 */

'use strict';
const when      = require("when");
const utils     = require("../../lib/utils.js");
const geometry  = require("../../lib/geometry.js");

const browser   = require("./browser.js");


const windrose = (function() {

    function cssVals(risen) {
        const px = risen ? 3 : 0;
        return {
            icon : {
                "bottom" : px+"px",
                "right"  : px+"px"
            },
            shadow : {
                "margin-top"  : px+"px",
                "margin-left" : px+"px"
            }
        };
    }

    /*
     * Windrose control providing low-lever icon behaviour.
     */
    class WindroseCtrl {

        constructor() {
            this.mDiv = $("#windrose");
            this.mIcon   = this.mDiv.find(".icon");
            this.mShadow = this.mDiv.find(".shadow");
            var self = this;
            this.mDiv.on("mousedown", function() {
                var css = cssVals();
                self.mIcon.css(css.icon);
                self.mShadow.css(css.shadow);
            });
            this.mDiv.on("mouseup", function() {
                if (self.mRisen) {
                    self.raise();
                }
            });
        }

        show() {
            this.mDiv.removeClass("hide");
            return this;
        }

        hide() {
            this.mDiv.addClass("hide");
            return this;
        }

        raise() {
            var css = cssVals(true);
            this.mIcon.animate(css.icon);
            this.mShadow.animate(css.shadow);
            this.mRisen = true;
            return this;
        }

        drop() {
            var css = cssVals();
            this.mIcon.animate(css.icon);
            this.mShadow.animate(css.shadow);
            this.mRisen = false;
            return this;
        }

        on() {
            this.mDiv.on.apply(this.mDiv, arguments);
            return this;
        }

        off() {
            this.mDiv.off.apply(this.mDiv, arguments);
            return this;
        }

        rotate(degrees) {
            this.mIcon.css({
                "transform" : "rotate("+ degrees +"deg)"
            });
            this.mShadow.css({
                "transform" : "rotate("+ degrees +"deg)"
            });
            return this;
        }
    }


    function navigateRoute(position) {
        !position.routeProjection || position.routeProjection.route.navigate(position.routeProjection.distanceFromStart, position.d3c.v);
    }


    /*
     *
     * Windrose State Machine
     * i.e.: how clicks on the windrose/compass change the map view:
     * if routeProjection                                -> turn off navigation mode
     * else if bearing != 0                         -> snap to 0
     * else if map not centered on current position -> fly to current position and follow its changes
     * else if navigable route in range             -> switch to navigation mode
     *
     * and how this map reacts to position updates:
     * if map centered on current position          -> follow
     */
    const KNavigationToleranceMts = 1000;
    const KRerouteToleranceMts    = 130;
    const KSnapToleranceMts       = 30;
    const KNavigationZoom         = 16;
    const KCenterZoom             = 12;
    const KFullScreenTransitionMs = 1200;
    class WindroseSM {

        constructor(map) {
            this.map = map;
            var self = this;
            map.geoLocator.correct(function(position) {
                if (self.isCentered()) {
                    return self.followPosition(position);
                }
                return position;
            });
            map.geoLocator.on("position", function(e) { navigateRoute(e.position); }); //call show on "position" as only then we know the speed
            map.on("movestart", function(e) {
                if (!self.mIgnoreMove) {
                    /*
                     * When 'this'/WindroseSM is in navigation or follow modes it wants to follow the gps position as it changes.
                     * The user can take 'this' out of the 'follow' mode by either dragging/pinching the map or invoking other code that
                     * changes the map position (e.g.: load a track somewhere). WindroseSM needs to recognise when this happens
                     * - when a 'movestart' is triggered by other code than WindroseSM own actions. WindroseSM therefore decorates the
                     * setView requests with custom event data (e.WindroseSM). If this event data is absent, some other code triggered
                     * the move and thus WindroseSM should exit the 'follow' mode.
                     */
                    self.mSelfMove = e.WindroseSM;
                }
            });
            map.on("moveend", function(e) {
                if (!(self.mIgnoreMove || self.isCentered())) {
                    self.stopNavigating();
                }
            });
        }

        isNavigating() {
            return !!this.mNavigatedRoute && this.isCentered();
        }

        isCentered() {
            return (this.mSelfMove || (
                this.mPrevLocallySetPosition &&
                this.mPrevLocallySetPosition.equals(this.map.getCenter(), this.mPrevLocallySetPosition.onemeterComparableDistance)
            ));
        }

        setView(options) {
            options.animate = options.animate  || true;
            options.zoom    = options.zoom     || Math.max(KCenterZoom, this.map.getZoom());
            this.map.setView(options, options.position ? {WindroseSM:true} : undefined);
            if (options.position) {
                this.mPrevLocallySetPosition = options.center;
            }
        }

        click() {
            var position = this.map.geoLocator.position();
            if (this.isNavigating()) {
                this.stopNavigating({position:position});
            } else if (this.map.getBearing() != 0) {
                this.setView({bearing:0});
            } else if (position) {
                if (this.isCentered()) {
                    navigateRoute(
                        this.followPosition(this.map.geoLocator.position(), true)
                    );
                } else {
                    this.setView({position:position});
                }
            }
        }

        enterFullScreen() {
            if (!this.map.fullScreen.enabled()) {
                var self = this;
                this.mIgnoreMove = true;
                return self.map.fullScreen.enable()
                .delay(KFullScreenTransitionMs)
                .then(function() {
                    delete self["mIgnoreMove"];
                });
            }
            return when(false);
        }

        exitFullScreen() {
            if (this.map.fullScreen.enabled()) {
                var self = this;
                this.mIgnoreMove = true;
                return self.map.fullScreen.exit()
                .delay(KFullScreenTransitionMs)
                .then(function() {
                    delete self["mIgnoreMove"];
                });
            }
            return when(false);
        }

        followPosition(position, tryNavigation) {
            const self      = this;
            let viewOptions = {
                bearing  : 0,
                pitch    : 0,
                position : position
            };
            let routeProjection  = (tryNavigation || this.isNavigating()) && window.zikes.journeys.current().projectOnto(
                position, KNavigationToleranceMts
            );
            if (routeProjection) {
                viewOptions.animate      = 800;
                viewOptions.zoom         = KNavigationZoom;
                viewOptions.bearing      = routeProjection.segment.line.bearing();
                viewOptions.pitch        = 60;
                viewOptions.centerOffset = true;
                position.routeProjection = routeProjection;
                if (this.mNavigatedRoute && this.mNavigatedRoute !== routeProjection.route) {
                    this.mNavigatedRoute.navigate(false);
                }
                this.mNavigatedRoute = routeProjection.route;

                let escapePath = this.mEscapePath;
                if (routeProjection.distanceMts <= KSnapToleranceMts) {
                    viewOptions.position = routeProjection.projection;
                    escapePath = this.mEscapePath = [viewOptions.position];
                    routeProjection.snapped = routeProjection.projection;
                } else if (routeProjection.distanceMts > KRerouteToleranceMts) {
                    if (routeProjection.route.distanceMts() - routeProjection.distanceFromStart < KRerouteToleranceMts) {
                        escapePath = [routeProjection.projection, position];
                    } else {
                        escapePath = escapePath || [routeProjection.projection];
                        escapePath.push(position);
                        routeProjection.route.reroute(routeProjection, escapePath)
                        .then(() => {
                            delete self["mEscapePath"];
                        });
                    }
                } else if (escapePath) {
                    escapePath.push(position);
                }

                if (escapePath && escapePath.length > 1) {
                    viewOptions.bearing = new geometry.GeoLine(
                        escapePath[escapePath.length-2],
                        escapePath[escapePath.length-1]
                    ).bearing();
                }

                this.enterFullScreen().then(function() {
                    self.setView(viewOptions);
                });
            } else {
                if (this.mNavigatedRoute) { //DEBUG. removeme
                    console.log("Fell off projection for position "+JSON.stringify(position.toArray()));
                    console.log("Journey :"+JSON.stringify(window.zikes.journeys.current().toJSON()));
                }

                delete this["mEscapePath"];
                this.stopNavigating(viewOptions, true);
            }
            return position;
        }

        stopNavigating(viewOptions, forceSetView) {
            let self = this;
            let routeToHide = this.mNavigatedRoute;
            this.mNavigatedRoute = undefined;
            return when(this.exitFullScreen())
            .then(function() {
                !routeToHide || routeToHide.navigate(false);
                if (routeToHide || forceSetView) {
                    viewOptions         = viewOptions || {};
                    viewOptions.bearing = viewOptions.bearing || 0;
                    viewOptions.pitch   = 0;
                    self.setView(viewOptions);
                }
            });
        }
    }

    /*
     * Top level, external wrapper over the Windrose Ctrl and its UI
     * state-machine
     */
    class Windrose {

        constructor(map) {
            var windrosCtrl = new WindroseCtrl();
            this.on  = windrosCtrl.on.bind(windrosCtrl);
            this.off = windrosCtrl.off.bind(windrosCtrl);

            map.loaded.then(function() {
                windrosCtrl.show();
            })
            map.on("rotate", function() {
                windrosCtrl.rotate(-map.getBearing());
            });
            var firstAcquired = false;
            var windroseSM = new WindroseSM(map);
            map.geoLocator.on("acquired", function() {
                windrosCtrl.raise();
                if (!firstAcquired) {
                    windroseSM.setView({ position: map.geoLocator.position() });
                }
                firstAcquired = true;
            });
            map.geoLocator.on("lost", function() {
                windrosCtrl.drop();
            });
            windrosCtrl.on("click", windroseSM.click.bind(windroseSM));
        }
    }

    return {
        Windrose  : Windrose
    }

})();

module.exports = windrose;