/**
 * scripts/lib/geocode.js
 *
 */

'use strict';

const addressComponentsSpecificity = [
    {//0
        name    : "country",
        priority:   1
    },
    {//1
        name: "administrative_area_level_1",
    },
    {//2
        name: "administrative_area_level_2"
    },
    {//3
        name: "locality",
        priority:   1
    },
    {//4
        name: "sublocality"
    },
    {//5
        name: "neighborhood",
        priority:   1
    },
    {//6
        name: "route",
        priority:   1
    },
    {//7
        name: "street_number"
    }
];

module.exports = {
    //Pass results of one or two different calls to reverseGeocodeFind
    //and obtain (respectivelly) one or two street addresses truncated to the delta between
    //them (cutting away the common part)
	humanReadable: function (geocodeResults, verbosity) {
        verbosity = verbosity || 2;
        //deep copy first:
        var unpack = false;
        if (geocodeResults.length != 2) {
            geocodeResults =  [geocodeResults];
            unpack = true;
        }

        function compress() {
            var compressed = [];
            var lastPresentComponentIdx = undefined;
            for (var i = 0; i < addressComponentsSpecificity.length; i++) {
                var prev = undefined;
                var current = undefined;
                for (var j = 0; j < geocodeResults.length; j++) {
                    if (geocodeResults[j] == undefined) {
                        continue //allow/ignore undefined slots in the query
                    }
                    current = geocodeResults[j][i];
                    if (current == undefined) {
                        break;
                    }
                    if (prev || geocodeResults.length == 1) {
                        if (prev == current && compressed.length == 0 && i < 6) {
                            break;
                        } else {
                            if (compressed.length == 0 && lastPresentComponentIdx) {
                                compressed.push({index:lastPresentComponentIdx, priority:-1});
                            }
                            compressed.push({index:i});
                        }
                    }
                    prev = current;
                }
                if (current) {
                    lastPresentComponentIdx = i;
                }

            }
            return compressed.map(function(delta) {
                delta.priority = delta.priority || addressComponentsSpecificity[delta.index].priority || 0;
                delta.delta    = [];
                for (var j = 0; j < geocodeResults.length; j++) {
                    delta.delta.push(geocodeResults[j][delta.index]);
                }
                return delta;
            });
        }
        var compressed = compress();
        var resultIndeces = [];
        for (var p = 7; p >= -1 && resultIndeces.length < verbosity; p--) {
            for (var i = 0; i < compressed.length && resultIndeces.length < verbosity; i++) {
                if (compressed[i].priority == p) {
                    resultIndeces.push(i);
                }
            }
        }
        resultIndeces = resultIndeces.sort();
        var result = geocodeResults.map(function(){return [];});
        for (var i = 0; i < resultIndeces.length;i++) {
            var delta = compressed[resultIndeces[i]].delta;
            for (var j = 0; j < delta.length; j++) {
                result[j].unshift(delta[j]);
            }
        }
        result = result.map(function(elem) {
            return elem.join(", ");
        });
        if (unpack) {
            result = result[0];
        }
        return result;
    }
};
