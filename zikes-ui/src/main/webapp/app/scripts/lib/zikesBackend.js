/**
 * scripts/lib/zikes.js
 *
 * This handles communication with the zikes-api, i.e.: the backend.
 */

'use strict';

const http = require("./http.js");
const auth = require("../ui/common/auth.js");
const when = require("when");
const turf = {
    union : require("@turf/union"),
    polygon: require("turf-helpers").polygon
};

const zikesBackend = (function() {

    function mergePolygons(polygons, maxSqrDistance, minVertices) {
        function tarten(polygon) {
            if (polygon[0] != polygon[polygon.length-1]) {
                polygon.push(polygon[0]);
            }
            return [polygon];
        }

        return polygons.reduce(function(cum, polygon) {
            return turf.union(cum, turf.polygon(tarten(polygon)));
        }, turf.polygon(tarten(polygons[0]))).geometry.coordinates.reduce(function(cum, lpoly) {
            return cum.concat(
                lpoly.filter(function(poly) {
                    return poly.length > minVertices;
                }).map(function(poly) {
                    return poly.reduce(function(all, point) {
                        if (all.length) {
                            var prev = all[all.length-1];
                            var sqrDistance = Math.pow(prev[0]-point[0], 2) + Math.pow(prev[1]-point[1], 2);
                            if (sqrDistance < maxSqrDistance) {
                                return all;
                            }
                        }
                        all.push(point);
                        return all;
                    }, []);
                })
            );
        }, []);
    }


    const KZikesBackendUrl = document.location.hostname != "localhost" ? "https://api-dot-zikes-web-client.appspot.com" : "http://localhost:8080";
    /****************************************************************************
     *
     *
     * Communication with the routing server
     *
     *
     ****************************************************************************/
    var pendingRequest = undefined;
    function requestRoute(coordinates, preferences) {
        if (pendingRequest) {
            return when.reject(new Error("busy"));
        }
        var coordinatesStr = "";
        for (var i = 0; i < coordinates.length; i++) {
            var milestone = coordinates[i];
            coordinatesStr+=milestone[0]+","+milestone[1];
            if (i+1 < coordinates.length) {
                coordinatesStr += ",";
            }
        }

        return http.post(KZikesBackendUrl + "/route?milestones="+coordinatesStr, preferences)
        .otherwise(function(reason) {
            if (reason.message) {
                reason = reason.message;
                try {
                    reason = JSON.parse(reason);
                } catch(err) {}
            }
            var error = new Error("Unable to route");
            error.reason = reason;
            throw error;
        });
    }
    var defaultRoutingPrefs = {};
    var metaPromise = undefined;
    function meta() {
        if (!metaPromise) {
            metaPromise = http.get(KZikesBackendUrl + "/meta.json").then(function(meta) {
                defaultRoutingPrefs = {};
                meta.defaultRoutingPrefs.map(function(preference){
                    defaultRoutingPrefs[preference.metadata.opaque.name] = preference;
                });
                if (meta.map.coverage) {
                    meta.map.coverage = mergePolygons(meta.map.coverage, 0.5, 20);
                }
                return meta;
            });
        }
        return metaPromise;
    }



    /****************************************************************************
     *
     *
     * Authentication with zikes-api function for the journey/user meta
     *
     *
     ****************************************************************************/
    var authProviderHeaderName = "Authorization-Provider";
    var authTokenHeaderName    = "Authorization";

    function authHeaders(authenticatedUser) {
        var result = {};
        result[authProviderHeaderName] = authenticatedUser.authResponse.network.toUpperCase();
        result[authTokenHeaderName]    = "Bearer "+authenticatedUser.authResponse.access_token; //Bearer see: http://self-issued.info/docs/draft-ietf-oauth-v2-bearer.html
        return result;
    }

    var userData = when.defer();
    auth.on("logout", function() {
        userData = when.defer();
    });
    auth.on("login", function(authenticatedUser) {
        authenticatedUser = authenticatedUser.user;
        return new http.HttpRequest(
            KZikesBackendUrl + "/users/metadata"
        ).headers(
            authHeaders(authenticatedUser)
        ).get().then(function(zikesUserMetaData) {
            if (zikesUserMetaData.opaque.name != authenticatedUser.userDetails.name) {
                //first time see this user, let's write what we
                //know of them
                new http.HttpRequest(
                    KZikesBackendUrl + "/users/metadata"
                ).headers(
                    authHeaders(authenticatedUser)
                ).put({
                    opaque : {
                        name: authenticatedUser.userDetails.name,
                        email: authenticatedUser.userDetails.email,
                        encodedId: zikesUserMetaData.encodedId
                    }
                });
            }
            userData.resolve({
                authenticatedUser : authenticatedUser,
                zikesUserMetaData : zikesUserMetaData
            });
        });
    });

    function login(speculative) {
        var prompt = "Login to complete action";
        if (speculative) {
            prompt = undefined;
        }
        return auth.login(prompt)
        .then(function(authenticatedUser) {
            if (authenticatedUser) {
                return userData.promise;
            }
        });
    }

    function isLoggedIn() {
        return userData.promise.inspect().state == "fulfilled";
    }

    function userId() {
        if (isLoggedIn() ) {
            return userData.promise.inspect().value.zikesUserMetaData.encodedId;
        }
    }


    /****************************************************************************
     *
     * Authenticated calls
     *
     ***************************************************************************/
    function userPath(uid) {
        return "/users/"+uid;
    }

    function userPlanPath(uid) {
        return userPath(uid)+"/plan";
    }

    function ifAuthenticated(url, method, data) {
        return authenticated(url, method, data, true);
    }

    function authenticated(url, method, data, speculative) {
        return login(speculative).then(function(userData) {
            if (userData) {
                url = typeof url == "function" ? url(userData.zikesUserMetaData.encodedId) : url;
                return new http.HttpRequest(
                    KZikesBackendUrl + url
                ).headers(
                    authHeaders(userData.authenticatedUser)
                )[method](data);
            }
        });
    }



    /****************************************************************************
     *
     *
     * Journeys
     *
     *
     ****************************************************************************/
    class Journey {

        constructor(kind) {
            this.kind = kind;
        }

        _path(id, uid) {
            id = id && ("/"+id);
            return userPath(uid)+"/"+this.kind+"/journeys"+(id || "");
        }

        list() {
            return authenticated(this._path.bind(this, undefined), "get");
        }

        delete(id) {
            return authenticated(this._path.bind(this, id), "delete");
        }

        upload(takenJourney) {
            return authenticated("/trackUpload", "post", takenJourney);
        }

        save(journey) {
            self = this;
            const method = journey.metadata.id ? "put" : "post";
            return login().then(function(userData) {
                return new http.HttpRequest(
                    KZikesBackendUrl + self._path(journey.metadata.id, userData.zikesUserMetaData.encodedId)
                ).headers(
                    authHeaders(userData.authenticatedUser)
                )[method](journey).then(function(response) {
                    journey.metadata        = response;
                    journey.metadata.userId = userData.zikesUserMetaData.encodedId;
                    return journey.metadata;
                });
            });
        }

        get(id, uid) {
            self = this;
            return when(login(!!uid)).then(function(userData) {
                uid = uid || userData.zikesUserMetaData.encodedId;
                var req = new http.HttpRequest(
                    KZikesBackendUrl + self._path(id, uid)
                );
                if (userData) {
                    req.headers(
                        authHeaders(userData.authenticatedUser)
                    );
                }
                return req.get().then(function(journey) {
                    if (userData) {
                        //this is the current user's journey - lets make it easy to share
                        journey.metadata.userId = userData.zikesUserMetaData.encodedId;
                    } else {
                        //this is someone else's journey, lets make it easy to copy
                        delete journey.metadata["id"];
                        delete journey.metadata.opaque["autosave"];
                    }
                    return journey;
                });
            });
        }
    }






    /****************************************************************************
     *
     *
     * Preferences
     *
     *
     ****************************************************************************/
    var userPreferences = {
        metadata: {
            access: {
                ro: "",
                rw: ""
            },
            opaque: {
                type: "user"
            }
        },
        preferences: {
            currentProfile: "vanilla bicycle"
        }

    };

    var userProfiles = {};
    function allProfiles() {
        var result = {}
        for (var key in defaultRoutingPrefs) {
            result[key] = defaultRoutingPrefs[key];
        }
        for (var key in userProfiles) {
            result[key] = userProfiles[key];
        }
        return result;
    }

    function userPlanPreferencesPath(uid) {
        return userPlanPath(uid)+"/preferences"
    }

    function userPlanPreferencePath(id, uid) {
        return userPlanPreferencesPath(uid)+"/"+(id || "");
    }

    var preferenceListerners = [];
    var profileListeners     = [];
    var preferencesLoaded    = when(userData.promise).then(function(userData) {
        return new http.HttpRequest(
            KZikesBackendUrl + userPlanPreferencesPath(userData.zikesUserMetaData.encodedId)
        ).headers(
            authHeaders(userData.authenticatedUser)
        ).get().then(function(allPreferenceMetas) {
            return when.all(
                allPreferenceMetas.map(function(preference) {
                    return getPreference(preference.id);
                })
            )
        }).then(function(allPreferences) {
            allPreferences.map(function(preference) {
                if (preference.metadata.opaque.type == "user") {
                    userPreferences = preference;
                } else {
                    userProfiles[preference.metadata.opaque.name] = preference;
                }
            });
        }).then(function() {
            preferenceListerners.map(function(cb) {
                cb(userPreferences.preferences);
            });
            profileListeners.map(function(cb) {
                cb(allProfiles());
            });
        });
    });

    function getProfiles(cb) {
        var result = allProfiles()
        if (cb) {
            profileListeners.push(cb);
            cb(result);
        }
        return result;
    }

    function getPreferences(cb) {
        if (cb) {
            preferenceListerners.push(cb)
            cb(userPreferences.preferences);
        }
        return userPreferences.preferences;
    }

    function setPreferences(newPreferences) {
        for (var k in newPreferences) {
            userPreferences.preferences[k] = newPreferences[k];
        }
        if (preferencesLoaded.inspect().state == "fulfilled") {
            when(userData.promise).then(function(userData) {
                var req = new http.HttpRequest(
                    KZikesBackendUrl + userPlanPreferencePath(userPreferences.metadata.id, userData.zikesUserMetaData.encodedId)
                ).headers(
                    authHeaders(userData.authenticatedUser)
                );
                if (userPreferences.metadata.id) {
                    //update
                    req = req.put(userPreferences);
                } else {
                    //create
                    req = req.post(userPreferences);
                }
                return req.then(function(response) {
                    userPreferences.metadata = response;
                    preferenceListerners.map(function(cb) {
                        cb(userPreferences.preferences);
                    });
                });
            });
        }
    }



    function deleteProfile(id) {
        return authenticated(userPlanPreferencePath.bind(null, id), "delete");
    }

    function getPreference(id) {
        return authenticated(userPlanPreferencePath.bind(null, id), "get");
    }

    function saveProfile(profile) {
        const method = profile.metadata.id ? "put" : "post";
        return login().then(function(userData) {
            return new http.HttpRequest(
                KZikesBackendUrl + userPlanPreferencePath(profile.metadata.id, userData.zikesUserMetaData.encodedId)
            ).headers(
                authHeaders(userData.authenticatedUser)
            )[method](profile).then(function(response) {
                profile.metadata = response;
                return profile.metadata;
            });
        });
    }


    return {
        requestRoute         : requestRoute,
        meta                 : meta,
        getPreferences       : getPreferences,
        setPreferences       : setPreferences,
        getProfiles          : getProfiles,
        saveProfile          : saveProfile,
        deleteProfile        : deleteProfile,
        journeys : {
            planned          : new Journey("plan"),
            taken            : new Journey("uploaded")
        },
        authenticated        : authenticated,
        ifAuthenticated      : ifAuthenticated,
        isLoggedIn           : isLoggedIn,
        newMeta              : function(opaque) {
            return {
                opaque: opaque || {},
                access: {
                    ro: "",
                    rw: ""
                }
            }
        }
    };
})();

module.exports = zikesBackend;