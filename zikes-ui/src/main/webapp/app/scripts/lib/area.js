/**
 * scripts/lib/area.js
 *
 * This, based on areas present on the map, adds the preference that
 * avoids/prfers them
 */

'use strict';

var areaPreference = (function() {

    class CirclesPreference {

        label() {
            return "areas";
        }

        fromJSON(json) {
            const self = this;
            this.circles().forEach((c) => {
                self.delete(c);
            });
            json && json.forEach((circle) => {
                self.add(circle);
            });
        }

        toJSON() {
            return Array.from(this.circles()).map((circle) => {
                circle = circle.geometry();
                return {
                    "centre" : circle.centre,
                    "radiusMts" : Math.round(circle.radiusMts),
                    "punishFactor" : 16
                }
            })
        }

        generatePreference(rootJson) {
            rootJson["circles"] = this.toJSON();
        }
    }

    return {
        CirclesPreference: CirclesPreference,
    }

})();

module.exports = areaPreference;