/**
 * scripts/lib/movement.js
 */

'use strict';
const utils       = require("./utils.js");
const geometry    = require("./geometry.js");
const trackModule = require("./track.js");

const movement = (function() {

    const KMaxAccelerationMpss   = (30/6); //reference to 100kph (30mps) in 6 seconds
    const KMovementDetectSamples = 4;      //trace so many datapoints back to detect
                                           //consistent movement
    const KMovementDistanceRatio = 0.38;   //The algorithm concludes consistent movement
                                           //across n samples, if and when the ratio of
                                           //- the start->end crow flight distance
                                           //to
                                           //- cululative distance between start and end
                                           //exceeds this ratio - namely, the signal exhibits
                                           //a clear direction of movement instead of wiggling
                                           //about the same spot (GPS signal noise)
    const KMaxDistanceMts        = 200;
    const KMaxTimeDeltaSec       = 30;
    const KMaxSpeedPercentile    = 5;
    const KTrackGlue             = { //default track glue
                                    ms  :  utils.mins2ms(30), //tracks will not merge if separated by more than this many millis
                                    mts : 100                 //tracks will not merge if separated by more than this many metres
    };

    class Movement extends trackModule.Plugin {

        constructor(track, tsExtract, vExtract, options) {
            super(track, "mov", ["durationSec"], options);
            this.tsValExtract = this._valueExtractor(tsExtract);
            this.v  = track.mountPlugin(new trackModule.StatsPlugin(track, "v", vExtract, ["avg"], {
                max: {
                    percentile : KMaxSpeedPercentile
                }
            }));
            const self = this;
            Object.defineProperty(this, "durationSec", {
                get: function() {
                    let it = self.track.iterator();
                    return it.current() ? (self.tsValExtract(it.back().current().point) - self.tsValExtract(it.front().current().point))/1000 : 0;
                },
                enumerable: true,
                configurable: false
            });
            Object.defineProperty(this, "ts", {
                get: function() {
                    let it = self.track.iterator();
                    if (it.current()) {
                        return {
                            start : self.tsValExtract(it.front().current().point),
                            end   : self.tsValExtract(it.back().current().point)
                        }
                    }
                },
                enumerable: true,
                configurable: false
            });
        }

        clone(track) {
            return new Movement(
                track,
                this.tsValExtract,
                this.v.valExtract,
                this.options
            );
        }

        toJSON() {
            return utils.mergeSimpleObjects(super.toJSON(), {
                ts : this.ts
            });
        }

        appendTrack(track) {
            return this.v.appendTrack(track);
        }

        wrap(plugins) {
            this.v.wrap();
            let elems;
            if (plugins) {
                //now we calculate a track concatenation differently as we need to know
                //what's the avg speed when the user actually travels, not when they rest
                //in between the tracks
                elems = plugins.reduce(function(elems, plugin) {
                    elems.distanceMts += plugin.track.distanceMts;
                    elems.durationSec += plugin.durationSec;
                    return elems;
                }, {distanceMts :0, durationSec:0});

            } else {
                elems = {
                    distanceMts :this.track.distanceMts,
                    durationSec: this.durationSec
                };
            }
            this.v.avg = elems.distanceMts/elems.durationSec;
        }

        append(point) {
            let event;
            if (!this.v.append(point).value) {
                const self = this;
                let v, prevPt;
                if (this.track.length) {
                    let dts, dist, tries  = KMovementDetectSamples;;
                    prevPt = this.track.iterator().back().backward().find(function(prevPt) {
                        dist = point.distanceTo(prevPt);
                        dts  = (self.tsValExtract(point) - self.tsValExtract(prevPt))/1000;
                        utils.assert(dts >= 0, "position fed against chrononology");
                        tries--;
                        //momentary speed - keep recaculating until dts exceeds 1sec
                        if (v == undefined || dts < 1) {
                            if (dts > KMaxTimeDeltaSec || dist > KMaxDistanceMts) {
                                v = undefined;
                                tries = 0;
                            } else {
                                v = dist/dts;
                            }

                            let acc = Math.abs((v - self.v.valueOf(prevPt))/dts);
                            if (acc > KMaxAccelerationMpss) {
                                //accelleration came out silly - spread further
                                v = undefined;
                            }
                        }
                        return !tries;
                    });
                } else {
                    v = 0;
                }

                if (v == undefined) {
                    point = undefined;
                } else {
                    if (prevPt) {
                        if (v) {
                            prevPt = prevPt.current().point;
                            let crowFlightDist = prevPt.distanceTo(point);
                            let cumDistance    = point.frmStrt - prevPt.frmStrt
                            let ratio = crowFlightDist/cumDistance;
                            if (ratio < KMovementDistanceRatio) {
                                v = 0;
                            }
                        }
                        if (v == 0) {
                            if (this.moving) {
                                delete this["moving"];
                                event = { type : "stopped" };
                            }
                        } else if (!this.moving) {
                            this.moving = true;
                            event = { type : "moving" };
                        }
                    }
                    this.v.valueOf(point, v);
                    this.v.append(point);
                }
            }
            return {
                point : point,
                event : event
            };
        }
    }
    Movement.create = function(tsExtract, vExtract, track) {
        return new Movement(track, tsExtract, vExtract)
    }

    const newTrack = function() {
        return new trackModule.Track(
            [Movement.create.bind(null, ["d3c","ts"], ["d3c","v"])]
        )
    }


    /*
     * "Leads" strategy of dealing with gsm noise.
     *
     * maintains up to options.maxLeadsNo leads favoring (see .append) the
     * most promising (.moving) one.
     */
    class Track extends utils.Subscribable {

        constructor(options) {
            super();
            const self                = this;
            this.options              = options || {};
            this.options.maxLeadsNo   = this.options.maxLeadsNo || 3;
            this.wrap();
            Object.defineProperty(this, "length", {
                get: function() {
                    return self.leading().length;
                },
                enumerable: true,
                configurable: false
            });
            Object.defineProperty(this, "mov", {
                get: function() {
                    return self.leading().mov;
                },
                enumerable: true,
                configurable: false
            });
            Object.defineProperty(this, "distanceMts", {
                get: function() {
                    return self.leading().distanceMts;
                },
                enumerable: true,
                configurable: false
            });
        }

        leading() {
            return this.moving || this.mTracks[0];
        }

        wrap() {
            let result = this.mTracks && this.leading().wrap();
            delete this["moving"];
            this.mTracks = [newTrack()];
            return result;
        }

        fire(type) {
            super.fire(type, {track:this.leading()});
        }

        append(position) {
            const self = this;
            function dropTrack(idx) {
                let dropped = self.mTracks.splice(idx,1)[0];
                if (self.moving === dropped) {
                    self.fire("stopped");
                    delete self["moving"];
                }
            }
            function purge(abandonedSinceMs) {
                for (var i = 0; i < self.mTracks.length; i++) {
                    let track = self.mTracks[i];
                    if (track.back() && track.back().d3c.ts <= abandonedSinceMs ) {
                        dropTrack(i--);
                    }
                }
                if (self.mTracks.length == 0) {
                    self.mTracks = [newTrack()];
                }
            }

            let found;
            this.mTracks.find(function(track, idx) {
                if (track.append(position)) {
                    found = idx;
                    if (self.moving) {
                        if (!track.mov.moving) {
                            self.fire("stopped");
                            delete self["moving"];
                        }
                    } else if (track.mov.moving) {
                        self.fire("moving");
                        self.moving = track;
                    }
                    return true;
                }
            })
            if (found != undefined) {
                if (found != 0) {
                    this.mTracks = this.mTracks.splice(found, 1).concat(this.mTracks);
                }
                if (this.moving) {
                    this.mTracks = [this.mTracks[0]];
                }
            } else {
                let track = newTrack();
                track.append(position);
                this.mTracks.unshift(track);
                this.mTracks.length <= this.options.maxLeadsNo || dropTrack(0);
            }

            clearTimeout(this.mPurgeTimeout);
            this.mPurgeTimeout = setTimeout(purge.bind(null, position.d3c.ts), KMaxTimeDeltaSec*1000);
        }
    }



    /*
     * Can group/concatenate instances of trackModule.Track into trackModule.Tracks
     * based on the acceptCb criterion.
     * As users mount their bikes and travel, they stop at red lights and/or shops
     * and this would be enough for Movement plugin to terminate a track.
     * but from user's perspective this can be too granular.
     */
    class Tracks {

        constructor(acceptCb) {
            this.acceptCb = acceptCb;
            this.tracks   = [];
            Object.defineProperty(this, "length", {
                get: function() {
                    return this.tracks.length
                },
                enumerable: true,
                configurable: false
            });
        }

        append(track) {
            utils.assert(!this.mLocked, "Tracks is locked");
            const self = this;
            if (Array.isArray(track)) {
                track.forEach(function(trk) {
                    self.append(trk);
                });
            } else {
                track = track instanceof trackModule.Track ? track : newTrack().fromJSON(track);
                track.wrap();
                let frontPoint = track.iterator().current();
                if (!frontPoint) {
                    return;
                }
                let backTrack = this.tracks.back();
                let backPoint = backTrack && backTrack.iterator().back().current();
                let host;
                if (backPoint && this.acceptCb(backPoint.point, frontPoint.point)) {
                    host = backTrack;
                } else {
                    backTrack && backTrack.wrap();
                    host = new trackModule.Tracks();
                    this.tracks.push(host);
                }
                host.append(track);
            }
            return this;
        }

        wrap() {
            this.tracks.back() && this.tracks.back().wrap();
            this.mLocked = true;
            return this;
        }

        filter(cb) {
            this.tracks = this.tracks.filter(cb);
            return this;
        }

        toJSON() {
            return this.tracks.map(function(track) {
                return track.toJSON();
            });
        }

        front() {
            return this.tracks.front();
        }

        back() {
            return this.tracks.back();
        }
    }

    /*
     * A TimeDistanceTracks is a Tracks instance that groups tracks based on
     * how far they are from each other wrt of time and distance. So a number
     * of trackModule.Track instances can be concatenated into one notional track
     * if what separates them is a couple of metres and minutes.
     */
    class TimeDistanceTracks extends Tracks {
        constructor(glue) {
            super(function(prev,next) {
                return (next.d3c.ts - prev.d3c.ts < glue.ms) && (next.distanceTo(prev) < glue.mts);
            });
        }
    }

    return {
        track              : {
            Plugin         : Movement,
            create         : newTrack
        },
        Track              : Track,
        TimeDistanceTracks : TimeDistanceTracks,
        TrackGlue          : KTrackGlue
    }

})();

module.exports = movement;
