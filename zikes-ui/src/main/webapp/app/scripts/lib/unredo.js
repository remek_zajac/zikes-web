/**
 * scripts/lib/unredo.js
 *
 * Engine for undo/redo with command pattern
 */

'use strict';

var unredo = (function() {

    $(document).on('keydown', function(e){
        if(e.ctrlKey && e.key === 'z') {
            undo();
            e.preventDefault();
        }
    });

    var KMaxStackDepth = 20;
    var undoStack      = [];
    function undo() {
        //undo may choose to explicitly return false, in which case it will be ignored as noop
        //this is useful in changing undo strategies.
        while (undoStack.length > 0 && undoStack.pop().undo() === false) {}
    }

    function push(checkpoint) {
        if (checkpoint) {
            undoStack.push(checkpoint);
            if (undoStack.length > KMaxStackDepth) {
                undoStack.shift();
            }
        }
    }

    function reset() {
        undoStack = [];
    }

    function deleteCheckpoints(filter) {
        while(true) {
            var deleteIdx = undoStack.findIndex(filter);
            if (deleteIdx == -1) {
                return;
            }
            undoStack.splice(deleteIdx, 1);
        }
    }

    function Checkpoint(action, context) {
        this.mUndo    = Array.isArray(action) ? action : [action];
        this.mContext = context;
    }

    Checkpoint.prototype.undo = function() {
        return this.mUndo.reduce(function(cum, cp) {
            var result = false;
            if (cp) {
                if (cp instanceof Checkpoint) {
                    result = cp.undo();
                } else if (typeof cp === "function") {
                    result = cp();
                }
            }
            return cum || result;
        }, false);
    }

    Checkpoint.prototype.push = function(action) {
        this.mUndo.push(action);
        return this;
    }

    Checkpoint.prototype.context = function(action) {
        return this.mContext;
    }

    return {
        push       : push,
        undo       : undo,
        reset      : reset,
        delete     : deleteCheckpoints,
        Checkpoint : Checkpoint
    }

})();

module.exports = unredo;