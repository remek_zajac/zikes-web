/**
 * scripts/lib/highways.js
 *
 * This, based on the value of the master slider,
 * and values of subordinate (per-highway) sliders
 * calculates and return the highway routing preference
 */

'use strict';

const when     = require("when");
const mustache = require("mustache");
const unredo   = require("./unredo.js");
const track    = require("./track.js");
const utils    = require("./utils.js");
const isMobile = utils.browserFeaturesFromUserAgent(navigator.userAgent).mobile;

const highwaysPreference = (function() {

    const verticalSliders = when.promise((resolve, reject) => {
        $.get("/templates/tools.template.html", function(template, textStatus, jqXhr) {
            resolve({
                special : $(template).filter("#routingPrefVerticalSlider_tmpl").html(),
                wtext   : $(template).filter("#routingPrefVerticalSliderWText_tmpl").html()
            });
        });
    });

    const KVerticalSliderSpreadFactor = 10;
    const KDistanceFunctionSlope = 1;
    const KHighwaysMeta = {
        "path" : {
            color: "#996633"
        },
        "track" : {
            color: "#c68c53"
        },
        "bridleway" : {
            foldInto : "track"
        },
        "pedestrian" : {
            color: "#ffff66"
        },
        "footway" : {
            foldInto : "pedestrian"
        },
        "cycleway" : {
            color: "#80ff80"
        },
        "residential" : {
            color: "#ffffff"
        },
        "living_street" : {
            foldInto: ["residential"]
        },
        "road" : {
            color: "#cce0ff"
        },
        "tertiary_link" : {
            foldInto: "road"
        },
        "unclassified" : {
            foldInto: "road"
        },
        "bus_guideway" : {
            foldInto: "road"
        },
        "service" : {
            foldInto: "road"
        },
        "tertiary" : {
            foldInto: "road"
        },
        "secondary" : {
            color: "#99c2ff"
        },
        "secondary_link" : {
            foldInto: "secondary"
        },
        "primary": {
            color: "#66a3ff"
        },
        "primary_link" : {
            foldInto: "primary"
        },
        "trunk" : {
            color: "#3385ff"
        },
        "trunk_link" : {
            foldInto: "trunk"
        },
        "motorway" : {
            color: "#0066ff"
        },
        "motorway_link" : {
            foldInto: "motorway"
        },
        "ferry" : {
            color   : "#0e90d2",
            special : {
                icon : "/graphics/ferry.png",
                baseFlatCost : 500
            }
        },
        "steps" : {
            color   : "#0e90d2",
            special : {
                icon : "/graphics/stairs.png"
            }
        }
    };

    class Highway {
        constructor(highways, name, id, meta) {
            this.mHighways = highways;
            this.mName     = name;
            this.mId       = id;
            this.mMeta     = meta;
            this.mIncluded = [];
        }

        name() {
            return this.mName;
        }

        special() {
            return this.mMeta.special;
        }

        id() {
            return this.mId;
        }

        color() {
            return this.root().mMeta.color;
        }

        included() {
            return this.mIncluded;
        }

        root() {
            if ("foldInto" in this.mMeta) {
                return this.mHighways.highway(this.mMeta.foldInto);
            }
            return this;
        }
    }


    class Highways {

        constructor(highways) {
            const self = this;
            this.mHighwaysById = {};
            for (var name in highways) {
                var id = highways[name];
                this.mHighwaysById[id] = name;
            }
            this.mHighways = {};
            for (var name in KHighwaysMeta) {
                this.mHighways[name] = new Highway(
                    this, name, this.mHighwaysById[name], KHighwaysMeta[name]
                );
            }
            this.mSpecialHighways = [];
            for (var name in this.mHighways) {
                var highway = this.mHighways[name];
                if ("foldInto" in highway.mMeta) {
                    this.mHighways[highway.mMeta.foldInto].mIncluded.push(highway);
                }
                if ("special" in highway.mMeta) {
                    this.mSpecialHighways.push(highway);
                }
            }
            this.mBySize = [
                "path","pedestrian","track","cycleway","residential","road","secondary","primary","trunk","motorway"
            ].map(function(name) {
                return self.mHighways[name];
            });
        }

        highway(idOrName) {
            var name = idOrName;
            if (typeof idOrName === 'number') {
                name = this.mHighwaysById[name];
            }
            return this.mHighways[name];
        }

        bySize() {
            return this.mBySize;
        }

        specialHighways() {
            return this.mSpecialHighways;
        }
    }


    /******************************************************************
     *
     * HighwaySlider
     * Managing a per-highway slider that produces values between:
     *
     * * 0 - slider all the way down -> MAX punishment
     * * 1 - slider all the way up   -> MIN (Neutral) punishment
     *
     * @highway    : a Highway object to represent
     * @resolution : the slider's resolution - so that the caller can have
     *               chance of implementing its slider cascades in a
     *               controlled way.
     *
     *****************************************************************/
    class HighwaySlider {

        constructor(highway, resolution, parent, template) {
            this.mParent     = parent;
            this.mHighway    = highway;
            this.mMax        = resolution;
            this.mTemplate   = template;
            this.mValue      = 0;
        }

        /*
         * _e2iValue & _i2eValue are functions transforming between:
         * * internal (UI) slider of values [0..resolution]
         * * external                       [1..0]
         * The slider ui component that we use doesn't accept a min greater than
         * max, but conceptually and visually, when this slider is half-way down
         * it communicates being half full of approval for this type of
         * highway and when it's all the way down, it conveys zero approval and
         * thus so are the values it gives to the outside world.
         */
        _e2iValue(externalValue) {
            var result = (1-externalValue) * this.mMax;
            return result;
        }

        _i2eValue(interalValue) {
            var result = 1-(interalValue/this.mMax);
            return result;
        }

        render(panel, widthpx) {
            const self = this;
            panel.append(
                mustache.render(this.mTemplate, {
                    label: this.mHighway.name(),
                    min: {
                        value : 0
                    },
                    max: {
                        value : this.mMax
                    },
                    value: this.mValue,
                    widthpx: widthpx
                })
            );
            this.mSlider = $("#"+this.mHighway.name()+"_slider").slider();
            const parent = this.mSlider.parent();
            const divs = {
                parent    : parent,
                handle    : parent.find(".slider-handle"),
                track     : parent.find(".slider-track"),
                selection : parent.find(".slider-selection")
            }
            if (this.mHighway.special() && this.mHighway.special().icon) {
                divs.handle.html('<img src="'+this.mHighway.special().icon+'" style="width:20px;height:20px"></img>');
            }
            divs.track.css("background", "#2b3e50");
            divs.selection.css("background", this.mHighway.color());
            divs.parent.css("margin-right", "4px");
            this.mSlider.on('slideStop', this.invalidate.bind(this));
            return divs;
        }

        name() {
            return this.mHighway.name();
        }

        included() {
            return this.mHighway.included();
        }

        setValue(value) {
            this.mSlider.setValue(value);
        }

        invalidate() {
            if (this.mValue != this.mSlider.getValue()) {
                const self = this;
                const prevValue = this.mValue;
                this.mValue     = this.mSlider.getValue();
                //only act when the new slider value has changed
                unredo.push(
                    new unredo.Checkpoint(function() {
                        self.setValue(prevValue);
                        self.mParent.mParent.invalidate();
                    })
                );
                self.mParent.mParent.invalidate();
                return true;
            }
        }

        toJSON() {
            return {
                value: this.value()
            }
        }

        fromJSON(json) {
            this.mValue = 0;
            if (json && "value" in json) {
                this.mValue = this._e2iValue(json.value);
            }
            this.mSlider.setValue(this.mValue);
        }

        //strictly returns 0..1 from slider min..max
        value() {
            return this._i2eValue(this.mSlider.getValue());
        }
    }


    /******************************************************************
     *
     * InfluencedHighwaySlider
     *
     * A HighwaySlider that is influenced (adjusted, see adjustTo) by the master
     * slider and in addition it may have been manipulated manaually (has
     * this.mManualInternalValue set).
     *
     * @highway    : a Highway object to represent
     * @resolution : the slider's resolution - so that the caller can have
     *               chance of implementing its slider cascades in a
     *               controlled way.
     *
     *****************************************************************/
    class InfluencedHighwaySlider extends HighwaySlider {

        constructor(highway, resolution, parent, template) {
            super(highway, resolution, parent, template);
            this.mDefaultInternalValue = 0;
        }

        render(panel, width, padding) {
            let margin  = 5;
            width -= margin;
            let widthpx = width + "px";
            const divs = super.render(panel, widthpx);
            divs.track.css("width", widthpx);
            divs.handle.css("display", "none");
            divs.handle.css("width", widthpx);
            if (isMobile) {
                margin += width-21; //TODO: awful fudge
            }
            padding && divs.parent.css("margin-right", margin+4+"px");
        }

        toJSON() {
            if (this.mManualInternalValue) {
                return super.toJSON();
            }
        }

        invalidate() {
            if (this.mValue != this.mSlider.getValue()) {
                this.mManualInternalValue = this.mSlider.getValue();
            }
            return super.invalidate()
        }

        setValue(value) {
            if (value == this.mDefaultInternalValue) {
                //here's one way of 'unmanipulating' the slider - set it to the
                //same value as that resulting from the master slider.
                delete self["mManualInternalValue"];
            }
            super.setValue(value);
        }

        fromJSON(json) {
            delete this["mManualInternalValue"];
            var sliderValue = this.mDefaultInternalValue;
            if (json && "value" in json) {
                sliderValue = this._e2iValue(json.value);
                if (sliderValue != self.mDefaultInternalValue) {
                    this.mManualInternalValue = sliderValue;
                }
            }
            this.mSlider.setValue(sliderValue);
        }

        adjustTo(masterSliderFactor) {
            var newSliderValue = this.mDefaultInternalValue = this.mMax - (this.mMax * masterSliderFactor);
            if (this.mManualInternalValue) {
                newSliderValue =  this.mMax - ((this.mMax - this.mManualInternalValue) * masterSliderFactor);
            }
            this.mSlider.setValue(newSliderValue);
        }
    }







    class HighwaysPreference {

        constructor(parent, map) {
            this.mParent   = parent;
            this.mHighways = map.highways;
        }

        masterSlideOptions() {
            this.mMaxValue = this.mHighways.bySize().length*KVerticalSliderSpreadFactor;
            this.mMinValue = 0;
            return {
                label: "roads",
                value: JSON.stringify([this.mMinValue, this.mMaxValue]),
                min: {
                    value: this.mMinValue,
                    label: "smaller roads"
                },
                max: {
                    value: this.mMaxValue,
                    label: "bigger roads"
                },
                step: 1
            }
        }

        /*
         * Must strictly generate value between 0..1
         */
        _factorFromMasterSlider(masterSliderValue, x) {
            var leftSliderValue  = Math.min(masterSliderValue[0], masterSliderValue[1]);
            var rightSliderValue = Math.max(masterSliderValue[0], masterSliderValue[1]);
            x *= KVerticalSliderSpreadFactor;
            var adjust = 0;
            if (x <= leftSliderValue) {
                var distance = leftSliderValue-x;
                adjust = distance/this.mMaxValue;
            } else if (x >= rightSliderValue) {
                var distance = x-rightSliderValue+KVerticalSliderSpreadFactor;
                adjust = distance/this.mMaxValue;
            }
            return 1.0 - adjust;
        }

        render(panel) {
            const self   = this;
            const highwaySliderResolution = this.mHighways.bySize().length+(this.mHighways.specialHighways().length/2);
            //TODO: monstrocity. panel has zero width before the accordion uncollapses
            const highwaySliderWidthCss   = Math.round(panel.parent().parent().innerWidth()/highwaySliderResolution);
            return when(verticalSliders)
            .then(function(templates) {
                self.mAdjustableSliders = self.mHighways.bySize().map(function(hwy, idx) {
                    return new InfluencedHighwaySlider(hwy, highwaySliderResolution, self, templates.wtext);
                });
                self.mHighwaySliders = self.mHighways.specialHighways().map(function(hwy, idx) {
                    return new HighwaySlider(hwy, highwaySliderResolution, self, templates.special);
                }).concat(self.mAdjustableSliders).map((hwy, idx, arr) => {
                    hwy.render(panel, highwaySliderWidthCss, idx != arr.length-1);
                    return hwy;
                });
            });
        }

        fromMasterSlider(masterSliderValue, rootJson) {
            this.mAdjustableSliders.map((slider, idx) => {
                slider.adjustTo(
                    this._factorFromMasterSlider(masterSliderValue, idx)
                );
            })
            if (rootJson) {
                return this.generate(rootJson);
            }
        }

        generate(rootJson) {
            var result = {};
            for (var i = 0; i < this.mHighwaySliders.length; i++) {
                let highwaySlider = this.mHighwaySliders[i];
                let factor = highwaySlider.value() || (1.0/12); //TODO: fixme (this file is a bid shambles)
                factor = Math.pow(2,(1/factor))-1;
                let param = {
                    cost: factor.toFixed(2)
                };
                let special = highwaySlider.mHighway.special();
                if (special && special.baseFlatCost) {
                    param["flatCost"] = Math.round(special.baseFlatCost * factor);
                }
                result[highwaySlider.name()] = param;
                for (var j = 0; j < highwaySlider.included().length; j++) {
                    result[highwaySlider.included()[j].name()] = param;
                }
            }
            rootJson["highwayCostFactors"] = result;
        }

        fromJSON(json) {
            for (var i = 0; i < this.mHighwaySliders.length; i++) {
                var highwaySlider = this.mHighwaySliders[i];
                var sliderJSON = json ? json[highwaySlider.name()] : undefined;
                highwaySlider.fromJSON(sliderJSON);
            }
        }

        toJSON() {
            var result = {};
            for (var i = 0; i < this.mHighwaySliders.length; i++) {
                var highwaySlider = this.mHighwaySliders[i];
                var highwayJson   = highwaySlider.toJSON();
                if (highwayJson) {
                    result[highwaySlider.name()] = highwayJson;
                }
            }
            return result;
        }
    }


    class HighwaysTrackPlugin extends track.KeyExtractorPlugin {

        constructor(track, valExtract) {
            super(track, "hwy", valExtract);
        }

        clone(track) {
            return new HighwaysTrackPlugin(track, this.valExtract);
        }

        getAt(mts, itRange) {
            let result = super.getAt(mts, itRange);
            return result &&
                  ((result[0] && this.valueOf(result[0].current().point)) || //it's hwy type forward so try left bound first
                   (result[1] && this.valueOf(result[1].current().point)));  //and fallback to right
        }


        rollup(rolledUpTrack) {
            const self = this;
            return new track.RollupBase(this, rolledUpTrack, function(value, it) { //append
                let reject      = false;
                let prevVal     = value;
                if (this.prev) {
                    prevVal = this.prev.value;
                    reject  = prevVal != value;
                }
                let curFrmStrt  = it.current().frmStrt;
                this.distances[prevVal]  = this.distances[prevVal] || 0;
                this.distances[prevVal] += curFrmStrt - ((this.prev && this.prev.frmStrt) || 0);
                this.prev = {
                    value   : value,
                    frmStrt : curFrmStrt
                };
                return reject;
            }, function(pt, startIt, stopIt) { //rollup
                let longest;
                for (var hwy in this.distances) {
                    let dist = this.distances[hwy];
                    longest = (!longest || longest.dist < dist) ? {
                        hwy : hwy, dist : dist
                    } : longest;
                }

                if (this.rolledUpTrack.length) {
                    self.valueOf(
                        this.rolledUpTrack.iterator().back().current().point,
                        longest ? parseInt(longest.hwy) : (this.prev && this.prev.value)
                    );
                    this.distances   = {};
                }
                return pt;
            }, {distances : {}});
        }
    }
    HighwaysTrackPlugin.create = function(valExtract, track) {
        return new HighwaysTrackPlugin(track, valExtract)
    }


    return {
        HighwaysPreference  : HighwaysPreference,
        sliderGenerator     : HighwaysPreference,
        Highways            : Highways,
        track               : {
            Plugin          : HighwaysTrackPlugin
        }
    }

})();

module.exports = highwaysPreference;