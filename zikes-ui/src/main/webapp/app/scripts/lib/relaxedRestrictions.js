/**
 * scripts/lib/relaxedRestrictions.js
 *
 * This, based on the value of the master slider,
 * calculates and returns the relaxedRestrictions preference
 */

'use strict';

var relaxedRestrictionsPreference = (function() {


    function RelaxedRestrictionsSliderPreference(parent, jsonDocument) {
        this.mParent   = parent;
        this.mJSON     = jsonDocument;
    }

    var KSlider =  {
        min : 1,
        max : 6
    };
    RelaxedRestrictionsSliderPreference.prototype.masterSlideOptions = function() {
        var result = {
            label: "relaxRestrictions",
            value: KSlider.min,
            min: {
                value: KSlider.min,
                label: ""
            },
            max: {
                value: KSlider.max,
                label: "relax restrictions"
            },
            step: 0.3
        }
        this.mMasterSliderValue = result.value;
        return result;
    }


    RelaxedRestrictionsSliderPreference.prototype.fromMasterSlider = function(masterSliderValue, rootJson) {
        this.mMasterSliderValue = masterSliderValue;
        if (rootJson) {
            return this.generate(rootJson);
        }
    }

    RelaxedRestrictionsSliderPreference.prototype.render = function(panel) {
    }

    RelaxedRestrictionsSliderPreference.prototype.generate = function(rootJson) {
        if (this.mMasterSliderValue == KSlider.min) {
            return;
        }
        var factor = KSlider.max-this.mMasterSliderValue+KSlider.min;
        factor = Math.pow(factor,2);
        var highwayFactors = rootJson["highwayCostFactors"]
        for (var highwayType in highwayFactors) {
            var factors4HighwayType = highwayFactors[highwayType];
            factors4HighwayType["forbidden"] = (parseInt(factors4HighwayType["cost"]) * factor).toFixed(2);
        }
        highwayFactors["default"] = {
            forbidden: factor.toFixed(2)
        }
    }

    return {
        RelaxedRestrictionsSliderPreference: RelaxedRestrictionsSliderPreference,
        sliderGenerator : RelaxedRestrictionsSliderPreference
    }

})();

module.exports = relaxedRestrictionsPreference;