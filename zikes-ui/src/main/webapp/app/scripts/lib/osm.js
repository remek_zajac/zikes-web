/**
 * scripts/lib/osm.js
 *
 * Openstreetmap utensils
 */

'use strict';

const geometry = require("./geometry.js");

const osm = (function() {

    class Tile extends geometry.Point {

        constructor(z,x,y) {
            super(x,y);
            this.z = z;
        }

        west() {
            var x = this.x-1;
            if (x < 0) {
                x = Math.pow(2, this.z);
            }
            return new Tile(this.z, x, this.y);
        }

        east() {
            var x = this.x+1;
            var max = Math.pow(2, this.z);
            if (x > max) {
                x = 0;
            }
            return new Tile(this.z, x, this.y);
        }

        north() {
            var y = this.y-1;
            if (y < 0) {
                y = Math.pow(2, this.z);
            }
            return new Tile(this.z, this.x, y);
        }

        south() {
            var y = this.y+1;
            var max = Math.pow(2, this.z);
            if (y > max) {
                y = 0;
            }
            return new Tile(this.z, this.x, y);
        }

        neighbours() {
            return [
                this.north(),
                this.east(),
                this.south(),
                this.west()
            ]
        }

        nw() {
            return num2deg(this);
        }

        ne() {
            return num2deg(this.east());
        }

        se() {
            return num2deg(this.east().south());
        }

        sw() {
            return num2deg(this.south());
        }

        centre() {
            let nw = this.nw();
            let vector = new geometry.Vector(nw, this.se()).half();
            return new geometry.LatLng(
                nw.lat + vector.y,
                nw.lng + vector.x
            );
        }

        polygon() {
            return [
                this.nw(), this.ne(), this.se(), this.sw()
            ];
        }

        equals(other) {
            return super.equals(other) && this.z == other.z;
        }

        toString() {
            return "z:"+this.z+"x:"+this.x+"y:"+this.y;
        }
    }


    function deg2num(geoPoint, zoom) {
        const lat_rad  = geometry.toRad(geoPoint.lat);
        const n = Math.pow(2, zoom);
        const xtiled = (geoPoint.lng+180.0) / 360.0 * n;
        const ytiled = ((1.0-Math.log(Math.tan(lat_rad) + (1 / Math.cos(lat_rad))) / Math.PI) / 2.0 *n);
        return new Tile(zoom, Math.floor(xtiled), Math.floor(ytiled));
    }

    function num2deg(tile) {
        const n = Math.pow(2, tile.z);
        const lon_deg = tile.x / n * 360.0 - 180.0;
        const lat_rad = Math.atan(Math.sinh(Math.PI * (1 - 2.0 * tile.y / n)));
        return new geometry.LatLng(geometry.toDeg(lat_rad), lon_deg);
    }

    return {
        deg2num: deg2num,
        num2deg: num2deg,
        Tile   : Tile
    }

})();

module.exports = osm;
