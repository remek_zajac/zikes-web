/**
 * scripts/lib/indexDB.js
 *
 * Wrappers for indexed db object stores
 */

'use strict';

class IndexedDB {

    constructor(dbName, storesArray) {
        var self   = this;
        this.stores = {};
        this.dbReady = new Promise(function(resolve, reject) {
            console.log("Initialising "+dbName);
            let openDbCreateReq = indexedDB.open(dbName, 1);
            openDbCreateReq.onsuccess = function(event) {
                console.log(dbName+" initialised");
                resolve(event.target.result);
            };

            openDbCreateReq.onerror = function(event) {
                reject(new Error("IndexedDB not supported here"));
            };

            openDbCreateReq.onupgradeneeded = function(event) {
                let db = event.target.result;
                if (db.objectStoreNames.length == 0) {
                    storesArray.forEach(function(store) {
                        if ("onupgradeneeded" in store) {
                            store.onupgradeneeded(db);
                        } else {
                            let keyPath = store.keyPath || "key";
                            store = db.createObjectStore(store.name, { keyPath: keyPath });
                            store.transaction.oncomplete = function(event) {};
                        }
                    });
                }
            };
        });

        storesArray.forEach(function(store) {
            self.stores[store.name] = store;
            store.dbReady           = self.dbReady;
        });
    }

    ready() {
        return this.dbReady;
    }
}

class IndexedDBObjStore {

    constructor(name, options) {
        this.name = name;
        this.key  = function(key) {
            return (typeof key == "object") ? options.obj2Key(key) : key;
        }
        if (options && options.keyPath) {
            this.keyPath = options.keyPath;
        }
    }

    get(key) {
        const self = this;
        return this.dbReady.then(function(db) {
            return new Promise(function(resolve, reject) {
                let req = db.transaction([self.name])
                .objectStore(self.name)
                .get(self.key(key));

                req.onsuccess = function(event) {
                    if(event.target.result) {
                        resolve(self.keyPath ? event.target.result : event.target.result.value);
                    } else {
                        resolve();
                    }
                };
                req.onerror = function(err) {
                    reject(err);
                };
            });
        });
    }

    has(key) {
        const self = this;
        return this.dbReady.then(function(db) {
            return new Promise(function(resolve, reject) {
                let req = db.transaction([self.name])
                .objectStore(self.name)
                .count(IDBKeyRange.bound(self.key(key),self.key(key)));

                req.onsuccess = function(event) {
                    if(event.target.result) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                };
                req.onerror = function(err) {
                    reject(err);
                };
            });
        });
    }

    allKeys() {
        const self = this;
        return this.dbReady.then(function(db) {
            return new Promise(function(resolve, reject) {
                let req = db.transaction([self.name])
                .objectStore(self.name)
                .getAllKeys();

                req.onsuccess = function(event) {
                    if(event.target.result) {
                        resolve(event.target.result);
                    } else {
                        resolve();
                    }
                };
                req.onerror = function(err) {
                    reject(err);
                };
            });
        });
    }

    all() {
        const self = this;
        return this.dbReady.then(function(db) {
            return new Promise(function(resolve, reject) {
                let req = db.transaction([self.name])
                .objectStore(self.name)
                .getAll();

                req.onsuccess = function(event) {
                    if(event.target.result) {
                        resolve(event.target.result);
                    } else {
                        resolve();
                    }
                };
                req.onerror = function(err) {
                    reject(err);
                };
            });
        });
    }

    put(key, value) {
        if (this.keyPath) {
            if (value) {
                throw new Error("key expected within the value as this.keyPath is set");
            }
            value = key;
        } else if (value) {
            value = {key: this.key(key), value:value};
        } else {
            throw new Error("Must specify key and value in absence of this.keyPath");
        }

        const self = this;
        return this.dbReady.then(function(db) {
            return new Promise(function(resolve, reject) {
                let req = db.transaction([self.name], "readwrite")
                .objectStore(self.name)
                .put(value);

                req.onsuccess = function(event) {
                    resolve(true);
                };
                req.onerror = function(err) {
                    reject(err);
                };
            });
        });
    }

    delete(keyOrRange) {
        if (Array.isArray(keyOrRange)) {
            keyOrRange = IDBKeyRange.bound(this.key(keyOrRange[0]), this.key(keyOrRange[1]));
        } else {
            keyOrRange = this.key(keyOrRange);
        }
        const self = this;
        return this.dbReady.then(function(db) {
            return new Promise(function(resolve, reject) {
                let req = db.transaction([self.name], "readwrite")
                .objectStore(self.name)
                .delete(keyOrRange);

                req.onsuccess = function(event) {
                    resolve();
                };
                req.onerror = function(err) {
                    reject(err);
                };
            });
        });
    }

    clear(keyRange) {
        const self = this;
        if (keyRange) { return this.delete(keyRange); }
        return this.dbReady.then(function(db) {
            return new Promise(function(resolve, reject) {
                let req = db.transaction([self.name], "readwrite")
                .objectStore(self.name)
                .clear();

                req.onsuccess = function(event) {
                    resolve();
                };
                req.onerror = function(err) {
                    reject(err);
                };
            });
        });
    }
}


class TileCacheDb extends IndexedDB {

    constructor() {
        super(
            "vectorTileCacheDb",
            [new IndexedDBObjStore(
                "tile", {
                obj2Key : function(tile) {
                    if (typeof tile === "object") {
                        return "z:"+tile.z+"x:"+tile.x+"y:"+tile.y;
                    } else if (typeof tile === "string") {
                        return tile;
                    }
                    throw new Error("Unrecognised tile key "+JSON.stringify(tile));
                }
            })]
        );
    }
}

const tileCacheDb = new TileCacheDb();
if (typeof module !== "undefined") {
    module.exports = {
        tileCacheDb       : tileCacheDb,
        IndexedDB         : IndexedDB,
        IndexedDBObjStore : IndexedDBObjStore
    };
}
