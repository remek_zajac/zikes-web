/**
 * scripts/lib/utils.js
 * Library of utils
 */

'use strict';

var utils = (function() {

    var urlParams;
    (window.onpopstate = function () {
        var match,
            pl     = /\+/g,  // Regex for replacing addition symbol with a space
            search = /([^&=]+)=?([^&]*)/g,
            decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
            query  = window.location.search.substring(1);

        urlParams = {};
        while (match = search.exec(query))
           urlParams[decode(match[1])] = decode(match[2]);
    })();

    function assert(condition, message) {
        if (!condition) {
            message = "Assertion failed: '"+message+"'";
            console.error(message);
            if (window.location.hostname == "localhost") {
                throw new Error(message);
            }
        }
        return condition;
    }

    if (!Array.prototype.back) {
        Array.prototype.back = function(){
            return this[this.length - 1];
        };
    };
    if (!Array.prototype.front) {
        Array.prototype.front = function(){
            return this[0];
        };
    };



    /*
     * A baseclass for all entities wishing to be source of generic (subscribe/fire) events
     */
    class Subscribable {

        constructor() {
            this.mSubscribers = {};
            this.mAll = [];
        }

        on(eventType, cb) {
            if (!cb && typeof eventType == "function") {
               this.mAll.push(eventType);
            } else {
                this.mSubscribers[eventType] = this.mSubscribers[eventType] || [];
                this.mSubscribers[eventType].push(cb);
            }
            return this;
        }

        off(cb) {
            function off(cbList) {
                while(true) {
                    let foundIdx = cbList.findIndex(function(subscriber) {return subscriber === cb;});
                    if (foundIdx != -1) {
                        cbList.splice(foundIdx,1);
                    } else {
                        break;
                    }
                }
            }
            var foundIdx;
            for (var eventSubscribers in this.mSubscribers) {
                off(this.mSubscribers[eventSubscribers])
            }
            off(this.mAll);
            return this;
        }

        fire(eventType, event, eventData) {
            const self = this;
            if (Array.isArray(event)) {
                event.map(function(evt) {
                    self.fire(eventType, evt);
                });
            }
            if (eventType in this.mSubscribers) {
                event = event || {};
                if (typeof event == "object") {
                    event.type = eventType;
                }
                if (eventData) {
                    if (typeof eventData != "object") {
                        eventData = {eventData : eventData};
                    }
                    for (var key in eventData) {
                        event[key] = eventData[key];
                    }
                }
                this.mSubscribers[eventType].forEach((subscriber) => {
                    try {
                        subscriber(event);
                    } catch(error) {
                        console.error("Error occured when dispatching event"+event, error);
                    }
                });
            }
            this.mAll.forEach((subscriber) => {
                subscriber(eventType, event, eventData);
            });
            return this;
        }
    }


    class EventSubscriptionGroup extends Subscribable {

        constructor() {
            super();
            this.group = new Set();
        }

        _forwardFor(eventType, item) {
            const self = this;
            item._EventSubscriptionGroup = item._EventSubscriptionGroup || {};
            if (!(eventType in item._EventSubscriptionGroup)) {
                item._EventSubscriptionGroup[eventType] = (e) => {
                    e.target = item
                    self.fire(eventType, e);
                }
            }
            return item._EventSubscriptionGroup[eventType];
        }

        add(item) {
            const self = this;
            this.group.add(item);
            Object.keys(this.mSubscribers).forEach( (eventType) => {
                item.on(eventType, self._forwardFor(eventType, item))
            });
        }

        delete(item) {
            const self = this;
            Object.keys(this.mSubscribers).forEach( (eventType) => {
                item.off(eventType, self._forwardFor(eventType, item))
            });
            this.group.delete(item);
        }

        on(eventType, cb) {
            const self = this;
            if (!(eventType in this.mSubscribers)) {
                Array.from(this.group).forEach((item) => {
                    item.on(eventType, self._forwardFor(eventType, item));
                });
            }
            super.on(eventType, cb);
        }

        off(eventType, cb) {
            const self = this;
            super.off(eventType, cb);
            if (!(eventType in this.mSubscribers)) {
                Array.from(this.group).forEach((item) => {
                    item.off(eventType, self._forwardFor(eventType, item));
                });
            }
        }
    }


    /**
     * Wrapper/helper for objects implementing event subscription pattern:
     * object.on(eventType, callback)/object.off(eventType, callback)
     *
     * where the controller/decorator of the object wishes to override/decorate the
     * given callbacks and still enable unsubscribing. Decoration includes additional
     * /derived events the controller wishes to offer;
     */
    class OverridenSubsciption {

        constructor(subject) {
            this.subject       = subject;
            this.subscriptions = {};
        }

        subscribe(method, key, overrides) {
            if (typeof overrides === "function") {
                let _overrides = {};
                _overrides[key.eventType] = overrides;
                overrides = _overrides;
            }

            var subscription = (this.subscriptions[key.eventType] = this.subscriptions[key.eventType] || []);
            subscription.push({
                key        : key,
                overrides  : overrides
            });
            for (var eventType in overrides) {
                this.subject[method](eventType, overrides[eventType]);
            }
        }

        /*
         * Subscribe to events on this.subject, where:
         *
         * key           - subscription key: {
         *                    eventType: xxxx,
         *                    cb       : xxxx,
         *                    context  : xxxx //optional to bind the cb to
         *                 }; What the controller's caller originally supplied
         *                 during the subscription and what they will supply
         *                 when unsubscribing;
         *
         * overrides     - overrides offered by the controller. A dictionary of
         *                 { eventType : cb } - to which this method will make a direct
         *                 subscription on this.subject
         */
        on(key, overrides) {
            this.subscribe("on", key, overrides);
        }

        once(key, overrides) {
            this.subscribe("once", key, overrides);
        }

        off() {
            var eventType, cb, context;
            if (arguments.length == 1 && typeof arguments[0] === "object") {
                eventType = arguments[0].eventType;
                cb        = arguments[0].cb;
                context   = arguments[0].context;
            } else {
                eventType = arguments[0];
                cb        = arguments[1];
                context   = arguments[2];
            }
            if (!eventType) {
                this.subject.off();
                this.subscriptions = {};
                return;
            }
            var etSubscriptions = this.subscriptions[eventType];
            if (etSubscriptions) {
                if (!cb) {
                    this.subject.off(eventType);
                    etSubscriptions.splice(0,etSubscriptions.length);
                }
                for (var i = 0; i < etSubscriptions.length; ) {
                    var subscription = etSubscriptions[i];
                    if (subscription.key.cb === cb && subscription.key.context == context) {
                        for (let et in subscription.overrides) {
                            this.subject.off(et, subscription.overrides[et])
                        }
                        etSubscriptions.splice(i, 1);
                    } else {
                        i++;
                    }
                }
                if (etSubscriptions.length == 0) {
                    delete this.subscriptions[eventType];
                }
                return;
            }
            this.subject.off.apply(this.subject, eventType, cb, context);
        }
    }

    return {
        urlParams   : urlParams,
        mtsToString : function(mts) {
            var result = "0m"
            if (mts) {
                if (mts < 1000) {
                    result = Math.round(mts)+"m";
                } else if (mts < 5000) {
                    result = (mts/1000).toFixed(1);
                    result += "km";
                } else {
                    result = Math.round((mts/1000) | 0) +"km";
                }
            }
            return result;
        },
        millisToString : function(millis) {
            var result = "0ms"
            if (millis) {
                if (millis > 1000) {
                    result = (millis/1000).toFixed(2) + "s";
                } else if (millis < 10) {
                    result = millis.toFixed(2);
                    result += "ms";
                } else {
                    result = (millis|0) +"ms";
                }
            }
            return result;
        },
        secondsToHrs : function(seconds, options={}) {
            var hours   = Math.floor(seconds / 3600);
            var minutes = Math.floor((seconds - (hours * 3600)) / 60);
            var seconds = Math.floor(seconds - (hours * 3600) - (minutes * 60));

            if (seconds < 10) {seconds = "0"+seconds;}
            if (hours < 1) {
                return minutes+"min";
            } else if (minutes < 10) {minutes = "0"+minutes;}
            return hours+'h'+minutes;
        },
        kph2Mps : function(kph) {
            return 1000*kph/3600;
        },
        mps2Kph : function(mps) {
            return 3600*mps/1000;
        },
        mins2ms : function(mins) {
            return mins*60*1000;
        },
        largeNumberToStr: function(number) {
            var result = "0"
            var suffixes = ["", "k", "m"];
            for (var i = 0; i < suffixes.length; i++) {
                if (number < 999) {
                    break;
                }
                number = (number / 1000).toFixed(2);
            }
            if (number > 10) {
                number = number | 0;
            }
            return number + suffixes[i];
        },
        assert: assert,
        pip: function(point, polygon) {
            // ray-casting algorithm based on
            // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
            var x = point.lat || point.x || point[0];
            var y = point.lng || point.y || point[1];

            var inside = false;
            for (var i = 0, j = polygon.length - 1; i < polygon.length; j = i++) {
                var xi = polygon[i][0], yi = polygon[i][1];
                var xj = polygon[j][0], yj = polygon[j][1];

                var intersect = ((yi > y) != (yj > y))
                    && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
                if (intersect) inside = !inside;
            }

            return inside;
        },
        pips: function (point, polygons) {
            for (var i = 0; i < polygons.length; i++) {
                if (utils.pip(point, polygons[i])) {
                    return true;
                }
            }
            return false;
        },
        range: function(from, to, step, cb) {
            var result = []
            if (from > to) {
                step = Math.sign(step) == -1 ? step : -step
                for (var i = from; i >= to; i+=step) {
                    result.push(cb(i));
                }
            } else {
                step = Math.sign(step) == 1 ? step : -step
                for (var i = from; i <= to; i+=step) {
                    result.push(cb(i));
                }
            }
            return result;
        },
        flatten : function(arrayOfArrays) {
            return arrayOfArrays.reduce(function (flat, toFlatten) {
                return flat.concat(Array.isArray(toFlatten) ? utils.flatten(toFlatten) : toFlatten);
            }, []);
        },
        nDimEuclideanDistance: function(from, to) {
            utils.assert(from.length == to.length);
            var distance = 0;
            for (var i = 0; i < from.length; i++) {
                distance += Math.pow(to[i]-from[i], 2);
            }
            return Math.sqrt(distance);
        },
        moveRange: function(from, to, options, cb) { //generates move path between from and to
            function toVector(what) {
                if ("x" in what && "y" in what) {
                    return [what.x, what.y]
                } else if ("left" in what && "top" in what) {
                    return [what.left, what.top];
                }
                utils.assert(Array.isArray(what) && what.length > 1, "moveRange accepts only arrays or {x:,y:} objects");
                return what;
            }
            from = toVector(from);
            to   = toVector(to);
            cb   = cb || function(v) { return v; }
            var distance = utils.nDimEuclideanDistance(from,to);

            var steps     = 10;
            var startFrom = 1;
            if (options) {
                if (options.step) {
                    steps = distance / options.step;
                } else if (options.resolution != undefined) {
                    steps = options.resolution;
                }
                if (options.includeOrigin) {
                    startFrom = 0;
                    steps++;
                }
            }
            var dstep = [];
            for (var d = 0; d < from.length; d++) {
                dstep.push((to[d]-from[d])/steps);
            }
            var result = [];
            for (var i = startFrom; i < steps; i++) {
                var point = [];
                for (var d = 0; d < from.length; d++) {
                    point.push(from[d]+dstep[d]*i);
                }
                result.push(cb(point));
            }
            result.push(cb(to));
            return result;
        },
        escapeXML: function(text) {
            return text.replace(/&/g, '&amp;')
                       .replace(/</g, '&lt;')
                       .replace(/>/g, '&gt;')
                       .replace(/"/g, '&quot;')
                       .replace(/'/g, '&apos;');
        },
        mergeSimpleObjects: function(dst, src, soft) {
            dst = dst || {};
            for (var k in src) {
                var v = src[k];
                if (k in dst) {
                    if (typeof v === 'object') {
                        utils.mergeSimpleObjects(dst[k], v, soft);
                    } else if (!soft) {
                        dst[k] = v;
                    }
                } else {
                    dst[k] = utils.cloneSimpleObject(v);
                }
            }
            return dst;
        },
        substring(str, fromIdx, toIdx) {
            function flip(idx) {
                return idx < 0 ? str.length + idx : idx;
            }
            return str.substring(flip(fromIdx), flip(toIdx));
        },
        browserFeaturesFromUserAgent: function(uaString) {
            /*
             * Observations on useragent strings:
             *
             * see: https://en.wikipedia.org/wiki/User_agent#User_agent_spoofing
             * 1. Chrome on Android:
             *    "Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5 Build/M4B30Z) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Mobile Safari/537.36"
             * 2. FF on Android:
             *    "Mozilla/5.0 (Android 6.0.1; Mobile; rv:51.0) Gecko/51.0 Firefox/51.0"
             * 3. Mac
             *  3.1. iPhone:
             *    "Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac OS X) AppleWebKit/602.1.38 (KHTML, like Gecko) Version/10.0 Mobile/14A300 Safari/602.1"
             *  3.2. iPod Touch:
             *    "Mozilla/5.0 (iPod; CPU iPhone OS 10_0 like Mac OS X) AppleWebKit/602.1.38 (KHTML, like Gecko) Version/10.0 Mobile/14A300 Safari/602.1"
             *  3.3 iPad
             *    "Mozilla/5.0 (iPad; CPU OS 10_0 like Mac OS X) AppleWebKit/602.1.38 (KHTML, like Gecko) Version/10.0 Mobile/14A300 Safari/602.1"
             *  3.4 MacBook
             *    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/602.4.8 (KHTML, like Gecko) Version/10.0.3 Safari/602.4.8"
             */
            let result = {};
            if (/Mac OS X/i.test(uaString) && !(/Macintosh/i.test(uaString))) {
                let version = uaString.match(/(\S+) like Mac OS X/);
                result.os = "iOS";
                result.iOS = {
                    version : version ? version[1] : undefined
                };
                result.browser = "Safari"; //effectivelly: “Apps that browse the web must use the iOS WebKit framework and WebKit Javascript.”, see:
                                           //https://www.howtogeek.com/184283/why-third-party-browsers-will-always-be-inferior-to-safari-on-iphone-and-ipad/
                result.Safari = {
                    version : version ? version[1] : undefined
                }
                if (/iPhone/i.test(uaString)) {
                    result.iPhone    = true;
                    result.mobile    = true;
                } else if (/iPad/i.test(uaString)) {
                    result.iPad      = true;
                    result.mobile    = true;
                }
            } else {
                if (/Android/i.test(uaString)) {
                    let version = uaString.match(/Android (\S+?);/);
                    result.os = "Android";
                    result.Android = {
                        version : version ? version[1] : undefined
                    };
                    result.mobile  = true;
                } else if (/IEMobile/i.test(uaString)) {
                    result.os = "WindowsMobile";
                    result.mobile  = true;
                } else if (/Macintosh/i.test(uaString)) {
                    let version = uaString.match(/\(Macintosh; Intel Mac OS X (\S+?)\)/);
                    result.os   = "OSX";
                    result.OSX  = {
                        version : version ? version[1] : undefined
                    }
                    result.Macintosh = true;
                }

                if (/Firefox/i.test(uaString)) {
                    result.browser = "Firefox";
                    let version = uaString.match(/Firefox\/(\S+)/);
                    result.Firefox = {
                        version : version ? version[1] : undefined
                    };
                } else if (/Chrome/i.test(uaString)) {
                    result.browser = "Chrome";
                    let version = uaString.match(/Chrome\/(\S+)/);
                    result.Chrome = {
                        version : version ? version[1] : undefined
                    };
                } else if (/Safari/i.test(uaString)) {
                    result.browser = "Safari";
                    result.Safari = {
                        version : result.OSX ? result.OSX.version : undefined
                    }
                }
            }
            return result;
        },
        cloneSimpleObject: function(o) {
            return o ? JSON.parse(
                JSON.stringify(o)
            ) : o;
        },
        Subscribable : Subscribable,
        EventSubscriptionGroup : EventSubscriptionGroup,
        OverridenSubsciption : OverridenSubsciption
    }

})();

//for Safari
Math.sign = Math.sign || function(x) {
    x = +x; // convert to a number
    if (x === 0 || isNaN(x)) {
        return x;
    }
    return x > 0 ? 1 : -1;
}

module.exports = utils;