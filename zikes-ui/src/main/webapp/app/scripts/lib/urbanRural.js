/**
 * scripts/lib/urbanRural.js
 *
 * This, based on the value of the master slider,
 * calculates and returns the urban/rural preference
 */

'use strict';

const urbanRural = (function() {

    const KSliderMax                  = 20;
    const KSliderMid                  = KSliderMax/2;
    const KTrafficSignalsUnitMts      = 200;

    function slider2Value(masterSliderValue) {
        return 2.0*(masterSliderValue-KSliderMid)/KSliderMid;
    }

    class UrbanRuralSliderPreference {

        constructor(parent, jsonDocument) {
            this.mParent   = parent;
            this.mJSON     = jsonDocument;
        }

        masterSlideOptions() {
            var result = {
                label: "urbanrural",
                value: KSliderMid,
                min: {
                    value: 0,
                    label: "more rural"
                },
                max: {
                    value: KSliderMax,
                    label: "more urban"
                },
                step: 1
            }
            this.mMasterSliderValue = slider2Value(result.value);
            return result;
        }


        fromMasterSlider(masterSliderValue, rootJson) {
            this.mMasterSliderValue = slider2Value(masterSliderValue);
            if (rootJson) {
                return this.generate(rootJson);
            }
        }

        render(panel) {}

        generate(rootJson) {
            if (this.mMasterSliderValue) {
                let urbanRural;
                if (this.mMasterSliderValue > 0) {
                    //reward urban
                    urbanRural = 1.0/(1+this.mMasterSliderValue)
                } else {
                    //punish urban
                    urbanRural = Math.abs(this.mMasterSliderValue);
                    rootJson["trafficSignals"] = {
                        punishmentMts : Math.round(
                            KTrafficSignalsUnitMts*(2*Math.sqrt(urbanRural)+1)
                        )
                    }
                    urbanRural += 1;
                }
                urbanRural = {
                    "urbanFactor" : urbanRural.toFixed(2),
                    "capacitorCharge" : {
                        "min": -7+Math.round(this.mMasterSliderValue),
                        "max":  7-Math.round(this.mMasterSliderValue)
                    }
                };
                rootJson["urbanRural"] = urbanRural;
            }
        }
    }


    return {
        UrbanRuralSliderPreference: UrbanRuralSliderPreference,
        sliderGenerator : UrbanRuralSliderPreference
    }

})();

module.exports = urbanRural;