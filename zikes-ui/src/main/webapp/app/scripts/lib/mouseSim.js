/**
 * scripts/lib/mouseSim.js
 *
 * This simulates mouse events and interaction
 */

'use strict';
var when      = require("when");
when.delay    = require("when/delay");

var utils     = require("../lib/utils.js");

var mouseSim = (function() {

    function SimulatedMouse(options) {
        this.mPosition    = L.point(0,0);
        this.mMouseDiv    = options && options.mouseDiv;
        this.mSpeedPixps  = (options && options.speedPixps) || 1100;
        this.mDelaysMs    = (options && options.delaysMs) || {}
        if (this.mMouseDiv) {
            this.mMouseDim = this.mMouseDiv[0].getBoundingClientRect();
            this.mMouseDim = new L.Point(
                this.mMouseDim.right - this.mMouseDim.left,
                this.mMouseDim.bottom - this.mMouseDim.top
            );
            this.mMouseDim.anchor = options.mouseAnchor || L.point(this.mMouseDim.x/2,this.mMouseDim.y)
        }
    }

    SimulatedMouse.prototype._delay = function(kind, delaysMs) {
        if (delaysMs && kind in delaysMs) {
            return delaysMs[kind];
        }
        return this.mDelaysMs[kind] || 0;
    }

    var KSizeFractionWhenPressed = 0.7;
    function animateMouseDown(self) {
        if (self.mMouseDiv) {
            var done = when.defer()
            self.mMouseDiv.animate({
                 width: self.mMouseDim.x*KSizeFractionWhenPressed,
                 height: self.mMouseDim.y*KSizeFractionWhenPressed,
                }, 200, function() {
                done.resolve();
            });
            return done.promise;
        }
        return when();
    }

    function animateMouseUp(self) {
        if (self.mMouseDiv) {
            var done = when.defer()
            self.mMouseDiv.animate({
                width: self.mMouseDim.x,
                height: self.mMouseDim.y
                }, 200, function() {
                done.resolve();
            });
            return done.promise;
        }
        return when();
    }

    SimulatedMouse.prototype.click = function(targetDiv, delaysMs) {
        var self = this;
        return when(targetDiv)
        .then(function(_targetDiv) {
            targetDiv = _targetDiv;
            return self.move(targetDiv);
        })
        .delay(self._delay("arrive", delaysMs))
        .then(function() {return animateMouseDown(self);})
        .then(function() {return animateMouseUp(self);})
        .then(function() {
            if (targetDiv instanceof jQuery) {
                targetDiv.trigger("click");
            }
        })
        .delay(self._delay("postClick", delaysMs));
    }

    SimulatedMouse.prototype.hide = function(durationMs) {
        if (this.mMouseDiv) {
            if (durationMs) {
                var done = when.defer()
                this.mMouseDiv.animate({
                    opacity: 0
                    }, durationMs, function() {
                    done.resolve();
                });
                return done.promise;
            } else {
                this.mMouseDiv.hide();
            }
        }
    }

    SimulatedMouse.prototype.show = function(durationMs) {
        if (this.mMouseDiv) {
            if (durationMs) {
                var done = when.defer()
                this.mMouseDiv.animate({
                    opacity: 1
                    }, durationMs, function() {
                    done.resolve();
                });
                return done.promise;
            } else {
                this.mMouseDiv.show();
            }
        }
    }

    /**
     * pageX and pageY:
     * Relative to the top left of the fully rendered content area in the browser. This reference point is below the url bar and back
     * button in the upper left. This point could be anywhere in the browser window and can actually change location if there are
     * embedded scrollable pages embedded within pages and the user moves a scrollbar.
     *
     * screenX and screenY:
     * Relative to the top left of the physical screen/monitor, this reference point only moves if you increase or decrease the number
     * of monitors or the monitor resolution.
     *
     * clientX and clientY:
     * Relative to the upper left edge of the content area (the viewport) of the browser window. This point does not move even if
     * the user moves a scrollbar from within the browser.
     */
    function trigger(target, event, clientPoint) {
        if (target && event) {
            if (typeof event === "string") {
                event = new MouseEvent(
                    event, {
                        clientX : clientPoint.x,
                        clientY : clientPoint.y,
                        pageX : clientPoint.x,
                        pageY : clientPoint.y,
                        bubbles : true,
                        view: window
                    }
                );
            }
            if (typeof target === "function") {
                return target(event);
            } else {
                return target[0].dispatchEvent(event);
            }
        }
    }

    SimulatedMouse.prototype.down = function(target) {
        var self = this;
        return animateMouseDown(this).then(function() {
            return trigger(target, "mousedown", self.whateverToCoords(target));
        });
    }

    SimulatedMouse.prototype.up = function(target) {
        var self = this;
        return animateMouseUp(this).then(function() {
            return trigger(target, "mouseup", self.whateverToCoords(target));
        });
    }

    function eventPath(from, to, durationMs, cb) {
        var events = utils.moveRange(
            from,
            to,
            {step: 5},
            function(v) {
                return new L.Point(v[0], v[1]);
            }
        );

        var delayMs = durationMs/events.length;
        function fireEvents(_events) {
            if (_events.length > 0) {
                cb(_events.shift());
                return when.delay(delayMs).then(function() {
                    return fireEvents(_events);
                });
            }
            return when(true);
        }
        return fireEvents(events);
    }

    SimulatedMouse.prototype.move = function(dst, target) {
        var self = this;
        var done = when.defer();
        dst = this.whateverToCoords(dst, target);
        var src = this.whateverToCoords(target || this.mMouseDiv);
        var durationMs = 1000*dst.distanceTo(src)/this.mSpeedPixps;
        if (!target) {
            if (this.mMouseDiv) {
                dst = {
                    left : dst.x - self.mMouseDim.anchor.x,
                    top  : dst.y - self.mMouseDim.anchor.y
                };
                this.mMouseDiv.animate(dst, durationMs, function() {
                    done.resolve();
                });
                return done.promise;
            } else {
                this.mPosition = this.whateverToCoords(dst);
            }
            return when();
        } else {
            return eventPath(
                this.whateverToCoords(target),
                dst,
                durationMs,
                function(event) {
                    if (self.mMouseDiv) {
                        self.mMouseDiv.css("left", event.x-self.mMouseDim.anchor.x);
                        self.mMouseDiv.css("top",  event.y-self.mMouseDim.anchor.y);
                    } else {
                        self.mPosition = event;
                    }
                    trigger(target, "mousemove", event)
                }
            );
        }
    }

    SimulatedMouse.prototype.drag = function(what, where, delaysMs) {
        var self = this;
        var firstMile = what instanceof jQuery ?
            this.move(what).delay(self._delay("arrive", delaysMs)) : when(true)

        return firstMile
        .then(function() { return self.down(what); })
        .then(function() { return self.move(where, what); })
        .then(function() { return self.up(what);})
        .delay(self._delay("arrive", delaysMs))
    }

    function _shakeDivOnce(distance, duration) {
        var finished = when.defer();
        this.animate({"margin-left":(distance*-1)}, duration/4)
        .animate({"margin-left":distance}, duration/2)
        .animate({"margin-left":0}, duration/4, function() {
            finished.resolve();
        });
        return finished.promise;
    };

    SimulatedMouse.prototype.shake = function(shakes, distance, duration) {
        if (this.mMouseDiv && shakes > 0) {
            var self = this;
            return _shakeDivOnce.bind(this.mMouseDiv)(distance, duration/shakes)
            .then(function() {
                return self.shake(shakes-1, distance - distance/shakes, duration - duration/shakes);
            });
        }
    }

    SimulatedMouse.prototype.whateverToCoords = function(whatevs, vectorReference) {
        if (!whatevs) {
            if (this.mMouseDiv) {
                whatevs = this.mMouseDiv[0];
            } else {
                return this.mPosition;
            }
        }
        if (whatevs instanceof L.Point) {
            return whatevs;
        }
        if ("top" in whatevs && "left" in whatevs) {
            if ("bottom" in whatevs && "right" in whatevs) {
                return new L.Point(
                    whatevs.left + (whatevs.right-whatevs.left)/2,
                    whatevs.top + (whatevs.bottom-whatevs.top)/2
                );
            }
            return new L.Point(whatevs.left, whatevs.top);
        }
        if (whatevs instanceof jQuery) {
            return this.whateverToCoords(whatevs[0]);
        }
        if (whatevs instanceof Element) {
            return this.whateverToCoords(
                whatevs.getBoundingClientRect()
            );
        }
        if (typeof whatevs === "object") {
            if ("vector" in whatevs) {
                var vector = this.whateverToCoords(whatevs.vector);
                var reference = this.whateverToCoords(
                    whatevs.reference || vectorReference || this.mMouseDiv
                );
                return new L.Point(
                    reference.x + vector.x,
                    reference.y + vector.y
                )
            } else if ("x" in whatevs && "y" in whatevs) {
                return new L.Point(whatevs.x, whatevs.y);
            }
        }
        return this.whateverToCoords();
    }

    return {
        SimulatedMouse: SimulatedMouse
    }

})();

module.exports = mouseSim;