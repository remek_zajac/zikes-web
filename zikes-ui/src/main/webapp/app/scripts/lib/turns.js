/**
 * scripts/lib/popularity.js
 *
 * This, based on the value of the master slider,
 * calculates and returns the turn preference
 */

'use strict';

var turnPreference = (function() {

    const KMinHighwayType = "secondary";
    const KMaxValue = 30;

    class TurnSliderPreference {
            constructor(parent, map) {
            this.mParent = parent;
            this.map     = map;
        }

        masterSlideOptions() {
            var result = {
                label: "turns",
                value: 0,
                min: {
                    value: 0,
                    label: "fewer turns"
                },
                max: {
                    value: KMaxValue,
                    label: ""
                },
                step: 1
            }
            this.mMasterSliderValue = 0;
            return result;
        }


        fromMasterSlider(masterSliderValue, rootJson) {
            this.mMasterSliderValue = Math.max(KMaxValue - masterSliderValue, 0);
            if (rootJson) {
                return this.generate(rootJson);
            }
        }

        render(panel) {}

        generate(rootJson) {
            if (this.mMasterSliderValue) {
                const self = this;
                let result = {
                    easy          : {},
                    difficult     : {},
                    trafficSignals : {}
                };
                let minFoundIdx = undefined;
                this.map.highways.bySize().forEach(function(highway, idx) {
                    if (highway.name() == KMinHighwayType) {
                        minFoundIdx = idx;
                    }
                    if (minFoundIdx != undefined) {
                        let difficult     = Math.round(((idx - minFoundIdx + 1) * self.mMasterSliderValue) + self.mMasterSliderValue);
                        let trafficSignals = Math.round((self.mMasterSliderValue + difficult)/2);
                        result.easy[highway.name()]           = self.mMasterSliderValue;
                        result.difficult[highway.name()]      = difficult;
                        result.trafficSignals[highway.name()] = trafficSignals;
                        highway.mIncluded && highway.mIncluded.forEach(function(highway) {
                            result.easy[highway.name()]           = self.mMasterSliderValue;
                            result.difficult[highway.name()]      = difficult;
                            result.trafficSignals[highway.name()] = trafficSignals;
                        });
                    }
                });
                rootJson["turnPunishment"] = result;
            }
        }
    }

    return {
        TurnSliderPreference: TurnSliderPreference,
        sliderGenerator : TurnSliderPreference
    }

})();

module.exports = turnPreference;