/**
 * scripts/lib/track.js
 *
 * Module that collects track points and can work out various
 * momentary or overall statistics, like speed, elevation profile
 */

'use strict';
const utils    = require("./utils.js");
const geometry = require("./geometry.js");

const trackModule = (function() {

    class SortingTreeNode {

        constructor(compareCb) {
            this.mCompareCb  = compareCb || function(a,b) { return a == b ? 0 : a < b ? -1 : 1; }
            this.count  = 0;
            this.values = [];
        }

        put(value) {
            let comparison = this.values.length ? this.mCompareCb(value, this.values[0]) : 0;
            if (comparison == 0) {
                this.values.push(value);
            } else if (comparison < 0) {
                this.mLeft = this.mLeft   || new SortingTreeNode(this.mCompareCb);
                this.mLeft.put(value);
            } else {
                this.mRight = this.mRight || new SortingTreeNode(this.mCompareCb);
                this.mRight.put(value);
            }
            this.count++;
        }

        lcr(cb) {
            const self = this;
            if (this.mLeft && this.mLeft.lcr(cb)) {
                return true;
            }
            if (this.values.find(function(value) {
                return cb.call(self, value);
            })) {
                return true;
            }
            if (this.mRight && this.mRight.lcr(cb)) {
                return true;
            }
        }

        rcl(cb) {
            const self = this;
            if (this.mRight && this.mRight.rcl(cb)) {
                return true;
            }
            if (this.values.find(function(value) {
                return cb.call(self, value);
            })) {
                return true;
            }
            if (this.mLeft && this.mLeft.rcl(cb)) {
                return true;
            }
        }

        filter(order, cb) {
            let result = [];
            let collect = function(v) {
                if (cb(v)) {
                    result.push(v);
                } else {
                    return true;
                }
            }
            this[order](collect);
            return result.sort(this.mCompareCb);
        }

        collect(order, number) {
            return this.filter(order, function() {
                return number-- > 0;
            });
        }
    }



    class TrackBase {
        constructor() {
            this.plugins = {
                dict : {},
                list : []
            };
        }

        toJSON() {
            let meta         = {};
            this.plugins.list.forEach(function(plugin) {
                meta[plugin.name] = plugin.toJSON();
            });
            meta.length      = this.length;
            meta.distanceMts = this.distanceMts;
            meta.wrapped     = this.mLocked;
            return {
                meta : meta,
                points : this.iterator().forward().map(function(pt) {
                    return pt.toArray();
                })
            };
        }

        mountPlugin(plugin, atThis) {
            utils.assert(!this.plugins.dict[plugin.name], "Name clash, plugins must have unique names");
            this.plugins.dict[plugin.name] = plugin;
            this.plugins.list.push(plugin);
            if (atThis) {
                this[plugin.name] = plugin;
            }
            return plugin;
        }

        iterator() {
            return new BidirIterator(this);
        }
    }



    class Track extends TrackBase {

        constructor(pluginFactories = []) {
            super();
            this.mPluginFactories = pluginFactories;
            const self   = this;
            pluginFactories.forEach(function(f) {
                self.mountPlugin(f(self), true);
            });
            this.points = [];
            Object.defineProperty(this, 'length', {
                get: function() {
                    return self.points.length;
                },
                enumerable: true,
                configurable: false
            });
            Object.defineProperty(this, 'distanceMts', {
                get: function() {
                    return self.length && self.points.back().frmStrt || 0;
                },
                enumerable: true,
                configurable: false
            });
        }

        clone() {
            return new Track(this.mPluginFactories);
        }

        fromJSON(json) {
            const self = this;
            json.points.forEach(function(pt) {
                self.append(geometry.LatLng.create(pt));
            });
            if (json.meta && json.meta.wrapped) {
                this.wrap();
            }
            return this;
        }

        append(point) {
            if (this.mLocked) {
                utils.assert(false, "Track locked for appending");
                return;
            }
            const self = this;
            if (Array.isArray(point)) {
                return point.reduce(function(events, pt) {
                    return events.concat(self.append(pt));
                }, []);
            }
            utils.assert(point instanceof geometry.LatLng, "only accepting geometry.LatLngs");
            if (this.points.length) {
                let back = this.points.back();
                point.frmStrt = back.frmStrt + back.distanceTo(point);
            } else {
                point.frmStrt = 0;
            }

            let events = [];
            for (var i = 0; i < this.plugins.list.length; i++) {
                point = this.plugins.list[i].append(point);
                if (!point) {
                    return;
                }
                if ("events" in point)  {
                    events = events.concat(point.events);
                } if ("event" in point) {
                    events.push(point.event);
                }
                if ("point" in point) {
                    point = point.point;
                    if (!point) {
                        return;
                    }
                }
            }
            if (point) {
                this.points.push(point);
            }
            return events;
        }

        wrap() {
            if (!this.mLocked) {
                this.plugins.list.forEach(function(plugin) {
                    plugin.wrap();
                });
                this.mLocked = true;
            }
            return this;
        }

        getAt(mts) {
            const self = this;
            function binSearch(lidx, ridx) {
                let left  = self.points[lidx];
                let right = self.points[ridx];
                if (mts == left.frmStrt) {
                    return [new BidirIterator(self, lidx),
                            new BidirIterator(self, lidx)];
                } else if (mts == right.frmStrt) {
                    return [new BidirIterator(self, ridx),
                            new BidirIterator(self, ridx)];
                } else if (mts > left.frmStrt && mts < right.frmStrt) {
                    let d = (ridx - lidx)+1;
                    if (d < 2) {
                        return;
                    } else if (d == 2) {
                        return [new BidirIterator(self, lidx),
                                new BidirIterator(self, ridx)
                        ];
                    }

                    let midx = lidx + Math.floor(d/2);
                    let mid  = self.points[midx];
                    if (mts < mid.frmStrt) {
                        return binSearch(lidx, midx);
                    } else {
                        return binSearch(midx, ridx);
                    }
                }
            }
            return binSearch(0, this.points.length-1);
        }

        back() {
            return this.points.back();
        }

        front() {
            return this.points.front();
        }
    }


    class BidirIterator {

        constructor(from, startIdx) {
            const self   = this;
            if (from instanceof BidirIterator) {
                this.tracks = from.tracks;
                if (from._idx) {
                    this._idx   = {
                        track : from._idx.track,
                        point : from._idx.point
                    }
                }
            } else {
                this.tracks  = (Array.isArray(from) ? from : [from]).reduce(function(cum, track) {
                    utils.assert(track instanceof Track, "must supply a Track or array of Tracks");
                    if (cum.length) {
                        let tailTrack = cum.back();
                        let tailPoint = tailTrack.track.points.back();
                        let headPoint = track.points.front();
                        return cum.concat({
                            frmStrt : tailTrack.frmStrt +
                                      tailPoint.frmStrt +
                                      tailPoint.distanceTo(headPoint),
                            track   : track
                        });
                    }
                    return [{
                        frmStrt : 0,
                        track   : track
                    }]
                }, []);
            }
            this._idx = this._idx || {track:0,point:0};
            if (startIdx) {
                if (typeof startIdx == "number") {
                    this._idx.point = startIdx
                } else if (typeof startIdx == "object") {
                    this._idx.point = startIdx.point || this._idx.point;
                    this._idx.track = startIdx.track || this._idx.track;
                } else {
                    utils.assert(false, "Unable to interpret startIdx "+JSON.stringify(startIdx));
                }
            }
            this._validate();
        }

        _validate() {
            if (this._idx) {
                let track = this.tracks[this._idx.track];
                if (!track) {
                    delete this["_idx"];
                    return;
                }
                let point = track.track.points[this._idx.point];
                if (!point) {
                    delete this["_idx"];
                    return;
                }
            }
        }

        current() {
            if (this._idx) {
                let track = this.tracks[this._idx.track];
                let point = track.track.points[this._idx.point];
                return {
                    point   : point,
                    track   : track,
                    frmStrt : track.frmStrt + point.frmStrt
                }
            }
        }

        front() {
            this._idx = {
                track : 0,
                point : 0
            }
            this._validate();
            return this;
        }

        back() {
            if (this.tracks.length) {
                let trackIdx = this.tracks.length-1;
                this._idx = {
                    track : trackIdx,
                    point : this.tracks[trackIdx].track.points.length-1
                };
            }
            this._validate();
            return this;
        }

        next() {
            if (++this._idx.point > this.tracks[this._idx.track].track.points.length-1) {
                return this.nextTrack();
            }
            return this;
        }

        peekNext() {
            return new Iterator(this).next().current();
        }

        nextTrack() {
            if (++this._idx.track > this.tracks.length-1) {
                delete this["_idx"]; //invalid now
            } else {
                this._idx.point = 0;
            }
            return this;
        }

        prev() {
            if (--this._idx.point < 0) {
                return this.prevTrack();
            }
            return this;
        }

        peekPrev() {
            return new Iterator(this).prev().current();
        }

        prevTrack() {
            if (--this._idx.track < 0) {
                delete this["_idx"]; //invalid now
            } else {
                this._idx.point = this.tracks[this._idx.track].track.points.length-1;
            }
            return this;
        }

        forward() {
            return new Iterator(this);
        }

        backward() {
            return new Iterator(this, {reverse:true});
        }

        clone() {
            return new BidirIterator(this);
        }

        equals(other) {
            return this.current() === other.current();
        }

        isBack() {
            return this._idx.track == this.tracks.length-1 &&
                   this._idx.point == this.tracks[this._idx.track].track.points.length-1;
        }

        isFront() {
            return this._idx.track == 0 && this._idx.point == 0;
        }
    }


    class Iterator {
        constructor(from, options) {
            if (from instanceof Iterator) {
                this.underlying = new BidirIterator(from.underlying);
            } else {
                this.underlying = new BidirIterator(from);
            }
            this.reverse = options && options.reverse;
            this.filters = [];
        }

        next() {
            this.reverse ? this.underlying.prev() : this.underlying.next();
            return this;
        }

        nextTrack() {
            this.reverse ? this.underlying.prevTrack() : this.underlying.nextTrack();
            return this;
        }

        peekNext() {
            this.reverse ? this.underlying.peekPrev() : this.underlying.peekNext();
            return this;
        }

        reverse() {
            return new Iterator(this, {reverse:!this.reverse});
        }

        current() {
            return this.underlying.current();
        }

        find(cb) {
            let current;
            while(current = this.current()) {
                if (this.filters.reduce(function(accept, filter) {
                    return accept && filter(current.point, this);
                }, true)) {
                    let result = cb(current.point, this);
                    if (result) {
                        return this;
                    }
                }
                this.next();
            }
        }

        forEach(cb) {
            this.find(function(pt, it) { cb(pt, it) });
        }

        map(cb) {
            let result = [];
            this.find(function(pt, it) {
                result.push(cb(pt, it));
            });
            return result;
        }

        reduce(cb, result, stopCb) {
            if (!stopCb) {
                if (typeof result == "function") {
                    stopCb  = result;
                    result  = undefined;
                }
            } else if (stopCb.equals) {
                let stopIt = stopCb;
                stopCb = function(result, pt, it) {
                    return stopIt.equals(it);
                }
            }
            if (!result) {
                result = this.current() && this.current().point;
                this.next();
            }
            this.find(function(pt, it) {
                result = cb(result, pt, it);
                return stopCb && stopCb(result, pt, it);
            });
            return result;
        }

        bidir() {
            return new BidirIterator(this.underlying);
        }

        filter(cb) {
            this.filters.push(cb);
            return this;
        }

        clone() {
            return new Iterator(this);
        }

        equals(other) {
            return this.current() === other.current();
        }

        isBack() {
            return this.reverse ? this.underlying.isFront() : this.underlying.isBack();
        }

        isFront() {
            return this.reverse ? this.underlying.isBack() : this.underlying.isFront();
        }
    }



    class Plugin {

        constructor(track, name, attrs2Serialise, options) {
            this.track = track;
            this.name  = name;
            if (attrs2Serialise) {
                this.attrs2Serialise = attrs2Serialise;
            }
            if (options) {
                this.options = options;
            }
        }

        toJSON() {
            function serialise(src) {
                let srcType = typeof src;
                if (srcType == "number" || srcType == "string") {
                    return src;
                } else if (srcType == "object") {
                    if (src instanceof geometry.LatLng) {
                        return {
                            ctor : "geometry.LatLng",
                            json : src.toArray()
                        };
                    } else if (Array.isArray(src)) {
                        return src.map(function(e) {
                            return serialise(e);
                        });
                    } else {
                        let result = {};
                        for (var k in src) {
                            result[k] = serialise(src[k]);
                        }
                        return result;
                    }
                } else {
                    utils.assert(false, "Unable to serialise '"+JSON.stringify(src)+"' if type '"+srcType+"'");
                }
            }
            function decorateSerialise(result, attrName) {
                if (this[attrName] != undefined) {
                    result[attrName] = serialise(this[attrName]);
                }
            }
            let result = {};
            !this.attrs2Serialise || this.attrs2Serialise.forEach(decorateSerialise.bind(this, result));
            return result;
        }

        fromJSON(json) {
            function deserialise(src) {
                let srcType = typeof src;
                if (srcType == "number" || srcType == "string") {
                    return src;
                } else if (srcType == "object") {
                    if (src.ctor) {
                        utils.assert(src.ctor == "geometry.LatLng", "can only deserialise geometry.LatLng for the moment");
                        return geometry.LatLng.create(src.json);
                    } else if (Array.isArray(src)) {
                        return src.map(function(e) {
                            return deserialise(e);
                        });
                    } else {
                        let result = {};
                        for (var k in src) {
                            result[k] = deserialise(src[k]);
                        }
                        return result;
                    }
                } else {
                    utils.assert(false, "Unable to serialise '"+JSON.stringify(src)+"' if type '"+srcType+"'");
                }
            }
            function decorateDeserialise(json) {
                for (var k in json) {
                    let descriptor = Object.getOwnPropertyDescriptor(this, k);
                    if (!descriptor || descriptor.writable) {
                        this[k] = deserialise(json[k]);
                    }
                }
            }
            decorateDeserialise.call(this, json);
        }

        append(point) {
            return point;
        }

        appendTrack(track) {
            let plugin = track.plugins.dict[this.name];
            utils.assert(plugin, "appended track has no plugin agent mounted");
            return plugin;
        }

        _valueExtractor(valExtract) {
            let valExtractType = typeof valExtract;
            if (valExtractType == "function") {
                return valExtract;
            } else if (Array.isArray(valExtract)) {
                utils.assert(valExtract.length, "path extract is empty");
                function valKeyExtract(path, obj, value) {
                    if (path.length > 1) {
                        if (obj[path[0]] == undefined) {
                            if (value == undefined) {
                                return;
                            }
                            obj[path[0]] = {};
                        }
                        return valKeyExtract(path.slice(1), obj[path[0]], value);
                    } else if (value != undefined) {
                        obj[path[0]] = value;
                    }
                    return obj[path[0]];
                }
                return valKeyExtract.bind(null, valExtract);
            } else if (valExtractType == "string" || valExtractType == "number") {
                return function(point, value) {
                    if (value != undefined) {
                        point[valExtract] = value;
                    }
                    return point[valExtract];
                }
            } else {
                utils.assert("Unagle to interpret the key extractor", JSON.stringify(valExtract))
            }
        }

        wrap() {}
    }


    class KeyExtractorPlugin extends Plugin {

        constructor(track, name, valExtract, attrs2Serialise=[], options={}) {
            super(track, name, attrs2Serialise.concat(["valueCount"]), options);
            this.valExtract = valExtract;
            this.valueCount = 0;
            this.valueOf    = this._valueExtractor(valExtract);
        }

        append(point) {
            let value = this.valueOf(point);
            if (value != undefined) {
                this.valueCount++;
            }
            return {
                point : point,
                value : value
            };
        }

        appendTrack(track) {
            let result = super.appendTrack(track);
            this.valueCount += result.valueCount;
            return result;
        }

        wrap() {
            return !!this.valueCount;
        }

        getAt(mts, itRange) {
            itRange = itRange || this.track.getAt(mts);
            const self = this;
            if (itRange) {
                let left  = itRange[0].backward().find(function(pt) {
                    return self.valueOf(pt) != undefined;
                });
                let right = itRange[1].forward().find(function(pt) {
                    return self.valueOf(pt) != undefined;
                });
                return (left || right) && [left && left.bidir(), right && right.bidir()];
            }
        }
    }



    class StatsPlugin extends KeyExtractorPlugin {

        constructor(track, name, valExtract, attrs2Serialise=[], options = {}) {
            super(track, name, valExtract, attrs2Serialise.concat(["min", "max", "mid", "d"]), options);
            const self = this;
            if ((this.options.max && this.options.max.percentile) ||
                (this.options.min && this.options.min.percentile)) {
                const self = this;
                this.mSortingTree = new SortingTreeNode(function(a,b) {
                    a = self.valueOf(a);
                    b = self.valueOf(b);
                    if (a == b) {
                        return 0;
                    } else if (a < b) {
                        return -1;
                    } else {
                        return 1;
                    }
                });
            }
            Object.defineProperty(this, "d", {
                get: function() {
                    return self.max && self.min && (self.max.value - self.min.value);
                },
                enumerable: true,
                configurable: false
            });
            Object.defineProperty(this, "mid", {
                get: function() {
                    return self.max && self.min && self.min.value + self.d/2;
                },
                enumerable: true,
                configurable: false
            });
        }

        _append(point) {
            if (!this.max || this.max.value < point.value) {
                this.max = point;
            }

            if (!this.min || this.min.value > point.value) {
                this.min = point;
            }
        }

        append(point) {
            let result = super.append(point);
            if (result.value != undefined) {
                if (this.mSortingTree) {
                    this.mSortingTree.put(point);
                };
                this._append(result);
            }
            return result;
        }

        appendTrack(track) {
            let result = super.appendTrack(track);
            this._append(result.max);
            this._append(result.min);
            return result;
        }

        wrap() {
            let something2Wrap = super.wrap();
            if (something2Wrap) {
                if (this.mSortingTree && this.mSortingTree.count) {
                    if (this.options.max && this.options.max.percentile) {
                        this.max = this.mSortingTree.collect(
                            "rcl",
                            this.mSortingTree.count * (this.options.max.percentile/100)
                        );
                        this.max = {
                            point : this.max.front(),
                            value : this.valueOf(this.max.front())
                        }
                    }
                    if (this.options.min && this.options.min.percentile) {
                        this.min = this.mSortingTree.collect(
                            "lcr",
                            this.mSortingTree.count * (this.options.min.percentile/100)
                        );
                        this.min = {
                            point : this.min.back(),
                            value : this.valueOf(this.min.back())
                        }
                    }
                }
            }
            delete this["mSortingTree"];
            return something2Wrap;
        }

        getAt(mts, itRange) {
            itRange = super.getAt(mts, itRange);
            if (itRange && itRange[0] && itRange[1]) {
                if (itRange[0].equals(itRange[1])) {
                    return this.valueOf(itRange[0].current().point);
                }
                let left  = itRange[0].current();
                let right = itRange[1].current();
                return geometry.linearApprox(
                    this.valueOf(left.point),
                    this.valueOf(right.point),
                    (left.track.frmStrt + left.point.frmStrt) - (right.track.frmStrt + right.point.frmStrt),
                    (left.track.frmStrt + left.point.frmStrt) - mts
                )
            }
        }
    }



    class RollupBase {
        constructor(plugin, rolledUpTrack, appendCb, rollupCb, fields) {
            this.plugin        = plugin;
            this.rolledUpTrack = rolledUpTrack;
            this.appendCb      = appendCb;
            this.rollupCb      = rollupCb;
            for (var k in fields) {
                utils.assert(!(k in this), "fields clash on: "+k);
                this[k] = fields[k];
            }
        }

        append(pt, it) {
            let value  = this.plugin.valueOf(pt);
            if (value != undefined) {
                return this.appendCb && this.appendCb.call(this, value, it);
            }
            return false;
        }

        rollup(pt, startIt, stopIt) {
            return this.rollupCb.call(this, pt, startIt, stopIt);
        }
    }




    class Tracks extends TrackBase {

        constructor() {
            super();
            this.tracks  = [];
            const self = this;
            Object.defineProperty(this, 'length', {
                get: function() {
                    return self.tracks.reduce(function(len, trk) {return len+trk.length}, 0);
                },
                enumerable: true,
                configurable: false
            });
            Object.defineProperty(this, 'distanceMts', {
                get: function() {
                    let back = new BidirIterator(self.tracks).back().current();
                    return (back && (back.track.frmStrt + back.point.frmStrt)) || 0;
                },
                enumerable: true,
                configurable: false
            });
        }

        append(track) {
            utils.assert(!this.mLocked, "Track locked for appending");
            utils.assert(track instanceof Track, "can only append Tracks");
            const self = this;
            if (Array.isArray(track)) {
                track.forEach(function(trk) {
                    self.append(trk);
                });
            } else {
                track.plugins.list.forEach(function(plugin) {
                    if (track[plugin.name] && !self[plugin.name]) {
                        let clone = plugin.clone(self);
                        self[plugin.name] = clone;
                        self.plugins.list.push(clone);
                    }
                });
                if (this.plugins.list.reduce(function(accept, plugin) {
                    return plugin.appendTrack(track);
                }, true)) {
                    this.tracks.push(track);
                }
            }
            return this;
        }

        wrap() {
            if (!this.mLocked) {
                const self = this;
                this.plugins.list.forEach(function(plugin) {
                    plugin.wrap(self.tracks.map(function(track) {
                        return track[plugin.name];
                    }));
                });
                this.mLocked = true;
            }
            return this.length ? this : undefined;
        }

        getAt(mts) {
            utils.assert(mts >= 0, "distance must be positive");
            let it = new BidirIterator(this.tracks);
            let current = it.current();
            while(current && mts >= current.track.frmStrt) {
                let adjMts = mts-current.track.frmStrt;
                if (adjMts <= current.track.track.points.back().frmStrt) {
                    return current.track.track.getAt(adjMts).map(function(tit) {
                        return new BidirIterator(it, {point:tit._idx.point});
                    });
                }
                current = it.nextTrack().current();
            }
            //we have an in-between tracks scenario
            if (current) {
                let right = it;
                let left  = it.clone().prev();
                return [left,right];
            }
        }

        iterator() {
            return new BidirIterator(this.tracks);
        }

        back() {
            return this.tracks.back() && this.tracks.back().back();
        }

        front() {
            return this.tracks.front() && this.tracks.front().front();
        }

        rollup(step) {
            if (this.tracks.length == 0) {
                return;
            }
            if (step) {
                if (typeof step == "number") {
                    step = {min:step}
                }
            } else {
                step = {};
            }
            step.min = step.min || 0;
            step.max = step.max || Infinity;

            let result = this.tracks.front().clone();
            let plugins = this.plugins.list.filter((plugin) => {
                return "rollup" in plugin;
            })
            .map((plugin) => {
                return plugin.rollup(result);
            });
            let segmentStartIt = this.iterator().forward();
            segmentStartIt.clone().forEach(function(pt, it) {
                let frmPrv = it.current().frmStrt - segmentStartIt.current().frmStrt;
                if (plugins.find(function(plugin) {
                    return plugin.append(pt, it);
                }) && frmPrv >= step.min ||
                      it.isFront() || it.isBack() || frmPrv >= step.max
                   ) {
                    pt = geometry.LatLng.create(pt);
                    result.append(plugins.reduce(function(pt, plugin) {
                        return plugin.rollup(pt, segmentStartIt, it);
                    }, pt));
                    pt.frmStrt = it.current().frmStrt;
                    segmentStartIt = it.clone();
                }
            });
            return result.wrap();
        }
    }

    const KMinDistanceForClimbMts = 50;
    class Elevation extends StatsPlugin {

        constructor(track, valExtract, options = {}) {
            super(track, "ele", valExtract, ["gain"], options);
            const self        = this;
            this.gain         = 0;
            this.climbBuckets = {};
            Object.defineProperty(this, 'rugged', {
                get: function() {
                    return this.d > 100 || (this.d > 10 && this.d > track.distanceMts/80);
                },
                enumerable: true,
                configurable: false
            });
        }

        clone(track) {
            return new Elevation(track, this.valExtract);
        }

        append(point) {
            let result = super.append(point);
            if (result.value) {
                const self = this;
                if (this.prev4Climb) {
                    let distanceToRef = result.point.frmStrt - this.prev4Climb.frmStrt;
                    if (distanceToRef >= KMinDistanceForClimbMts) {
                        let climbReferenceVal = this.valueOf(this.prev4Climb)
                        point.clmbBk = Math.round(
                            100*(result.value - climbReferenceVal)/distanceToRef
                        );
                        this.climbBuckets[point.clmbBk]  = this.climbBuckets[point.clmbBk] || 0;
                        this.climbBuckets[point.clmbBk] += Math.round(distanceToRef);
                        let d = result.value - climbReferenceVal;
                        if (d > 0) {
                            this.gain += d;
                        }
                        this.prev4Climb = result.point;
                    }
                } else {
                    this.prev4Climb = result.point;
                }
            }
            return result;
        }

        appendTrack(track) {
            let result = super.appendTrack(track);
            this.gain += result.gain;
            for (var bucket in result.climbBuckets) {
                this.climbBuckets[bucket] =  this.climbBuckets[bucket] || 0;
                this.climbBuckets[bucket] += result.climbBuckets[bucket];
            }
            return result;
        }

        getAt(mts, itRange) {
            let result = super.getAt(mts, itRange);
            return result && Math.round(result);
        }

        rollup(rolledUpTrack) {
            const self = this;
            return new RollupBase(this, rolledUpTrack, function(e, it) { //append
                let clmbBk = it.current().point.clmbBk;
                let reject = false;
                if (clmbBk != undefined) {
                    if (this.prevClmbBk != undefined) {
                        reject = Math.sign(this.prevClmbBk) != Math.sign(this.prevClmbBk);
                    }
                    this.prevClmbBk = clmbBk;
                }
                this.max = Math.max(this.max || -Infinity, e);
                this.min = Math.min(this.min ||  Infinity, e);
                return reject;
            }, function(pt, startIt, stopIt) { //rollup
                startIt   = startIt.bidir();
                stopIt    = stopIt.bidir();
                let right = self.getAt(stopIt.current().frmStrt,  [stopIt, stopIt]);
                let left  = self.getAt(startIt.current().frmStrt, [startIt, startIt]);
                this.max = Math.max(this.max || -Infinity, left);
                this.min = Math.min(this.min ||  Infinity, left);

                if (this.max != undefined) {
                    let span = this.max - this.min;
                    let mid  = this.min + span/2;
                    span = Math.abs(span);
                    function extreme(e) {
                        if (Math.abs(e-mid) > span) {
                            return e || e+0.1; //to avoid undefined/0 confusion
                        }
                    }
                    self.valueOf(pt, Math.round(extreme(this.max) || extreme(this.min) || right));
                }
                return pt;
            });
        }

        wrap(plugins) {
            const self = this;
            if (!plugins && this.valueCount) {
                function approxFrom(what, fromWhat) {
                    if (fromWhat.length > 1) {
                        return Math.round(
                            geometry.linearApprox(
                                self.valueOf(fromWhat[0]),
                                self.valueOf(fromWhat[1]),
                                fromWhat[0].frmStrt - fromWhat[1].frmStrt,
                                fromWhat[0].frmStrt - what.frmStrt
                            )
                        );
                    }
                }
                let it    = this.track.iterator();
                let front = it.current().point;
                if (this.valueOf(front) == undefined) {
                    this.valueOf(
                        front,
                        approxFrom(
                            front,
                            it.forward().reduce(function(approxFrom, pt) {
                                let value = self.valueOf(pt);
                                if (value != undefined) {
                                    approxFrom.push(pt);
                                }
                                return approxFrom;
                            }, [], function(approxFrom) {
                                return approxFrom.length > 1;
                            })
                        )
                    );
                    this.append(front);
                }
                let back = it.back().current().point;
                if (this.valueOf(back) == undefined) {
                    this.valueOf(
                        back,
                        approxFrom(
                            back,
                            it.backward().reduce(function(approxFrom, pt) {
                                let value = self.valueOf(pt);
                                if (value != undefined) {
                                    approxFrom.push(pt);
                                }
                                return approxFrom;
                            }, [], function(approxFrom) {
                                return approxFrom.length > 1;
                            })
                        )
                    );
                    this.append(back);
                }
            }
            delete this["prev4Climb"];
            return super.wrap();
        }
    }
    Elevation.create = function(valExtract, track) {
        return new Elevation(track, valExtract)
    }

    return {
        Elevation           : Elevation,
        Plugin              : Plugin,
        KeyExtractorPlugin  : KeyExtractorPlugin,
        StatsPlugin         : StatsPlugin,
        RollupBase          : RollupBase,
        Track               : Track,
        Tracks              : Tracks
    };

})();

module.exports = trackModule;