/**
 * scripts/lib/popularity.js
 *
 * This, based on the value of the master slider,
 * calculates and returns the popularity preference
 */

'use strict';

var popularity = (function() {

    function PopularitySliderPreference(parent, jsonDocument) {
        this.mParent   = parent;
        this.mJSON     = jsonDocument;
    }

    PopularitySliderPreference.prototype.masterSlideOptions = function() {
        var result = {
            label: "popularity",
            value: 1,
            min: {
                value: 1,
                label: "more popular"
            },
            max: {
                value: 0.3
            },
            step: -0.05,
            underConstruction: true
        }
        this.mMasterSliderValue = result.value;
        return result;
    }


    PopularitySliderPreference.prototype.fromMasterSlider = function(masterSliderValue, rootJson) {
        this.mMasterSliderValue = masterSliderValue
        if (rootJson) {
            return this.generate(rootJson);
        }
    }

    PopularitySliderPreference.prototype.render = function(panel) {
    }

    PopularitySliderPreference.prototype.generate = function(rootJson) {
    }

    return {
        PopularitySliderPreference: PopularitySliderPreference,
        sliderGenerator : PopularitySliderPreference
    }

})();

module.exports = popularity;