/**
 * scripts/lib/geometry.js
 */

'use strict';
const turf       = {
    distance : require("turf-distance"),
    point    : require('turf-helpers').point
}

const intersects = require("../external/line-segments-intersect.js");
const utils      = require("./utils.js");

var geometry = (function() {

    const KEarthRadiusMts = 6371000;

    function dot(v1,v2) {
        return (v1.x*v2.x)+(v1.y*v2.y);
    }

    function cross(v1, v2) {
        return (v1.x*v2.y)-(v1.y*v2.x)
    }

    function project(line, point, force) {
        var vline =             {
            x : line[1].x-line[0].x,
            y : line[1].y-line[0].y
        };
        var vlineLengthSqrd = Math.pow(vline.x, 2)+Math.pow(vline.y,2);
        if (vlineLengthSqrd == 0) {
            //0-lenght segment => point
            return force ? line[0] : undefined;
        }
        var projectionRatio = dot(
            vline,
            {
                x : point.x - line[0].x,
                y : point.y - line[0].y
            }
        )/vlineLengthSqrd;

        if (projectionRatio < 0) {
            //beyond the mStart end of the segment
            return force ? line[0] : undefined;
        } else if (projectionRatio > 1) {
            //beyond the mEnd end of the segment
            return force ? line[1] : undefined;
        }
        return {
            x: line[0].x + (projectionRatio * vline.x),
            y: line[0].y + (projectionRatio * vline.y)
        };
    }

    function comparableDistance(a,b) {
        return Math.pow(a.x-b.x, 2) + Math.pow(a.y-b.y, 2);
    }

    function toRad(deg) {
         return deg * Math.PI / 180;
    }
    function toDeg(rad) {
        return rad * 180 / Math.PI;
    }

    const KMapBoxPresent = "mapboxgl" in window;

    var LngLat = KMapBoxPresent ? mapboxgl.LngLat : class Foo {}
    class InvertedLngLat extends LngLat {
        constructor(lat, lng) {
            super(lng, lat)
        }
    }
    function addXYProps(clazz) {
        if (!clazz.prototype.hasOwnProperty("x")) {
            Object.defineProperty(clazz.prototype, 'x', {
                get: function() { return this.lng },
                enumerable  : false,
                configurable: false
            });
            Object.defineProperty(clazz.prototype, 'y', {
                get: function() { return this.lat },
                enumerable  : false,
                configurable: false
            });
        }
    }
    const LatLng       = KMapBoxPresent ? InvertedLngLat        : L.LatLng;
    const LatLngBounds = KMapBoxPresent ? mapboxgl.LngLatBounds : L.LatLngBounds;
    if (!("contains" in LatLngBounds.prototype)) {
        LatLngBounds.prototype.contains = function(other) {
            if (other.lat != undefined && other.lng != undefined) {
                return other.lng >= this.getWest() &&
                       other.lng <= this.getEast() &&
                       other.lat >= this.getSouth() &&
                       other.lat <= this.getNorth()
            }
            utils.assert(false, "Unable to verify whether " + other + " is contained in " + this);
        }
    }
    if (KMapBoxPresent) {
        addXYProps(mapboxgl.LngLat);
        mapboxgl.LngLat.prototype.clone = function() {
            return new LatLng(this.lat, this.lng);
        }
        mapboxgl.LngLat.prototype.distanceTo = function(other) {
            return turf.distance(
                turf.point([this.lng,this.lat]),
                turf.point([other.lng, other.lat])
            ) * 1000; //in metres
        }
        mapboxgl.LngLat.prototype.comparableDistance = function(other) {
            return Math.pow(this.lat - other.lat, 2) + Math.pow(this.lng - other.lng, 2);
        }
        mapboxgl.LngLat.prototype.equals = function(other, comparableDistance) {
            return (comparableDistance && this.comparableDistance(other) < comparableDistance) || this.lat == other.lat && this.lng == other.lng;
        }
        mapboxgl.LngLat.prototype.onemeterComparableDistance = 8*Math.pow(10,-11); //new LatLng(50,0).offsetCourse(1,0).comparableDistance(new LatLng(50,0)) = 8.087793490743076e-11

        mapboxgl.LngLatBounds.prototype.isValid = function() {
            return this._ne && this._sw;
        }
        mapboxgl.Point.manhattanLenght
    } else {
        addXYProps(L.LatLng);
    }

    LatLng.prototype.toArray = function(pt) {
        let result = [this.lat, this.lng];
        let d3c = {};
        let val;
        for (var key in this.d3c) {
            if (key[0] == "_") {continue;}
            val = this.d3c[key];
            if (val == undefined)    {continue;}
            d3c[key] = val;
        }
        if (Object.keys(d3c).length) {
            result.push(d3c);
        }
        return result;
    }

    LatLng.create = function(pt, d3c) {
        if (Array.isArray(pt)) {
            d3c = d3c || pt[2];
            pt = new geometry.LatLng(pt[0], pt[1]);
        } else {
            d3c = d3c || pt.d3c;
            if ("lat" in pt && "lng" in pt) {
                pt = new geometry.LatLng(pt.lat, pt.lng);
            } else if ("x" in pt && "y" in pt) {
                pt = new geometry.LatLng(pt.y, pt.x);
            } else {
                utils.assert(false, "Unable to construct a milestone from "+JSON.stringify(pt))
                return;
            }
        }
        if (d3c) {
            pt.d3c = utils.cloneSimpleObject(d3c);
        }
        return pt;
    }

    LatLng.prototype.offsetCourse = function(distanceMts, aCourseRad) {
        var earthPerimeter = 2 * Math.PI * KEarthRadiusMts;
        var distance       = 2 * Math.PI * distanceMts / earthPerimeter;
        var srcLat         = toRad(this.lat);
        var srctLon        = toRad(this.lng);

        // http://williams.best.vwh.net/avform.htm#LL
        var dstLat = Math.asin(Math.sin(srcLat)*Math.cos(distance)+Math.cos(srcLat)*Math.sin(distance)*Math.cos(aCourseRad));
        var dlon   = Math.atan2(Math.sin(aCourseRad)*Math.sin(distance)*Math.cos(srcLat),Math.cos(distance)-Math.sin(srcLat)*Math.sin(dstLat));
        var dstLon = srctLon - dlon;

        return new LatLng(toDeg(dstLat), toDeg(dstLon));
    }

    LatLngBounds.prototype.expand = function(by) {
        if ("metres" in by) {
            let span = {
                lat : this.getNorthWest().distanceTo(this.getSouthWest()),
                lng : this.getNorthWest().distanceTo(this.getNorthEast())
            }
            return this.expand({
                factor : {
                    lat : 1 + (by.metres / span.lat),
                    lng : 1 + (by.metres / span.lng)
                }
            });
        } else if ("factor" in by) {
            let factor = by.factor;
            if (typeof factor === "number") {
                factor = {
                    lat : factor,
                    lng : factor
                }
            }
            var dLat = Math.abs(this.getNorth() - this.getSouth());
            var dLng = Math.abs(this.getWest()  - this.getEast());
            var expandLat = (dLat * factor.lat - dLat)/2;
            var expandLng = (dLng * factor.lng - dLng)/2;
            this.extend( //new north west
                new LatLng(this.getNorth() + expandLat, this.getWest() - expandLng)
            );
            this.extend( //new south east
                new LatLng(this.getSouth() - expandLat, this.getEast() + expandLng)
            );
        } else {
            utils.assert(false, "unable to expand LatLngBounds by "+by);
        }
        return this;
    }
    LatLngBounds.prototype.clone = function() {
        return new LatLngBounds(this.getSouthWest(), this.getNorthEast());
    }
    /*LatLngBounds.prototype.polygon = function() {
        return [
            [this.getNorth(), this.getWest()],
            [this.getNorth(), this.getEast()],
            [this.getSouth(), this.getEast()],
            [this.getSouth(), this.getWest()],
            [this.getNorth(), this.getWest()]
        ];
    }*/


    class Point {
        constructor() {
            utils.assert(arguments.length, "Point c'tor arguments missing");
            if (arguments.length == 1) {
                var from = arguments[0];
                if (Array.isArray(from) && from.length > 1) {
                    this.x = from[0];
                    this.y = from[1];
                } else if (typeof from === "object" && "x" in from && "y" in from) {
                    this.x = from.x;
                    this.y = from.y;
                } else {
                    utils.assert(false, "Unable to construct Point from ");
                    console.dir(from);
                }
            } else {
                utils.assert(arguments.length <= 2, "Too many Point c'tor arguments "+arguments+", accepting x, y");
                this.x = arguments[0];
                this.y = arguments[1];
            }
        }

        distanceTo(other) {
            return Math.sqrt(this.distanceSqrd(other));
        }

        offsetCourse(length, courseRad) {
            return new Point(length * Math.cos(courseRad) + this.x, length * Math.sin(courseRad) + this.y);
        }

        distanceSqrd(other) {
            return Math.pow(this.x-other.x, 2) + Math.pow(this.y-other.y, 2);
        }

        rotate(angle, centerPoint) {
            var radians = toRad(angle),
                cos = Math.cos(radians),
                sin = Math.sin(radians),
                nx = (cos * (this.x - centerPoint.x)) + (sin * (this.y - centerPoint.y)) + centerPoint.x,
                ny = (cos * (this.y - centerPoint.y)) - (sin * (this.x - centerPoint.x)) + centerPoint.y;
            return new Point(nx, ny);
        }

        equals(other) {
            return other && this.x == other.x && this.y == other.y;
        }
    }


    function extractPoints() {
        if (arguments.length) {
            if (arguments.length == 1) {
                var argument = arguments[0];
                if (Array.isArray(argument)) {
                    return extractPoints(argument);
                }
                if (argument instanceof Point) {
                    return [argument];
                }
                if (argument instanceof Bounds) {
                    return [argument.min, argument.max];
                }
                if (argument instanceof DOMRect) {
                    return [
                        new Point(argument.left, argument.top),
                        new Point(argument.right, argument.bottom)
                    ];
                }
                return [new Point(argument)];
            } else {
                return arguments.reduce(function(cum, argument) {
                    return cum.concat(
                        extractPoints(argument)
                    );
                }, []);
            }
        }
    }

    class Bounds {

        constructor() {
            this.extend.apply(this, arguments);
        }

        extend() {
            var self = this;
            var points = extractPoints.apply(null, arguments);
            points.forEach(function(point) {
                if (self.max) {
                    self.max.x = Math.max(point.x, self.max.x);
                    self.max.y = Math.max(point.y, self.max.y);
                } else {
                    self.max = new Point(point);
                }
                if (self.min) {
                    self.min.x = Math.min(point.x, self.min.x);
                    self.min.y = Math.min(point.y, self.min.y);
                } else {
                    self.min = new Point(point);
                }
            });
            return this;
        }

        expand(by) {
            if (this.max && this.min) {
                this.max.x += by;
                this.max.y += by;
                this.min.x -= by;
                this.min.y -= by;
            }
            return this;
        }

        contains() {
            if (this.max && this.min) {
                var self = this;
                var points = extractPoints.apply(null, arguments);
                return points.every(function(point) {
                    return point.x >= self.min.x &&
                           point.x <= self.max.x &&
                           point.y >= self.min.y &&
                           point.y <= self.max.y
                });
            }
        }
    }


    class Vector extends Point {

        constructor() {
            if (arguments.length == 1 && arguments instanceof Point) {
                super(arguments[0].x, arguments[0].y);
            } else if (arguments.length == 2) {
                if (typeof arguments[0] === "object" && typeof arguments[1] === "object") {
                    super(arguments[1].x - arguments[0].x, arguments[1].y - arguments[0].y);
                } else {
                    super(arguments[0], arguments[1]);
                }
            } else {
                utils.assert(false, "Unable to construct Vector from "+arguments);
            }
        }

        angle() {
            return toDeg(Math.atan2(this.x, this.y));
        }

        //see: http://www.freesteel.co.uk/wpblog/2009/06/05/encoding-2d-angles-without-trigonometry/
        diamondAngle(other) {
            var result = undefined;
            if (this.y >= 0) {
                result = (this.x >= 0 ? this.y/(this.x+this.y) : 1-this.x/(-this.x+this.y));
            } else {
                result = (this.x < 0 ? 2-this.y/(-this.x-this.y) : 3+this.x/(this.x-this.y));
            }
            result = 2-result;
            if (other) {
                result = result - other.diamondAngle();
            }
            return result;
        }


        dot(other) {
            return dot(this, other);
        }

        cross(other) {
            return cross(this, other);
        }

        slope() {
            return this.y/this.x;
        }

        lengthSqrd() {
            return Math.pow(this.x,2) + Math.pow(this.y, 2);
        }

        length() {
            return Math.sqrt(this.lengthSqrd());
        }

        manhattanLenght() {
            return this.x + this.y;
        }

        half() {
            return new this.constructor(this.x/2, this.y/2);
        }

        toArray() {
            return [this.x, this.y];
        }

        toJSON() {
            return {x : this.x, y: this.y};
        }
    }

    class GeoVector extends Vector {
        constructor() {
            switch (arguments.length) {
                case 1: utils.assert("not supported");
                case 2: super(arguments[1], arguments[0]);
                default: utils.assert("not supported");
            }
            ;
            Object.defineProperty(this, 'lng', {
              get: function() { return this.x; },
              enumerable: true,
              configurable: false
            });
            Object.defineProperty(this, 'lat', {
              get: function() { return this.y; },
              enumerable: true,
              configurable: false
            });
        }

        toJSON() {
            return {lat : this.lat, lng: this.lng};
        }
    }


    class GeoLine {
        constructor(start, end) {
            this.mStart = start;
            this.mEnd   = end;
            this.mV     = new GeoVector(this.mEnd.lat - this.mStart.lat, this.mEnd.lng - this.mStart.lng);
        }

        projectOnto(geoPoint, force) {
            var projection = project([this.mStart, this.mEnd], geoPoint, force);
            if (projection) {
                return new LatLng(projection.y, projection.x);
            }
        }

        offsetMts(metresFromStart) {
            var segmentLengthMts   = this.mStart.distanceTo(this.mEnd);
            return this.offsetRatio(metresFromStart/segmentLengthMts);
        }

        offsetRatio(ratio) {
            return new LatLng(
                this.mStart.lat + (this.mV.lat * ratio),
                this.mStart.lng + (this.mV.lng * ratio)
            );
        }

        lengthSqrd() {
            return Math.pow(this.mV.lat,2)+Math.pow(this.mV.lng,2);
        }

        length() {
            return Math.sqrt(this.lengthSqrd());
        }

        flip() {
            return new GeoLine(this.mEnd, this.mStart);
        }

        intersect(other) {
            var point = intersects(
                [[this.mStart.lng, this.mStart.lat], [this.mEnd.lng, this.mEnd.lat]],
                [[other.mStart.lng, other.mStart.lat], [other.mEnd.lng, other.mEnd.lat]]
            );
            if (point) {
                return new LatLng(point[1], point[0]);
            }
        }

        bearing() {
            //after: http://www.movable-type.co.uk/scripts/latlong.html
            var from = new LatLng(toRad(this.mStart.lat), toRad(this.mStart.lng));
            var to   = new LatLng(toRad(this.mEnd.lat), toRad(this.mEnd.lng));

            var y = Math.sin(to.lng-from.lng) * Math.cos(to.lat);
            var x = Math.cos(from.lat)*Math.sin(to.lat) -
                    Math.sin(from.lat)*Math.cos(to.lat)*Math.cos(to.lng-from.lng);
            var brng = Math.atan2(y, x);
            return (toDeg(brng)+ 360) % 360;
        }
    }




    class GeoLineString {
        constructor(points) {
            this.mPoints = points;
        }

        projectOnto(point) {
            var best =  undefined;
            for (var i = 1; i < this.mPoints.length; i++) {
                let projection = project(
                    [ this.mPoints[i-1], this.mPoints[i] ],
                    point,
                    true
                );
                let distance = comparableDistance(projection, point);
                if (!best || best.comparableDistance > distance) {
                    best = {
                        projection : projection,
                        comparableDistance : distance,
                        index : {
                            start : i-1,
                            end   : i
                        }
                    }
                }
            }
            if (best) {
                best.projection    = new LatLng(best.projection.y, best.projection.x);
                best = {
                    projection  : best.projection,
                    distanceMts : best.projection.distanceTo(point),
                    segment     : {
                        start : {
                            index : best.index.start,
                            point : this.mPoints[best.index.start],
                            distanceMts: point.distanceTo(this.mPoints[best.index.start])
                        },
                        end   : {
                            index : best.index.end,
                            point : this.mPoints[best.index.end],
                            distanceMts : point.distanceTo(this.mPoints[best.index.end])
                        },
                        line  : new GeoLine(this.mPoints[best.index.start], this.mPoints[best.index.end])
                    }
                }
                best.segment.closest = (best.segment.start.distanceMts > best.segment.end.distanceMts) ?
                    best.segment.end : best.segment.start;
            }

            return best;
        }
    }



    class PixLine {
        constructor(start, end) {
            this.mStart = start;
            this.mEnd   = end;
            this.mV     = new Vector(this.mEnd.x - this.mStart.x, this.mEnd.y - this.mStart.y);
        }

        projectOnto(pixPoint, force) {
            var projection = project([this.mStart, this.mEnd], pixPoint, force);
            if (projection) {
                return new Point(projection.x, projection.y);
            }
        }

        offsetMts(metresFromStart) {
            var segmentLengthMts   = this.mStart.distanceTo(this.mEnd);
            return this.offsetRatio(metresFromStart/segmentLengthMts);
        }

        offsetRatio(ratio) {
            return new L.LatLng(
                this.mStart.x + (this.mV.x * ratio),
                this.mStart.y + (this.mV.y * ratio)
            );
        }

        lengthSqrd() {
            return Math.pow(this.mV.x,2)+Math.pow(this.mV.y,2);
        }

        length() {
            return Math.sqrt(this.lengthSqrd());
        }

        flip() {
            return new PixLine(this.mEnd, this.mStart);
        }

        toString() {
            return "["+this.mStart.toString()+", "+this.mEnd.toString()+"]";
        }

        intersect(other) {
            var point = intersects(
                [[this.mStart.x, this.mStart.y], [this.mEnd.x, this.mEnd.y]],
                [[other.mStart.x, other.mStart.y], [other.mEnd.x, other.mEnd.y]]
            );
            if (point) {
                return new Point(point[0], point[1]);
            }
        }

        rotate(angle, centerPoint) {
            return new PixLine(
                this.mStart.rotate(angle, centerPoint),
                this.mEnd.rotate(angle, centerPoint)
            );
        }
    }



    function linearApprox(v1, v2, v1v2Distance, approxAtDistance) {
        if (!v1v2Distance) {
            if (approxAtDistance == 0) {
                return v1;
            }
            utils.assert(false, "distance between references musn't be 0");
        }
        var slope = 1.0*(v2-v1)/v1v2Distance;
        return v1+(slope*approxAtDistance);
    }

    return {
        Point         : Point,
        Bounds        : Bounds,
        LatLng        : LatLng,
        LatLngBounds  : LatLngBounds,
        GeoLine       : GeoLine,
        PixLine       : PixLine,
        Vector        : Vector,
        GeoVector     : GeoVector,
        GeoLineString : GeoLineString,
        linearApprox  : linearApprox,
        toRad         : toRad,
        toDeg         : toDeg
    }

})();

module.exports = geometry;