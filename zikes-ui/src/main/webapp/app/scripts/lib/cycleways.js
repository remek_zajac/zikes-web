/**
 * scripts/lib/cycleway.js
 *
 * This, based on the value of the master slider,
 * calculates and returns the cycleway preference
 */

'use strict';

var cyclewayPreference = (function() {



    const KSlider =  {
        min : 1,
        max : 40
    };
    const KFactor = {
        min: 0.1,
        max: 1.0
    }
    const Ka = (KFactor.min - KFactor.max)/(KSlider.max-KSlider.min);
    const Kb = KFactor.max - Ka;

    class CyclewaySliderPreference {
        constructor(parent) {
            this.mParent   = parent;
        }

        masterSlideOptions() {
            var result = {
                label: "cycleways",
                value: KSlider.min,
                min: {
                    value: KSlider.min,
                    label: ""
                },
                max: {
                    value: KSlider.max,
                    label: "more cycleways"
                },
                step: 1
            }
            this.mMasterSliderValue = result.value;
            return result;
        }


        fromMasterSlider(masterSliderValue, rootJson) {
            this.mMasterSliderValue = masterSliderValue
            if (rootJson) {
                return this.generate(rootJson);
            }
        }

        render(panel) {}

        generate(rootJson) {
            var factor = Math.round(100*(Ka*this.mMasterSliderValue + Kb))/100.0;
            var highwayFactors = rootJson["highwayCostFactors"]
            for (var highwayType in highwayFactors) {
                var factors4HighwayType = highwayFactors[highwayType];
                factors4HighwayType["designated"] = (parseFloat(factors4HighwayType["cost"]) * factor).toFixed(2);
            }
            highwayFactors["default"] = {
                designated: factor.toFixed(2)
            }
        }
    }

    return {
        CyclewaySliderPreference: CyclewaySliderPreference,
        sliderGenerator : CyclewaySliderPreference
    }

})();

module.exports = cyclewayPreference;