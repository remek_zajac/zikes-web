/**
 * scripts/lib/mapquest.js
 *
 */

'use strict';

const when = require("when");
const http = require("./http.js");

const KEY="Fmjtd|luur2du7nq,72=o5-9ar2uz"

module.exports = {
	elevation: function(latlngs) {
		if (latlngs.length == 0) {
			return when([]);
		} else if (latlngs[0].lat) {
			latlngs = latlngs.map((pt) => {return [pt.lat,pt.lng]})
		}
		latlngs=latlngs.join(",");
		return http.get("//open.mapquestapi.com/elevation/v1/profile?key="+KEY+"&shapeFormat=raw&latLngCollection="+latlngs)
        .then(function(response) {
            return response.elevationProfile.map((pt) => {return pt.height});
        });
	}
};
