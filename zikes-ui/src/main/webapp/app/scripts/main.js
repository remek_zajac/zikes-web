/**
 * scripts/main.js
 *
 * This is the starting point for your application.
 * Take a look at http://browserify.org/ for more info
 */

'use strict';

var journeys = require("./ui/desktop/journeys.js");
//the bootstrap for the big browser
global.zikes = {
	journeys: new journeys.Journeys( //this is to allow html refer to some entry points
		require("./ui/desktop/map.js"), $("#journeys"), $("#journey")
	)
};
