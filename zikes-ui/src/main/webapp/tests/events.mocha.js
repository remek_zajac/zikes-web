var assert        = require("assert");
var when          = require("when");
var mouseSim      = require("../app/scripts/lib/mouseSim.js");
var utils         = require("../app/scripts/lib/utils.js");
var map           = require("./mapStub.js").map;

suite("Events", function() {

  suite("OverridenSubsciption", function() {

    test("should trigger basic event", function(done) {
        map.on("click", function() {
            done();
        });
        map.fireEvent("click", {
          latlng: map.getCenter()
        });
    });

    var overridesUnderTest = new utils.OverridenSubsciption(map);
    test("should subscribe and unsubscribe a callback", function(done) {
        var cbTimes = 0;
        var key = {
            eventType: "mousemove",
            cb       : function() {cbTimes++;}
        }
        overridesUnderTest.on(key, key.cb);
        map.fireEvent("mousemove", {
          latlng: map.getCenter()
        });
        map.fireEvent("mousemove", {
          latlng: map.getCenter()
        });
        when.delay(0)
        .then(function() {
            assert.equal(cbTimes,2);
        })
        .then(function() {
            overridesUnderTest.off(key);
            map.fireEvent("mousemove", {
              latlng: map.getCenter()
            });
        }).delay(50)
        .then(function() {
            assert.equal(cbTimes,2);
            assert.equal(Object.keys(overridesUnderTest.subscriptions).length, 0);
        })
        .then(done)
        .otherwise(done)
    });

    class O {
        constructor() {
            this.times = 0;
        }
        cb() {
            this.times++;
        }
    }

    test("should subscribe and unsubscribe a callback with context", function(done) {
        var o1 = new O();
        var o2 = new O();
        var key1 = {
            eventType: "mousemove",
            context  : o1,
            cb       : o1.cb
        }
        var key2 = {
            eventType: "mousemove",
            context  : o2,
            cb       : o2.cb
        }
        overridesUnderTest.on(key1, key1.cb.bind(o1));
        overridesUnderTest.on(key2, key2.cb.bind(o2));
        map.fireEvent("mousemove", {
          latlng: map.getCenter()
        });
        when.delay(0)
        .then(function() {
            assert.equal(o1.times,1);
            assert.equal(o2.times,1);
        })
        .then(function() {
            overridesUnderTest.off(key1);
            map.fireEvent("mousemove", {
              latlng: map.getCenter()
            });
        }).delay(50)
        .then(function() {
            assert.equal(o1.times,1);
            assert.equal(o2.times,2);
            overridesUnderTest.off(key2);
            map.fireEvent("mousemove", {
              latlng: map.getCenter()
            });
        }).delay(50)
        .then(function() {
            assert.equal(o1.times,1);
            assert.equal(o2.times,2);
            assert.equal(Object.keys(overridesUnderTest.subscriptions).length, 0);
        })
        .then(done)
        .otherwise(done)
    });


    test("should subscribe and unsubscribe all callbacks", function(done) {
        var o1 = new O();
        var o2 = new O();
        var key1 = {
            eventType: "mousemove",
            context  : o1,
            cb       : o1.cb
        }
        var key2 = {
            eventType: "mousemove",
            context  : o2,
            cb       : o2.cb
        }
        overridesUnderTest.on(key1, key1.cb.bind(o1));
        overridesUnderTest.on(key2, key2.cb.bind(o2));
        map.fireEvent("mousemove", {
          latlng: map.getCenter()
        });
        when.delay(0)
        .then(function() {
            assert.equal(o1.times,1);
            assert.equal(o2.times,1);
        })
        .then(function() {
            overridesUnderTest.off({eventType: "mousemove"});
            map.fireEvent("mousemove", {
              latlng: map.getCenter()
            });
        }).delay(50)
        .then(function() {
            assert.equal(o1.times,1);
            assert.equal(o2.times,1);
            assert.equal(Object.keys(overridesUnderTest.subscriptions).length, 0);
        })
        .then(done)
        .otherwise(done)
    });

    test("should be able to construct compund event", function(done) {
        //single subscription key for two events
        var calledTimes = 0;
        function cb() {calledTimes++};
        var key = {
            eventType: "mousedown",
            cb       : cb
        }
        overridesUnderTest.on(key, cb);
        overridesUnderTest.on(key, {"mouseup": cb});
        map.fireEvent("mousedown", {latlng: map.getCenter()});
        map.fireEvent("mouseup", {latlng: map.getCenter()});
        when.delay(0)
        .then(function() {
            assert.equal(calledTimes,2);
        })
        .then(function() {
            overridesUnderTest.off(key);
            map.fireEvent("mousedown", {
              latlng: map.getCenter()
            });
        }).delay(50)
        .then(function() {
            assert.equal(calledTimes,2);
            assert.equal(Object.keys(overridesUnderTest.subscriptions).length, 0);
        })
        .then(done)
        .otherwise(done)
    });

  });
});