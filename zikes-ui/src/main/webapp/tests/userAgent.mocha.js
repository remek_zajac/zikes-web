var assert   = require("assert");
var utils    = require("../app/scripts/lib/utils.js");


suite("User agents", function() {

    test("all supported browsers", function() {
        assert.deepEqual(
          utils.browserFeaturesFromUserAgent("Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5 Build/M4B30Z) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Mobile Safari/537.36"),
          {"os":"Android","Android":{"version":"6.0.1"},"mobile":true,"browser":"Chrome","Chrome":{"version":"56.0.2924.87"}}
        );
        assert.deepEqual(
          utils.browserFeaturesFromUserAgent("Mozilla/5.0 (Android 6.0.1; Mobile; rv:51.0) Gecko/51.0 Firefox/51.0"),
          {"os":"Android","Android":{"version":"6.0.1"},"mobile":true,"browser":"Firefox","Firefox":{"version":"51.0"}}
        );
        assert.deepEqual(
          utils.browserFeaturesFromUserAgent("Mozilla/5.0 (iPhone; CPU iPhone OS 10_0 like Mac OS X) AppleWebKit/602.1.38 (KHTML, like Gecko) Version/10.0 Mobile/14A300 Safari/602.1"),
          {"os":"iOS","iOS":{"version":"10_0"},"mobile":true,"browser":"Safari","Safari":{"version":"10_0"},"iPhone":true}
        );
        assert.deepEqual(
          utils.browserFeaturesFromUserAgent("Mozilla/5.0 (iPod; CPU iPhone OS 10_0 like Mac OS X) AppleWebKit/602.1.38 (KHTML, like Gecko) Version/10.0 Mobile/14A300 Safari/602.1"),
          {"os":"iOS","iOS":{"version":"10_0"},"mobile":true,"browser":"Safari","Safari":{"version":"10_0"},"iPhone":true}
        );
        assert.deepEqual(
          utils.browserFeaturesFromUserAgent("Mozilla/5.0 (iPad; CPU OS 10_0 like Mac OS X) AppleWebKit/602.1.38 (KHTML, like Gecko) Version/10.0 Mobile/14A300 Safari/602.1"),
          {"os":"iOS","iOS":{"version":"10_0"},"mobile":true,"browser":"Safari","Safari":{"version":"10_0"},"iPad":true}
        );
        assert.deepEqual(
          utils.browserFeaturesFromUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/602.4.8 (KHTML, like Gecko) Version/10.0.3 Safari/602.4.8"),
          {"os":"OSX","OSX":{"version":"10_12_3"},"browser":"Safari","Safari":{"version":"10_12_3"}, "Macintosh":true}
        );
    });
});