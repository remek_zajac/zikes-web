/**
 * tests/utils.js
 *
 * This has some mocking primitives
 */
'use strict';

var when = require("when");

var mocks = (function() {

    class MockObject {}

    function argsMatch(agrsRhs, argsLhs) {
        return JSON.stringify(agrsRhs) === JSON.stringify(argsLhs);
    }

    function checkSubscription(subscription, invocation) {
        if ( argsMatch(subscription.args, invocation) ) {
            if (--subscription.times == 0) {
                setTimeout(function() {
                    subscription.promise.resolve(invocation);
                }, 5);
            }
        }
    }

    class MockInvocation {

        constructor() {
            this.mInvocations   = [];
            this.mSubscriptions = [];
            this.method         = this.call.bind(this);
            this.method.mock    = this;
            var self            = this;
            this.method["calledTimes"] = function() {
                let times = 0
                let args = (arguments.length === 1?[arguments[0]]:Array.apply(null, arguments));
                for (var i = 0; i < self.mInvocations.length; i++) {
                    if (argsMatch(self.mInvocations[i], args)) {
                        ++times;
                    }
                }
                return times;
            }
            this.method["whenCalled"] = function(args, times) {
                args = args == undefined ? [] : args;
                var promise = when.defer();
                var subscription = {
                    promise:promise,
                    args   : args,
                    times  : times || 1
                };
                self.mInvocations.forEach(function(invocation) {
                    checkSubscription(subscription, invocation);
                });
                self.mSubscriptions.push(subscription);
                return promise.promise;
            }
            this.method["reset"] = function() {
                self.mInvocations   = [];
                self.mSubscriptions = [];
            }
            return this;
        }

        returns(what) {
            this.mReturns = what;
            return this;
        }

        call() {
            this.mInvocations.push(
                (arguments.length === 1?[arguments[0]]:Array.apply(null, arguments))
            );
            var self = this;
            this.mSubscriptions.forEach(function(subscription) {
                self.mInvocations.forEach(function(invocation) {
                    checkSubscription(subscription, invocation)
                });
            });
            return typeof this.mReturns === "function" ? this.mReturns.apply(this, arguments) : this.mReturns;
        }
    }

    return {
        MockObject      : MockObject,
        MockInvocation  : MockInvocation,
        assertions      : function(origAssert) {
            origAssert.almostEqual = function(a,b,precision,message) {
                message = message || "" + a + " != " + b + " within "+ JSON.stringify(precision) + " precision";
                origAssert(typeof a == "number", message);
                origAssert(typeof b == "number", message);
                if (typeof precision == "number") {
                    //no of decimal points that matter
                    while (Math.abs(a) >= 1 || Math.abs(b) >= 1) {
                        a/=10;
                        b/=10;
                    }
                    while(precision--) {
                        a*=10;
                        b*=10;
                    }
                    origAssert.equal(Math.floor(a), Math.floor(b), message);
                } else if (precision.percent != undefined) {
                    let base  = a || b;
                    let delta = (base == 0 ? 0 : (100*Math.abs(a-b)/base));
                    message += " (arguments actually within "+delta+"%)";
                    origAssert(delta < precision.percent, message);
                } else {
                    origAssert(false, "unable to intepret precision argument: "+JSON.stringify(precision));
                }
            }
        }
    }

})();

module.exports = mocks;