var assert        = require("assert");
var geometry      = require("../app/scripts/lib/geometry.js");
var mapboxSprites = require("../app/scripts/ui/mapbox/sprites.js");

function almostEqual(a,b,precission, comment) {
    precission = Math.pow(10,precission);
    function bothAre(type) {
        if (type == "array") {
            return Array.isArray(a) && Array.isArray(b);
        }
        return typeof a === type && typeof b === type;
    }
    if (bothAre("number")) {
        assert.equal(
            Math.round(a*precission), Math.round(b*precission), comment
        );
    } else if (bothAre("object")) {
        for (var key in a) {
            assert(key in b, comment);
            almostEqual(a[key], b[key], precission, comment);
        }
    } else if (bothAre("array")) {
        assert.equal(a.length, b.length, comment);
        for (var i = 0; i < a.length; i++) {
            almostEqual(a[i], b[i], precission, comment);
        }
    } else {
        assert(false, "Unable to compare a: "+JSON.stringify(a) + " with b: "+JSON.stringify(b));
    }
}
assert.almostEqual = almostEqual;

suite("Geometry", function() {

    test("should be able to project a point onto a line", function() {
        assert.deepEqual(
            new geometry.GeoLine(
                new geometry.LatLng(
                    51.5280828104,-0.151345191193
                ),
                new geometry.LatLng(
                    51.5289371874,-0.145980773163
                )
            ).projectOnto(
                new geometry.LatLng(
                    51.5294044249,-0.148469863129
                )
            ),
            new geometry.LatLng(
                51.52856212240955,-0.1483357117875383
            )
        );

        assert.deepEqual(
            new geometry.PixLine(
                new geometry.Point(662,650),
                new geometry.Point(726,668)
            ).projectOnto(
                new geometry.Point(726, 670), true
            ),
            new geometry.Point(726,668)
        );
    });


    test("should be able to project onto a lineString", function() {
        const pointToProject = [[51.54636046997395, -0.20298968075547738]].map(function(pt) {return new geometry.LatLng(pt[0], pt[1])})[0];
        const wrongResult    = [[51.54631555125231, -0.2030304813148084]].map(function(pt) {return new geometry.LatLng(pt[0], pt[1])})[0];
        var   projection     = new geometry.GeoLine(
            new geometry.LatLng(
                51.5463638, -0.2030836
            ),
            new geometry.LatLng(
                51.5457535, -0.2024117
            )
        ).projectOnto(pointToProject, true);
        assert.deepEqual(projection, wrongResult);
        const lineString     = new geometry.GeoLineString([
              [51.5464478, -0.2031748],[51.5463638, -0.2030836],[51.5457535, -0.2024117],[51.5453911, -0.2020426]
            ].map(function(pt) {return new geometry.LatLng(pt[0], pt[1])})
        );
        projection = lineString.projectOnto(pointToProject);
        assert.deepEqual(projection.projection, wrongResult);
    });
});


