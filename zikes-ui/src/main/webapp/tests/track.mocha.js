const assert      = require("assert");
const trackModule = require("../app/scripts/lib/track.js");
const hwyModule   = require("../app/scripts/lib/highways.js");
const geometry    = require("../app/scripts/lib/geometry.js");
                    require("./utils.js").assertions(assert);

suite("Track", function() {

    suite("elevation", function() {

        test("track", function() {
            let prev  = geometry.LatLng.create([51.5594942985,-0.265564379883]);
            let track = new trackModule.Track(
                [trackModule.Elevation.create.bind(null, ["d3c","e"])]
            );
            track.append(
                [undefined, 50, 100, undefined, 110, 210, undefined].map(function(ele) {
                    prev = geometry.LatLng.create(prev.offsetCourse(100, 0), {e : ele});
                    return prev;
                })
            );
            track.wrap();
            assert.equal(track.ele.max.value, 310);
            assert.equal(track.ele.min.value, 0);
            assert.equal(track.ele.d, 310);
            assert.equal(track.points.back().d3c.e, 310);
            assert.equal(track.points.front().d3c.e, 0);
            assert.equal(Math.round(track.distanceMts), 600);


            //confirm it can approx the elevation at a distance
            assert.equal(track.ele.getAt(300), 105);
            assert.equal(track.ele.getAt(150), 75);
            assert.equal(track.ele.getAt(400), 110);

            //confirm gain
            assert.equal(track.ele.gain, 260); //the first 0->50 is approximated, thus not counted
            assert.deepEqual(track.ele.climbBuckets, {"5":200,"50":100,"100":200});

            //confirm correct climb percent
            assert.equal(track.points[0].clmbBk, undefined);
            assert.equal(track.points[1].clmbBk, undefined);
            assert.equal(track.points[2].clmbBk, 50);
            assert.equal(track.points[3].clmbBk, undefined);
            assert.equal(track.points[4].clmbBk, 5);
            assert.equal(track.points[5].clmbBk, 100);
        });

        test("longTrack", function() {
            let prev  = geometry.LatLng.create([51.5594942985,-0.265564379883]);
            let track = new trackModule.Track(
                [trackModule.Elevation.create.bind(null, ["d3c","e"])]
            );
            for (var i = 0; i < 20000; i++) {
                track.append(prev);
                prev = geometry.LatLng.create(prev.offsetCourse(10, 0), {e : Math.round(Math.random()*100)});
            }
            track.wrap();
            assert.almostEqual(track.distanceMts, 200000, {percent:0.1});

            for (var i = 0; i < 5; i++) {
                track.ele.getAt(Math.random()*200000);
            }
        });

        test("tracks", function() {
            let prev   = geometry.LatLng.create([51.5594942985,-0.265564379883]);
            let tracks = new trackModule.Tracks();
            for (var i = 1; i <= 3; i++) {
                let track = new trackModule.Track(
                    [trackModule.Elevation.create.bind(null, ["d3c","e"])]
                );
                track.append(
                    [undefined, i*10, undefined, i*30, undefined].map(function(ele) {
                        prev = geometry.LatLng.create(prev.offsetCourse(20, 0), {e : ele});
                        return prev;
                    })
                );
                track.wrap();
                tracks.append(track);
            }
            tracks.wrap();

            assert.equal(tracks.ele.max.value, 120);
            assert.equal(tracks.ele.min.value, 0);
            assert.equal(tracks.ele.d, 120);
            assert.equal(Math.round(tracks.distanceMts), 280);

            //confirm gain
            assert.equal(tracks.ele.gain, 30+30*2+30*3);
            assert.deepEqual(tracks.ele.climbBuckets, {"50":60,"100":60,"150":60});

            //confirm it can approx the elevation at a distance
            assert.equal(tracks.ele.getAt(10), 5);
            assert.equal(tracks.ele.getAt(90), 20);
            assert.equal(tracks.ele.getAt(120), 20);
            assert.equal(tracks.ele.getAt(260), 90);
            assert.equal(tracks.ele.getAt(tracks.distanceMts), 120);
            assert.equal(tracks.ele.getAt(300), undefined);
        });

        test("rollup", function() {
            let prev  = geometry.LatLng.create([51.5594942985,-0.265564379883]);
            let track = new trackModule.Track(
                [trackModule.Elevation.create.bind(null, ["d3c","e"])]
            );
            track.append(
                [50, undefined, undefined, 100].map(function(ele) {
                    prev = geometry.LatLng.create(prev.offsetCourse(10, 0), {e : ele});
                    return prev;
                })
            );
            track.wrap();
            let tracks = new trackModule.Tracks();
            tracks.append(track);
            tracks.wrap();
            let rollup = tracks.rollup({max:20});
            [[0,   50],
             [20,  83],
             [30, 100]].map(function(expect, idx) {
                assert.equal(Math.round(rollup.points[idx].frmStrt), expect[0]);
                assert.equal(rollup.points[idx].d3c.e,               expect[1]);
             });
        });

    });

    suite("highways", function() {

        test("tracks", function() {
            let prev   = geometry.LatLng.create([51.5594942985,-0.265564379883]);
            let tracks = new trackModule.Tracks();
            for (var i = 1; i <= 3; i++) {
                let track = new trackModule.Track(
                    [hwyModule.track.Plugin.create.bind(null, ["d3c","h"])]
                );
                track.append(
                    [undefined, i, undefined, i*10, undefined].map(function(hwyType) {
                        prev = geometry.LatLng.create(prev.offsetCourse(20, 0), {h : hwyType});
                        return prev;
                    })
                );
                track.wrap();
                tracks.append(track);
            }
            tracks.wrap();
            assert.equal(Math.round(tracks.distanceMts), 280);

            //confirm it can approx the elevation at a distance
            assert.equal(tracks.hwy.getAt(10), 1);
            assert.equal(tracks.hwy.getAt(tracks.iterator().next().current().point.frmStrt), 1);
            assert.equal(tracks.hwy.getAt(21), 1);
            assert.equal(tracks.hwy.getAt(61), 10);
            assert.equal(tracks.hwy.getAt(90), 10);
            assert.equal(tracks.hwy.getAt(121), 2);
            assert.equal(tracks.hwy.getAt(tracks.distanceMts), 30);
            assert.equal(tracks.hwy.getAt(300), undefined);

            let rollup = tracks.rollup();
            [[0,    1],
             [60,  10],
             [120,  2],
             [160, 20],
             [220,  3],
             [260, 30],
             [280, undefined]].map(function(expect, idx) {
                assert.equal(Math.round(rollup.points[idx].frmStrt), expect[0]);
                assert.equal(rollup.points[idx].d3c.h,               expect[1]);
             });
             rollup = tracks.rollup({min:80});
            [[0,    1],
             [120,  20],
             [220,  3],
             [280,  undefined]].map(function(expect, idx) {
                assert.equal(Math.round(rollup.points[idx].frmStrt), expect[0]);
                assert.equal(rollup.points[idx].d3c.h,               expect[1]);
             });
             rollup = tracks.rollup({min:80, max:80});
            [[0,    1],
             [100,  10],
             [200,  20],
             [280,  undefined]].map(function(expect, idx) {
                assert.equal(Math.round(rollup.points[idx].frmStrt), expect[0]);
                assert.equal(rollup.points[idx].d3c.h,               expect[1]);
             });
        });
    });

});