var assert   = require("assert");
var geocode   = require("../app/scripts/lib/geocode.js");


suite("Geocode", function() {

  suite("basic delta", function() {
    var home = ["GB", "England", "Greater London", "London", undefined, undefined, "Christchurch Ave", "16C"];
    var work = ["GB", "England", "Greater London", "London", undefined, "Marylebone", "All Souls' Pl", "2"];

    test("should identify basic geocode delta", function() {
      assert.deepEqual(geocode.humanReadable([home,work]), ["16C, Christchurch Ave","2, All Souls' Pl"]);
    });

    test("should propose a sensible geocode for a single point", function() {
      assert.deepEqual(geocode.humanReadable(home), "London, GB");
      assert.deepEqual(geocode.humanReadable(home,3), "Christchurch Ave, London, GB");

      assert.deepEqual(geocode.humanReadable(
        ["GB", "England", "Greater London", "London", "City of London", undefined, "Old Broad St", "31"], 3
      ), "Old Broad St, London, GB");
      assert.deepEqual(geocode.humanReadable(
        ["PL", "Województwo dolnośląskie", "wrocławski", "Radwanice", "Szostakowice", undefined, "Bzowa", "16"], 3
      ), "Bzowa, Radwanice, PL");
      assert.deepEqual(geocode.humanReadable(
        ["DE", "Thüringen", undefined, "Zeulenroda-Triebes", "Zeulenroda", undefined, "Str. der DSF", "5"], 3
        ), "Str. der DSF, Zeulenroda-Triebes, DE");
    });

    test("should identify delta when the address only differs at a higher level", function() {
      //here this has the same number, but the streets are different.
      var a = ["GB","England","Greater London","London","Soho","Soho","Foubert's Pl","19"];
      var b = ["GB","England","Greater London","London","Soho","Soho","Marshall St","19"];
      assert.deepEqual(geocode.humanReadable([a,b]), ["19, Foubert's Pl","19, Marshall St"])
    });

    test("should identify delta when the address only differs slighly", function() {
      //here this has the same number, but the streets are different.
      var a = ["GB","England","Greater London","London",null,null,"Priory Ln",null];
      var b = ["GB","England","Greater London","London",null,null,"Priory Ln","140"];
      assert.deepEqual(geocode.humanReadable([a,b]), ["Priory Ln, London","Priory Ln, London"])
    });

    test("should identify delta when the address only differs slighly", function() {
      //here this has the same number, but the streets are different.
      var a = ["DE","Berlin",null,"Berlin","Mitte",null,"Krausenstraße","14"];
      var b = ["PL","Województwo dolnośląskie","Wrocław","Wrocław","Śródmieście","Ołbin","Wodna",null];
      assert.deepEqual(geocode.humanReadable([a,b]), ["Berlin, DE","Wrocław, PL"])
    });

    test("should identify delta when the address only differs slighly 2", function() {
      //here this has the same number, but the streets are different.
      var a = ["PL","Województwo dolnośląskie","świdnicki",null,null,null,"Unnamed Road",null];
      var b = ["PL","Województwo dolnośląskie","świdnicki",null,null,null,"Szkolna",null];
      assert.deepEqual(geocode.humanReadable([a,b]), ["Unnamed Road, świdnicki","Szkolna, świdnicki"])
    });
  });
});