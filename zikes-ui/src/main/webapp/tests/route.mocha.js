var assert        = require("assert");
var when          = require("when");
var mocks         = require("./utils.js");
var utils         = require("../app/scripts/lib/utils.js");
var proxyquire    = require('proxyquireify')(require);
var map           = require("./mapStub.js").map;

var stubs = {
  "./map.js" : map,
  "../../lib/mapquest.js" : {
    elevation : function(latlngs) {
        return when(
            Array.apply(null, Array(latlngs.length)).map(Number.prototype.valueOf, 100)
        );
    }
  },
  "@noCallThru": true
}


var routeModule   = proxyquire("../app/scripts/ui/common/mapRoute.js", stubs);
var sectionModule = proxyquire("../app/scripts/ui/common/sections.js", stubs);


var mockServerRouteMeta = {
    avgLookup_us:48,minLookup_us:33,maxLookup_us:62,routing_us:44,lengthMts:175,routingRequests:[{name:"astar",visitedJunctions:19}]
};


suite("Route", function() {

  suite("a->b", function() {
    var from  = [51.53636589077571,-0.09503602981567383];
    var to    = [51.53739360230881,-0.09237527847290039];
    var route = [[51.5363235474,-0.0950741171837,{"e":21,"h":12}],[51.5362243652,-0.0949624031782],[51.5361976624,-0.0949330031872,{"e":21,"h":12}],[51.5362739563,-0.0947683006525,{"e":21,"h":12}],[51.5363922119,-0.0945058986545,{"e":21,"h":12}],[51.5366973877,-0.0938593000174,{"e":21,"h":12}],[51.5368537903,-0.0935136005282,{"e":21,"h":12}],[51.5369873047,-0.09320320189,{"e":21,"h":12}],[51.5372467041,-0.0926453024149,{"e":21,"h":12}],[51.5373725891,-0.0923748984933, {"e":21}]]
    var distanceRawMts = 0;
    for (var i = 1; i < route.length; i++) {
        var f = new L.LatLng(route[i-1][0], route[i-1][1]);
        var t = new L.LatLng(route[i][0], route[i][1]);
        distanceRawMts += f.distanceTo(t);
    }
    distanceRawMts = distanceRawMts | 0;

    test("should (de)serialse both ways", function(done) {
        var routeUnderTest = new routeModule.Route({
            map   : map,
            route : function(milestones) {
                return when({
                    waypoints: utils.cloneSimpleObject(route),
                    meta: mockServerRouteMeta
                });
            },
            invalidate: function() {},
            highlight: function() {},
            style: function() {
                return {
                    color : {
                        base : '#FF0000'
                    }
                }
            }
        });
        routeUnderTest.from(from);
        routeUnderTest.to(to);
        when(routeUnderTest.invalidate(true))
        .then(function() {
            assert.deepEqual(routeUnderTest.toJSON(), [{
                from:from,
                ctor:"Section",
                waypoints:route
            },{
                from:to,
                ctor:"Section"
            }]);
            assert.equal(routeUnderTest.distanceMts(), distanceRawMts);
        })
        .otherwise(function(err) {
            done(err);
        })
        .then(function() {
            done();
        }).ensure(function() {
            routeUnderTest.delete();
        });
    });
  });

  suite("a->b->c", function() {
    var from  = [51.51216458809261,-0.14804184436798096];
    var via   = [51.51171388332373,-0.1472318172454834];
    var to    = [51.512141218325326,-0.14508068561553955];
    var route = [[51.5121307373,-0.148056000471,{"e":32,"h":13}],[51.5125312805,-0.14590279758,{"e":28,"h":11}],[51.5123939514,-0.145521402359,{"e":28,"h":11}],[51.5122413635,-0.145161002874],[51.5121192932,-0.14503569901,{"e":28,"h":8}],[51.5121040344,-0.145098894835,{"e":28,"h":8}],[51.5117912292,-0.146770894527,{"e":30,"h":8}],[51.5117034912,-0.147229924798,{"e":30,"t":true}],[51.5117034912,-0.147229924798,{"e":30,"h":8}],[51.5115966797,-0.147805199027,{"e":31,"h":13}],[51.5121307373,-0.148056000471,{"e":32,"h":13}],[51.5125312805,-0.14590279758,{"e":28,"h":11}],[51.5123939514,-0.145521402359,{"e":28,"h":11}],[51.5122413635,-0.145161002874],[51.5121192932,-0.14503569901,{"e":28}]]

    function prepareRoute(parent) {
        parent.invalidate.reset();
        var routeUnderTest = new routeModule.Route(parent);
        routeUnderTest.from(from);
        routeUnderTest.to(to);
        routeUnderTest.mHead.append(
            new sectionModule.Section(map, {
                parent   : routeUnderTest,
                latlng   : via,
                icon     : routeUnderTest.mIcons.from
            })
        );
        routeUnderTest.invalidate();
        map.fitBounds(
            new L.LatLngBounds(from, to),
            { animate: true }
        );

        return when(parent.invalidate.whenCalled())
        .then(function() { return routeUnderTest; });
    }

    function prepareParent() {
        var mockParent = new mocks.MockObject();
        mockParent["map"] = map;
        mockParent["route"] = new mocks.MockInvocation()
        .returns(function() {
            return {
                waypoints: utils.cloneSimpleObject(route),
                meta: mockServerRouteMeta
            };
        }).method;
        mockParent["invalidate"] = new mocks.MockInvocation().method;
        mockParent["highlight"] = new mocks.MockInvocation().method;
        mockParent["style"] = new mocks.MockInvocation()
        .returns(function() {
            return {
                color : {
                    base : '#FF0000'
                }
            };
        }).method;
        return mockParent;
    }

    test("should (de)serialse a complex a->b->c route", function(done) {
        var parent = prepareParent();
        var routeUnderTest;
        prepareRoute(parent)
        .then(function(rut) {
            routeUnderTest = rut;
            var routeCopy = utils.cloneSimpleObject(route);
            var s1 = routeCopy.slice(0,8);
            delete s1[s1.length-1][2].t;
            var s2 = routeCopy.slice(8);
            assert.deepEqual(routeUnderTest.toJSON(), [
                {
                    from: from,
                    ctor: "Section",
                    waypoints: s1
                },
                {
                    from: via,
                    ctor: "Section",
                    waypoints: s2
                },
                {
                    from: to,
                    ctor: "Section"
                }
            ]);
            assert.equal(parent.invalidate.calledTimes(), 1);
            assert.equal(parent.route.calledTimes([from,via,to]), 1);
        })
        .otherwise(function(err) {
            done(err);
        })
        .then(function() {
            done();
        }).ensure(function() {
            routeUnderTest.delete();
        });
    });


    test("should delete a marker and reroute", function(done) {
        var parent = prepareParent();
        var routeUnderTest;
        prepareRoute(parent)
        .then(function(rut) {
            routeUnderTest = rut;
            routeUnderTest.mHead.mNext.delete();
            routeUnderTest.invalidate();
        });
        parent.invalidate.whenCalled(undefined, 1)
        .then(function() {
            assert.equal(parent.route.calledTimes([from,to]), 1);
            assert.equal(routeUnderTest.mHead.mNext.mNext, undefined);
            done();
        })
        .otherwise(function(err) {
            done(err);
        }).ensure(function() {
            routeUnderTest.delete();
        });
    });


    test("should deserialise a legacy route", function(done) {
        var routeUnderTest;
        prepareRoute(prepareParent())
        .then(function(rut) {
            routeUnderTest = rut;
            var legacyJSON = [{"from":[51.246578226884715,16.89654350280762],"ctor":"Section","waypoints":[[51.2466316,16.8965321,{"e":113,"h":6}],[51.2466888,16.8967857,{"e":113,"h":6}],[51.2474823,16.9002743],[51.2479095,16.902092],[51.2479057,16.9022522,{"e":112}]]},{"from":[51.24786766736992,16.902251243591312],"ctor":"RubberbandSection","waypoints":[[51.24786766736992,16.902251243591312,{"e":112}],[51.2495062791539,16.904482841491703,{"e":115.4919281005859}]]},{"from":[51.2495062791539,16.904482841491703],"ctor":"RubberbandSection","waypoints":[[51.2495062791539,16.904482841491703,{"e":115.4919281005859}],[51.24774897689292,16.907865900491288,{"e":114.9454193115234}]]},{"from":[51.24774897689292,16.907865900491288],"ctor":"Section","waypoints":[[51.24774897689292,16.907865900491288],[51.2475395,16.9150543,{"e":120,"h":16}],[51.2474823,16.9150505,{"e":120,"h":16}],[51.2474022,16.9150467,{"e":120,"h":16}],[51.2469368,16.9150238,{"e":117,"h":16}],[51.2468338,16.9183483,{"e":120,"h":16}],[51.2459526,16.9182816,{"e":119,"h":16}],[51.2454453,16.9182434,{"e":119,"h":16}],[51.2446594,16.9181843,{"e":117,"h":16}],[51.2446556,16.919117,{"e":116}]]},{"from":[51.2445902686383,16.919116973876957],"ctor":"Section","waypoints":[[51.2446556,16.919117,{"e":116,"h":16}],[51.2446518,16.9198132,{"e":116,"h":16}],[51.2446632,16.9202747],[51.2447281,16.9213047,{"e":118,"h":16}],[51.2449379,16.9239883,{"e":122,"h":16}],[51.2450562,16.9256306,{"e":121,"h":16}],[51.246315,16.9254189],[51.2466431,16.9253922],[51.2472038,16.9254379,{"e":129,"h":6}],[51.2472,16.9255562,{"e":129}]]},{"from":[51.24714236154552,16.9255542755127],"ctor":"Section"}];
            return routeUnderTest.fromJSON(legacyJSON)
        })
        .then(function() {
            var current = routeUnderTest.mHead;
            var sectionCount = 0;
            while(current) {
                sectionCount++;
                current = current.mNext;
            }
            assert.equal(sectionCount, 6);
            assert.equal(routeUnderTest.distanceMts(), 2784);
            done();
        })
        .otherwise(function(err) {
            done(err);
        }).ensure(function() {
            routeUnderTest.delete();
        });
     });
  });

  suite("elevation profile", function() {

    test("should calculate correct elevation profile", function(done) {
        routeUnderTest = new routeModule.Route({
            map : map,
            style: function() {
                return {
                    color : {
                        base : '#FF0000'
                    }
                }
            },
            invalidate: function() {}
        });
        var json = [{"from":[50.888091157848635,16.864700317382816],"ctor":"Section","waypoints":[[50.8880806,16.864521,{"e":149,"h":22}],[50.8874855,16.8648701],[50.8874321,16.8649769,{"e":149,"h":4}],[50.8873978,16.8649521],[50.8866844,16.8643246,{"e":149,"h":4}],[50.8863678,16.8640442],[50.8853455,16.8630562],[50.8848152,16.862545],[50.8842697,16.8620949],[50.883728,16.8616199],[50.8832283,16.8611889],[50.8829269,16.8609428,{"e":151,"h":4}],[50.8827782,16.8608208],[50.8825798,16.8606758],[50.8822746,16.8604565],[50.8819389,16.8602829],[50.8816147,16.8601704,{"e":151}]]},{"from":[50.88159328285729,16.860237121582035,{"e":151}],"ctor":"RubberbandSection"},{"from":[50.86296101809254,16.71226501464844,{"e":635}],"ctor":"RubberbandSection"},{"from":[50.836950060547196,16.85886383056641,{"e":163}],"ctor":"Section","waypoints":[[50.8369751,16.8587608,{"e":163,"h":4}],[50.8368797,16.8587933],[50.8368149,16.8588142],[50.8367767,16.8588314,{"e":164,"h":4}],[50.8367271,16.8588562],[50.8366737,16.8588829,{"e":164,"h":4}],[50.8366318,16.8589115],[50.8366089,16.8589249],[50.8364944,16.8590107],[50.8364372,16.8590488],[50.8363457,16.8591175],[50.8362732,16.8591785],[50.8361931,16.8592567],[50.8360443,16.8593941],[50.835865,16.8595562],[50.8356819,16.8597164],[50.8354988,16.8598728],[50.8353348,16.8599796],[50.8352623,16.8600254,{"e":162,"h":4}],[50.8352051,16.860054],[50.8351707,16.8600712],[50.8349724,16.8601818],[50.8344917,16.8603344],[50.8342972,16.8603954],[50.8340073,16.8604736],[50.8337364,16.8605461],[50.8332329,16.8606834,{"e":162}]]},{"from":[50.83315558401709,16.860408782958988],"ctor":"Section"}]
        routeUnderTest.fromJSON(json)
        .then(function() {
            var expectedDistanceMts = 22545;
            var expectedSectionProfiles = [
                {distanceMts: 812,   min:149, max:151},
                {distanceMts: 10588, min:151, max:635},
                {distanceMts: 10690, min:163, max:635},
                {distanceMts: 442,   min:162, max:164}
            ];

            var profile = routeUnderTest.getProfile();
            assert.equal(Math.round(profile.distanceMts), expectedDistanceMts);
            assert.equal(routeUnderTest.distanceMts(), expectedDistanceMts);
            assert.equal(profile.ele.getAt(0), 149);
            assert.equal(profile.ele.getAt(expectedDistanceMts), 162);
            assert.equal(profile.ele.getAt(expectedSectionProfiles[0].distanceMts), expectedSectionProfiles[1].min);
            assert.equal(
                profile.ele.getAt(
                    expectedSectionProfiles[0].distanceMts + expectedSectionProfiles[1].distanceMts/2
                ), expectedSectionProfiles[1].min + (expectedSectionProfiles[1].max - expectedSectionProfiles[1].min)/2
            );


            assert.equal(profile.tracks.length, 4);
            for (var i = 0; i < profile.tracks.length; i++) {
                var sectionProfile = profile.tracks[i];
                assert.equal(Math.round(sectionProfile.distanceMts), expectedSectionProfiles[i].distanceMts);
                assert.equal(sectionProfile.ele.min.value, expectedSectionProfiles[i].min);
                assert.equal(sectionProfile.ele.max.value, expectedSectionProfiles[i].max);
                assert.equal(sectionProfile.ele.d,   expectedSectionProfiles[i].max - expectedSectionProfiles[i].min);
                expectedDistanceMts -= sectionProfile.distanceMts;
            }
            assert.equal(Math.round(expectedDistanceMts), 13); //those are unaccounted distances between (tail->head) sections 1->2 + 3->4
            done();
        })
        .otherwise(function(err) {
            done(err);
        }).ensure(function() {
            routeUnderTest.delete();
        });

      });
   });
});