const when           = require("when");
const assert         = require("assert");
const movementModule = require("../app/scripts/lib/movement.js");
const trackModule    = require("../app/scripts/lib/track.js");
const geometry       = require("../app/scripts/lib/geometry.js");
const utils          = require("../app/scripts/lib/utils.js");
const geoLocator     = require("../app/scripts/ui/common/geoLocator.js");
const proxyquire     = require('proxyquireify')(require);
const MockGeoPath    = require("./mockPath.js").MockGeoPath;
const mocks          = require("./utils.js");

mocks.assertions(assert);
const MockZikesBackend = {
    newMeta  : function(opaque) {
        return {
            opaque: opaque || {}
        }
    },
    journeys : {
        taken : {
            get    : new mocks.MockInvocation().method,
            delete : new mocks.MockInvocation().method,
            save   : new mocks.MockInvocation().returns(function(journey) {
                return journey.metadata;
            }).method
        }
    }
}
const stubs = {
  "../../lib/mapquest.js" : {
    elevation : function(latlngs) {
        return when(
            Array.apply(null, Array(latlngs.length)).map(Number.prototype.valueOf, 100)
        );
    }
  },
  "../mapbox/geocode.js": {
    reverse: function(latlng) {
        return when(["lat"+latlng[0], "lng"+latlng[1]]);
    },
    humanReadable: function(a) {
        return a.map((d) => {return ""})
    }
  },
  "../../lib/zikesBackend.js" : MockZikesBackend,
  "@noCallThru": true
}
const takenJourneys   = proxyquire("../app/scripts/ui/common/takenJourneys.js", stubs);

suite("Movement", function() {

    suite("Track", function() {

        test("detects consistent movement", function(done) {
            const geoLocatorUnderTest = new geoLocator.GeoLocatorMov();
            let track = [[50.5343898,18.336766900000004,{"ts":1489772148266}],[50.534395607455345,18.33717298562296,{"ts":1489772169477}],[50.53426168331518,18.337698043627814,{"ts":1489772171546}],[50.53412634269246,18.33822221954627,{"ts":1489772174426}],[50.53399100223639,18.3387463948193,{"ts":1489772193419}]];
            let gotMovingEvent, gotStoppedEvent;
            geoLocatorUnderTest.on("moving", function() {
                gotMovingEvent = true;
            });
            geoLocatorUnderTest.on("stopped", function() {
                gotStoppedEvent = true;
            });
            geoLocatorUnderTest.simulate(track, {durationSec:Infinity, quick:true})
            .then(function() {
                assert(gotMovingEvent, "didn't get 'moving'");
                assert(!gotStoppedEvent, "did get 'stopped'");
                assert(geoLocatorUnderTest.mRegister.moving, "registry not moving");
                done();
            })
            .otherwise(function(e) {
                done(e);
            });

        });

        test("detects wiggling", function(done) {
            const geoLocatorUnderTest = new geoLocator.GeoLocatorMov();
            let track = [[51.1103219724,17.032040181],[51.1099851953,17.033477845],[51.1103489146,17.0331988953],[51.1095675916,17.031975808],[51.11041627,17.0309672974],[51.1105644519,17.033477845]];
            let gotMovingEvent, gotStoppedEvent;
            geoLocatorUnderTest.on("moving", function() {
                gotMovingEvent = true;
            });
            geoLocatorUnderTest.on("stopped", function() {
                gotStoppedEvent = true;
            });
            geoLocatorUnderTest.simulate(track, {durationSec:Infinity, quick:true})
            .then(function() {
                assert(!geoLocatorUnderTest.mRegister.moving);
                assert(!gotMovingEvent);
                assert(!gotStoppedEvent);
                done();
            })
            .otherwise(function(e) {
                done(e);
            });
        });

        function testTrack(options) {
            options = utils.mergeSimpleObjects({
                distanceMts : 1000,
                resolution  : {
                    points  : 20
                },
                speedMps : utils.kph2Mps(30)
            }, options);
            return new MockGeoPath(
                [[51.1098494906,17.0177837036]], options
            ).track();
        }

        test("mockPath", function(done) {
            let track = testTrack();
            const geoLocatorUnderTest = new geoLocator.GeoLocatorMov();
            geoLocatorUnderTest.simulate(track.result, {durationSec:Infinity, quick:true})
            .then(function() {
                assert.equal(geoLocatorUnderTest.mRegister.length, track.options.resolution.points+2);
                assert.almostEqual(geoLocatorUnderTest.mRegister.distanceMts, track.distanceMts, {percent:0.01});
                assert.almostEqual(geoLocatorUnderTest.mRegister.mov.durationSec, track.distanceMts/track.options.speedMps, {percent:0.01});
                geoLocatorUnderTest.mRegister.moving.iterator().forward().forEach(function(pt, it) {
                    assert.almostEqual(pt.d3c.v, it.isFront() ? 0 : track.options.speedMps, {percent:0.01});
                });
                let wrapped = geoLocatorUnderTest.mRegister.wrap();
                assert.almostEqual(wrapped.mov.v.max.value, track.options.speedMps, {percent:0.01});
                done();
            })
            .otherwise(function(e) {
                done(e);
            });
        });

        test("max speed percentile", function(done) {
            this.timeout(3500);
            let startIndex = 50;
            let track = testTrack({
                distanceMts : 10000,
                resolution  : {
                    points  : 100
                }
            });
            let dt = track.result[1].d3c.ts - track.result[0].d3c.ts;
            for (var i = 2; i < track.result.length; i++) {
                assert.equal(track.result[i].d3c.ts - track.result[i-1].d3c.ts, dt);
            }
            let ready = when();
            for (var i = 0; i <= 5; i++) {
                let fastMilestones = i;
                ready = when(ready).then(function() {
                    track.move({ d3c : { ts : -dt/2 }}, startIndex+fastMilestones);
                    const geoLocatorUnderTest = new geoLocator.GeoLocatorMov();
                    return geoLocatorUnderTest.simulate(track.result, {durationSec:Infinity, quick:true})
                    .then(function() {
                        assert.equal(geoLocatorUnderTest.mRegister.length, 102);
                        let idx = 0;
                        geoLocatorUnderTest.mRegister.moving.iterator().forward().forEach(function(pt, it) {
                            if (idx >= startIndex && idx <= startIndex + fastMilestones) {
                                assert.almostEqual(pt.d3c.v, utils.kph2Mps(60), {percent:0.01});
                            } else if (idx != 0) {
                                assert.almostEqual(pt.d3c.v, utils.kph2Mps(30), {percent:0.01});
                            }
                            idx++;
                        });
                        let wrapped = geoLocatorUnderTest.mRegister.wrap();
                        assert.almostEqual(wrapped.mov.v.max.value, fastMilestones >= 5 ? utils.kph2Mps(60) : utils.kph2Mps(30), {percent:0.01});
                        console.log("avg speed is "+utils.mps2Kph(wrapped.mov.v.avg)+"k/h");
                        assert.almostEqual(wrapped.mov.v.avg, utils.kph2Mps(30), {percent:5});
                    });
                });
            }

            when(ready)
            .then(function() {
                done();
            })
            .otherwise(function(e) {
                done(e);
            });
        });

        test("filters out an outlier", function(done) {
            let trackLength = testTrack().options.resolution.points+1;
            when.all([6/*0, 6, trackLength*/].map(function(idx) {
                let track    = testTrack();
                track.move(new geometry.GeoVector(
                        track.result[idx],
                        track.result[idx].offsetCourse(300, Math.PI/2)
                    ).toJSON(), idx, idx+1
                );
                console.log("filtering gps outlier for:" + JSON.stringify(track.result.map(function(pt) {return [pt.lat, pt.lng]})));
                const expectedDistanceMts     = idx !=0 && idx != trackLength ?
                    track.options.distanceMts :
                    track.options.distanceMts - (track.options.distanceMts/(track.options.resolution.points+1))
                const geoLocatorUnderTest = new geoLocator.GeoLocatorMov();
                return geoLocatorUnderTest.simulate(track.result, {durationSec:Infinity, quick:true})
                .then(function() {
                    assert.equal(geoLocatorUnderTest.mRegister.length, track.options.resolution.points+1);
                    assert.equal(geoLocatorUnderTest.mRegister.mTracks.length, idx == trackLength ? 2 : 1); //only for the last one it wouldn't manage to purge
                    assert.almostEqual(geoLocatorUnderTest.mRegister.distanceMts, expectedDistanceMts, {percent:0.01});
                    assert.almostEqual(geoLocatorUnderTest.mRegister.mov.durationSec, expectedDistanceMts/track.options.speedMps, {percent:0.01});
                    geoLocatorUnderTest.mRegister.moving.iterator().forward().forEach(function(pt, it) {
                        assert.almostEqual(pt.d3c.v, it.isFront() ? 0 : track.options.speedMps, {percent:0.01});
                    });
                    let wrapped = geoLocatorUnderTest.mRegister.wrap();
                    assert.almostEqual(wrapped.mov.v.avg, track.options.speedMps, {percent:0.01});
                });
            }))
            .then(function() {
                done();
            })
            .otherwise(function(e) {
                done(e);
            });
        });
    });

    function toTrack(track) {
        const result = new trackModule.Track(
            [movementModule.track.Plugin.create.bind(null, ["d3c","ts"], ["d3c","v"])]
        )
        result.append(
            track.map(
                function(pt) { return geometry.LatLng.create(pt); }
            )
        );
        result.wrap();
        return result;
    }

    suite("Collection", function() {

        //two tracks - simulated at speed of 50kph
        const twoAdjoint = [
            [[50.5343782,18.336778,{"ts":1490114663086}],[50.534395014835304,18.337174870110793,{"ts":1490114681960}],[50.53426118254948,18.337699983099615,{"ts":1490114683985}],[50.53412584192738,18.338224159015684,{"ts":1490114686865}],[50.533990501471926,18.338748334286322,{"ts":1490114689745}],[50.533780915585815,18.339059144975682,{"ts":1490114692625}],[50.53342731556372,18.33895609374117,{"ts":1490114694931}],[50.53313171048642,18.338815829553017,{"ts":1490114697811}],[50.53294119550613,18.338336028412574,{"ts":1490114700285}],[50.53274914601115,18.337857705039553,{"ts":1490114703165}],[50.532557096795585,18.33737938236247,{"ts":1490114706046}],[50.53234175370059,18.336927292149017,{"ts":1490114708925}],[50.53211590411305,18.33648703408786,{"ts":1490114711801}],[50.53189005485288,18.336046776664876,{"ts":1490114714681}],[50.53166420592009,18.33560651988006,{"ts":1490114717561}],[50.53145825008169,18.33514348892931,{"ts":1490114720443}],[50.53126313603496,18.33466842942359,{"ts":1490114723319}],[50.53110975062635,18.334157268619236,{"ts":1490114726198}]],
            [[50.53145016861242,18.335144952868177,{"ts":1490114754011}],[50.531264802869295,18.33467342628721,{"ts":1490114756012}],[50.53111114927003,18.33416246507816,{"ts":1490114758835}],[50.53094826739002,18.333706625824313,{"ts":1490114761713}],[50.53081942825802,18.333178447011278,{"ts":1490114764376}],[50.53066361803483,18.33267237674755,{"ts":1490114767255}],[50.53046030284226,18.33220581295318,{"ts":1490114770118}],[50.53024839685586,18.331748735890304,{"ts":1490114772998}],[50.53002832599823,18.331301433335945,{"ts":1490114775877}],[50.529796869534756,18.330868604903376,{"ts":1490114778757}],[50.529539090081435,18.330475562698993,{"ts":1490114781637}],[50.52926976473568,18.33010068846847,{"ts":1490114784512}],[50.52900117246459,18.329724517775134,{"ts":1490114787392}],[50.52872998627035,18.32935307585055,{"ts":1490114790272}],[50.528454273981005,18.328989879743837,{"ts":1490114793151}],[50.528185603995425,18.32861396133217,{"ts":1490114796031}],[50.52791788462848,18.328236298526658,{"ts":1490114798911}],[50.5277634,18.3279114,{"ts":1490114801791}]]
        ].map(toTrack);

        const first  = twoAdjoint[0];
        const second = twoAdjoint[1];

        const concatenation = new trackModule.Tracks();
        concatenation.append(first);
        concatenation.append(second);
        concatenation.wrap();



        test("basic measures", function() {
            assert.equal(first.length, 18);
            assert.equal(first.iterator().front().current().point.d3c.ts, 1490114663086);
            assert.equal(first.iterator().back().current().point.d3c.ts, 1490114726198);
            assert.equal(Math.round(first.mov.durationSec), Math.round((1490114726198-1490114663086)/1000));
            assert.equal(Math.round(first.distanceMts), 654);
            assert.equal(Math.round(utils.mps2Kph(first.mov.v.max.value)), 62);
            assert.equal(first.mov.v.avg, first.distanceMts/first.mov.durationSec);

            assert.equal(second.length, 17); //3rd got eaten as it was accelerating too much
            assert.equal(second.iterator().front().current().point.d3c.ts, 1490114754011);
            assert.equal(second.iterator().back().current().point.d3c.ts, 1490114801791);
            assert.equal(Math.round(second.mov.durationSec), Math.round((1490114801791-1490114754011)/1000));
            assert.equal(Math.round(second.distanceMts), 664);
            assert.equal(Math.round(utils.mps2Kph(second.mov.v.max.value)), 59);
            assert.equal(second.mov.v.avg, second.distanceMts/second.mov.durationSec);

            assert.equal(concatenation.mov.v.avg, (first.distanceMts+second.distanceMts)/(first.mov.durationSec+second.mov.durationSec));
        });

        test("merges two adjoint tracks into one", function() {
            const geoLocatorUnderTest = new geoLocator.GeoLocatorMov();
            let result = geoLocatorUnderTest.collectTracks(
                twoAdjoint.map(function(t) {return t.toJSON();}), {
                    maxSpeed       : utils.kph2Mps(71),
                    minAvgSpeed    : utils.kph2Mps(8),
                    minDistanceMts : 1000,
                    trackGlue  : {
                        ms     :  utils.mins2ms(30),    //tracks will not merge if separated by more than this many mins
                        mts    :  100                   //tracks will not merge if separated by more than this many metres
                    }
                }
            );
            let journey = result[0];
            assert.equal(result[1], undefined);
            assert.equal(journey.journey.raw.length, 1);   //which has 1 track
            assert.equal(journey.journey.raw.front().meta.distanceMts,
                         first.distanceMts + second.distanceMts + first.back().distanceTo(second.front()));
        });

        test("doesn't merge two adjoint tracks if they're too far apart (distance)", function() {
            const geoLocatorUnderTest = new geoLocator.GeoLocatorMov();
            let result = geoLocatorUnderTest.collectTracks(
                twoAdjoint.map(function(t) {return t.toJSON();}), {
                    maxSpeed       : utils.kph2Mps(71),
                    minAvgSpeed    : utils.kph2Mps(8),
                    minDistanceMts : 500,
                    trackGlue  : {
                        ms     :  utils.mins2ms(30),     //tracks will not merge if separated by more than this many mins
                        mts    :  50                     //tracks will not merge if separated by more than this many metres
                    }
                }
            );
            let journey = result[0];
            assert.equal(result[1], undefined);
            assert.equal(journey.journey.raw.length, 2);
        });

        test("filters out too short tracks", function() {
            const geoLocatorUnderTest = new geoLocator.GeoLocatorMov();
            let result = geoLocatorUnderTest.collectTracks(
                twoAdjoint.map(function(t) {return t.toJSON();}), {
                    maxSpeed       : utils.kph2Mps(71),
                    minAvgSpeed    : utils.kph2Mps(8),
                    minDistanceMts : 654+1, //shorter of the two
                    trackGlue  : {
                        ms      :  utils.mins2ms(30),    //tracks will not merge if separated by more than this many mins
                        mts     :  50                    //tracks will not merge if separated by more than this many metres
                    }
                }
            );
            let journey = result[0];
            assert.equal(result[1], undefined);
            assert.equal(journey.journey.raw.length, 1);
            assert.equal(journey.journey.raw.front().meta.distanceMts, second.distanceMts);
        });

        test("produces no journeys if no tracks qualify", function() {
            const geoLocatorUnderTest = new geoLocator.GeoLocatorMov();
            let result = geoLocatorUnderTest.collectTracks(
                twoAdjoint.map(function(t) {return t.toJSON();}), {
                    maxSpeed       : utils.kph2Mps(50),  //this should disqualify both tracks
                    minAvgSpeed    : utils.kph2Mps(8),
                    minDistanceMts : 500,
                    trackGlue : {
                        ms    :  utils.mins2ms(30),    //tracks will not merge if separated by more than this many mins
                        mts   :  50                    //tracks will not merge if separated by more than this many metres
                    }
                }
            );
            assert.equal(result[0], undefined);
            assert.equal(result[1], undefined);
        });
    });


    suite("Taken Journeys", function() {

        let takenJourney2;
        test("raw->taken", function(done) {
            const geoLocatorUnderTest = new geoLocator.GeoLocatorMov();
            let track = toTrack(
                new MockGeoPath(
                    [[51.1098494906,17.0177837036]], {
                        distanceMts : 2000,
                        resolution  : {
                            points  : 10
                        },
                        speedMps : utils.kph2Mps(30)
                    }
                ).track().result
            );
            let rawJourney = geoLocatorUnderTest.collectTracks([track])[0];
            assert(rawJourney.metadata.opaque.type == "rawRecording");
            rawJourney.metadata.id = 1;
            MockZikesBackend.journeys.taken.save.mock.returns(function(takenJourney) {
                assert.equal(takenJourney.journey.items.length, 1);
                assert.equal(takenJourney.journey.items.front().route.sections.length, 2);
                assert.equal(Math.round(takenJourney.journey.items.front().route.meta.distanceMts), 2000);
                assert.equal(
                    Math.round(utils.mps2Kph(takenJourney.journey.items.front().route.meta.v.avg)), 30
                );
                assert.equal(
                    Math.round(utils.mps2Kph(takenJourney.journey.items.front().route.meta.v.max.value)), 30
                );

                takenJourney.metadata.id = 2;
                takenJourney2 = takenJourney;
                return when(takenJourney.metadata);
            });
            MockZikesBackend.journeys.taken.get.mock.returns(when(rawJourney));
            new takenJourneys.TakenJourneys().process([rawJourney.metadata])
            .then(function(metadatas) {
                assert.equal(metadatas.length, 1);
                assert.equal(MockZikesBackend.journeys.taken.delete.calledTimes(1), 1);
                assert.equal(MockZikesBackend.journeys.taken.save.mock.mInvocations.length, 1);
                assert(metadatas.front().opaque.type == "taken");
                done();
            })
            .otherwise(function(e) {
                done(e);
            })
            .ensure(function() {
                MockZikesBackend.journeys.taken.delete.reset();
                MockZikesBackend.journeys.taken.save.reset();
            });
        });

        test("taken<-raw", function(done) {
            const geoLocatorUnderTest = new geoLocator.GeoLocatorMov();
            let tail  = geometry.LatLng.create(takenJourney2.journey.items.back().route.sections.back().from);
            let track = toTrack(
                new MockGeoPath(
                    [tail.offsetCourse(movementModule.TrackGlue.mts-1, Math.PI/2)], {
                        distanceMts : 2000,
                        resolution  : {
                            points  : 20
                        },
                        timestart : tail.d3c.ts+2000,
                        speedMps  : utils.kph2Mps(15)
                    }
                ).track().result
            );
            let rawJourney = geoLocatorUnderTest.collectTracks([track])[0];
            rawJourney.metadata.id = 1;
            MockZikesBackend.journeys.taken.get.mock.returns(function(id) {
                if (id == 1) {
                    return rawJourney;
                } else if (id == 2) {
                    return takenJourney2;
                }
            });
            MockZikesBackend.journeys.taken.save.mock.returns(function(takenJourney) {
                assert.equal(takenJourney.metadata.id, 2);
                assert.equal(takenJourney.journey.items.length, 1);

                assert.equal(takenJourney.journey.items.front().route.sections.length, 2);
                assert.equal(Math.round(takenJourney.journey.items.front().route.meta.distanceMts), 4000+movementModule.TrackGlue.mts-1);
                assert.equal(
                    Math.round(utils.mps2Kph(takenJourney.journey.items.front().route.meta.v.avg)), 20
                );
                assert.equal(
                    Math.round(utils.mps2Kph(takenJourney.journey.items.front().route.meta.v.max.value)), 30
                );

                let theSection = takenJourney.journey.items.front().route.sections.front().waypoints;
                assert.equal(theSection.length, 34);
                assert.deepEqual(theSection.front(), takenJourney2.metadata.opaque.from);
                assert.equal(theSection.back()[0], track.back().lat);
                assert.equal(theSection.back()[1], track.back().lng);
                takenJourney2 = takenJourney;
                return when(takenJourney.metadata);
            });
            new takenJourneys.TakenJourneys().process([rawJourney.metadata, takenJourney2.metadata])
            .then(function(metadatas) {
                assert.equal(metadatas.length, 1);
                assert.equal(MockZikesBackend.journeys.taken.delete.calledTimes(1), 1);
                assert.equal(MockZikesBackend.journeys.taken.save.mock.mInvocations.length, 1);
                done();
            })
            .otherwise(function(e) {
                done(e);
            });
        });

        test("taken..<-raw.....<-raw", function(done) {
            const geoLocatorUnderTest = new geoLocator.GeoLocatorMov();
            let tail  = geometry.LatLng.create(takenJourney2.journey.items.back().route.sections.back().from);
            let track2Journey = toTrack(
                new MockGeoPath(
                    [tail.offsetCourse(movementModule.TrackGlue.mts+1, Math.PI/2)], {
                        distanceMts : 2000,
                        resolution  : {
                            points  : 20
                        },
                        timestart : tail.d3c.ts+2000,
                        speedMps  : utils.kph2Mps(15)
                    }
                ).track().result
            );
            let separateJourney = toTrack(
                new MockGeoPath(
                    [tail.offsetCourse(movementModule.TrackGlue.mts-1, Math.PI/2)], {
                        distanceMts : 2000,
                        resolution  : {
                            points  : 20
                        },
                        timestart : tail.d3c.ts+(25*60*60*1000), //25hrs later
                        speedMps  : utils.kph2Mps(15)
                    }
                ).track().result
            );
            let rawJourney = geoLocatorUnderTest.collectTracks([track2Journey, separateJourney])[0];
            rawJourney.metadata.id = 1;
            MockZikesBackend.journeys.taken.get.mock.returns(function(id) {
                if (id == 1) {
                    return rawJourney;
                } else if (id == 2) {
                    return takenJourney2;
                }
            });
            MockZikesBackend.journeys.taken.save.mock.returns(function(takenJourney) {
                if (takenJourney.metadata.id == 2) {
                    assert.equal(takenJourney.journey.items.length, 2);
                } else if (takenJourney.metadata.id == undefined) {
                    assert.equal(takenJourney.journey.items.length, 1);
                } else {
                    assert(false);
                }
                return when(takenJourney.metadata);
            });
            new takenJourneys.TakenJourneys().process([rawJourney.metadata, takenJourney2.metadata])
            .then(function(metadatas) {
                assert.equal(metadatas.length, 2);
                done();
            })
            .otherwise(function(e) {
                done(e);
            });
        });
    });
});

