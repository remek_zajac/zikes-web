/**
 * tests/mockPath.js
 *
 * Mock routing server returning simple (straight-line) routes from A -> .. -> Z
 */


const when     = require("when");
const utils    = require("../app/scripts/lib/utils.js");
const geometry = require("../app/scripts/lib/geometry.js");

const mockPath = (function() {

    function decorate(dp, withWhat) {
        let decoration;
        if (Array.isArray(dp)) {
            if (typeof dp[dp.length-1] != "object") {
                dp.push({});
            }
            dp[dp.length-1] = utils.mergeSimpleObjects(dp[dp.length-1], withWhat);
        } else {
            dp.decoration = utils.mergeSimpleObjects(dp.decoration, withWhat);
        }
    }

    class MockPath {

        constructor(milestones, options) {
            this.options            = options || {};
            this.options.resolution = this.options.resolution || { points : 0 };
            this.milestones         = milestones.map(this.p2p.bind(this));
            utils.assert(this.milestones.length > 0, "Must supply at least the origin milestone");
            if (this.milestones.length == 1) {
                utils.assert(this.options.distanceMts, "If you're only offering the origin, you must supply the distanceMts");
                this.milestones = [this.milestones[0], this.milestones[0].offsetCourse(this.options.distanceMts, this.options.bearing || 0)];
            }
            this.distanceMts = 0;
            for (var i = 1; i < this.milestones.length; i++) {
                this.distanceMts += this.milestones[i-1].distanceTo(this.milestones[i]);
            }
        }

        p2p(pt) {
            if (!(pt instanceof geometry.Point)) {
                if (Array.isArray(pt)) {
                    pt = new geometry.Point(pt[0], pt[1]);
                } else if ("x" in pt && "y" in pt) {
                    pt = new geometry.Point(pt.x, pt.y);
                } else {
                    utils.assert(false, "Unable to construct a milestone from "+pt)
                }
            }
            return pt;
        }

        path(everyPtCb, everyMilestoneCb) {
            everyPtCb               = everyPtCb        || function(a) {return a;}
            everyMilestoneCb        = everyMilestoneCb || function(a) {return a;}
            this.result = [];
            for (var i = 1; i < this.milestones.length; i++) {
                const context  = {
                    from       : this.milestones[i-1],
                    to         : this.milestones[i],
                    resolution : this.options.resolution.points || 0,
                    includeOrigin:  true,
                    lastMilestone:  i == this.milestones.length-1
                };
                context.distance = context.from.distanceTo(context.to);
                if (!this.options.resolution.points && this.options.resolution.every) {
                    context.resolution = Math.round(context.distance/this.options.resolution.every);
                }
                const subRoute = utils.moveRange(
                    context.from,
                    context.to,
                    context,
                    everyPtCb.bind(context)
                );
                everyMilestoneCb.apply(context, subRoute);
                this.result = this.result.concat(subRoute);
            }
            return this;
        }

        route() {
            this.path(undefined, function(subRoute) {
                if (!this.lastMilestone) {
                    decorate(subRoute[subRoute.length-1], {t:true});
                }
            });
            if (this.options.resolution.junctions) {
                utils.assert(this.options.profile, "When u ask for junctions, you need to supply ele/hwy profile");
                const junctionEvery = Math.floor(this.result.length/this.options.resolution.junctions);
                for (var i = 0; i < this.result.length; i++) {
                    let last = (i == this.result.length-1);
                    if ((i % junctionEvery) == 0 || last) {
                        decorate(
                            this.result[i],
                            utils.cloneSimpleObject(
                                this.options.profile[
                                    Math.floor(
                                        (i*this.options.profile.length)/this.result.length
                                    )
                                ]
                            )
                        );
                    }
                }
            }
            return this;
        }

        track() {
            let speedMps = utils.assert(this.options.speedMps);
            let time = this.options.timestart || new Date().getTime();
            return this.path(function(pt) {
                let result = geometry.LatLng.create([pt[1], pt[0], {ts: time}]);
                //resolution is the intermediate points between edges and thus there are
                //resolution+1 steps between said edges
                time += 1000*(this.distance/(this.resolution+1))/speedMps;
                return result;
            });
        }

        move(vector, startIdx, stopIdx) {
            function move(val, vector) {
                if (typeof vector == "number") {
                    utils.assert(typeof val == "number");
                    return val + vector;
                } else if (Array.isArray(vector)) {
                    utils.assert(Array.isArray(vector));
                    return val.map(function(e, idx) {
                        let v = vector[idx];
                        return v != undefined ? move(e,v) : e;
                    });
                } else if (typeof vector == "object") {
                    for (var vectorKey in vector) {
                        utils.assert(vectorKey in val, "No "+vectorKey+" in the path point");
                        val[vectorKey] = move(val[vectorKey], vector[vectorKey]);
                    }
                    return val;
                } else {
                    utils.assert(false, "unable to move by the vector value of " + JSON.stringify(vector));
                }
            }
            utils.assert(this.result, "No path to move");
            startIdx = startIdx || 0;
            stopIdx  = stopIdx  || this.result.length;
            let pt;
            for (var i = startIdx; i < stopIdx; i++) {
                this.result[i] = move(this.result[i], vector);
            }
            return this;
        }
    }

    class MockGeoPath extends MockPath {

        constructor() {
            super(...arguments);
        }

        p2p(pt) {
            return geometry.LatLng.create(pt);
        }
    }

    return {
        MockPath    : MockPath,
        MockGeoPath : MockGeoPath
    }

})();

module.exports = mockPath;