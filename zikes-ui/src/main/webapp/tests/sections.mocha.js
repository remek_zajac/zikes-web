var assert           = require("assert");
var when             = require("when");
var proxyquire       = require('proxyquireify')(require);

var mouseSim         = require("../app/scripts/lib/mouseSim.js");
var unredo           = require("../app/scripts/lib/unredo.js");
var track            = require("../app/scripts/lib/track.js");
var utils            = require("../app/scripts/lib/utils.js");
var mocks            = require("./utils.js");
var MockPath         = require("./mockPath.js").MockPath;
var map              = require("./mapStub.js").map;

var stubs = {
  "./map.js" : map,
  "../../lib/unredo.js" : unredo,
  "../../lib/track.js"  : track,
  "../../lib/mapquest.js" : {
    elevation : function(latlngs) {
        return when(
            Array.apply(null, Array(latlngs.length)).map(Number.prototype.valueOf, 100)
        );
    }
  },
  "@noCallThru": true
}

var routeModule   = proxyquire("../app/scripts/ui/common/mapRoute.js", stubs);
var sectionModule = proxyquire("../app/scripts/ui/common/sections.js", stubs);
var mockServerRouteMeta = {
    avgLookup_us:48,minLookup_us:33,maxLookup_us:62,routing_us:44,lengthMts:175,routingRequests:[{name:"astar",visitedJunctions:19}]
};


function containerToClient(containerCoords) {
    var mapRect = $("#map")[0].getBoundingClientRect();
    return new L.point(
        containerCoords.x + mapRect.left,
        containerCoords.y + mapRect.top
    )
}

function dragSection(mouse, section, dstLatLng) {
    var srcLatlngPoint = new L.LatLng(
        section.waypoints()[0].lat + (section.waypoints()[section.waypoints().length-1].lat - section.waypoints()[0].lat)/2,
        section.waypoints()[0].lng + (section.waypoints()[section.waypoints().length-1].lng - section.waypoints()[0].lng)/2
    )
    mouse.move(
        containerToClient(
            map.project(srcLatlngPoint)
        )
    );
    var dragTo    = containerToClient(
        map.project(dstLatLng)
    );
    var viaMarker = when.defer();
    map.on("layeradd", function (layer) {
        if (layer.layer._icon && layer.layer._icon.classList.contains("toFromIcon")) {
            viaMarker.resolve(layer.layer);
        }
    });

    var containerPoint = map.project(srcLatlngPoint);
    var clientPoint    = containerToClient(containerPoint);
    section.mDrawnSegment.fireEvent("mouseover", {
      latlng        : srcLatlngPoint,
      containerPoint: containerPoint,
      originalEvent : {
        clientX : clientPoint.x,
        clientY : clientPoint.y
      }
    });

    return when(viaMarker.promise)
    .then(function(viaMarker) {
        return mouse.drag(function(e) {
            viaMarker._icon.dispatchEvent(e);
        }, dragTo);
    });
}


suite("Sections", function() {

    function isIcon(routeUnderTest, iconType, marker) {
        return (routeUnderTest.mIcons[iconType].html || "") == marker.getIcon().innerHTML;
    }

    function verifySections(routeUnderTest, expectedSections, lastIs) {
        expectedSections = utils.cloneSimpleObject(expectedSections)
        var current = routeUnderTest.mHead;
        while (current) {
            if (current.mPrev) {
                if (current.mNext) {
                    assert(isIcon(
                        routeUnderTest,
                        "via",
                        current.startMarker
                    ));
                    assert(current.waypoints().length);
                } else {
                    assert(isIcon(
                        routeUnderTest,
                        "to",
                        current.startMarker
                    ));
                    assert(!current.waypoints());
                }
            } else {
                assert(isIcon(
                    routeUnderTest,
                    current.mNext ? "from" : lastIs,
                    current.startMarker
                ));
                assert((!current.waypoints() && !current.mNext) || current.waypoints().length);
            }
            assert(current.constructor.name == expectedSections.shift());
            current = current.mNext;
        }
        assert.equal(expectedSections.length, 0);
    }

    function verifyTailDelete(route, expectedSections, undo) {
        if (undo == undefined) {
            undo = true;
        }
        verifySections(route, expectedSections,"from");
        if (expectedSections.length) {
            route.mParent.invalidate.reset();
            unredo.push(
                route.mHead.tail().delete().push(route.invalidate.bind(route))
            );
            return route.invalidate()
            .then(function() {
                assert(route.mHasFrom);
                if (undo) {
                    route.mParent.invalidate.reset();
                    unredo.undo();
                    return route.mParent.invalidate.whenCalled()
                    .then(function() {
                        return verifyTailDelete(route, expectedSections, !undo);
                    });
                } else {
                    expectedSections.pop();
                    return verifyTailDelete(route, expectedSections, !undo);
                }
            });
        }
    }

    function verifyHeadDelete(route, expectedSections, undo) {
        if (undo == undefined) {
            undo = true;
        }
        verifySections(route, expectedSections,"to");
        if (expectedSections.length) {
            route.mParent.invalidate.reset();
            unredo.push(
                route.mHead.delete().push(route.invalidate.bind(route))
            );
            return route.invalidate()
            .then(function() {
                assert(route.mHead || !route.mHasFrom);
                if (undo) {
                    route.mParent.invalidate.reset();
                    unredo.undo();
                    return route.mParent.invalidate.whenCalled()
                    .then(function() {
                        return verifyHeadDelete(route, expectedSections, !undo);
                    });
                } else {
                    expectedSections.shift();
                    return verifyHeadDelete(route, expectedSections, !undo);
                }
            });
        }
    }

    function verifyUndo(expectedSections, undoOrder) {
        verifySections(routeUnderTest, expectedSections, "from");
        if (expectedSections.length) {
            routeUnderTest.mParent.invalidate.reset();
            unredo.undo();
            return routeUnderTest.mParent.invalidate.whenCalled()
            .then(function() {
                var sectionIdx = undoOrder.shift();
                if (sectionIdx != undefined) {
                    expectedSections.splice(sectionIdx, 1);
                }
                return verifyUndo(expectedSections, undoOrder);
            });
        }
    }

    var from = new L.LatLng(51.5594942985,-0.265564379883);
    var to   = new L.LatLng(51.5681406456,-0.211319384766);
    var mockRouteFunction = new mocks.MockInvocation();
    mockRouteFunction.returns(function(milestones) {
        return when({
            waypoints: new MockPath(milestones, {
                resolution : {points: 20, junctions:5},
                profile    : [{e:110,h:10},{e:120,h:10}]
            }).route().result,
            meta: mockServerRouteMeta
        });
    });
    var mockInvalidateFunction = new mocks.MockInvocation();
    var routeUnderTest = new routeModule.Route({
        map   : map,
        route : mockRouteFunction.method,
        invalidate: mockInvalidateFunction.method,
        highlight: function() {},
        style: function() {
            return {
                color : {
                    base : '#FF0000'
                }
            }
        }
    });

    function plot(route = routeUnderTest) {
        route.from([from.lat,from.lng]);
        route.to([to.lat,to.lng]);
        return when(route.invalidate(true))
        .then(function() {
            map.fitBounds(
                new L.LatLngBounds(from, to)
            );
        })
    }

    function drag(route = routeUnderTest) {
        return dragSection(
            new mouseSim.SimulatedMouse(),
            route.mHead,
            new L.LatLng(51.5695177174,-0.243454394531)
        );
    }

    suite("basic", function() {

        function generateExpectedSections(n) {
            var result = [];
            for (var i = 0; i < n; i++) {
                result.push("Section");
            }
            return result;
        }

        test("should drag a route and then (tail) delete it", function(done) {
            plot()
            .then(function() {
                verifySections(routeUnderTest, generateExpectedSections(2));
                return drag();
            })
            .then(function() {
                return verifyTailDelete(routeUnderTest, generateExpectedSections(3))
            })
            .otherwise(function(err) {
                done(err);
            })
            .then(function() {
                done();
            });
        });

        test("should drag a route and then (head) delete it", function(done) {
            plot()
            .then(function() {
                verifySections(routeUnderTest, generateExpectedSections(2));
                return drag();
            })
            .then(function() {
                return verifyHeadDelete(routeUnderTest, generateExpectedSections(3))
            })
            .otherwise(function(err) {
                done(err);
            })
            .then(function() {
                done();
            });
        });

        test("should drag a route and then (mid) delete it", function(done) {
            plot().then(drag)
            .then(function() {
                mockRouteFunction.method.reset();
                routeUnderTest.mHead.mNext.delete();
                routeUnderTest.invalidate();
                return mockRouteFunction.method.whenCalled(
                    [[[51.5594942985,-0.265564379883],[51.5681406456,-0.211319384766]]]
                );
            })
            .otherwise(function(err) {
                done(err);
            })
            .then(function() {
                return verifyHeadDelete(routeUnderTest, generateExpectedSections(2));
            })
            .then(function() {
                done();
            });
        });

        test("should drag a sparse route and then (tail) delete it", function(done) {
            let mockRouteFunction = new mocks.MockInvocation();
            mockRouteFunction.returns(function(milestones) {
                return when({
                    waypoints: new MockPath(milestones, {
                        resolution : {points: 0, junctions:2},
                        profile    : [{e:110,h:10},{e:120,h:10}]
                    }).route().result,
                    meta: mockServerRouteMeta
                });
            });
            let mockInvalidateFunction = new mocks.MockInvocation();
            let routeUnderTest = new routeModule.Route({
                map   : map,
                route : mockRouteFunction.method,
                invalidate: mockInvalidateFunction.method,
                highlight: function() {},
                style: function() {
                    return {
                        color : {
                            base : '#FF0000'
                        }
                    }
                }
            });
            plot(routeUnderTest)
            .then(function() {
                verifySections(routeUnderTest, generateExpectedSections(2));
                return drag(routeUnderTest);
            })
            .then(function() {
                return verifyTailDelete(routeUnderTest, generateExpectedSections(3))
            })
            .otherwise(function(err) {
                done(err);
            })
            .then(function() {
                done();
            });
        });
    });


    function insertRubberband() {
        var dragTo    = containerToClient(
            map.project(new L.LatLng(51.5635517325,-0.248037753296))
        );
        var mouse = new mouseSim.SimulatedMouse();

        return plot()
        .then(drag)
        .then(function() {
            new sectionModule.RubberbandSection(map, {
                appendBefore : routeUnderTest.mHead.mNext,
                icon         : routeUnderTest.mIcons.via,
                parent       : routeUnderTest
            });
            mouse.move(routeUnderTest.mHead.mNext.startMarker.getIcon())
        })
        .delay(0)
        .then(function() {
            return mouse.drag(function(e) {
                routeUnderTest.mHead.mNext.startMarker.getIcon().dispatchEvent(e);
            }, dragTo);
        });
    }

    function dragRubberband() {
        return insertRubberband()
        .then(function() {
            return dragSection(
                new mouseSim.SimulatedMouse(),
                routeUnderTest.mHead.mNext,
                new L.LatLng(51.566379393,-0.25214475174)
            );
        });
    }

    suite("rubberband", function() {

        test("should drag a rubberband and then (tail) delete it", function(done) {
            insertRubberband()
            .then(function() {
                return verifyTailDelete(routeUnderTest, ["Section", "RubberbandSection", "Section", "Section"]);
            })
            .otherwise(function(err) {
                done(err);
            })
            .then(function() {
                done();
            });
        });

        test("should drag a rubberband and then (mid) delete it", function(done) {
            dragRubberband()
            .then(function() {
                verifySections(routeUnderTest, ["Section", "RubberbandSection", "RubberbandSection", "Section", "Section"]);
                routeUnderTest.mHead.mNext.mNext.delete();
                return routeUnderTest.invalidate();
            })
            .then(function() {
                return verifyHeadDelete(routeUnderTest, ["Section", "RubberbandSection", "Section", "Section"]);
            })
            .otherwise(function(err) {
                done(err);
            })
            .then(function() {
                done();
            });
        });

        test("should insert a rubberband and then undo", function(done) {
            insertRubberband()
            .then(function() {
                return verifyUndo(
                    ["Section", "RubberbandSection", "Section", "Section"],
                    [undefined,1,1,1,0] //the second one is undo the move of the inserted, zero-length rubberband
                );
            })
            .otherwise(function(err) {
                done(err);
            })
            .then(function() {
                done();
            });
        });
    });
});
