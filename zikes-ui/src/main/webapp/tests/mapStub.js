var proxyquire       = require('proxyquireify')(require);
var when             = require("when");

var zikesRouterStub  = {
      meta: function() {
          return when({
              defaultRoutingPrefs : [],
                  map : {
                      highways : {},
                      coverage: [[[51.5566576697,-0.191200671387],
                                  [51.5566576697,-0.0195392944336],
                                  [51.477618897,-0.0195392944336],
                                  [51.477618897,-0.191200671387]]]
                  }
              });
      },
      saveProfile: function() {},
      getPreferences: function() {},
      getProfiles: function() {}
  };

var stubs = {
  "../mapbox/geocode.js" : function(accessToken, map) {
    return {
      ui : function() {}
    }
  },
  "../lib/geoip.js" : {
    get : function() {return when({"latlng":{"lat":8.5667,"lng":-82.4168},"country":"Panama"})}
  },
  "../lib/zikesBackend.js" : zikesRouterStub,
  "@noCallThru": true
};

module.exports = {
  map : proxyquire("../app/scripts/ui/desktop/map.js", stubs),
  stubs : {
    zikesRouter : zikesRouterStub
  }
};