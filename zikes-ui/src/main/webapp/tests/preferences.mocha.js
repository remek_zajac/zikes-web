var assert       = require("assert");
var when         = require("when");
var proxyquire   = require('proxyquireify')(require);

var mouseSim     = require("../app/scripts/lib/mouseSim.js");
var unredo       = require("../app/scripts/lib/unredo.js")
var mocks        = require("./utils.js");
var mapStub      = require("./mapStub.js");

var stubs = {
  "../ui/desktop/map.js" : mapStub.map,
  "./map.js" : mapStub.map,
  "../lib/unredo.js" : unredo,
  "./unredo.js" : unredo,
  "../lib/zikesBackend.js" : mapStub.stubs.zikesRouter,
  "@noCallThru": true
}
var highways       = proxyquire("../app/scripts/lib/highways.js", stubs);
var routingPrefs   = proxyquire("../app/scripts/ui/routingPreference.js", stubs);

suite("RoutingPreferences", function() {
    var prefsChanged = new mocks.MockInvocation();
    var KPreferences;

    suiteSetup(function(done) {
        when(mapStub.loaded).then(function() {
            KPreferences = new routingPrefs.RoutingPreference(
                mapStub.map,
                prefsChanged.method
            );

            setTimeout(function() {
                done();
            },300);
        });
    });

    suite("highways", function() {

        var KRoadSliders;
        suiteSetup(function() {
            KRoadSliders = KPreferences.mPreferences[0];
        });

        function rectCentre(rect) {
            if (rect instanceof jQuery) {
                rect = rect[0];
            }
            if (rect instanceof Element) {
                rect = rect.getBoundingClientRect()
            }
            return new L.Point(
                rect.left + (rect.right-rect.left)/2,
                rect.top + (rect.bottom-rect.top)/2
            );
        }

        test("colapses and opens", function(done) {
            assert(
                !$("#collapse_roads").hasClass("in")
            );
            var sliderHeads = $("#title_roads").find(".slider-handle");
            assert(sliderHeads.length == 2);
            assert.equal(sliderHeads[0].style["left"], "0%");
            assert.equal(sliderHeads[1].style["left"], "100%");
            new mouseSim.SimulatedMouse().down(
                $(sliderHeads[1])
            );
            new mouseSim.SimulatedMouse().up(
                $(sliderHeads[1])
            );
            setTimeout(function() {
                assert(
                    $("#collapse_roads").hasClass("in")
                );
                assert.equal(prefsChanged.method.calledTimes(), 0);
                done();
            }, 400)
        });

        var neutralJSON;
        function isNeutral(highwayPrefs) {
            var preferneces = {}
            KRoadSliders.generatePreference(preferneces);
            for (var highway in preferneces.highwayCostFactors) {
                var highwayPreference = preferneces.highwayCostFactors[highway];
                assert.equal(highwayPreference.cost, "1.00");
            }
            KRoadSliders.mGenerator.mHighwaySliders.forEach(function(slider) {
                assert.equal(slider.value(), 1);
            });
        }

        test("starts neutral", function() {
            isNeutral();
            neutralJSON = KRoadSliders.toJSON();
        });

        var leftAtMidJSON;
        function verifyValuesForLeftAtMid() {
            assert.deepEqual($("#roads_slider").getValue(), [50,100]);
            var preferneces = {}
            KRoadSliders.generatePreference(preferneces);
            assert.deepEqual(preferneces.highwayCostFactors, {"bridleway":{"cost":"1.69"},"bus_guideway":{"cost":"1.00"},"cycleway":{"cost":"1.38"},"ferry":{"cost":"1.00","flatCost":500},"footway":{"cost":"2.17"},"living_street":{"cost":"1.16"},"motorway":{"cost":"1.00"},"motorway_link":{"cost":"1.00"},"path":{"cost":"3.00"},"pedestrian":{"cost":"2.17"},"primary":{"cost":"1.00"},"primary_link":{"cost":"1.00"},"residential":{"cost":"1.16"},"road":{"cost":"1.00"},"secondary":{"cost":"1.00"},"secondary_link":{"cost":"1.00"},"service":{"cost":"1.00"},"steps":{"cost":"1.00"},"tertiary":{"cost":"1.00"},"tertiary_link":{"cost":"1.00"},"track":{"cost":"1.69"},"trunk":{"cost":"1.00"},"trunk_link":{"cost":"1.00"},"unclassified":{"cost":"1.00"}});
            if (leftAtMidJSON) {
                assert.deepEqual(leftAtMidJSON, KRoadSliders.toJSON());
            } else {
                leftAtMidJSON = KRoadSliders.toJSON();
            }
        }

        test("handles big slider movements", function(done) {
            var sliderHeads = $("#title_roads").find(".slider-handle");
            assert.deepEqual($("#roads_slider").getValue(), [0,100]);
            var distance = rectCentre(sliderHeads[0])
                .distanceTo(rectCentre(sliderHeads[1]));

            new mouseSim.SimulatedMouse().drag(
                $(sliderHeads[0]), {vector: {x: distance/2, y: 0}}
            ).then(function() {
                verifyValuesForLeftAtMid();
                assert.equal(prefsChanged.method.calledTimes(), 1);
                unredo.undo();
                assert.equal(prefsChanged.method.calledTimes(), 2);
                isNeutral();
                done();
            }).otherwise(function(err) {
                done(err);
            });
        });

        var pathAllDownJSON;
        function verifyPathAllDown(pathSlider) {
            assert.equal($("#path_slider").getValue(), 11);
            assert.equal(pathSlider.value(),0);
            var preferneces = {}
            KRoadSliders.generatePreference(preferneces);
            assert.deepEqual(preferneces.highwayCostFactors, {"bridleway":{"cost":"1.00"},"bus_guideway":{"cost":"1.00"},"cycleway":{"cost":"1.00"},"ferry":{"cost":"1.00","flatCost":500},"footway":{"cost":"1.00"},"living_street":{"cost":"1.00"},"motorway":{"cost":"1.00"},"motorway_link":{"cost":"1.00"},"path":{"cost":"4095.00"},"pedestrian":{"cost":"1.00"},"primary":{"cost":"1.00"},"primary_link":{"cost":"1.00"},"residential":{"cost":"1.00"},"road":{"cost":"1.00"},"secondary":{"cost":"1.00"},"secondary_link":{"cost":"1.00"},"service":{"cost":"1.00"},"steps":{"cost":"1.00"},"tertiary":{"cost":"1.00"},"tertiary_link":{"cost":"1.00"},"track":{"cost":"1.00"},"trunk":{"cost":"1.00"},"trunk_link":{"cost":"1.00"},"unclassified":{"cost":"1.00"}});
            if (pathAllDownJSON) {
                assert.deepEqual(pathAllDownJSON, KRoadSliders.toJSON());
            } else {
                pathAllDownJSON = KRoadSliders.toJSON();
            }
        }

        test("handles small slider movements", function(done) {
            prefsChanged.method.reset();
            var pathSliderDiv = $("#collapse_roads").find("#path").find(".slider");
            var pathSlider    = KRoadSliders.mGenerator.mHighwaySliders[2];
            assert.equal(pathSlider.value(),1);
            assert.equal($("#path_slider").getValue(), 0);
            new mouseSim.SimulatedMouse().drag(
                pathSliderDiv, {vector: {x: 0, y: 40}}
            ).then(function() {
                verifyPathAllDown(pathSlider);
                assert.equal(prefsChanged.method.calledTimes(), 1);
                unredo.undo();
                assert.equal(prefsChanged.method.calledTimes(), 2);
                isNeutral();
                done();
            }).otherwise(function(err) {
                done(err);
            });
        });

        test("writes and loads to/from JSON", function() {
            isNeutral();
            KRoadSliders.fromJSON(leftAtMidJSON);
            verifyValuesForLeftAtMid();
            KRoadSliders.fromJSON(pathAllDownJSON);
            verifyPathAllDown(KRoadSliders.mGenerator.mHighwaySliders[2]);
            KRoadSliders.fromJSON(neutralJSON);
            isNeutral();
        });
    });
});
