Zikes
================================

This application lets you create cycling tours. Better Than You Ever Imagined

# Architecture overview
Application contains two modules: ui + api both individually deployable as Google App Engine services.

## API
Since API module serves like a proxy server for a set of endpoints we include the following three running modes :
1. Local (assumes local engine is running and listening port 8087)
2. Mock (serves pre loaded static responses)
3. Production (goes to production server)

Spring profile is generated dynamically on pre-compile step and according to a given environment
In a root folder you can find two predefined properties files:
1. gradle-local.properties
2. gradle-mock.properties

gradle-prod.properties file is confidential and should never be checked-in
If not explicitly specified gradle-local is going to be used by default
To specify desired mode please use './gradlew [YOUR COMMMAND HERE] -PenvironmentName=mock/local/prod'

For instance, to run zikes-api in the local mode, from zikes-api folder run:
```../gradlew appengineRun -PenvironmentName=local```
to deploy:
```../gradlew appengineDeploy -PenvironmentName=prod```

### Run Tests
1. cd zikes-api
2. ../gradlew test (html report can be found in /build/reports/index.html)
3. ../gradlew appengineFunctionalTest -PenvironmentName=mock

## UI
Static web app

### Run locally
Runs on node v8.17.0
```
cd zikes-ui/src/main/webapp/
npm install
npm run gulp
```

### Deploy
```
cd zikes-ui
../gradlew appengineDeploy
```
